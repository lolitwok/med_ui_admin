import os

from OpenGL.GL import *
from OpenGL.GLU import *
from PyQt4.QtOpenGL import *
from PyQt4 import QtGui, QtCore

from applibs.objparser_1 import ObjLoader

resources_path = 'resources'


class MainWindow(QtGui.QWidget):
	def __init__(self):
		super(MainWindow, self).__init__()

		self.widget = glWidget(self)

		self.button = QtGui.QPushButton('Test', self)

		main_layout = QtGui.QHBoxLayout()
		main_layout.addWidget(self.widget)
		main_layout.addWidget(self.button)

		self.button.clicked.connect(self.cc)

		self.setLayout(main_layout)

	def cc(self):
		self.widget.rnd()


class glWidget(QGLWidget):
	def __init__(self, parent):
		QGLWidget.__init__(self, parent)
		self.setMinimumSize(640, 480)
		self.x = 0
		self.y = 0
		self.z = 0
		self.m = QtCore.QPoint()
		self.length = -10
		self.gl_cube_list = None
		# from applibs import objloader
		# self.a = objloader.Model(os.path.join(resources_path, '1.obj'))
		# self.a.obj_load()
		self.test_model = ObjLoader(os.path.join(resources_path, '1.obj'))

	def glInit(self):
		# self.rnd()
		self.makeCubeList()
		super(glWidget, self).glInit()



	def paintGL(self):

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
		# glClearColor(0.7,0.9,1,1)
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()

		# Add ambient light:
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, [0.2, 0.2, 0.2, 1.0])

		# Add positioned light:
		# glLightfv(GL_LIGHT0, GL_DIFFUSE, [2, 2, 2, 1])
		# glLightfv(GL_LIGHT0, GL_POSITION, [4, 8, 1, 1])

		# glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
		# glLoadIdentity()

		glTranslatef(0.0, 0.0, self.length)
		glColor3f(1.0, 1.0, 0.0)
		glPolygonMode(GL_FRONT, GL_FILL)

		glRotated(self.x * 0.8, 1.0, 0, 0)
		glRotated(self.y * 0.8, 0, 1.0, 0)
		glRotated(self.z * 0.8, 0, 0, 1.0)

		self.draw()

		glFlush()

	# def rnd(self):
	#
	# 	self.gl_list = glGenLists(1)
	# 	glNewList(self.gl_list, GL_COMPILE)
	#
	# 	glEnable(GL_TEXTURE_2D)
	# 	glFrontFace(GL_CCW)
	# 	for i in xrange(len(self.a.vertsOut)):
	# 		glBegin(GL_POLYGON)
	#
	# 		# if self.a.normsOut[i] > 0:
	# 		glNormal3fv(self.a.normsOut[i])
	# 		glVertex3fv(self.a.vertsOut[i])
	# 		glEnd()
	# 	glDisable(GL_TEXTURE_2D)
	# 	glEndList()

	def draw(self):
		# glCallList(self.gl_cube_list)
		glPushMatrix()
		glTranslatef(0.5, 0.2, -1.0)
		# if self.monkey.last_texture is None:
		# 	self.monkey.render_texture(self.surface_id, ((0, 0), (1, 0), (1, 10), (0, 1)))
		# else:
		# 	self.monkey.render_texture_last()
		glColor3f(255, 159, 122)
		self.test_model.render_scene(1)
		glPopMatrix()

	def initializeGL(self):
		glClearDepth(1.0)
		glDepthFunc(GL_LESS)
		glEnable(GL_DEPTH_TEST)
		glShadeModel(GL_SMOOTH)

		glViewport(0, 0, 620, 460)

		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(45.0, 1.33, 0.1, 100.0)
		glMatrixMode(GL_MODELVIEW)

		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(45, 640.0 / 480.0, 0.1, 200.0)
		glEnable(GL_DEPTH_TEST)
		# glEnable(GL_LIGHTING)
		glEnable(GL_LIGHT0)
		glEnable(GL_NORMALIZE)

	def resizeEvent(self, e):
		pass

	def mousePressEvent(self, e):
		self.m = e.pos()

	def wheelEvent(self, e):
		self.length += e.delta() / 100
		self.updateGL()

	def mouseMoveEvent(self, e):
		delX = self.m.x() - e.x()
		delY = self.m.y() - e.y()
		if e.buttons() & QtCore.Qt.LeftButton:
			self.x += 1 * delX
			self.y += 1 * delY
		if e.buttons() & QtCore.Qt.RightButton:
			self.x += 1 * delX
			self.z += 1 * delY

		if e.buttons():
			self.updateGL()

		self.m = e.pos()


if __name__ == '__main__':
	app = QtGui.QApplication(['Yo'])
	window = MainWindow()
	window.show()
	app.exec_()
