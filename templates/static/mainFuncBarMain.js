var ma = Math;
var myBarChart = null;
var color = ['rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)'];

var borderColor = ['rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'];

var colorArr = [];
var colorBorderArr = [];

function resizeGraph() {
    $("#ctx").css("width", $(window).width());
    $("#ctx").css("height", "100vh");
    $("#ctx").attr("width", $(window).width());
    $("#ctx").attr("height", $(window).height());
}

window.onload = function () {
    $("#ctx").css("width", $(window).width());
    $("#ctx").css("height", $(window).height());
    $("#ctx").attr("width", $(window).width());
    $("#ctx").attr("height", $(window).height());

    function unicodeToChar(text) {
        return text.replace(/\\u[\dA-F]{4}/gi,
            function (match) {
                return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
            });
    }

    var _labels = [];
    var _dataBar = [];
    var _data1Bar = [];
    var t = null;

    function setColor(val) {
        var tmpArr = [];
        var colorNum = 0;

        for (var i=0; i<val;i++) {
            tmpArr.push(color[colorNum]);
            colorNum++;
            if (colorNum>5) { colorNum = 0}
        }

        return tmpArr;
    }

    function setColorBorder(val) {
        var tmpArr = [];
        var colorNum = 0;

        for (var i=0; i<val;i++) {
            tmpArr.push(borderColor[colorNum]);
            colorNum++;
            if (colorNum>5) { colorNum = 0}
        }

        return tmpArr;
    }

    $.ajax({
        url: 'http://localhost:5001/dias_for_points',
        data: 'get',
        success: function (data) {
            //console.log(data);
            if ('data' in data) {

                if ('z' in data.data) {
                    for (var i = 0; i < data.data.n.length; i++) {
                        _labels.push(unicodeToChar(data.data.n[i].name));
                        _dataBar.push(parseInt(data.data.n[i].mid));
                    }
                    for (var i = 0; i < data.data.z.length; i++) {
                        _data1Bar.push(parseInt(data.data.z[i].mid));
                    }
                    t = 1;
                }
                else {

                    for (var i = 0; i < data.data.length; i++) {
                        _labels.push(unicodeToChar(data.data[i].name));
                        _dataBar.push(parseInt(data.data[i].mid));
                    }
                    t = 2;
                }
                //console.log(t);
                //console.log(_labels);
                colorArr = setColor(_dataBar.length);
                colorBorderArr = setColor(_dataBar.length);
                render_all();
            }
        },
        dataType: 'json'
    });

    function render_all() {
        var __data = function () {
            if (t == 2) {
                return {
                    labels: _labels,
                    datasets: [
                        {
                            backgroundColor: colorArr,
                            borderColor: colorBorderArr,
                            borderWidth: 1,
                            data: _dataBar
                        }
                    ]
                }
            }
            else {
                return {
                    labels: _labels,
                    datasets: [
                        {
                            label: 'Без назода',
                            data: _dataBar,
                            backgroundColor: "rgba(153,255,51,1)"
                        }, {
                            label: 'С назодом',
                            data: _data1Bar,
                            backgroundColor: "rgba(255,153,0,1)"
                        }
                    ]
                }
            }
        };

        var canvas = document.getElementById("ctx");

        if (!myBarChart) {
            var myBarChart = new Chart(canvas, {
                type: 'bar',
                data: __data(),
                options: {
                    responsive: true,
                    min: 0,
                    max: 100,
                    legend: {
                        display: false,
                        labels: {
                            display: false
                        }
                    },
                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                min: 0,
                                max: 100,
                                stepSize: 20
                            }
                        }],
                        xAxes: [{
                            display: false
                        }]
                    }
                }
            });
        }

        myBarChart.update();
    }
    function drawGraph() {
        myBarChart.update();
    }
};


$(window).resize(function (e) {
    resizeGraph();
});


