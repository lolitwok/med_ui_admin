var _data = [[], []];
var __data = null;
var _labels = [];
var _dataBarForChart = [];
var _dataBar = [];
var _data1Bar = [];
var _labels1 = [];
var _dataBar1 = [];
var _data1Bar1 = [];
var t = null;
var data1 = [[], []];
var dataArr1 = [];
var dataArr2 = [];
var dataArr2Nazod = [];
var dataArr2Nazod2 = [];
var index = 0;
var tmp = [];
var tmp1 = [];
var ma = Math;
var hB = 0;
var wB = 0;
//var tmpArr = [];
//var kip = 0;


var obj = null;
var updates = 0;
var data = [];
var i = 0;
var myRadarChart = null;
var myRadarChart2 = null;
var myBarChart = null;

var color = ['rgba(255, 99, 132, 0.2)',
	'rgba(54, 162, 235, 0.2)',
	'rgba(255, 206, 86, 0.2)',
	'rgba(75, 192, 192, 0.2)',
	'rgba(153, 102, 255, 0.2)',
	'rgba(255, 159, 64, 0.2)'];

var borderColor = ['rgba(255,99,132,1)',
	'rgba(54, 162, 235, 1)',
	'rgba(255, 206, 86, 1)',
	'rgba(75, 192, 192, 1)',
	'rgba(153, 102, 255, 1)',
	'rgba(255, 159, 64, 1)'];

var colorArr = [];
var colorBorderArr = [];

function intersect_safe(a, b) {
	var ai = 0, bi = 0;
	var result = [];
	while (ai < a.length && bi < b.length) {
		if (a[ai] < b[bi]) {
			ai++;
		}
		else if (a[ai] > b[bi]) {
			bi++;
		}
		else /* they're equal */
		{
			result.push(a[ai]);
			ai++;
			bi++;
		}
	}
	return result;
}

var w = $(window).width(),
		h = $(window).height();

function Resize() {
	$("#cvs").attr("width", w - 32);
	$("#cvs").attr("height", h - 32);

	$("#cvs2").attr("width", w - 32);
	$("#cvs2").attr("height", h - 32);

	$("#ctx").css("width", w);
	$("#ctx").css("height", h/2);
	$("#ctx").attr("width", w);
	$("#ctx").attr("height", h/2);
}

function retColor (num) {
	switch(true) {
		case (num < -14):
			return '#34495e';
		case (num > -15 && num < -7):
			return '#2980b9';
		case (num > -8 && num < -4):
			return '#3498db';
		case (num > -5 && num < 6):
			return '#27ae60';
		case (num > 5 && num < 8):
			return '#f1c40f';
		case (num > 7 && num < 16):
			return '#e74c3c';
		case (num > 15):
			return '#c0392b';
	}
}

window.onload = function () {
	w > h ? w = h : h = w;

	var canvas = document.getElementById("cvs");
	var canvas2 = document.getElementById("cvs2");
	var canvas3 = document.getElementById("ctx");
	Resize();

	function unicodeToChar(text) {
		return text.replace(/\\u[\dA-F]{4}/gi,
				function (match) {
					return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
				});
	}

	function setColor(val) {
		var tmpArr = [];
		var colorNum = 0;

		for (var i = 0; i < val; i++) {
			tmpArr.push(color[colorNum]);
			colorNum++;
			if (colorNum > 5) {
				colorNum = 0
			}
		}

		return tmpArr;
	}

	function setColorBorder(val) {
		var tmpArr = [];
		var colorNum = 0;

		for (var i = 0; i < val; i++) {
			tmpArr.push(borderColor[colorNum]);
			colorNum++;
			if (colorNum > 5) {
				colorNum = 0
			}
		}

		return tmpArr;
	}

	function __dataRadar1 () {
		if (t == 2) {
			return	{
				labels: tmp,
				datasets: [
					{
						data: dataArr1,
						label: 'Значение',
						backgroundColor: "rgba(179,181,198,0.2)",
						borderColor: "rgba(179,181,198,1)",
						pointBackgroundColor: "rgba(179,181,198,1)",
						pointBorderColor: "#fff",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "rgba(179,181,198,1)",
						borderWidth: 1
					},
					{
						data: [100,0],
						label: ' ',
						backgroundColor: "rgba(179,181,198,0)",
						borderColor: "rgba(179,181,198,0)",
						pointBackgroundColor: "rgba(179,181,198,0)",
						pointBorderColor: "rgba(179,181,198,0)",
						pointHoverBackgroundColor: "rgba(179,181,198,0)",
						pointHoverBorderColor: "rgba(179,181,198,0)",
						borderWidth: 0.001
					}
				]
			}
		} else {
			return {
				labels: tmp,
				datasets: [
					{
						data: dataArr1,
						label: 'Без нозода ',
						backgroundColor: "rgba(179,181,198,0.2)",
						borderColor: "rgba(179,181,198,1)",
						pointBackgroundColor: "rgba(179,181,198,1)",
						pointBorderColor: "#fff",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "rgba(179,181,198,1)",
						borderWidth: 1
					},
					{	data: dataArr2Nazod,
						label: 'С нозодом ',
						backgroundColor: "rgba(18, 175, 203,0.2)",
						borderColor: "#12AFCB",
						pointBackgroundColor: "#fff",
						pointBorderColor: "#12AFCB",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "#12AFCB",
						borderWidth: 1
					},
					{
						data: [100,0],
						label: ' ',
						backgroundColor: "rgba(179,181,198,0)",
						borderColor: "rgba(179,181,198,0)",
						pointBackgroundColor: "rgba(179,181,198,0)",
						pointBorderColor: "rgba(179,181,198,0)",
						pointHoverBackgroundColor: "rgba(179,181,198,0)",
						pointHoverBorderColor: "rgba(179,181,198,0)",
						borderWidth: 0.001
					}
				]
			}
		}
	}

	function __dataRadar2 () {
		if (t == 2) {
			return	{
				labels: tmp1,
				datasets: [
					{
						data: dataArr2,
						label: 'Значение',
						backgroundColor: "rgba(179,181,198,0.2)",
						borderColor: "rgba(179,181,198,1)",
						pointBackgroundColor: "rgba(179,181,198,1)",
						pointBorderColor: "#fff",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "rgba(179,181,198,1)",
						borderWidth: 1
					},
					{
						data: [100,0],
						label: ' ',
						backgroundColor: "rgba(179,181,198,0)",
						borderColor: "rgba(179,181,198,0)",
						pointBackgroundColor: "rgba(179,181,198,0)",
						pointBorderColor: "rgba(179,181,198,0)",
						pointHoverBackgroundColor: "rgba(179,181,198,0)",
						pointHoverBorderColor: "rgba(179,181,198,0)",
						borderWidth: 0.001
					}
				]
			}
		} else {
			return {
				labels: tmp1,
				datasets: [
					{
						data: dataArr2,
						label: 'Без нозода ',
						backgroundColor: "rgba(18, 175, 203,0.2)",
						borderColor: "#12AFCB",
						pointBackgroundColor: "rgba(179,181,198,1)",
						pointBorderColor: "#fff",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "rgba(179,181,198,1)",
						borderWidth: 1
					},
					{	data: dataArr2Nazod2,
						label: 'С нозодом ',
						backgroundColor: "rgba(179,181,198,0.2)",
						borderColor: "rgba(179,181,198,1)",
						pointBackgroundColor: "rgba(179,181,198,1)",
						pointBorderColor: "#fff",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "rgba(179,181,198,1)",
						borderWidth: 1
					},
					{
						data: [100,0],
						label: ' ',
						backgroundColor: "rgba(179,181,198,0)",
						borderColor: "rgba(179,181,198,0)",
						pointBackgroundColor: "rgba(179,181,198,0)",
						pointBorderColor: "rgba(179,181,198,0)",
						pointHoverBackgroundColor: "rgba(179,181,198,0)",
						pointHoverBorderColor: "rgba(179,181,198,0)",
						borderWidth: 0.001
					}
				]
			}
		}
	}

	if (!myRadarChart) {
		var myRadarChart = new Chart(canvas, {
			type: 'radar',
			data: __dataRadar1(),
			options: {
				responsive: true,
				scaleOverride: false,
				scaleSteps: 5,
				scaleStepWidth: 20,
				scaleStartValue: 0,
				beginAtZero: true,
				legend: {
					display: false,
					labels: {
						display: false
					}
				}
			}
		});
	}

	if (!myRadarChart2) {
		var myRadarChart2 = new Chart(canvas2, {
			type: 'radar',
			data: __dataRadar2(),
			options: {
				responsive: true,
				scaleOverride: false,
				scaleSteps: 5,
				scaleStepWidth: 20,
				scaleStartValue: 0,
				beginAtZero: true,
				legend: {
					display: false,
					labels: {
						display: false
					}
				}
			}
		});
	}

	myRadarChart.update();
	myRadarChart2.update();

	String.prototype.hashCode = function() {
		var hash = 0, i, chr, len;
		if (this.length === 0) return hash;
		for (i = 0, len = this.length; i < len; i++) {
			chr   = this.charCodeAt(i);
			hash  = ((hash << 5) - hash) + chr;
			hash |= 0; // Convert to 32bit integer
		}
		return hash;
	};

	var ff = function(arr1) {var gg =""; arr1 = arr1.sort();$.each(arr1, function(ind ,el){gg+=el});return gg.hashCode()};

	function CreateBar() {
		hB = _dataBarForChart.length*50;
		wB = $(window).width()-20;
		$('svg').css('height', hB);
		$('svg').css('width', wB);
		var kip=0;

		if (t == 2) {
			for (var i = 0; i < _dataBarForChart.length; i++) {
				if(_dataBarForChart[i].is_name == true) {
					kip = _dataBarForChart[i].value;
					break;
				}
			}

			_dataBarForChart.sort(function(a, b) { return a.value - b.value; });
		}

		var svg = d3.select("svg"),
				margin = {top: 20, right: 20, bottom: 30, left: 180},
				width = wB - margin.left - margin.right,
				height = hB - margin.top - margin.bottom;

		var tooltip = d3.select("body").append("div").attr("class", "toolTip");

		var x = d3.scaleLinear().range([0, width]);
		var y = d3.scaleBand().range([height, 0]);

		var g = svg.append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		x.domain([0, d3.max(_dataBarForChart, function(d) { return d.value; })]);
		y.domain(_dataBarForChart.map(function(d) { return d.name; })).padding(0.1);

		var xAxis = g.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(d3.axisBottom(x).ticks(5).tickFormat(function(d) { return parseInt(d); }).tickSizeInner([-height]));

		xAxis.transition(200)
				.attr("transform", "translate(0," + height + ")")
				.call(d3.axisBottom(x).ticks(5).tickFormat(function(d) { return parseInt(d); }).tickSizeInner([-height]));

		g.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + 0 + ")")
				.call(d3.axisTop(x).ticks(5).tickFormat(function(d) { return parseInt(d); }).tickSizeInner([-height]));

		g.append("g")
				.attr("class", "y axis")
				.call(d3.axisLeft(y));

		var rect = g.selectAll(".bar")
				.data(_dataBarForChart);

		rect.enter().append("rect")
				.attr("class", "bar")
				.attr("x", 0)
				.attr("height", y.bandwidth())
				.attr("y", function(d) { return y(d.name); })
				.attr("width", function(d) { return x(d.value); })
				.attr('fill', function(d){
					if (t == 1) {
						return (d.color)
					} else {
						return (retColor(d.value-kip))
					}
				})
				.on("mousemove", function(d){
					if (t == 1) {
						if (!!d.e){
							if (!!d.n){
								tooltip
										.style("left", d3.event.pageX - 50 + "px")
										.style("top", d3.event.pageY - 70 + "px")
										.style("display", "inline-block")
										.html((d.name1) +  "<br>" + "С нозодом: " + (d.value));
								return;
							}
							tooltip
									.style("left", d3.event.pageX - 50 + "px")
									.style("top", d3.event.pageY - 70 + "px")
									.style("display", "inline-block")
									.html((d.name1) + "<br>" + "Без нозода: " + (d.value));
						}
					} else {
						tooltip
								.style("left", d3.event.pageX - 50 + "px")
								.style("top", d3.event.pageY - 70 + "px")
								.style("display", "inline-block")
								.html((d.name) + "<br>" + "Значение:" + (d.value));
					}
				})
				.on("mouseout", function(d){ tooltip.style("display", "none");});

		rect.transition(100)
				.attr("height", y.bandwidth())
				.attr("y", function(d) { return y(d.name); })
				.attr("width", function(d) { return x(d.value); })
				.attr('fill', function(d){
					if (t == 1) {
						return (d.color)
					} else {
						return (retColor(d.value-kip))
					}
				});

		rect.exit().remove();

		var insertLinebreaks = function (d) {
			var el = d3.select(this);
			el.text('');
			if (d.search('{{D}}')>-1){
				return
			}
			var words = d.split(' ');
			var temp = [];
			var col = '';

			for (var i=0; i<words.length;i++) {
				if (!col.length) {
					col = words[i];
				} else {
					if ((col+words[i]).length+1 < 20) {
						col += (col.length > 0 ? ' ':'') + words[i];
					} else {
						temp.push(col);
						col = words[i];
					}
				}

				if (i == words.length-1 && !!col) {
					temp.push(col);
				}
			}

			for (var i = 0; i < temp.length; i++) {
				var tspan = el.append('tspan').text(temp[i]);
				tspan.attr('x', -90).attr('dy', '15').attr('text-anchor','middle').attr('dominant-baseline','center');
			}
			el.attr('transform', 'translate(0,'+ (-temp.length*8) +')');
		};

		svg.selectAll('g.y.axis g text').each(insertLinebreaks);
	}

	setInterval(function () {
		var a = $.ajax({
			url: 'http://localhost:5001/dias_for_points',
			data: 'get',
			success: function (data) {
				data1 = _data;
				_data = [[], []];

				function clear () {
					_dataBar1 = [];
					_data1Bar1 = [];
					_labels1 = [];
				}

				clear();

				if ('z' in data.data) {
					_dataBarForChart = [];
					//$('svg').css('display', 'none');
					for (var i = 0; i < data.data.n.length; i++) {
						_labels1.push(unicodeToChar(data.data.n[i].name));
						_dataBar1.push(parseInt(data.data.n[i].mid));
						if (i!=0) {
							_dataBarForChart.push({
								name: '{{D}}'+i,
								value: 0,
								e: null,
								n: null,
								color: null
							});
						}
						_dataBarForChart.push({
							name: "С нозодом - " + unicodeToChar(data.data.z[i].name),
							name1: unicodeToChar(data.data.z[i].name),
							value: parseInt(data.data.z[i].mid),
							e: true,
							n: true,
							color: retColor(data.data.z[i].mid - data.data.n[i].mid)
						});
						_dataBarForChart.push({
							name:  "Без нозода - " + unicodeToChar(data.data.n[i].name),
							name1: unicodeToChar(data.data.n[i].name),
							value: parseInt(data.data.n[i].mid),
							e: true,
							n: false,
							color: retColor(0)
						});

					}
					for (var i = 0; i < data.data.z.length; i++) {
						_data1Bar1.push(parseInt(data.data.z[i].mid));
					}
					t = 1;
				}
				else {
					$('#ctx').css('display', 'none');
					_dataBarForChart = [];
					var isName = false;
					for (var i = 0; i < data.data.length; i++) {
						if (data.data[i].is_main) {
							isName = true;
						}
						_dataBarForChart.push({
							name: unicodeToChar(data.data[i].name),
							value: parseInt(data.data[i].mid),
							is_name: isName
						});

						isName = false;
						_labels1.push(unicodeToChar(data.data[i].name));
						_dataBar1.push(parseInt(data.data[i].mid));
					}
					t = 2;
				}

				if (ff(_dataBar1) != ff(_dataBar)) {
					_dataBar = _dataBar1;
					_data1Bar = _data1Bar1;
					_labels = _labels1;
					colorArr = setColor(_dataBar.length);
					colorBorderArr = setColor(_dataBar.length);
					if (t == 1){
						render_all();
						CreateBar();
					} else {
						CreateBar();
					}
					updateChart();
				}
			},
			dataType: 'json'
		});
	}, 100);

	function updateChart () {
		if (t == 2) {
			if (_dataBar.length > 20) {
				$(canvas2).css('display', 'block');
				for (var i = 0; i < Math.floor((_dataBar.length) / 2); i++) {
					dataArr1[i] = _dataBar[i];
					dataArr2Nazod[i] = _data1Bar[i];
					tmp[i] = (_labels[i]).toString();
				}
				for (var i = Math.floor((_dataBar.length / 2)); i < _dataBar.length; i++) {
					dataArr2[i - Math.floor(_dataBar.length / 2)] = _dataBar[i];
					dataArr2Nazod2[i - Math.floor(_dataBar.length / 2)] = _data1Bar[i];
					tmp1[i - Math.floor(_dataBar.length / 2)] = (_labels[i]).toString();
				}
			} else {
				$(canvas2).css('display', 'none');
				dataArr2 = [];
				for (var i = 0; i < _dataBar.length; i++) {
					dataArr1[i] = _dataBar[i];
					dataArr2Nazod[i] = _data1Bar[i];
					tmp[i] = (_labels[i]).toString();
				}
			}

			myRadarChart.data.datasets[0].data = dataArr1;
			myRadarChart.data.datasets[1].data = dataArr2Nazod;
			myRadarChart.data.labels = tmp;

			myRadarChart2.data.datasets[0].data = dataArr2;
			myRadarChart.data.datasets[1].data = dataArr2Nazod2;
			myRadarChart2.data.labels = tmp1;

			drawGraph();
		} else {
			if (_dataBar1.length > 20) {
				$(canvas2).css('display', 'block');
				for (var i = 0; i < Math.floor((_dataBar1.length) / 2); i++) {
					dataArr1[i] = _dataBar1[i];
					dataArr2Nazod[i] = _data1Bar1[i];
					tmp[i] = (_labels1[i]).toString();
				}
				for (var i = Math.floor((_dataBar1.length / 2)); i < _dataBar1.length; i++) {
					dataArr2[i - Math.floor(_dataBar1.length / 2)] = _dataBar1[i];
					dataArr2Nazod[i - Math.floor(_dataBar1.length / 2)] = _data1Bar1[i];
					tmp1[i - Math.floor(_dataBar1.length / 2)] = (_labels1[i]).toString();
				}
			} else {
				$(canvas2).css('display', 'none');
				dataArr2 = [];
				for (var i = 0; i < _dataBar1.length; i++) {
					dataArr1[i] = _dataBar1[i];
					dataArr2Nazod[i] = _data1Bar1[i];
					tmp[i] = (_labels1[i]).toString();
				}
			}

			myRadarChart.data.datasets[0].data = dataArr1;
			myRadarChart.data.datasets[1].data = dataArr2Nazod;
			myRadarChart.data.labels = tmp;

			myRadarChart2.data.datasets[0].data = dataArr2;
			myRadarChart2.data.datasets[1].data = dataArr2Nazod;
			myRadarChart2.data.labels = tmp1;

			drawGraph();
		}
	};

	function render_all() {
		function colorGenerator () {
			var arr = [];
			for (var i=0; i < _dataBar.length;i++) {
				arr.push(retColor(_data1Bar[i] - _dataBar[i]));
			}
			return arr;
		}

		__data = function () {
			if (t == 2) {
				return {
					labels: _labels,
					datasets: [
						{
							backgroundColor: colorArr,
							borderColor: colorBorderArr,
							borderWidth: 1,
							data: _dataBar
						}
					]
				}
			}
			else {
				return {
					labels: _labels,
					datasets: [
						{
							label: 'Без назода',
							data: _dataBar,
							backgroundColor: "#27ae60"
						}, {
							label: 'С назодом',
							data: _data1Bar,
							//backgroundColor: "rgba(255,153,0,1)"
							backgroundColor: colorGenerator()
						}
					]
				}
			}
		};

		if (!myBarChart) {
			myBarChart = new Chart(canvas3, {
				type: 'bar',
				data: __data(),
				options: {
					responsive: true,
					min: 0,
					max: 100,
					legend: {
						display: false,
						labels: {
							display: false
						}
					},
					scales: {
						yAxes: [{
							display: true,
							ticks: {
								min: 0,
								max: 100,
								stepSize: 20
							}
						}],
						xAxes: [{
							display: false
						}]
					}
				}
			});
		}
		else {
			myBarChart.update();
		}
	}

	function drawGraph() {
		myRadarChart.update();
		myRadarChart2.update();
	}
};

$(window).resize(
		function () {
			location.reload(false);
		}
);