//4 values + diamond for compare
var barData = [0,0,0,0,0];

// up right bottom left up
var coordinates = [{
	'x': 0,
	'y': 0
}, {
	'x': 0,
	'y': 0
}, {
	'x': 0,
	'y': 0
}, {
	'x': 0,
	'y': 0
}, {
	'x': 0,
	'y': 0
}];

var coordinatesCompare = [{
	'x': 0,
	'y': 0
}, {
	'x': 0,
	'y': 0
}, {
	'x': 0,
	'y': 0
}, {
	'x': 0,
	'y': 0
}, {
	'x': 0,
	'y': 0
}];

window.onload = function () {
	var w = $(window).width(),
			h = $(window).height();

	if (w>h) {
		$('svg').css('left', (w-h)/2);
	} else {
		$('svg').css('top', (h-w)/2);
	}

	w>h ? w=h : h=w;

	$('svg').css('width', w);
	$('svg').css('height', h);

	InitChart(w,h);

	function InitChart(w, h) {
		var vis = d3.select('#visualisation'),
				WIDTH = w,
				HEIGHT = h,
				MARGINS = {
					top: 20,
					right: 20,
					bottom: 20,
					left: 20
				},

				xRange = d3.scale.linear().range([WIDTH/2, WIDTH - MARGINS.right]).domain([0, 100]),

				yRange = d3.scale.linear().range([HEIGHT/2 , MARGINS.bottom]).domain([0, 100]),

				xRangeMinus = d3.scale.linear().range([WIDTH/2, MARGINS.left]).domain([0, 100]),

				yRangeMinus = d3.scale.linear().range([MARGINS.top-MARGINS.bottom, HEIGHT/2 -MARGINS.bottom]).domain([0, 100]),

				xAxis = d3.svg.axis()
						.scale(xRange)
						.tickSize(5)
						.ticks(5)
						.tickSubdivide(true),

				yAxis = d3.svg.axis()
						.scale(yRange)
						.tickSize(5)
						.ticks(5)
						.orient("left")
						.tickSubdivide(true),

				xAxisMinus = d3.svg.axis()
						.scale(xRangeMinus)
						.tickSize(5)
						.ticks(5)
						.tickSubdivide(true),

				yAxisMinus = d3.svg.axis()
						.scale(yRangeMinus)
						.tickSize(5)
						.ticks(5)
						.orient("left")
						.tickSubdivide(true);


		vis.append('svg:g')
				.attr('class', 'x axis')
				.attr('transform', 'translate(0,' + (HEIGHT/2) + ')')
				.call(xAxis);

		vis.append('svg:g')
				.attr('class', 'y axis')
				.attr('transform', 'translate(' + (WIDTH/2) + ',0)')
				.call(yAxis);

		vis.append('svg:g')
				.attr('class', 'x axis')
				.attr('transform', 'translate('+ 0 +',' + (HEIGHT/2) + ')')
				.call(xAxisMinus);

		vis.append('svg:g')
				.attr('class', 'y axis')
				.attr('transform', 'translate(' + (WIDTH/2) + ','+ (HEIGHT/2) +')')
				.call(yAxisMinus);

		function updateValues () {
			coordinates[0].x = Math.round(w/2);
			coordinates[0].y = Math.round(yRange(barData[0]));

			coordinates[1].x = Math.round(xRange(barData[1]));
			coordinates[1].y = Math.round(h/2);

			coordinates[2].x = Math.round((w/2));
			coordinates[2].y = Math.round(h/2+yRangeMinus(barData[2]));

			coordinates[3].x = Math.round(xRangeMinus(barData[3]));
			coordinates[3].y = Math.round(h/2);

			coordinates[4].x = coordinates[0].x;
			coordinates[4].y = coordinates[0].y;
		}

		function updateCopmareValue () {
			coordinatesCompare[0].x = Math.round(w/2);
			coordinatesCompare[0].y = Math.round(yRange(barData[4]));

			coordinatesCompare[1].x = Math.round(xRange(barData[4]));
			coordinatesCompare[1].y = Math.round(h/2);

			coordinatesCompare[2].x = Math.round((w/2));
			coordinatesCompare[2].y = Math.round(h/2+yRangeMinus(barData[4]));

			coordinatesCompare[3].x = Math.round(xRangeMinus(barData[4]));
			coordinatesCompare[3].y = Math.round(h/2);

			coordinatesCompare[4].x = coordinatesCompare[0].x;
			coordinatesCompare[4].y = coordinatesCompare[0].y;
		}

		function refreshChart () {
			updateValues();
			updateCopmareValue();

			var lineGraph = vis.selectAll('path.lol')
					.data([coordinates]);

			//This is the accessor function we talked about above
			var lineFunction = d3.svg.line()
					.x(function(d) { return d.x; })
					.y(function(d) { return d.y; })
					.interpolate("linear");

			//The line SVG Path we draw
			lineGraph.enter()
					.append("path")
					.attr('class','lol')
					.attr("d", lineFunction(coordinates))
					.attr("stroke", "#12AFCB")
					.attr("stroke-width", 2)
					.attr("fill", "rgba(18, 175, 203,0.2)");

			lineGraph.transition(100)
					.attr("d", lineFunction(coordinates));

			lineGraph.exit().remove();

			//compare diamond
			var lineGraphCompare = vis.selectAll('path.lolCompare')
					.data([coordinatesCompare]);

			var lineFunctionCompare = d3.svg.line()
					.x(function(d) { return d.x; })
					.y(function(d) { return d.y; })
					.interpolate("linear");
			//The line SVG Path we draw
			lineGraphCompare.enter()
					.append("path")
					.attr('class','lolCompare')
					.attr("d", lineFunctionCompare(coordinatesCompare))
					.attr("stroke", "#27ae60")
					.attr("stroke-width", 2)
					.attr("fill", "none");

			lineGraphCompare.transition(100)
					.attr("d", lineFunctionCompare(coordinatesCompare));

			lineGraphCompare.exit().remove();
		}
		refreshChart();

		var allow_ajax = true;
		setInterval(function () {
			if (!allow_ajax) {
				return;
			}
			allow_ajax = false;
			var a = $.ajax({
				url: 'http://localhost:5001/pmap',
				data: 'get',
				success: function (d) {
					if ('data' in d) {
						barData[0] = d.data.t;
						barData[1] = d.data.r;
						barData[2] = d.data.b;
						barData[3] = d.data.l;
						barData[4] = d.data.n;
						updateValues();
						updateCopmareValue();
						refreshChart();
					}
					allow_ajax = true;
				},
				dataType: 'json',
				complete: function () {
				}
			});
		}, 100);
	}
};

$(window).resize(function (e) {
	location.reload(false);
});
