function unicodeToChar(text) {
	return text.replace(/\\u[\dA-F]{4}/gi,
			function (match) {
				return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
			});
}

var barData = [{
	'x': "\u041A\u0418\u041F \u0421\u041F\u042D\u0414",
	'y': 0,
	'color': '#27ae60'
}, {
	'x': "\u0422\u0415\u041A\u0423\u0429\u0418\u0419",
	'y': 0,
	'color': '#27ae60'
}];

function getColor (num) {
	switch(true) {
		case (num < -14):
			return '#34495e';
		case (num > -15 && num < -7):
			return '#2980b9';
		case (num > -8 && num < -4):
			return '#3498db';
		case (num > -5 && num < 6):
			return '#27ae60';
		case (num > 5 && num < 8):
			return '#f1c40f';
		case (num > 7 && num < 16):
			return '#e74c3c';
		case (num > 15):
			return '#c0392b';
	}
}

function setColorCurrent() {
	if (barData[0].y == 0) {
		barData[1].color = 'grey';
	} else {
		barData[1].color = getColor(barData[1].y-barData[0].y);
	}
}

var key = function(d) {
	return d.y;
};

var ff = null;

window.onload = function () {
	var w = $(window).width(),
	h = $(window).height();

	InitChart(w,h);

	$('svg').attr('height', h);
	$('svg').attr('width', w);

	function InitChart(w, h) {
		var vis = d3.select('#visualisation'),
				WIDTH = w,
				HEIGHT = h,
				MARGINS = {
					top: 20,
					right: 20,
					bottom: 20,
					left: 35
				},
				xRange = d3.scale.ordinal().rangeRoundBands([MARGINS.left, WIDTH - MARGINS.right], 0.1).domain(barData.map(function (d) {
					return  d.x;
				})),

				yRange = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([0, 100]),

				xAxis = d3.svg.axis()
						.scale(xRange)
						.tickSize(5)
						.tickSubdivide(true),

				yAxis = d3.svg.axis()
						.scale(yRange)
						.tickSize(5)
						.ticks(5)
						.orient("left")
						.tickSubdivide(true);

		vis.append('svg:g')
				.attr('class', 'x axis')
				.attr('transform', 'translate(0,' + (HEIGHT - MARGINS.bottom) + ')')
				.call(xAxis);

		vis.append('svg:g')
				.attr('class', 'y axis')
				.attr('transform', 'translate(' + (MARGINS.left) + ',0)')
				.call(yAxis);

		function refreshChart () {
			var rect = vis.selectAll('rect')
					.data(barData);

			rect.enter()
					.append('rect')
					.attr('x', function (d) {
						return xRange(d.x);
					})
					.attr('y', function (d) {
						return yRange(d.y);
					})
					.attr('width', xRange.rangeBand())
					.attr('height', function (d) {
						return ((HEIGHT - MARGINS.bottom) - yRange(d.y));
					})
					.attr('stroke', '#858585')
					.attr('stroke-width', '1px')
					.attr('fill',  function (d) {
						return d.color;
					});

			rect.transition(100)
					.attr('y', function (d) {
						return yRange(d.y);
					})
					.attr('height', function (d) {
						return ((HEIGHT - MARGINS.bottom) - yRange(d.y));
					})
					.attr('fill',  function (d) {
						return d.color;
					});

			rect.exit()
					.remove();

			//Create labels
			var label = vis.selectAll('text.val')
					.data(barData);

			label.enter()
					.append('text')
					.attr('class','val')
					.attr('text-anchor', 'middle')
					.text(function(d) {return d.y})
					.attr('x', function (d) {
						return xRange(d.x)+xRange.rangeBand()/2;
					})
					.attr('y', function (d) {
						return yRange(d.y)-10;
					});

			label.transition(100)
					.text(function(d) {return d.y.toFixed(0)})
					.attr('y', function (d) {
						return yRange(d.y)-10;
					});
		}
		refreshChart();

		var allow_ajax = true;
		setInterval(function () {
			if (!allow_ajax) {
				return;
			}
			allow_ajax = false;
			var a = $.ajax({
				url: 'http://localhost:5001/kip',
				data: 'get',
				success: function (d) {
					if ('k' in d) {
						barData[0].y = d.k;
						barData[1].y = d.v;
						setColorCurrent();
						refreshChart();
					}
					allow_ajax = true;
				},
				dataType: 'json',
				complete: function () {

				}
			});
		}, 100);
	}
};

$(window).resize(function (e) {
    location.reload(false);
});
