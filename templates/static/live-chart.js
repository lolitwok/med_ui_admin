	$( document ).ready(function() {
		function displayGraphExample(id, width, height, interpolation, animate, updateDelay, transitionDelay) {
			var MARGINS = {
				top: 15,
				right: 40,
				bottom: 15,
				left: 40
			};
			var _data = [];
			var numvalues = 50;
			var last_time = 0;

			for (var i = 0; i < numvalues; ++i) {
				_data.push(0);
			}

			var graph = d3.select(id).append("svg:svg").attr("width", "100%").attr("height", "100%");

			// X scale will fit values from 0-10 within pixels 0-100
			var x = d3.scale.linear().domain([0, numvalues]).range([MARGINS.left, width - MARGINS.right]);
			// Y scale will fit values from 0-10 within pixels 0-100
			var y = d3.scale.linear().domain([0, 100]).range([height - MARGINS.top, MARGINS.bottom]);

			// create a line object that represents the SVN line we're creating
			var line = d3.svg.line()
					// assign the X function to plot our line as we wish
					.x(function(d,i) {
						return x(i);
					})
					.y(function(d) {
						return y(d);
					})
					.interpolate(interpolation);

			var yAxis = d3.svg.axis().scale(y).orient("left").ticks(5);
			var yAxis2 = d3.svg.axis().scale(y).orient("right").ticks(5);
			var yAxisline = d3.svg.axis().scale(y).orient("left").ticks(5).tickSize(width-MARGINS.right-MARGINS.left);

			var xAxisline = d3.svg.axis().scale(x).orient("top").ticks(10).tickSize(height-MARGINS.top-MARGINS.bottom);

			graph.append("g")
					.attr("class", "y axis")
					.attr("transform", "translate(" + (MARGINS.left) + ",0)")
					.call(yAxis);

			graph.append("g")
					.attr("class", "y axis")
					.attr("transform", "translate(" + (width - MARGINS.right) + ",0)")
					.call(yAxis2);

			graph.append("g")
					.attr("class", "y axis axisline")
					.attr("transform", "translate(" + (width - MARGINS.right) + ",0)")
					.call(yAxisline);

			var __x =graph.append("g")
					.attr("class", "x axis axisline")
					.attr("transform", "translate(0," + (height - MARGINS.bottom)  + ")")
					.call(xAxisline);

			graph.append("svg:path").attr("d", line(_data));

			function redrawWithAnimation() {
				// update with animation
				graph.selectAll("svg > path")
						.data([_data]) // set the new data
						.attr("transform", "translate(" + (x(1)-MARGINS.left) + ")") // set the transform to the right by x(1) pixels (6 for the scale we've set) to hide the new value
						.attr("d", line) // apply the new data values ... but the new value is hidden at this point off the right of the canvas
						.transition() // start a transition to bring the new value into view
						.ease("linear")
						.duration(transitionDelay) // for this demo we want a continual slide so set this to the same as the setInterval amount below
						.attr("transform", "translate(" + (x(0)-MARGINS.left) + ")"); // animate a slide to the left back to x(0) pixels to reveal the new value
				/* thanks to 'barrym' for examples of transform: https://gist.github.com/1137131 */
			}

			function redrawxAxis(){
				__x.attr("transform", "translate(" + (x(5)-MARGINS.left) + "," + (height - MARGINS.bottom)  + ")") // set the transform to the right by x(1) pixels (6 for the scale we've set) to hide the new value
						.transition() // start a transition to bring the new value into view
						.ease("linear")
						.duration(transitionDelay*5) // for this demo we want a continual slide so set this to the same as the setInterval amount below
						.attr("transform", "translate(" + (x(0)-MARGINS.left) + "," + (height - MARGINS.bottom)  + ")"); // animate a slide to the left back to x(0) pixels to reveal the new value
			}

			var allow_ajax = true;

			function redrawLine() {
				if (!allow_ajax) {
					return;
				}
				allow_ajax = false;
				var a = $.ajax({
					url: 'http://localhost:5001/data',
					data: 'get',
					success: function (d) {
						if (d.hasOwnProperty('data')) {
							if (!d.data[d.data.length-1]) {
								last_time = 0;
							} else {
								last_time = d.data[d.data.length-1][0];
							}
							_data.push(last_time ==-10? 0: last_time );

							while (_data.length > numvalues) {
								_data.shift();
							}
						}
						allow_ajax = true;
					},
					dataType: 'json',
					complete: function () { }
				});
				redrawWithAnimation();
				$('.curr_value').html(last_time);
			}
			redrawLine();
			setInterval(redrawLine, updateDelay);
			redrawxAxis();
			setInterval(redrawxAxis, updateDelay*5);
		}

		var w = $("#graph1").width();
		var h = $("#graph1").height();
		//console.log(w,h);

		displayGraphExample("#graph1", w, h, "basis", true, 100, 100);
	});

	$(window).resize(
    function () {
        location.reload(false);
    }
);