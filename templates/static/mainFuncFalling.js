var _data = [[], []];
var index = 0;
var tmp = [];
//var RG = RGraph;
var ma = Math;

var obj = null;
var updates = 0;
var data = [];
var i = 0;

var currentPage = 0;
var currArray = [];
var currArrayLabels = [];
var ElementsPerPage = 15;
var maxPage = 0;

var Resize = function () {
    //var canvas = document.getElementById("cvs");
    //var bar = document.getElementById("ctx");
    $("#cvs").css("width", $(window).width());
    $("#cvs").css("height", $(window).height() - 65);
    $("#cvs").attr("width", $(window).width());
    $("#cvs").attr("height", $(window).height() - 65);
    //$("#cvs_rgraph_domtext_wrapper").css("width", $(window).width());
    //$("#cvs_rgraph_domtext_wrapper").css("height", $(window).height() - 50);
    //$("#cvs_rgraph_domtext_wrapper").attr("width", $(window).width());
    //$("#cvs_rgraph_domtext_wrapper").attr("height", $(window).height() - 50);
    $("#ctx").css("width", $(window).width());
    $("#ctx").css("height", $(window).height() - 65);
    $("#ctx").attr("width", $(window).width());
    $("#ctx").attr("height", $(window).height() - 65);
};

window.onload = function () {
    var canvas = document.getElementById("cvs");
    var bar = document.getElementById("ctx");

    Resize();

    if (!myChart) {
        var myChart = new Chart(cvs, {
                type: 'line',
                data: {
                    labels: tmp,
                    datasets: [{
                        data: tmp,
                        backgroundColor: "rgba(18, 175, 203,0.2)",
                        borderColor: "#12AFCB",
                        borderRadius: 1,
                        pointRadius: 0
                    }]
                },
                options: {
                    responsive: false,
                    legend: {
                        display: false,
                        labels: {
                            display: false
                        }
                    },
                    tooltips: {
                        enabled: false
                    },
                    animation: false,
                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                min: 0,
                                max: 100,
                                stepSize: 20
                            }
                        }],
                        xAxes: [{
                            display: false
                        }]
                    }
                }
            }
        );
        //myChart.defaults.global.legend.display = false;
    }

    var bgSet1 = [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
    ];
    borderSet1 = [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)',
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)',
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)',
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
    ];

    var data = {
        labels: [],
        datasets: [
            {
                backgroundColor: bgSet1,
                borderColor: borderSet1,
                borderWidth: 1,
                data: []
            }
        ]
    };

    if (!myBarChart) {
        var myBarChart = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: {
                responsive: true,
                min: 0,
                max: 100,
                legend: {
                    display: false,
                    labels: {
                        display: false
                    }
                },
                animation: false,
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            min: 0,
                            max: 100,
                            stepSize: 20
                        }
                    }],
                    xAxes: [{
                        display: false
                    }]
                }
            }
        });
    }

    var fallingInterval = function () {
        var a = $.ajax({
            url: 'http://localhost:5001/cur_dia',
            data: 'get',
            success: function (data) {
                //console.log(data);
                if ('data' in data) {
                    tmp = data.data.points;
                    tmp.unshift(0);
                    updateChartFalling();
                }
            },
            dataType: 'json'
        });
    };

    var barInterval = function () {
        var a = $.ajax({
            url: 'http://localhost:5001/dias',
            data: 'get',
            success: function (data) {
                //console.log(data);
                middle = [];
                measurArr = [];
                _labels = [];
                if ('data' in data) {
                    for (var j = 0; j < data.data.dias.length; j++) {
                        middle.push(data.data.middle);
                    }

                    if ('dias' in data.data) {
                        for (var i = 0; i < data.data.dias.length; i++) {
                            measurArr.push(data.data.dias[i].val);
                            _labels.push(data.data.dias[i].name);


                            //setPage();
                        }
                        maxPage = parseInt((measurArr.length) / 15);
                        if (parseInt((measurArr.length) / 15) < (measurArr.length) / 15) {
                            maxPage++;
                        }
                        if (currentPage > maxPage)
                            currentPage = maxPage;
                        updateChartBar();
                    }
                    //console.log(middle);
                    //console.log(measurArr);

                }
            }
            ,
            dataType: 'json'
        });
    };

    var updateChartFalling = function () {
        myChart.data.datasets[0].data = tmp;
        myChart.data.labels = tmp;
        console.log(tmp);
        //myChart.data.labels = _data;
        myChart.update();
        //obj.original_data[0] = tmp;
        //RG.Clear(canvas);
        //obj.draw();
    };


    var updateChartBar = function () {
        setPage(currentPage);
        console.log('Current arrauy' + currArray);
        myBarChart.data.datasets[0].data = currArray;
        myBarChart.data.labels = currArrayLabels;
        myBarChart.update();
    };

    var barIntervals = null;
    var fallingIntervals = null;

    $(".graphswitch > .fa").live("click", function (e) {
        //e.PreventDefault();
        var _this = $(this);
        if (_this.hasClass('fa-bar-chart')) {
            _this.addClass('active');
            $('.fa-area-chart').removeClass('active');
            clearInterval(fallingInterval);
            var barIntervals = setInterval(barInterval, 200);
            $('#cvs').css('display', 'none');
            //$('#cvs_rgraph_domtext_wrapper').css('display', 'none');

            $('.switch').css('display', '');
            //RG.Clear(canvas);
            $('#ctx').css('display', '');
            Resize();
            console.log("bar int start");
        }
        if (_this.hasClass('fa-area-chart')) {
            _this.addClass('active');
            $('.fa-bar-chart').removeClass('active');
            clearInterval(barInterval);
            var fallingIntervals = setInterval(fallingInterval, 200);
            console.log("falling int start");
            $('#ctx').css('display', 'none');
            //$('#cvs_rgraph_domtext_wrapper').css('display', '');
            $('#cvs').css('display', '');
            $('.switch').css('display', 'none');
            Resize();
        }
    });

    $('.fa-bar-chart').click();
    //$('.fa-area-chart').click();
};

$(window).resize(
    //Resize()
    function() {location.reload(false);}
);

$(document).ready (
    function () {
        $('.fa-angle-left').click ( minus_page);
        $('.fa-angle-right').click ( plus_page);
    });

function setPage() {
    var page = currentPage;

    var from_element = page * ElementsPerPage;
    var to_element = page * ElementsPerPage + ElementsPerPage;
    if (to_element > measurArr.length) {
        to_element = measurArr.length;
        from_element = measurArr.length - ElementsPerPage;
        if (from_element < 0) {
            from_element = 0;
        }
    }
    currArray = [];
    currArrayLabels = [];
    for (var i = from_element; i < to_element; i++) {
        currArray.push(measurArr[i]);
        currArrayLabels.push(_labels[i]);
    }
    //console.log("all=", measurArr.length, " from = ", from_element, " to= ", to_element);

    $('.page').text((page+1) + ' из ' + (maxPage+1));
}

function plus_page() {
    currentPage++;
    if (currentPage > maxPage) {
        currentPage = maxPage;
    }
    setPage();
}


function minus_page() {
    currentPage--;
    if (currentPage < 0) {
        currentPage = 0;
    }
    setPage();

}
