var _data = [0];
var RG = RGraph;
var ma = Math;
var canvas = null;
var canvas2 = null;
var obj = null;
var obj2 = null;
var data = [];
var last_time = 0;
var l = 0; // The letter 'L' - NOT a one
var numvalues = 20;
var updates = 0;
var timestamp = [];

var myChart = null;


window.onload = function () {
    var ctx = document.getElementById('myChart').getContext('2d');
    var canvas = document.getElementById("myChart");
    $("#myChart").css("width", $(window).width()-100);
    $("#myChart").css("height", "100vh");
    $("#myChart").attr("width", $(window).width()-100);
    $("#myChart").attr("height", $(window).height());
    canvas2 = document.getElementById("cvs2");

    $("#cvs2").css("height", $(window).height());
    $("#cvs2").attr("height", $(window).height());

    // Pre-pad the arrays with null values
    for (var i = 0; i < numvalues; ++i) {
        if (i < 11) {
            data.push(0);
        }
        _data.push(0);
    }

    if (!obj2) {
        obj2 = new RG.VProgress({
            id: 'cvs2',
            min: 0,
            max: 100,
            value: 0,
            options: {
                colors: ['#12AFCB'],
                shadowColor: ['rgba(0,0,0,0)'],
                gutterTop: 6,
                gutterBottom: 6,
                gutterRight: 40,
                gutterLeft: 0,
                textAccessible: true,
                bevel: false,
                numticksInner: 5
            }
        })
    }

    if (!myChart) {
        var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: _data,
                    datasets: [{
                        data: _data,
                        backgroundColor: "rgba(18, 175, 203,0.2)",
                        borderColor: "#12AFCB",
                        borderRadius: 1,
                        pointRadius: 0
                    }]
                },
                options: {
                    responsive: false,
                    legend: {
                        display: false,
                        labels: {
                            display: false
                        }
                    },
                    tooltips: {
                        enabled: false
                    },
                    animation: false,
                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                min: 0,
                                max: 100,
                                stepSize: 20
                            }
                        }],
                        xAxes: [{
                            display: false
                        }]
                    }
                }
            }
        );
    }

    var allow_ajax = true;
    setInterval(function () {
        if (!allow_ajax) {
            return;
        }
        allow_ajax = false;
        var a = $.ajax({
            url: 'http://localhost:5001/data',
            data: 'get',
            success: function (d) {
                if ('data' in d) {
                    console.log("Data: " , d.data);
                    if (!d.data[d.data.length-1]) {
                        _data.push(0);
                        last_time = 0;
                    } else {
                        for (var i = 0; i < d.data.length; i++) {
                            if (d.data[i][0] == -10 ) {
                                continue;
                            }
                            if (d.data[i][1] != last_time) {
                                _data.push(d.data[i][0]);
                                last_time = d.data[i][1];
                            }
                        }

                    }
                    while (_data.length > numvalues) {
                        _data.shift();
                        //console.log(_data);
                    }
                    updateChart('loh');
                }
                allow_ajax = true;
            },
            dataType: 'json',
            complete: function () { }
        });
    }, 100);

    var updateChart = function () {
        console.log(_data);
        myChart.data.datasets[0].data = _data;
        myChart.data.labels = _data;

        //console.log(myChart.data.datasets[0].data);
        document.getElementById('number').innerHTML = _data[_data.length - 1];

        obj2.value = _data[_data.length - 1];
        RG.Clear(canvas2);
        obj2.draw();
        drawGraph();
    };

    function drawGraph() {
        myChart.update();


        updates++;
        if (updates % 100 === 0) {
            console.log(updates);
        }
    }
};

$(window).resize(
    function () {
        location.reload(false);
    }
);
