var _data = [[], []];
var index = 0;
var tmp = [];
var ma = Math;
var measurArr = [];
var _labels = [];

var myBarChart = null;
var updates = 0;
var data = [];
var i = 0;

function resizeGraph() {
	var canvas = document.getElementById("ctx");
	$("#ctx").css("width", $(window).width());
	$("#ctx").css("height", "100vh");
	$("#ctx").attr("width", $(window).width());
	$("#ctx").attr("height", $(window).height());
	myBarChart.update();
}

window.onload = function () {
	var canvas = document.getElementById("ctx");
	$("#ctx").css("width", $(window).width());
	$("#ctx").css("height", "100vh");
	$("#ctx").attr("width", $(window).width());
	$("#ctx").attr("height", $(window).height());

	function unicodeToChar(text) {
		return text.replace(/\\u[\dA-F]{4}/gi,
				function (match) {
					return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
				});
	}

	var data = {
		labels: [],
		datasets: [
			{
				backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(255, 159, 64, 0.2)',
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(153, 102, 255, 0.2)',
					'rgba(255, 159, 64, 0.2)'
				],
				borderColor: [
					'rgba(255,99,132,1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)',
					'rgba(255,99,132,1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)'
				],
				borderWidth: 1,
				data: []
			}
		]
	};

	if (!myBarChart){
		var myBarChart = new Chart(ctx, {
			type: 'bar',
			data: data,
			options: {
				responsive: true,
				min: 0,
				max: 100,
				scales: {
					yAxes: [{
						ticks: {
							max: 100,
							min: 0,
							stepSize: 10
						}
					}]
				}
			}
		});
	}

	setInterval(function () {
		var a = $.ajax({
			url: 'http://localhost:5001/dias',
			data: 'get',
			success: function (data) {
				console.log(data);
				middle = [];
				measurArr = [];
				_labels = [];
				if ('data' in data) {
					for (var j=0;j<data.data.dias.length;j++) {
						middle.push(data.data.middle);
					}

					if ('dias' in data.data) {
						for (var i = 0; i < data.data.dias.length; i++) {
							measurArr.push(data.data.dias[i].val);
							_labels.push(data.data.dias[i].name);
						}
						updateChart();
					}
					console.log(middle);
					console.log(measurArr);

				}
			},
			dataType: 'json'
		});
	}, 100);


	var updateChart = function () {
		myBarChart.data.datasets[0].data = measurArr;
		myBarChart.data.labels = _labels;
		console.log(myBarChart.data.datasets[0].data);
		console.log(myBarChart.data.labels);

		drawGraph();
	};

	function drawGraph() {
		myBarChart.update();

		updates++;
		if (updates % 100 === 0) {
			console.log(updates);
		}
	}
};


$(window).resize(
		resizeGraph()
);


