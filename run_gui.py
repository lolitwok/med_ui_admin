# -*- coding: utf-8 -*-
import sys
from PyQt4 import QtGui, uic, QtCore, Qt

from applibs.dao import MainSession, cur_main_session, main_db_engine
from applibs.ui.dia_main import Window
from applibs.models.main_models import DModel


if __name__ == '__main__':
	from applibs.models.main_models import Base
	Base.metadata.create_all(main_db_engine)

	# if cur_session.query(DModel).filter(DModel.name == "Hand Normal").first() is None:
	# 	m = DModel()
	# 	m.name = "Hand Normal"
	# 	m.file_name = "hand.obj"
	# 	cur_session.add(m)
	# 	cur_session.commit()
	#
	# if cur_session.query(DModel).filter(DModel.name == "Hand Norma-t22r").first() is None:
	# 	m = DModel()
	# 	m.name = "Hand Norma-t22r"
	# 	m.file_name = "22-r.obj"
	# 	cur_session.add(m)
	# 	cur_session.commit()

	app = QtGui.QApplication(sys.argv)
	app.setQuitOnLastWindowClosed(False)
	win = Window()
	win.show()
	ec = app.exec_()
	sys.exit(ec)
