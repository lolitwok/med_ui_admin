# -*- coding: utf-8 -*-
import os
import pygame
from OpenGL.GL import *
from applibs import _base_dir, BASE_DIR
import random


def MTL(filename, res_dir):
	contents = {}
	mtl = None
	for line in open(os.path.join(res_dir, filename), "r"):
		if line.startswith('#'): continue
		values = line.split()
		if not values: continue
		if values[0] == 'newmtl':
			mtl = contents[values[1]] = {}
		elif mtl is None:
			raise ValueError, "mtl file doesn't start with newmtl stmt"
		elif values[0] == 'map_Kd' or values[0] == 'map_Ka' or values[0] == 'map_Ks':
			# load the texture referred to by this declaration
			mtl[values[0]] = values[1]
			surf = pygame.image.load(os.path.join(_base_dir, 'resources', mtl[values[0]]))
			image = pygame.image.tostring(surf, 'RGBA', 1)
			ix, iy = surf.get_rect().size

			if values[0] == 'map_Kd':
				texid = mtl['texture_Kd'] = glGenTextures(1)
			elif values[0] == 'map_Ka':
				texid = mtl['texture_Ka'] = glGenTextures(1)
			else:
				texid = mtl['texture_Ks'] = glGenTextures(1)

			glBindTexture(GL_TEXTURE_2D, texid)
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
							GL_LINEAR)
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
							GL_LINEAR)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ix, iy, 0, GL_RGBA,
						 GL_UNSIGNED_BYTE, image)
		else:
			mtl[values[0]] = map(float, values[1:])
	return contents


class OBJ:
	def __init__(self, filename, swapyz=False, _id=1):
		"""Loads a Wavefront OBJ file. """
		self.vertices = []
		self.normals = []
		self.texcoords = []
		self.faces = []
		self.res_dir = os.path.join(BASE_DIR, 'resources')
		self.gl_list = None

		material = None
		for line in open(os.path.join(self.res_dir, filename), "r"):
			if line.startswith('#'): continue
			values = line.split()
			if not values: continue
			if values[0] == 'v':
				v = map(float, values[1:4])
				if swapyz:
					v = v[0], v[2], v[1]
				self.vertices.append(v)
			elif values[0] == 'vn':
				v = map(float, values[1:4])
				if swapyz:
					v = v[0], v[2], v[1]
				self.normals.append(v)
			elif values[0] == 'vt':
				self.texcoords.append(map(float, values[1:3]))
			elif values[0] in ('usemtl', 'usemat'):
				material = values[1]
			elif values[0] == 'mtllib':
				self.mtl = MTL(values[1], self.res_dir)
			elif values[0] == 'f':
				face = []
				texcoords = []
				norms = []
				for v in values[1:]:
					w = v.split('/')
					face.append(int(w[0]))
					if len(w) >= 2 and len(w[1]) > 0:
						texcoords.append(int(w[1]))
					else:
						texcoords.append(0)
					if len(w) >= 3 and len(w[2]) > 0:
						norms.append(int(w[2]))
					else:
						norms.append(0)
				self.faces.append((face, norms, texcoords, material))

			# def render(self):
		self.gl_list = glGenLists(_id)
		glNewList(self.gl_list, GL_COMPILE)
		glEnable(GL_TEXTURE_2D)
		glFrontFace(GL_CCW)
		ind = 0
		for face in self.faces:
			vertices, normals, texture_coords, material = face

			mtl = self.mtl[material]
			if 'texture_Kd' in mtl:
				# use diffuse texmap
				glBindTexture(GL_TEXTURE_2D, mtl['texture_Kd'])
			else:
				# just use diffuse colour
				glColor(*mtl['Kd'])

			glBegin(GL_POLYGON)
			for i in range(len(vertices)):
				if normals[i] > 0:
					glNormal3fv(self.normals[normals[i] - 1])
				if texture_coords[i] > 0:
					glTexCoord2fv(self.texcoords[texture_coords[i] - 1])
				glVertex3fv(self.vertices[vertices[i] - 1])
			glEnd()
			# ind += 1
			# print "loading %s from %s %%" % (ind, len(self.faces))
		glDisable(GL_TEXTURE_2D)
		glEndList()
