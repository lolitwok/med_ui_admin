# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui


class BtnsEmuDialog(QtGui.QDialog):
	load_progress = QtCore.pyqtSignal(str)

	def __init__(self, parent=None, pp=None):
		super(BtnsEmuDialog, self).__init__(parent)

		self.pp = pp

		self.btn1 = QtGui.QPushButton('btn1', self)
		self.btn2 = QtGui.QPushButton('btn2', self)

		self.btn1.clicked.connect(self.btn1_click)
		self.btn2.clicked.connect(self.btn2_click)

		hbox = QtGui.QHBoxLayout()
		hbox.addWidget(self.btn1)
		hbox.addWidget(self.btn2)
		self.setLayout(hbox)

	def showEvent(self, *args, **kwargs):
		pass

	def closeEvent(self, e):
		pass

	def btn1_click(self):
		self.pp.device_btns.emit(1)

	def btn2_click(self):
		self.pp.device_btns.emit(2)
