# -*- coding: utf-8 -*-
import sys
import time
from applibs.main_ui.dialog.cur_session_view_dialog import CurSessionViewDialog
import logging
from PIL.ImageQt import ImageQt
from applibs import _translate as tr
from applibs.main_ui.buttons_emulator import BtnsEmuDialog
from applibs.main_ui.dialog.hand_dialog import HandDialog
from applibs.main_ui.dialog.login_dialog import LoginDialog
from applibs.main_ui.dialog.start_dia_dialog import StartDialog
from applibs.main_ui.dialog.patient_info_dialog import PatientInfoDialog
from applibs.main_ui.utils import DAI_NUMB, PointMeasurementSettings, SessionInfo, MeasurementArr
from applibs.main_ui.widgets.more_menu_item import MoreActionsWidget
from applibs.main_ui.widgets.users import UsersListWidget
from applibs.objloaderthread import GLLoader
from applibs.serverthread import IOSocketThread
from PyQt4 import QtGui, QtCore
from applibs.main_ui.src.mainWindow import Ui_MainWindow
from applibs.driver import DriverThread
# from applibs.fake_driver import DriverThread

from applibs.simple_logger import SLogger
import types

logger = SLogger.get_logger()
autologin = False

from applibs.main_ui.widgets.diag_select import DiagSelectWidget
from applibs.main_ui.widgets.functional_map import FunctionalMapWidget
from applibs.main_ui.widgets.point_map import PointMapWidget
from applibs.main_ui.widgets.nazod_diagnostic import NazodDiagnosticWidget
from applibs.main_ui.widgets.patiens_list import PatientListWidget
from applibs.main_ui.widgets.patient_edit_form import PatientEditForm
from applibs.main_ui.widgets.patient_info import PatientInfoWidget
from applibs.main_ui.widgets.sessions_list import SessionsListWidget
from applibs.main_ui.widgets.session_view import SessionsViewWidget
from applibs.main_ui.widgets.customer_nozods import CustomerNozodsWidget

WIDGETS = {
	'all_diagnostics': ('all_diagnostics', DiagSelectWidget),
	'diagnostic1': ('diagnostic1', FunctionalMapWidget),
	'diagnostic2': ('diagnostic2', NazodDiagnosticWidget),
	'diagnostic3': ('diagnostic3', PointMapWidget),
	'diagnostic4': ('diagnostic4', None),
	'diagnostic5': ('diagnostic5', None),
	'patients': ('patients', PatientListWidget),
	'reports': ('reports', None),
	'study': ('study', None),
	'settings': ('settings', None),
	'history': ('history', None),
	'add_edit_patient': ('add_edit_patient', PatientEditForm),
	'patient_info': ('patient_info', PatientInfoWidget),
	# 'patient_select': ('patient_select', PatientSelectWidget),
	'all_sessions': ('all_sessions', SessionsListWidget),
	'session_view': ('session_view', SessionsViewWidget),
	'more_actions_view': ('more_actions_view', MoreActionsWidget),
	'users_view': ('users_view', UsersListWidget),
	'customer_nozods': ('customer_nozods', CustomerNozodsWidget),
}

loaded_widgets = {k: None for k in WIDGETS}

DIAGNOSTICS = {
	1: (3, WIDGETS['diagnostic1']),
	2: (4, WIDGETS['diagnostic2']),
	3: (5, WIDGETS['diagnostic3']),
}

NORMAL = """QWidget{
background-color:#34425A;
border-right: 5px solid #34425A;
border-left: 5px solid #34425A;
}"""

SELECTED = """QWidget{
background-color:#2B384E;
border-right: 5px solid #2B384E;
border-left: 5px solid #22BAA0;
}"""


def try_end_dia(_self=None):
	def wrepper(func):
		def wrepper_f(self, *args, **kwargs):
			this_self = _self if _self else self

			if this_self.cur_session is None or this_self.cur_session.cur_diagnostic is None:
				return func(self, *args, **kwargs)

			mes = QtGui.QMessageBox.warning(
				this_self,
				tr(u"Диагностика"),
				tr(u"Вы действительно хотите завершить диагностику?"),
				QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
			if mes != QtGui.QMessageBox.Yes:
				return False

			return func(self, *args, **kwargs)

		return wrepper_f

	return wrepper


class Window(QtGui.QMainWindow):
	update_value = QtCore.pyqtSignal(int)
	device_status = QtCore.pyqtSignal(int)
	update_dia_list = QtCore.pyqtSignal(object)

	device_btns = QtCore.pyqtSignal(int)
	# shown = QtCore.pyqtSignal()
	is_initialize = False
	driver = None
	server = None
	timer = None
	models_preloader = None

	def __init__(self, parent=None):
		super(Window, self).__init__(parent)
		orig = sys.excepthook

		def _excepthook(excType, excValue, traceback):
			def extract_function_name(tb):
				import traceback
				stk = ''.join(traceback.format_list(traceback.extract_tb(tb)))
				return stk

			orig(excType, excValue, traceback)
			logging.getLogger().critical(extract_function_name(traceback) + "\n" + str(excValue))

		sys.excepthook = _excepthook

		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.user = None

		self.login_dialog = None
		self.start_dialog = None
		self.patient_dialog = None
		self.cur_session_view_dialog = None

		self.hand_dialog = None
		# self.log_dialog = None

		# self.hand_dialog.start()

		self.point_settings = PointMeasurementSettings()

		self.cur_session = None
		self.cur_patient = None
		self.cur_complaints = None

		self.selected_diagnostic = None
		self.selected_measurement_array = None

		self.report = None
		self.start_dia_after_create = False

		self.device_status.connect(self.device_status_slot)
		self.device_btns.connect(self.device_btns_slot)

		self.ui.w_dia.mousePressEvent = types.MethodType(self.w_dia_clicked, self.ui.w_dia)
		self.ui.w_patients.mousePressEvent = types.MethodType(self.w_patients_clicked, self.ui.w_patients)
		self.ui.w_reports.mousePressEvent = types.MethodType(self.w_reports_clicked, self.ui.w_reports)
		self.ui.w_more.mousePressEvent = types.MethodType(self.w_more_clicked, self.ui.w_more)
		self.ui.btnLogout.mousePressEvent = types.MethodType(self.log_out, self.ui.btnLogout)

		self.ui.end_session_button.clicked.connect(self.end_session_click)
		self.ui.pushButton_2.clicked.connect(self.open_patient_info)
		self.ui.btn_show_cur_session_4.clicked.connect(self.open_cur_session)
		self.end_diagnostic()
		self.load_widget(WIDGETS['all_diagnostics'])
		self.showMaximized()

	@property
	def get_cur_point_type(self):
		if self.current_type == DAI_NUMB['NAZOD'] and self.current_point_type == 0:
			return 'with_nazod'
		return 'no_nazod'

	def start_new_diagnostic(self):
		self.point_settings = PointMeasurementSettings()

	# self.cur_session.cur_diagnostic = DiagnosticInfo()
	# self.ui.dia_info.setVisible(True)

	def end_diagnostic(self):
		self.point_settings = PointMeasurementSettings()
		self.selected_diagnostic = None
		# self.ui.dia_info.setVisible(False)
		if self.hand_dialog:
			self.hand_dialog.close()

	def start_session(self):
		self.start_dialog = StartDialog(self)
		self.start_dialog.exec_()
		if self.start_dialog.result() != 0:
			return

		if self.start_dialog.res != 1:
			return

		self.ui.widgetCurrentPatient.setVisible(True)
		self.ui.session_info.setVisible(True)
		self.ui.pushButton_2.setVisible(True)

		self.ui.labCurrentPatient.setText(unicode("%s %s" % (self.cur_patient.first_name, self.cur_patient.last_name)))
		if self.cur_patient.patient_type == -1:
			self.ui.pushButton_2.setVisible(False)

		self.cur_session = SessionInfo()
		self.cur_session.cur_patient = self.cur_patient
		self.set_patient(self.cur_patient)
		if self.cur_complaints:
			self.cur_session.complaints = self.cur_complaints

		self.load_widget(self.selected_diagnostic[1])

	def set_patient(self, p):
		if p is None:
			self.ui.widgetCurrentPatient.setVisible(False)
			self.ui.session_info.setVisible(False)
			return

		if self.cur_session is None:
			return

		self.ui.widgetCurrentPatient.setVisible(True)
		self.ui.pushButton_2.setVisible(True)

		img = p.get_pic()
		img.thumbnail((56, 56))
		self.ui.label.setPixmap(QtGui.QPixmap.fromImage(ImageQt(img)))

		self.cur_patient = p
		self.ui.labCurrentPatient.setText(unicode("%s %s" % (p.first_name, p.last_name)))
		if p.patient_type == -1:
			self.ui.pushButton_2.setVisible(False)

		self.cur_session.cur_patient = p
		if self.cur_complaints:
			self.cur_session.complaints = self.cur_complaints

		# if p is not None and self.selected_diagnostic is not None:
		# 	self.load_widget(self.selected_diagnostic[1])
		#
		# elif p is not None:
		# 	self.load_widget(WIDGETS['patient_select'])
		#
		# else:
		# 	self.load_widget(WIDGETS['all_diagnostics'])

	def load_widget(self, w=None, *args, **kwargs):
		stucked = self.ui.stackedWidget
		if stucked.count() > 0:
			stucked.currentWidget().close()
			stucked.removeWidget(stucked.currentWidget())

		if w is None:
			w = WIDGETS['all_diagnostics']

		if loaded_widgets[w[0]] is None:
			loaded_widgets[w[0]] = w[1](self, self.load_widget)

		loaded_widgets[w[0]].renew(*args, **kwargs)
		stucked.addWidget(loaded_widgets[w[0]])
		stucked.setCurrentWidget(loaded_widgets[w[0]])

	def close_all_widgets(self):
		for i in loaded_widgets:
			if loaded_widgets[i] is not None:
				loaded_widgets[i].close()

	def showEvent(self, *args, **kwargs):
		if self.is_initialize:
			return
		# if self.login_dialog.result() != 0:
		# 	return
		#
		# if self.login_dialog.res != 1:
		# 	return
		self.cur_complaints = None
		self.set_patient(None)

		if self.driver is None:
			self.driver = DriverThread(logger, main_window=self)
			self.driver.auto_init(True)
			self.driver.start()

		if self.server is None:
			self.server = IOSocketThread(port=5001, parent=self)
			self.server.start()

		if self.timer is None:
			self.timer = QtCore.QTimer()
			self.timer.setInterval(100)
			self.timer.timeout.connect(self.timer_tick)

			self.timer.start(100)
		if self.models_preloader is None:
			self.models_preloader = GLLoader(self)
			self.models_preloader.start()

		self.hand_dialog = HandDialog(preloader=self.models_preloader)

		self.w_dia_clicked(1, 2)

		if self.user is None:
			QtCore.QTimer.singleShot(10, self.after_show_event)

		self.is_initialize = True

		self.test_btns = BtnsEmuDialog(pp=self)
		self.test_btns.show()

	# self.log_dialog = LogDialog(self)
	# self.log_dialog.show()


	def after_show_event(self):
		self.setVisible(False)
		# self.hand_dialog.configure(preload=True)
		# self.hand_dialog.exec_()

		self.login_dialog = LoginDialog(self, autologin=autologin)
		self.login_dialog.exec_()

		if self.user is None:
			QtCore.QCoreApplication.quit()
		else:
			self.setVisible(True)
			self.ui.labelUserName.setText(unicode(self.user.login))
			self.load_widget(None)
			self.un_select_btns()
			self.ui.w_dia.setStyleSheet(SELECTED)
			self.raise_()

		return False

	def closeEvent(self, e, *args, **kwargs):
		if self.user is not None:
			mes = QtGui.QMessageBox.warning(
				self,
				tr("Выход"),
				tr("Вы действительно хотите выйти с программы ?"),
				QtGui.QMessageBox.Yes | QtGui.QMessageBox.Cancel
			)

			if mes != QtGui.QMessageBox.Yes:
				e.ignore()
				return

		self.close_all_widgets()

		self.driver.terminate()
		self.server.terminate()

		self.driver.join()

		self.server.join()
		self.timer.stop()

		if self.cur_session is not None:
			self.cur_session.save()

		self.driver = None
		self.server = None

		sys.exit(0)

	def end_session_click(self):
		if not self.try_end_session():
			return False

		self.load_widget(None)

	def timer_tick(self):
		if not self.point_settings.is_allow_measurement:
			return

		if len(self.driver.values) > 0:
			value = self.driver.values.pop()
			# self.server.send_msg(dict(value=value.get_value()))
			if value and value.get_value() is not None:
				self.server.push_data_value([value.get_value(), value.time])

				self.update_value.emit(value.get_value())

				if self.point_settings.is_show_only:
					return

				if not self.point_settings.is_start_measurement:
					if not self.point_settings.is_allow_next_measurement:
						if value.get_value() < 1:
							self.point_settings.is_allow_next_measurement = True

					if value.get_value() > 2 and self.point_settings.is_allow_next_measurement:
						if time.time() - self.point_settings.last_dia_time > 1.0:
							self.point_settings.is_start_measurement = True
							self.point_settings.last_dia_time = time.time()

							self.cur_session.cur_diagnostic.cur_node.cur_measurement = MeasurementArr()
							self.cur_session.cur_diagnostic.cur_node.cur_measurement.start_time = time.time()
							self.cur_session.cur_diagnostic.cur_node.cur_measurement.points.append(value.get_value())

				else:
					if time.time() - self.point_settings.last_dia_time > 60.0 or value.get_value() < 1:
						self.point_settings.is_start_measurement = False
						self.point_settings.is_allow_next_measurement = False

						self.cur_session.cur_diagnostic.cur_node.cur_measurement.end_time = \
							self.point_settings.last_dia_time = time.time()
						if self.cur_session.cur_diagnostic.cur_node.append_measurement():
							self.update_dia_list.emit(self.cur_session.cur_diagnostic.cur_node.get_cur_points()[-1])

					else:
						self.cur_session.cur_diagnostic.cur_node.cur_measurement.points.append(value.get_value())

	def save_diagnostic_click(self, widget):
		@try_end_dia(self)
		def wrapper(this=None):
			widget.before_end_diagnostic()
			self.load_widget()
			self.cur_session.append_current()
			self.start_new_diagnostic()
			self.load_widget()

		return wrapper

	def un_select_btns(self):
		self.ui.w_reports.setStyleSheet(NORMAL)
		self.ui.w_patients.setStyleSheet(NORMAL)
		self.ui.w_dia.setStyleSheet(NORMAL)
		self.ui.w_more.setStyleSheet(NORMAL)

	@try_end_dia()
	def w_dia_clicked(self, this, event):
		self.un_select_btns()
		self.ui.w_dia.setStyleSheet(SELECTED)
		self.load_widget(WIDGETS['all_diagnostics'])

	@try_end_dia()
	def w_patients_clicked(self, this, event):
		self.un_select_btns()
		self.ui.w_patients.setStyleSheet(SELECTED)
		self.load_widget(WIDGETS['patients'])

	@try_end_dia()
	def w_reports_clicked(self, this, event):
		self.un_select_btns()
		self.ui.w_reports.setStyleSheet(SELECTED)
		self.load_widget(WIDGETS['all_sessions'])

	@try_end_dia()
	def w_more_clicked(self, this, event):
		self.un_select_btns()
		self.ui.w_more.setStyleSheet(SELECTED)
		self.load_widget(WIDGETS['more_actions_view'])

	def log_out(self, this, event):
		if self.cur_session is not None:
			self.end_session_click()
		self.user = None
		self.setVisible(False)
		self.after_show_event()

	def try_end_session(self):
		if self.cur_session is None:
			return True

		mes = QtGui.QMessageBox.warning(
			self,
			tr(u"Cессия"),
			tr(u"Вы действительно хотите завершить сессию?"),
			QtGui.QMessageBox.Yes | QtGui.QMessageBox.No
		)
		if mes != QtGui.QMessageBox.Yes:
			return False

		self.cur_session.append_current()
		self.end_diagnostic()

		self.cur_session.save()
		self.cur_session = None
		self.cur_complaints = None
		self.cur_patient = None

		self.ui.widgetCurrentPatient.setVisible(False)
		self.ui.session_info.setVisible(False)

		return True

	def open_patient_info(self):
		self.patient_dialog = PatientInfoDialog(self)
		self.patient_dialog.exec_()

	def open_cur_session(self):
		self.cur_session_view_dialog = CurSessionViewDialog(self)
		self.cur_session_view_dialog.exec_()

	@QtCore.pyqtSlot(int)
	def device_status_slot(self, value):
		if value == 1:
			pixmap = QtGui.QPixmap(20, 20)
			pixmap.fill(QtGui.QColor("transparent"))
			painter = QtGui.QPainter(pixmap)
			painter.setBrush(QtGui.QBrush(QtCore.Qt.green))
			painter.drawEllipse(QtCore.QPoint(10, 10), 10, 10)
			painter.end()

			self.ui.status_label.setPixmap(pixmap)

		if value == 0:
			pixmap = QtGui.QPixmap(20, 20)
			pixmap.fill(QtGui.QColor("transparent"))
			painter = QtGui.QPainter(pixmap)
			painter.setBrush(QtGui.QBrush(QtCore.Qt.red))
			painter.drawEllipse(QtCore.QPoint(10, 10), 10, 10)
			painter.end()

			self.ui.status_label.setPixmap(pixmap)

	@QtCore.pyqtSlot(int)
	def device_btns_slot(self, value):
		def btn_pic(color=QtCore.Qt.red, text='1'):

			pict = QtGui.QPixmap(20, 20)
			pict.fill(QtGui.QColor("transparent"))
			painter = QtGui.QPainter(pict)
			painter.setBrush(QtGui.QBrush(color))
			painter.drawEllipse(QtCore.QPoint(10, 10), 10, 10)
			painter.setPen(QtCore.Qt.white)
			painter.drawText(QtCore.QPoint(7, 14), text)
			painter.end()

			return pict

		if value == 0:
			self.ui.status_btn1.setPixmap(btn_pic())
			self.ui.status_btn2.setPixmap(btn_pic(text='2'))

		elif value == 1:
			self.ui.status_btn1.setPixmap(btn_pic(QtCore.Qt.green))
			self.ui.status_btn2.setPixmap(btn_pic(text='2'))

		elif value == 2:
			self.ui.status_btn1.setPixmap(btn_pic())
			self.ui.status_btn2.setPixmap(btn_pic(QtCore.Qt.green, text='2'))
