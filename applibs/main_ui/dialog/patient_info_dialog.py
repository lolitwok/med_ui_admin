# -*- coding: utf-8 -*-
from datetime import date
from PIL.ImageQt import ImageQt
from PyQt4 import QtCore, QtGui
from applibs import _translate as tr
from applibs.main_ui.dialog.src.patient_info import Ui_Dialog


class PatientInfoDialog(QtGui.QDialog):
	def __init__(self, parent=None):
		super(PatientInfoDialog, self).__init__(parent)
		self.ui = Ui_Dialog()
		self.ui.setupUi(self)
		self.m_parent = parent

	def showEvent(self, *args, **kwargs):
		patient = self.m_parent.cur_patient

		img = self.m_parent.cur_patient.get_pic()
		img.thumbnail((200, 200))
		self.ui.labelPicture.setPixmap(QtGui.QPixmap.fromImage(ImageQt(img)))

		if patient.birthday_date:
			now = date.today()
			age = int((now - patient.birthday_date).days / 365.25)
			if age < 0:
				age = 0

			self.ui.labelYears.setText(unicode(tr('Возраст %s лет')) % unicode(age))
		else:
			self.ui.labelYears.setText(tr('Нет информациио возрасте'))

		self.ui.labelName.setText(patient.full_name)

		self.ui.labelLastSession.setText(patient.last_session_str())

		if self.m_parent.cur_session and self.m_parent.cur_session.complaints:
			self.ui.label_2.setText(unicode(self.m_parent.cur_session.complaints))
		else:
			self.ui.label_2.setText(unicode(tr("Жалоб нет.")))

		self.load_anamnes()

	def closeEvent(self, e):
		pass

	def load_anamnes(self):
		tw = self.ui.tableWidget
		tw.setRowCount(0)

		patient = self.m_parent.cur_patient

		for ind, item in enumerate(patient.anamnes_list):
			tw.insertRow(ind)
			for j in xrange(3):
				t = QtGui.QTableWidgetItem(unicode(item[j]))
				t.setFlags(t.flags() ^ QtCore.Qt.ItemIsEditable)
				tw.setItem(ind, j, t)
