# -*- coding: utf-8 -*-
from PyQt4.QtGui import QMessageBox
import datetime
from PyQt4 import QtCore, QtGui, Qt
from applibs import _translate as tr
from applibs.dao import cur_user_session
from applibs.main_ui.dialog.src.start_dialog import Ui_Dialog
from applibs.models.user_stuff.patient import Patient

from sqlalchemy import or_
from sqlalchemy import func
import types


class StartDialog(QtGui.QDialog):
	def __init__(self, parent=None, start_from=None):
		super(StartDialog, self).__init__(parent)

		self.ui = Ui_Dialog()
		self.ui.setupUi(self)
		self.resize(640, 480)
		self.setMaximumSize(QtCore.QSize(640, 480))
		self.m_parent = parent

		self.res = 0

		self.ui.pushButton_2.clicked.connect(self.select_exist_user)
		self.ui.pushButton.clicked.connect(self.select_anonimous_user)

		self.ui.pushButton_5.clicked.connect(self.select_dia_1)
		self.ui.pushButton_6.clicked.connect(self.select_dia_2)
		self.ui.pushButton_3.clicked.connect(self.select_dia_3)

		self.ui.btn_pat.clicked.connect(self.set_patients_view)

		self.ui.btn_dia.clicked.connect(self.set_dias_view)
		self.ui.btn_com.clicked.connect(self.set_complaints_view)

		self.ui.btn_compt.clicked.connect(self.set_complaints_view_step1)
		self.ui.btn_anam.clicked.connect(self.set_complaints_view_step2)

		self.ui.pushButton_8.clicked.connect(self.add_anamnes_row)
		self.ui.pushButton_9.clicked.connect(self.del_anamnes_row)

		self.ui.tableWidget.resizeColumnsToContents()
		self.ui.tableWidget.resizeRowsToContents()

		# self.ui.pushButton_13.clicked.connect(self.set_patients_view)
		self.ui.pushButton_12.clicked.connect(self.start)

		self.ui.pushButton_4.clicked.connect(self.btn_search_click)
		self.ui.lineEditSearch.keyPressEvent = types.MethodType(self.btn_search_kp, self.ui.lineEditSearch)

		self.set_complaints_view_step1()
		if self.m_parent.selected_diagnostic:
			self.dias_from_parent(self.m_parent.selected_diagnostic[0])
		self.select_exist_user()
		if self.m_parent.cur_patient is not None:
			self.refresh_patients(self.m_parent.cur_patient.id)
		else:
			self.refresh_patients()

		if start_from is None:
			if self.m_parent.cur_patient and self.m_parent.selected_diagnostic:
				start_from = 2
			elif self.m_parent.selected_diagnostic:
				start_from = 1
			else:
				start_from = 0

		if start_from == 0:
			self.set_dias_view()
		elif start_from == 1:
			self.set_patients_view()
		else:
			self.set_complaints_view()

	def dias_from_parent(self, sel):
		from applibs.main_ui.main_window import DIAGNOSTICS

		self.ui.pushButton_5.setEnabled(True)
		self.ui.pushButton_6.setEnabled(True)
		self.ui.pushButton_3.setEnabled(True)

		if sel:
			if sel == DIAGNOSTICS[1][0]:
				self.ui.pushButton_5.setDisabled(True)
			if sel == DIAGNOSTICS[2][0]:
				self.ui.pushButton_6.setDisabled(True)
			if sel == DIAGNOSTICS[3][0]:
				self.ui.pushButton_3.setDisabled(True)

	def select_dia_1(self):
		self.ui.pushButton_6.setEnabled(True)
		self.ui.pushButton_3.setEnabled(True)
		self.ui.pushButton_5.setDisabled(True)
		self.ui.listWidget.setDisabled(False)

	def select_dia_2(self):
		self.ui.pushButton_5.setEnabled(True)
		self.ui.pushButton_3.setEnabled(True)
		self.ui.pushButton_6.setDisabled(True)

	def select_dia_3(self):
		self.ui.pushButton_5.setEnabled(True)
		self.ui.pushButton_6.setEnabled(True)
		self.ui.pushButton_3.setDisabled(True)

	def select_exist_user(self):
		self.ui.lineEditSearch.setEnabled(True)
		self.ui.pushButton_4.setEnabled(True)
		self.ui.listWidget.setEnabled(True)

		self.ui.pushButton.setEnabled(True)
		self.ui.pushButton_2.setDisabled(True)

	def select_anonimous_user(self):
		self.ui.lineEditSearch.setDisabled(True)
		self.ui.pushButton_4.setDisabled(True)
		self.ui.listWidget.setDisabled(True)

		self.ui.pushButton.setDisabled(True)
		self.ui.pushButton_2.setEnabled(True)

	def showEvent(self, *args, **kwargs):
		pass

	def set_patients_view(self):
		self.ui.dias.hide()
		self.ui.complaints.hide()
		self.ui.patients.show()

	def set_complaints_view(self):
		self.ui.dias.hide()
		self.ui.patients.hide()
		self.ui.complaints.show()
		self.load_anamnes()

	def set_complaints_view_step1(self):
		self.ui.widget_8.hide()
		self.ui.widget_14.show()

	def set_complaints_view_step2(self):
		self.ui.widget_14.hide()
		self.ui.widget_8.show()

	def set_dias_view(self):
		self.ui.complaints.hide()
		self.ui.patients.hide()
		self.ui.dias.show()

	def closeEvent(self, e):
		pass

	def start(self):
		from applibs.main_ui.main_window import DIAGNOSTICS

		if self.ui.pushButton_5.isEnabled() and self.ui.pushButton_6.isEnabled() and self.ui.pushButton_3.isEnabled():
			QMessageBox.warning(
				self.m_parent,
				tr("Начало диагностики"),
				tr("Выберите диагностику"),
				QtGui.QMessageBox.Ok
			)
			self.set_dias_view()
			return

		sel_dia = None
		if not self.ui.pushButton_5.isEnabled():
			sel_dia = DIAGNOSTICS[1]

		if not self.ui.pushButton_6.isEnabled():
			sel_dia = DIAGNOSTICS[2]

		if not self.ui.pushButton_3.isEnabled():
			sel_dia = DIAGNOSTICS[3]

		if self.ui.pushButton_2.isEnabled():
			anonimous = Patient()
			anonimous.first_name = u"Анонимный Пациент"
			anonimous.last_name = u"%s" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
			anonimous.patient_type = -1
			anonimous.doctor = self.m_parent.user
			cur_user_session.add(anonimous)
			cur_user_session.commit()
			self.m_parent.cur_patient = anonimous

		if self.ui.pushButton.isEnabled():
			if len(self.ui.listWidget.selectedItems()) == 0:
				QMessageBox.warning(
					self.m_parent,
					tr("Начало диагностики"),
					tr("Выберите пацыетна"),
					QtGui.QMessageBox.Ok
				)
				self.set_patients_view()
				return

			patient = self.ui.listWidget.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()
			self.m_parent.cur_patient = patient
			self.m_parent.selected_diagnostic = sel_dia
			self.m_parent.cur_complaints = unicode(self.ui.textEdit.toPlainText())

		self.save_anamnes(self.m_parent.cur_patient)

		self.res = 1

		self.close()

	def refresh_patients(self, selected_patient_id=None, search=None):

		self.ui.listWidget.clear()
		patients_query = cur_user_session.query(Patient).filter(Patient.doctor_id == self.m_parent.user.id)

		if search is not None:
			search_words = search.strip().split(' ')
			if len(search_words) > 0:
				for i in search_words:
					query_list = []
					if len(i) > 1:
						query_list += [
							func.lower(Patient.first_name).like(func.lower(u"%{0}%".format(unicode(i)))),
							func.lower(Patient.last_name).like(func.lower(u"%{0}%".format(unicode(i)))),
							func.lower(Patient.middle_name).like(func.lower(u"%{0}%".format(unicode(i)))),
						]

					patients_query = patients_query.filter(or_(*query_list))

		patients = patients_query.all()

		for i in patients:
			t = QtGui.QListWidgetItem()
			t.setText(u"%s %s %s" % (
				unicode(i.first_name),
				unicode(i.last_name),
				unicode(i.middle_name),
			))
			t.setData(32, i)
			self.ui.listWidget.addItem(t)
			if selected_patient_id and selected_patient_id == i.id:
				self.ui.listWidget.setCurrentRow(self.ui.listWidget.count() - 1)

		if selected_patient_id is None:
			self.ui.listWidget.setCurrentRow(1)

	def btn_search_click(self):
		search = None
		if len(self.ui.lineEditSearch.text()) > 0:
			search = unicode(self.ui.lineEditSearch.text())

		self.refresh_patients(search=search)

	def btn_search_kp(self, e, event):
		if event.key() == QtCore.Qt.Key_Return:
			self.btn_search_click()
			return

		Qt.QLineEdit.keyPressEvent(e, event)

	def load_anamnes(self):
		tw = self.ui.tableWidget
		tw.setRowCount(0)
		if self.ui.pushButton_2.isEnabled():
			return

		if len(self.ui.listWidget.selectedItems()) == 0:
			return

		patient = self.ui.listWidget.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()

		for ind, item in enumerate(patient.anamnes_list):
			tw.insertRow(ind)
			for j in xrange(3):
				tw.setItem(ind, j, QtGui.QTableWidgetItem(unicode(item[j])))

	def save_anamnes(self, patient):
		tw = self.ui.tableWidget
		l = []
		for i in xrange(tw.rowCount()):
			if not (tw.item(i, 0).text() or tw.item(i, 1).text() or tw.item(i, 2).text()):
				continue

			it1 = tw.item(i, 0)
			it2 = tw.item(i, 1)
			it3 = tw.item(i, 2)

			l.append(
				(
					unicode(it1.text()),
					unicode(it2.text()),
					unicode(it3.text()),
				)
			)

		patient.set_anamnes(l)
		cur_user_session.add(patient)
		cur_user_session.commit()

	def add_anamnes_row(self):
		tw = self.ui.tableWidget
		pos = tw.rowCount()
		tw.insertRow(pos)
		tw.setItem(pos, 0, QtGui.QTableWidgetItem(""))
		tw.setItem(pos, 1, QtGui.QTableWidgetItem(""))
		tw.setItem(pos, 2, QtGui.QTableWidgetItem(""))

	def del_anamnes_row(self):
		tw = self.ui.tableWidget
		sel = tw.currentRow()

		tw.removeRow(sel)
