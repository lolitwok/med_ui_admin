# -*- coding: utf-8 -*-
from PyQt4 import QtGui
from applibs.main_ui.dialog.src.complaints_info import Ui_Dialog


class ComplaintsDialog(QtGui.QDialog):
	def __init__(self, parent=None, session=None):
		super(ComplaintsDialog, self).__init__(parent)
		self.ui = Ui_Dialog()
		self.ui.setupUi(self)
		self.resize(640, 480)
		self.m_parent = parent

		self.ui.complaints_label.setText(unicode(session.complaints[0].text))

	def showEvent(self, *args, **kwargs):
		pass

	def closeEvent(self, e):
		pass
