# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
from applibs.dao import cur_user_session
from applibs.main_ui.dialog.src.login_dialog import Ui_Dialog
from applibs.models.user_stuff.auth import User


class LoginDialog(QtGui.QDialog):
	def __init__(self, parent=None, start_from=None, autologin=False):
		super(LoginDialog, self).__init__(None)
		self.ui = Ui_Dialog()
		self.ui.setupUi(self)
		self.m_parent = parent
		self.a = autologin
		self.ui.label_3.setVisible(False)

		self.ui.login_btn.clicked.connect(self.login_clicked)

	def showEvent(self, *args, **kwargs):
		QtCore.QTimer.singleShot(20, self.foc)

	def foc(self):
		self.raise_()
		if self.a:
			self.ui.login.setText('adm')
			self.ui.password.setText('123')
			self.login_clicked()

	def closeEvent(self, e):
		if self.m_parent.user is None:
			QtCore.QCoreApplication.quit()

	def login_clicked(self):
		self.ui.label_3.setVisible(True)

		login = unicode(self.ui.login.text())
		user = cur_user_session.query(User).filter(User.login == login).first()
		if not user:
			self.ui.label_3.setVisible(True)
			return

		password = unicode(self.ui.password.text())
		if not user.check_password(password):
			self.ui.label_3.setVisible(True)
			return

		self.m_parent.user = user
		self.close()
