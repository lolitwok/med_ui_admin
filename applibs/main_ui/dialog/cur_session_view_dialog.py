# -*- coding: utf-8 -*-
import datetime
from applibs.main_ui import utils
from applibs.models.main_models import DNode
import copy
import os
from PyQt4 import QtCore, QtGui, Qt, uic
from applibs import BASE_DIR
from applibs.main_ui.dialog.src.cur_session_view_dialog import Ui_Form
from applibs.main_ui.utils import local_html, TREE_ITEMS_TYPE, WITH_NAZODS, middle_value
from applibs.models.user_stuff import  session_models
import json
import pdfkit


class CurSessionViewDialog(QtGui.QDialog):
	def __init__(self, parent=None, start_from=None):
		super(CurSessionViewDialog, self).__init__(parent)

		# self.ui = ui_widget()
		self.ui = Ui_Form()
		self.ui.setupUi(self)
		# self.resize(640, 480)
		# self.setMaximumSize(QtCore.QSize(640, 480))
		self.m_parent = parent

		self.ui.pushButton.clicked.connect(self.show_session)
		self.ui.treeWidget.itemChanged.connect(self.on_item_changed)
		self.ui.checkBox.stateChanged.connect(self.on_change_falling)

		self.session = None

		self.ui.webView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

		self.falling_graph_for_points = []

		self.m_parent.report = None
		if self.m_parent.cur_session is None:
			self.close()

		self.from_cur_session = copy.deepcopy(self.m_parent.cur_session)
		if self.from_cur_session.cur_diagnostic is not None:
			if self.from_cur_session.cur_diagnostic.cur_node is not None:
				self.from_cur_session.cur_diagnostic.append_node()

			self.from_cur_session.append_current()

	def showEvent(self, *args, **kwargs):
		self.refresh_list()
		self.ui.webView.load(Qt.QUrl(local_html('report.html')))

	# if not self.from_cur_session.conclusion:
	# 	self.ui.textEdit.setPlainText(unicode(u"Заключение програмы..."))
	#
	# else:
	# 	self.ui.textEdit.setPlainText(unicode(self.from_cur_session.conclusion))

	def closeEvent(self, e):
		pass

	def refresh_list(self):
		def add_item(p, title, udata=None, tristate=True):
			_item = QtGui.QTreeWidgetItem(p)
			_item.setText(0, title)
			_item.setToolTip(0, title)
			_item.setCheckState(0, QtCore.Qt.Checked)
			_item.setFlags(
				_item.flags() |
				QtCore.Qt.ItemIsUserCheckable |
				QtCore.Qt.ItemIsEnabled
			)
			if tristate:
				_item.setFlags(
					_item.flags() |
					QtCore.Qt.ItemIsTristate
				)

			_item.setData(0, QtCore.Qt.UserRole, udata)
			return _item

		def make_dia_item(parent, dia):
			# points -|:
			# 		  |-> falling
			for i in dia.nodes:
				_parent = add_item(parent, i.get_name(), i, tristate=False)
				_fbox = add_item(_parent, TREE_ITEMS_TYPE[6][1], 6)
				self.falling_graph_for_points.append(_fbox)
				if i.is_point_map():
					add_item(_parent, TREE_ITEMS_TYPE[7][1], 7)

			# graph1:
			add_item(parent, TREE_ITEMS_TYPE[4][1], 4)
			# graph2:
			add_item(parent, TREE_ITEMS_TYPE[5][1], 5)

		self.falling_graph_for_points = list()
		self.ui.treeWidget.clear()

		if self.from_cur_session.complaints:
			add_item(self.ui.treeWidget, TREE_ITEMS_TYPE[1][1], 1)

		for n in self.from_cur_session.dias_array:
			_p = add_item(self.ui.treeWidget, n.get_name(), n)
			make_dia_item(_p, n)
		#
		# if self.from_cur_session > 0:
		# 	add_item(self.ui.treeWidget, TREE_ITEMS_TYPE[3][1], 3)

	def get_kipspedo(self, _parent_dia):
		for i in _parent_dia.nodes:
			if i.for_node and isinstance(i.for_node, DNode) and i.for_node.is_main:
				return i.normal_points_middle
		return 0.0

	def show_session(self):
		def parse_point(point_child, _parent_dia, dia_type=None):
			_item = point_child.data(0, QtCore.Qt.UserRole).toPyObject()
			res = {
				"name": _item.get_name(),
				"value": _item.normal_points_middle,
				"delta": _item.normal_points_middle - self.get_kipspedo(_parent_dia),
				"description": _item.for_node.description if _item.for_node else "",
			}

			if dia_type == 4:
				res['z_value'] = _item.points_with_nazod_middle

			if dia_type == 5 and point_child.childCount() > 1 and point_child.child(1).data(0, QtCore.Qt.UserRole) == 7:
				if point_child.child(1).checkState(0) == QtCore.Qt.Checked:
					res['map_points'] = _item.get_map_data()

			if point_child.child(0).checkState(0) == QtCore.Qt.Checked:
				res['falling_graph'] = _item.get_best_ex()
				if dia_type == 4:
					res['z_falling_graph'] = _item.get_with_nazod_best_ex()

			main_parent = _item.for_node.get_main_parent.get_name() if _item.for_node else False

			return res, main_parent

		def parse_dia(dia_child):
			_dia = dia_child.data(0, QtCore.Qt.UserRole).toPyObject()

			res = {
				"categories": {},
				"name": _dia.get_name(),
				'dia_type': _dia.dia_type,
			}

			for _dc in xrange(dia_child.childCount()):
				if dia_child.child(_dc).checkState(0) == QtCore.Qt.Unchecked:
					continue

				_dia_child_item = dia_child.child(_dc).data(0, QtCore.Qt.UserRole).toPyObject()
				if isinstance(_dia_child_item, utils.NodeInfo):
					_cur_item, _main_name = parse_point(dia_child.child(_dc), _dia, dia_type=_dia.dia_type)
					if _main_name not in res['categories']:
						res["categories"][_main_name] = []

					res["categories"][_main_name].append(_cur_item)
					continue

				if isinstance(_dia_child_item, int):
					if _dia_child_item == 4:
						res['graph1'] = True

					if _dia_child_item == 5:
						res['graph2'] = True

					continue

			return res

		_out = dict()

		_out['dias'] = []

		_out['patient_info'] = {
			'all': unicode(u'%s %s %s' % (
				self.m_parent.cur_patient.first_name if self.m_parent.cur_patient.first_name else "",
				self.m_parent.cur_patient.last_name if self.m_parent.cur_patient.last_name else "",
				self.m_parent.cur_patient.middle_name if self.m_parent.cur_patient.middle_name else "",
			))
		}

		for _ti in xrange(self.ui.treeWidget.topLevelItemCount()):
			if self.ui.treeWidget.topLevelItem(_ti).checkState(0) == QtCore.Qt.Unchecked:
				continue

			_child_item = self.ui.treeWidget.topLevelItem(_ti).data(0, QtCore.Qt.UserRole).toPyObject()
			if isinstance(_child_item, utils.DiagnosticInfo):
				_out["dias"].append(parse_dia(self.ui.treeWidget.topLevelItem(_ti)))
				continue

			if isinstance(_child_item, int):
				if _child_item == 1:
					_out['complaints'] = self.from_cur_session.complaints[0].text \
						if self.from_cur_session.complaints \
						else ""

				# if _child_item == 2:
				# 	_out['program_recommendations'] = self.get_program_recommendations()
				#
				# if _child_item == 3:
				# 	_out['doctor_recommendations'] = self.session.conclusion

				continue

		self.m_parent.report = _out
		# time.sleep(50)
		self.ui.webView.reload()

	def on_item_changed(self, item):
		data = item.data(0, QtCore.Qt.UserRole).toPyObject()
		if isinstance(data, session_models.DiaPoint):
			if item.checkState(0) == QtCore.Qt.Unchecked:
				item.child(0).setCheckState(0, QtCore.Qt.Unchecked)

		if isinstance(data, int) and data == 6:
			if item.checkState(0) == QtCore.Qt.Checked:
				item.parent().setCheckState(0, QtCore.Qt.Checked)

			self.check_boxes()

	def set_state(self, state):
		self.ui.checkBox.blockSignals(True)
		self.ui.checkBox.setCheckState(state)
		self.ui.checkBox.blockSignals(False)

	def check_boxes(self):
		any_one_checked = False
		any_one_unchecked = False

		for i in self.falling_graph_for_points:
			if i.parent().checkState(0) == QtCore.Qt.Checked:
				if i.checkState(0) == QtCore.Qt.Checked:
					any_one_checked = True
				else:
					any_one_unchecked = True

		if any_one_checked and any_one_unchecked:
			self.set_state(QtCore.Qt.PartiallyChecked)
		elif any_one_checked:
			self.set_state(QtCore.Qt.Checked)
		else:
			self.set_state(QtCore.Qt.Unchecked)

	def on_change_falling(self, state):
		if state == QtCore.Qt.PartiallyChecked:
			self.ui.checkBox.setCheckState(QtCore.Qt.Checked)
			return

		if state == QtCore.Qt.Checked:
			for i in self.falling_graph_for_points:
				if i.parent().checkState(0) == QtCore.Qt.Checked:
					i.setCheckState(0, QtCore.Qt.Checked)
			return

		for i in self.falling_graph_for_points:
			if i.parent().checkState(0) == QtCore.Qt.Checked:
				i.setCheckState(0, QtCore.Qt.Unchecked)
