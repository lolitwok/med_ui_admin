# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
from applibs import _translate as tr
from applibs.ui.glwidget import glWidget
import time
from applibs.main_ui.dialog.src.hand_dialog import Ui_Dialog


class HandDialog(QtGui.QDialog):
	load_progress = QtCore.pyqtSignal(str)

	def __init__(self, parent=None, preloader=None):
		super(HandDialog, self).__init__(parent)
		self.ui = Ui_Dialog()
		self.ui.setupUi(self)

		self.m_parent = parent
		self.preloader = preloader
		self.gl_widget = None
		self.points = []
		self.model_file_name = None

	# self.load_progress.connect(self.set_progress)

	# self.gl_widget.setMinimumSize(200, 200)

	def get_gl(self):
		if not self.gl_widget:
			while not self.preloader.loaded:
				time.sleep(0.1)

			self.gl_widget = glWidget(self, self.preloader)
			sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
			sizePolicy.setHorizontalStretch(0)
			sizePolicy.setVerticalStretch(0)
			self.gl_widget.setSizePolicy(sizePolicy)
			self.ui.verticalLayout_4.addWidget(self.gl_widget)

		return self.gl_widget

	def showEvent(self, *args, **kwargs):
		self.setMaximumSize(16000, 16000)
		self.setMinimumSize(640, 480)
		self.setWindowTitle(tr("Модель руки"))
		self.ui.loading_label.setVisible(False)
		self.ui.widget.setVisible(True)

		if not self.gl_widget and not self.preloader.loaded:
			self.preloader.loaded_signal.connect(self.models_loaded)
			return

		self.models_loaded()

	def draw(self):
		if self.points:
			self.get_gl().points = list()
			self.get_gl().points = self.points

			self.get_gl().reinit(model_file=self.model_file_name, changeable=True, dev=False)

			if self.get_gl().initia:
				self.get_gl().show()
				self.get_gl().render_points()

				self.get_gl().updateGL()

	# self.gl_widget.reinit(model_file="hand.obj", point=None)

	def set_points(self, points=list()):
		self.points = []
		for point in points:
			self.points.append(point.to_gl_point_dict())

		if points:
			self.model_file_name = points[0].model.file_name

		if self.gl_widget:
			self.draw()

	def set_point_name(self, _str):
		if _str is None:
			self.ui.label_2.setVisible(False)
			self.ui.label_2.setText("")
			return

		self.ui.label_2.setVisible(True)
		self.ui.label_2.setText(_str)

	def closeEvent(self, e):
		pass

	def models_loaded(self):
		self.get_gl().reinit(dev=False, changeable=True)
		self.get_gl().show()
		self.get_gl().updateGL()

		self.draw()
