# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
from applibs.main_ui.dialog.src.nazod_storage import Ui_Dialog
import copy


class NazodStorageDialog(QtGui.QDialog):
	def __init__(self, parent=None):
		super(NazodStorageDialog, self).__init__(None)

		self.ui = Ui_Dialog()
		self.ui.setupUi(self)
		self.m_parent = parent
		self.ui.push_right.clicked.connect(self.right_button_Clicked)
		self.ui.selected_nazods_list.itemClicked.connect(self.selected_nazods_list_Clicked)
		self.ui.selected_nazods_list.itemDoubleClicked.connect(self.right_button_Clicked)
		self.ui.push_left.clicked.connect(self.left_button_Clicked)
		self.ui.all_nazods_list.itemDoubleClicked.connect(self.left_button_Clicked)

		self.ui.btn_ok.clicked.connect(self.btn_ok_clicked)
		self.ui.btn_cancel.clicked.connect(self.btn_cancel_clicked)

		self.setResult(0)

		self.selected_nazods = copy.deepcopy(self.m_parent.selected_nazods)

	def showEvent(self, *args, **kwargs):
		self.refresh_nazods_lists()

	def closeEvent(self, e):
		pass

	def right_button_Clicked(self):
		if len(self.ui.selected_nazods_list.selectedItems()) > 0:
			point = self.ui.selected_nazods_list.selectedItems()[0].data(32).toPyObject()
			for ind, item in enumerate(self.selected_nazods):
				if item[0].get_uuid() == point.get_uuid():
					del self.selected_nazods[ind]

			t = QtGui.QListWidgetItem()
			t.setText(point.get_name())
			t.setData(32, point)
			self.ui.all_nazods_list.addItem(t)

			self.ui.selected_nazods_list.takeItem(
				self.ui.selected_nazods_list.row(self.ui.selected_nazods_list.selectedItems()[0])
			)

			self.ui.selected_nazods_list.clearSelection()
			self.ui.selected_nazods_list.clearFocus()

	def left_button_Clicked(self):
		if len(self.ui.all_nazods_list.selectedItems()) > 0:
			point = self.ui.all_nazods_list.selectedItems()[0].data(32).toPyObject()
			self.selected_nazods.append([point, True])
			t = QtGui.QListWidgetItem()
			t.setFlags(t.flags() | QtCore.Qt.ItemIsUserCheckable)
			t.setCheckState(QtCore.Qt.Checked)
			t.setText(point.get_name())
			t.setData(32, point)
			self.ui.selected_nazods_list.addItem(t)

			self.ui.all_nazods_list.takeItem(
				self.ui.all_nazods_list.row(self.ui.all_nazods_list.selectedItems()[0])
			)

			self.ui.all_nazods_list.clearSelection()
			self.ui.all_nazods_list.clearFocus()

	def selected_nazods_list_Clicked(self, item):
		p = item.data(QtCore.Qt.UserRole).toPyObject()
		for i in self.selected_nazods:
			if i[0].get_uuid() == p.get_uuid():
				i[1] = True if item.checkState() == QtCore.Qt.Checked else False

	def refresh_nazods_lists(self):
		self.ui.selected_nazods_list.clear()
		self.ui.all_nazods_list.clear()
		#
		for i in self.selected_nazods:
			t = QtGui.QListWidgetItem()
			t.setFlags(t.flags() | QtCore.Qt.ItemIsUserCheckable)
			if i[1]:
				t.setCheckState(QtCore.Qt.Checked)
			else:
				t.setCheckState(QtCore.Qt.Unchecked)
			t.setText(i[0].get_name())
			t.setData(32, i[0])
			self.ui.selected_nazods_list.addItem(t)

		for i in self.m_parent.allowed_nazods:
			if i.get_uuid() not in [j[0].get_uuid() for j in self.selected_nazods]:
				t = QtGui.QListWidgetItem()
				t.setText(i.get_name())
				t.setData(32, i)
				self.ui.all_nazods_list.addItem(t)

	def btn_ok_clicked(self):
		self.setResult(1)
		self.m_parent.selected_nazods = self.selected_nazods
		self.close()

	def btn_cancel_clicked(self):
		self.setResult(0)
		self.close()
