# -*- coding: utf-8 -*-
import os

from PyQt4 import QtGui

from applibs.simple_logger import SLogger
from applibs.main_ui.widgets.src.more_menu_item import Ui_Form

base_path = os.path.dirname(os.path.abspath(__file__))
logger = SLogger.get_logger()


class MoreActionsWidget(QtGui.QWidget):
	def __init__(self, parent=None, load_method=None):
		super(MoreActionsWidget, self).__init__(parent)
		self.load_method = load_method
		self.ui = Ui_Form()
		self.ui.setupUi(self)
		self.m_parent = parent

		self.ui.users_control_btn.clicked.connect(self.users_control_btn_clicked)
		self.ui.nozods_control_btn.clicked.connect(self.nozods_control_btn_clicked)

	def renew(self):
		pass

	def showEvent(self, *args, **kwargs):
		self.ui.users_control_btn.setVisible(False)
		if self.m_parent.user.is_admin:
			self.ui.users_control_btn.setVisible(True)

	def closeEvent(self, e):
		pass

	def users_control_btn_clicked(self, e):
		from applibs.main_ui.main_window import WIDGETS
		self.load_method(WIDGETS['users_view'])

	def nozods_control_btn_clicked(self, e):
		from applibs.main_ui.main_window import WIDGETS
		self.load_method(WIDGETS['customer_nozods'])
