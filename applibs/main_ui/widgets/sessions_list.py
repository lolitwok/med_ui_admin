# -*- coding: utf-8 -*-
from applibs.dao import cur_user_session
from applibs.models.user_stuff.patient import PATIENT_TYPE, Patient
from applibs.models.user_stuff.session_models import DSession
import os
from datetime import date
from PyQt4 import QtCore, QtGui
from PIL.ImageQt import ImageQt, Image

from applibs import _translate as tr
from applibs.main_ui.widgets.items.sessions_list_item import SessionsListItemWidget
from applibs.simple_logger import SLogger
from applibs.main_ui.widgets.src.session_list import Ui_Form

base_path = os.path.dirname(os.path.abspath(__file__))
logger = SLogger.get_logger()


class SessionsListWidget(QtGui.QWidget):
	def __init__(self, parent=None, load_method=None):
		super(SessionsListWidget, self).__init__(parent)
		self.m_parent = parent
		self.load_method = load_method
		self.ui = Ui_Form()
		self.ui.setupUi(self)

		self.ui.verticalLayout_3.setAlignment(QtCore.Qt.AlignTop)
		self.ui.pushButton.clicked.connect(self.open_session_view)
		self.ui.user_info_btn.clicked.connect(self.open_user_info)

		self.selected_session = None

	def renew(self):
		self.selected_session = None

	def showEvent(self, *args, **kwargs):
		self.refresh_list()

	def closeEvent(self, e):
		pass

	def refresh_list(self):
		for i in reversed(range(self.ui.verticalLayout_3.count())):
			item = self.ui.verticalLayout_3.itemAt(i).widget()
			item.deleteLater()
			self.ui.verticalLayout_3.removeWidget(item)
			del item

		sessions = cur_user_session. \
			query(DSession, Patient). \
			join(Patient, Patient.id == DSession.patient_id). \
			filter(Patient.doctor_id == self.m_parent.user.id). \
			filter(DSession.date_end != None). \
			order_by(DSession.id.desc()).all()

		for i, j in sessions:
			sliw = SessionsListItemWidget(
				parent=self,
				item=i,
			)

			self.ui.verticalLayout_3.addWidget(sliw)
			sliw.set_selected(False)

		if self.ui.verticalLayout_3.count() > 0:
			item = self.ui.verticalLayout_3.itemAt(0).widget()
			item.set_selected(True)
			self.event_selected_item(item)

	def open_session_view(self):
		from applibs.main_ui.main_window import WIDGETS
		if self.selected_session is not None:
			self.load_method(WIDGETS['session_view'], item=self.selected_session)

	def event_selected_item(self, item):
		for i in reversed(range(self.ui.verticalLayout_3.count())):
			self.ui.verticalLayout_3.itemAt(i).widget().set_selected(False)

		item.set_selected(True)

		self.selected_session = item.item

		self.ui.label.setText(u"")
		img = self.selected_session.patient.get_pic()
		img.thumbnail((150, 150), Image.ANTIALIAS)
		self.ui.label.setPixmap(QtGui.QPixmap.fromImage(ImageQt(img)))

		self.ui.label_4.setText(PATIENT_TYPE[self.selected_session.patient.patient_type])

		if self.selected_session.patient.birthday_date:
			now = date.today()
			age = int((now - self.selected_session.patient.birthday_date).days / 365.25)
			if age < 0:
				age = 0

			self.ui.label_3.setText(u'Возраст %s лет' % unicode(age))
		else:
			self.ui.label_3.setText(u'Нет информациио возрасте')

		self.ui.label_2.setText(unicode(
			u"%s %s %s" % (
				unicode(self.selected_session.patient.first_name),
				unicode(self.selected_session.patient.last_name),
				unicode(self.selected_session.patient.middle_name),
			)
		))

		if self.selected_session.complaints:
			self.ui.label_6.setText(unicode(self.selected_session.complaints[0].text))

		else:
			self.ui.label_6.setText(unicode(tr(u'Жалобы не указаны')))

		self.set_dias_from_session()

	def set_dias_from_session(self):
		self.ui.listWidgetDiaItems.clear()

		for i in self.selected_session.diagnostics:
			item = QtGui.QListWidgetItem(i.get_name())
			item.setData(32, i)
			self.ui.listWidgetDiaItems.addItem(item)

	def open_user_info(self):
		if self.selected_session is None:
			return

		from applibs.main_ui.main_window import WIDGETS
		self.load_method(WIDGETS['patient_info'], patient=self.selected_session.patient, prev_ref='all_sessions')
