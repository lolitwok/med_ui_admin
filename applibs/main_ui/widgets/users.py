# -*- coding: utf-8 -*-
from applibs.dao import cur_user_session
import os

from PyQt4 import QtCore, QtGui

from applibs.models.user_stuff import auth
from applibs import _translate as tr
from applibs.simple_logger import SLogger
from applibs.main_ui.widgets.src.users_edit import Ui_Form

base_path = os.path.dirname(os.path.abspath(__file__))
logger = SLogger.get_logger()


class UsersListWidget(QtGui.QWidget):
	def __init__(self, parent=None, load_method=None):
		super(UsersListWidget, self).__init__(parent)
		self.load_method = load_method
		self.ui = Ui_Form()
		self.ui.setupUi(self)
		self.m_parent = parent
		self.cur_user = None

		self.ui.add_btn.clicked.connect(self.add_btn_clicked)
		self.ui.save_btn.clicked.connect(self.save_btn_clicked)
		self.ui.delete_btn.clicked.connect(self.delete_btn_clicked)

		self.ui.user_list.itemClicked.connect(self.user_list_itemClicked)

	def renew(self):
		pass

	def showEvent(self, *args, **kwargs):
		self.refresh_users_list()

	def closeEvent(self, e):
		pass

	def refresh_info(self):
		self.ui.password_edit.setText("")

		if self.cur_user:
			self.ui.user_id_label.setText(unicode(self.cur_user.id))
			self.ui.login_edit.setText(unicode(self.cur_user.login))
			return

		self.ui.user_id_label.setText('-1')
		self.ui.login_edit.setText('')

	def add_btn_clicked(self):
		self.cur_user = None
		self.refresh_info()

	def delete_btn_clicked(self):
		if not self.cur_user:
			return

		if self.cur_user.is_admin:
			QtGui.QMessageBox.critical(
				self,
				tr("Удаление"),
				tr("Нельзя удалить администратора"),
				QtGui.QMessageBox.Ok
			)
			return

		mb = QtGui.QMessageBox.warning(
			self,
			tr("Удаление"),
			tr("Вы действительно хотите удалить пользователя %s?" % self.cur_user.login),
			QtGui.QMessageBox.Yes | QtGui.QMessageBox.No
		)

		if mb != QtGui.QMessageBox.Yes:
			return

		cur_user_session.delete(self.cur_user)
		cur_user_session.commit()
		self.refresh_users_list()

	def save_btn_clicked(self):
		if self.cur_user:
			self.cur_user.login = unicode(self.ui.login_edit.text())
			if self.ui.password_edit.text():
				self.cur_user.set_password(unicode(self.ui.password_edit.text()))
		else:
			self.cur_user = auth.User(
				login=unicode(self.ui.login_edit.text()),
				password=unicode(self.ui.password_edit.text())
			)

		cur_user_session.add(self.cur_user)
		cur_user_session.commit()
		self.refresh_users_list(selected_id=self.cur_user.id)

	def user_list_itemClicked(self, item):
		if not self.ui.user_list.selectedItems():
			return

		selected_user = self.ui.user_list.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()

		self.cur_user = selected_user

		self.refresh_info()

	def refresh_users_list(self, selected_id=None):
		self.ui.user_list.clear()

		users = cur_user_session.query(auth.User).all()

		for i in users:
			t = QtGui.QListWidgetItem()
			t.setText("#%s %s" % (i.id, i.login))

			t.setData(32, i)
			if i.is_admin:
				t.setBackground(QtGui.QColor(246, 230, 230))

			self.ui.user_list.addItem(t)
			if selected_id and i.id == selected_id:
				self.ui.user_list.setItemSelected(t, True)
				self.user_list_itemClicked(None)

		if users and not selected_id:
			self.ui.user_list.setCurrentRow(0)
			self.user_list_itemClicked(None)
