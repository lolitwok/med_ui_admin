# -*- coding: utf-8 -*-
from applibs.dao import cur_main_session, cur_user_session
import os
import types

from PyQt4 import QtCore, QtGui, Qt

from sqlalchemy.orm.util import aliased
from sqlalchemy.sql.functions import count

from applibs.main_ui.widgets.src.nazodDiagnostics import Ui_Form
from applibs import _fromUtf8, _translate as tr

from applibs.main_ui.dialog.nazod_storage import NazodStorageDialog
from applibs.models.main_models import dnode, dpoint
from applibs.models.user_stuff.customer_staff import CustomerNozod
from applibs.main_ui.utils import DiagnosticInfo, NodeInfo, get_selected_nazodes
from applibs.main_ui.utils import local_html

base_path = os.path.dirname(os.path.abspath(__file__))
active_btn = """QPushButton,
QWidget#widget_nazod_measurement,
QWidget#widget_normal_measurement
{
background-color:rgba(0, 0, 0, 20);
color:#333;
border: 1px solid #dce1e4;
}

QPushButton:hover,
QWidget#widget_nazod_measurement:hover,
QWidget#widget_normal_measurement:hover
{
background-color:rgba(0, 0, 0, 20);
}"""
normal_btn = """QPushButton,
	QWidget#widget_nazod_measurement,
	QWidget#widget_normal_measurement {
	background-color:#fff;
	color:#333;
	border: 1px solid #dce1e4;
}

QPushButton:hover,
QWidget#widget_nazod_measurement:hover,
QWidget#widget_normal_measurement:hover{
background-color:rgba(0, 0, 0, 10);}"""


class NazodDiagnosticWidget(QtGui.QWidget):
	def __init__(self, parent=None, load_method=None):
		super(NazodDiagnosticWidget, self).__init__(parent)
		self.load_method = load_method
		self.ui = Ui_Form()
		self.ui.setupUi(self)

		self.m_parent = parent

		self.m_parent.device_btns.connect(self.slot_btn)
		self.ui.btn_open_hand.clicked.connect(self.open_hand)

		self.ui.btn_show_tab1.clicked.connect(self.show_tab1)
		self.ui.btn_show_tab2.clicked.connect(self.show_tab2)

		self.ui.btn_node_left.clicked.connect(self.node_left)
		self.ui.btn_node_right.clicked.connect(self.node_right)

		self.ui.list.itemClicked.connect(self.node_item_click)
		self.ui.list.doubleClicked.connect(self.node_item_next_click)

		self.ui.btnGoToTop.clicked.connect(self.btnGoToTop_click)
		self.ui.btnShowPrevLebel.clicked.connect(self.btnShowPrevLebel_click)

		self.ui.btnSearch.clicked.connect(self.btnSearch_click)

		self.ui.widget_nazod_measurement.mousePressEvent = types.MethodType(
			self.select_nazod_points,
			self.ui.widget_nazod_measurement
		)
		self.ui.widget_normal_measurement.mousePressEvent = types.MethodType(
			self.select_normal_points,
			self.ui.widget_normal_measurement
		)

		self.ui.btn_open_storage.clicked.connect(self.open_storage_click)
		self.ui.btn_select_point.clicked.connect(self.btn_select_point_clicked)
		self.ui.btn_select_storage.clicked.connect(self.btn_select_storage_clicked)

		self.ui.btnClearData.clicked.connect(self.btnClearData_click)

		self.ui.btn_end_dia.clicked.connect(self.m_parent.save_diagnostic_click(self))

		self.ui.lineEdit.keyPressEvent = types.MethodType(self.btnSearch_kp, self.ui.lineEdit)

		self.m_parent.update_value.connect(self.on_update_value)
		self.m_parent.update_dia_list.connect(self.on_update_dia_list)

		self.current_node = None

		self.selected_nazods = None

		self.allowed_nazods = []
		self.last_is_storage = False
		self.showed = False

	def slot_btn(self, _int):
		if self.ui.tab2.isVisible():
			if _int == 1:
				self.trigger_nazod_points()
			elif _int == 2:
				if not self.last_is_storage:
					self.btn_select_storage_clicked(ignore_messages=True)

				self.open_storage_click()

	def showEvent(self, *args, **kwargs):
		if not self.showed:
			self.ui.retranslateUi(self)
			self.showed = True
			self.refresh_nazod_list()

			self.selected_nazods = []
			self.allowed_nazods = []
			self.current_node = None
			self.last_is_storage = False
			self.btn_select_point_clicked(initial=True)
			self.select_normal_points()
			self.on_update_dia_list()

			self.show_tab2()

	def renew(self):

		self.m_parent.start_new_diagnostic()
		self.m_parent.cur_session.cur_diagnostic = DiagnosticInfo(dia_type=4)
		self.current_node = None

		self.ui.webGraphLiveValueGraph.load(Qt.QUrl(local_html('live-chart.html')))
		self.ui.webGraphLiveValueArrow.load(Qt.QUrl(local_html('ArrowGraph.html')))
		self.ui.web_points_graph.load(Qt.QUrl(local_html('nazodCompare.html')))

		all_points = cur_main_session.query(dpoint.DPoint) \
			.join(dnode.DNode, dpoint.DPoint.d_node_guid == dnode.DNode.guid) \
			.filter(dnode.DNode.d_item_guid == '06dba33b-94ad-466a-8cea-3e5578ca865d') \
			.all()

		self.m_parent.hand_dialog.set_points(all_points)
		self.m_parent.hand_dialog.set_point_name(None)
		self.showed = False

	def refresh_nazod_list(self, n_item_guid=None, search=None):
		if search is not None:
			search = unicode(search).lower()
		self.ui.list.clear()
		main_table = aliased(dnode.DNode)
		second_table = aliased(dnode.DNode)
		instruments = []
		nodes = []
		if n_item_guid is not False:
			nodes_query = cur_main_session.query(main_table, count(second_table.id)) \
				.outerjoin(second_table, main_table.guid == second_table.parrent_guid) \
				.filter(main_table.d_item_guid == 'b2b4dcb6-f5ea-4101-a7d8-7ae122c64171') \
				.group_by(main_table.id)

			if search is None:
				nodes_query = nodes_query.filter(main_table.parrent_guid == n_item_guid)

			if search:
				nodes_query = nodes_query.filter(main_table.name_lower.like(u"%{0}%".format(search)))

			nodes = nodes_query.all()

		if n_item_guid is False:
			instruments = cur_user_session.query(CustomerNozod) \
				.filter(CustomerNozod.user == self.m_parent.user) \
				.all()

		if search:
			instruments = cur_user_session.query(CustomerNozod).filter() \
				.filter(CustomerNozod.user == self.m_parent.user) \
				.filter(CustomerNozod.name_lower.like(u"%{0}%".format(search))) \
				.all()

		for i, _count in nodes:
			t = QtGui.QListWidgetItem()
			t.setText(i.get_name())
			t.setData(32, i)
			if _count:
				t.setBackground(QtGui.QColor(246, 230, 230))

			self.ui.list.addItem(t)

		for i in instruments:
			t = QtGui.QListWidgetItem()
			t.setText(i.get_name())
			t.setData(32, i)
			self.ui.list.addItem(t)

		if n_item_guid is None and search is None and cur_user_session.query(CustomerNozod) \
				.filter(CustomerNozod.user == self.m_parent.user).count():
			t = QtGui.QListWidgetItem()
			t.setText(tr(u'Препараты'))
			t.setData(32, 1)
			t.setBackground(QtGui.QColor(246, 230, 230))
			self.ui.list.addItem(t)

		if self.ui.list.count():
			self.ui.list.setCurrentRow(1)

	def closeEvent(self, e):
		self.ui.webGraphLiveValueGraph.load(Qt.QUrl("about:blank"))
		self.ui.webViewRadar.load(Qt.QUrl("about:blank"))
		self.ui.web_tab2_2.load(Qt.QUrl("about:blank"))
		self.ui.webGraphLiveValueArrow.load(Qt.QUrl("about:blank"))
		self.ui.web_points_graph.load(Qt.QUrl("about:blank"))

	def before_end_diagnostic(self):
		self.save_cur_node()

	def on_update_value(self, value):
		pass

	def on_update_dia_list(self):
		if self.m_parent.cur_session.cur_diagnostic.cur_node:
			self.ui.label_normal_measurement_value.setText(
				unicode("(%0.0f)" % round(self.m_parent.cur_session.cur_diagnostic.cur_node.normal_points_middle))
			)
			self.ui.label_nazod_measurement_value.setText(
				unicode("(%0.0f)" % round(self.m_parent.cur_session.cur_diagnostic.cur_node.points_with_nazod_middle))
			)

	def node_item_click(self, item):
		if len(self.ui.list.selectedItems()) == 0:
			return

		n_item = self.ui.list.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()
		if n_item == 1 or (isinstance(n_item, dnode.DNode) and not n_item.usable_us_dia):
			return

		if self.current_node and n_item.get_uuid() == self.current_node.get_uuid():
			return

		self.save_cur_node()
		self.ui.label_3.setText(n_item.get_name())

		self.current_node = n_item
		self.check_selected_target()

	def node_item_next_click(self, n_item):
		if len(self.ui.list.selectedItems()) == 0:
			return

		n_item = self.ui.list.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()
		if n_item == 1:
			return self.refresh_nazod_list(n_item_guid=False)

		items_count = cur_main_session.query(dnode.DNode) \
			.filter(dnode.DNode.parrent_guid == n_item.guid) \
			.count()

		if items_count == 0:
			return

		self.refresh_nazod_list(n_item.guid)

	def btnGoToTop_click(self):
		self.refresh_nazod_list()

	def btnShowPrevLebel_click(self):
		if self.ui.list.count() > 0:
			prev_item_id = self.ui.list.item(0).data(QtCore.Qt.UserRole).toPyObject().parrent_guid
			if prev_item_id is None:
				return

			if prev_item_id is False:
				self.refresh_nazod_list(n_item_guid=None)
				return
			prev_item = cur_main_session.query(dnode.DNode).filter(dnode.DNode.guid == prev_item_id).first()
			self.refresh_nazod_list(n_item_guid=prev_item.parrent_guid)

	def btnSearch_click(self):
		search = None
		if len(self.ui.lineEdit.text()) > 0:
			search = self.ui.lineEdit.text()

		self.refresh_nazod_list(search=search)

	def btnSearch_kp(self, e, event):
		if event.key() == QtCore.Qt.Key_Return:
			self.btnSearch_click()
			return

		Qt.QLineEdit.keyPressEvent(e, event)

	def save_cur_node(self):
		if self.m_parent.cur_session.cur_diagnostic.cur_node is not None and \
				self.m_parent.cur_session.cur_diagnostic.cur_node.normal_points:

			_node = self.m_parent.cur_session.cur_diagnostic.cur_node.for_node
			if _node is not None and _node.get_uuid() not in [i.get_uuid() for i in self.allowed_nazods]:
				self.allowed_nazods.append(self.m_parent.cur_session.cur_diagnostic.cur_node.for_node)

		self.m_parent.cur_session.cur_diagnostic.append_node()
		self.m_parent.point_settings.is_show_only = True

	# nazods select

	def trigger_nazod_points(self):
		if self.m_parent.cur_session.cur_diagnostic.cur_node:
			if self.m_parent.cur_session.cur_diagnostic.cur_node.nazod_trigger:
				self.select_normal_points()
			else:
				self.select_nazod_points()

	def select_normal_points(self, *args):
		self.ui.widget_normal_measurement.setStyleSheet(_fromUtf8(active_btn))
		self.ui.widget_nazod_measurement.setStyleSheet(_fromUtf8(normal_btn))
		if self.m_parent.cur_session.cur_diagnostic.cur_node:
			self.m_parent.cur_session.cur_diagnostic.cur_node.nazod_trigger = False
		self.on_update_dia_list()

	def select_nazod_points(self, *args):
		self.ui.widget_normal_measurement.setStyleSheet(_fromUtf8(normal_btn))
		self.ui.widget_nazod_measurement.setStyleSheet(_fromUtf8(active_btn))
		if self.m_parent.cur_session.cur_diagnostic.cur_node:
			self.m_parent.cur_session.cur_diagnostic.cur_node.nazod_trigger = True
		self.on_update_dia_list()

	def btnClearData_click(self):
		if self.m_parent.cur_session.cur_diagnostic.cur_node:
			self.m_parent.cur_session.cur_diagnostic.cur_node.clear_cur_points()
			self.on_update_dia_list()

	def set_node(self):
		if self.current_node is None and not self.last_is_storage:
			return False

		old_node = self.m_parent.cur_session.cur_diagnostic.get_node_by_id(
			None,
			nazods=[i[0] for i in get_selected_nazodes(self.selected_nazods)],
		) if self.last_is_storage else \
			self.m_parent.cur_session.cur_diagnostic.get_node_by_id(self.current_node.get_uuid())

		if old_node is False:
			self.m_parent.cur_session.cur_diagnostic.cur_node = NodeInfo(
				node=None,
				nazods=[i[0] for i in get_selected_nazodes(self.selected_nazods)],
			) if self.last_is_storage else \
				NodeInfo(node=self.current_node)

		else:
			self.m_parent.cur_session.cur_diagnostic.cur_node = old_node

		self.ui.label_2.setText(self.m_parent.cur_session.cur_diagnostic.cur_node.get_name())

		self.m_parent.point_settings.is_show_only = False
		self.m_parent.point_settings.is_allow_measurement = True
		self.select_normal_points()

		if not self.last_is_storage:
			ind = self.ui.list.currentRow()
			before = False
			after = False

			for i in xrange(ind - 1, -1, -1):

				b = self.ui.list.item(i).data(QtCore.Qt.UserRole).toPyObject()
				if b.usable_us_dia:
					before = i
					break

			for i in xrange(ind + 1, self.ui.list.count(), 1):
				b = self.ui.list.item(i).data(QtCore.Qt.UserRole).toPyObject()
				if b.usable_us_dia:
					after = i
					break

			if before is False:
				self.ui.btn_node_left.setDisabled(True)
			else:
				self.ui.btn_node_left.setEnabled(True)

			if after is False:
				self.ui.btn_node_right.setDisabled(True)
			else:
				self.ui.btn_node_right.setEnabled(True)
		else:

			self.ui.btn_node_right.setDisabled(True)
			self.ui.btn_node_left.setDisabled(True)

		self.m_parent.point_settings.is_allow_measurement = True
		return True

	def open_hand(self):
		if not self.m_parent.hand_dialog.isVisible():
			self.m_parent.hand_dialog.show()
		else:
			self.m_parent.hand_dialog.raise_()

	def show_tab1(self):
		self.m_parent.point_settings.is_allow_measurement = False

		self.save_cur_node()

		self.ui.webViewRadar.load(Qt.QUrl(local_html('radar.html')))

		self.ui.webGraphLiveValueGraph.load(Qt.QUrl("about:blank"))
		self.ui.web_tab2_2.load(Qt.QUrl("about:blank"))
		self.ui.webGraphLiveValueArrow.load(Qt.QUrl("about:blank"))
		self.ui.web_points_graph.load(Qt.QUrl("about:blank"))

		self.ui.tab2.setVisible(False)

		self.ui.tab1.setVisible(True)
		self.ui.top_btns.setVisible(False)

		self.ui.btn_show_tab2.setEnabled(True)

		self.ui.btn_show_tab1.setDisabled(True)
		self.refresh_cur_dia_list()

	def show_tab2(self):

		self.ui.webGraphLiveValueGraph.load(Qt.QUrl(local_html('live-chart.html')))
		self.ui.webGraphLiveValueArrow.load(Qt.QUrl(local_html('ArrowGraph.html')))
		self.ui.web_tab2_2.load(Qt.QUrl(local_html('falling.html')))
		self.ui.web_points_graph.load(Qt.QUrl(local_html('nazodCompare.html')))
		self.ui.webViewRadar.load(Qt.QUrl("about:blank"))

		self.ui.tab2.setVisible(True)
		self.ui.tab1.setVisible(False)
		self.ui.top_btns.setVisible(True)

		self.ui.btn_show_tab1.setEnabled(True)
		self.ui.btn_show_tab2.setDisabled(True)
		self.m_parent.point_settings.is_show_only = True
		self.m_parent.point_settings.is_allow_measurement = True

		if self.set_node() is False:
			return

	def node_left(self):
		if self.last_is_storage:
			return

		ind = self.ui.list.currentRow()
		before = False

		for i in xrange(ind - 1, -1, -1):
			b = self.ui.list.item(i).data(QtCore.Qt.UserRole).toPyObject()
			if b.usable_us_dia:
				before = i
				break

		if before is False:
			return

		self.m_parent.cur_session.cur_diagnostic.append_node()
		self.ui.list.setCurrentRow(before)
		self.node_item_click(None)

		self.set_node()

	def node_right(self):
		if self.last_is_storage:
			return

		ind = self.ui.list.currentRow()
		after = False

		for i in xrange(ind + 1, self.ui.list.count(), 1):
			b = self.ui.list.item(i).data(QtCore.Qt.UserRole).toPyObject()
			if isinstance(b, dnode.DNode) and b.usable_us_dia:
				after = i
				break

		if after is False:
			return

		self.m_parent.cur_session.cur_diagnostic.append_node()
		self.ui.list.setCurrentRow(after)
		self.node_item_click(None)
		self.set_node()

	def resizeEvent(self, QResizeEvent):
		pass

	def refresh_cur_dia_list(self):
		self.ui.all_mea_nodes.setUpdatesEnabled(False)
		self.ui.all_mea_nodes.clearContents()
		self.ui.all_mea_nodes.setRowCount(len(self.m_parent.cur_session.cur_diagnostic.nodes))

		for index, item in enumerate(self.m_parent.cur_session.cur_diagnostic.nodes):
			t1 = QtGui.QTableWidgetItem(item.get_name())
			t1.setFlags(QtCore.Qt.ItemIsEnabled)

			normal = item.normal_points_middle if len(item.normal_points) > 0 else None
			t2 = QtGui.QTableWidgetItem(unicode(normal if normal is not None else '-'))
			t2.setFlags(QtCore.Qt.ItemIsEnabled)

			with_nazod = item.points_with_nazod_middle if len(item.points_with_nazod) > 0 else None
			t3 = QtGui.QTableWidgetItem(unicode(with_nazod if with_nazod is not None else '-'))
			t3.setFlags(QtCore.Qt.ItemIsEnabled)

			dif = abs(with_nazod - normal) if with_nazod is not None and normal is not None else "-"
			t4 = QtGui.QTableWidgetItem(unicode(dif))
			t4.setFlags(QtCore.Qt.ItemIsEnabled)

			self.ui.all_mea_nodes.setItem(index, 0, t1)
			self.ui.all_mea_nodes.setItem(index, 1, t2)
			self.ui.all_mea_nodes.setItem(index, 2, t3)
			self.ui.all_mea_nodes.setItem(index, 3, t4)

		self.ui.all_mea_nodes.resizeColumnsToContents()
		self.ui.all_mea_nodes.resizeRowsToContents()
		self.ui.all_mea_nodes.sortItems(0, QtCore.Qt.DescendingOrder)
		self.ui.all_mea_nodes.setUpdatesEnabled(True)

	def open_storage_click(self):
		self.nazod_storage = NazodStorageDialog(self)
		an_nazodes = []
		self.allowed_nazods = []

		for i in self.m_parent.cur_session.cur_diagnostic.nodes:
			if i.for_node is None:
				continue

			if i.for_node.get_uuid() not in an_nazodes:
				an_nazodes.append(i.for_node.get_uuid())
				self.allowed_nazods.append(i.for_node)

		for ind, i in reversed(list(enumerate(self.selected_nazods))):
			if i[0].get_uuid() not in an_nazodes:
				del self.selected_nazods[ind]

		self.save_cur_node()
		res = self.nazod_storage.exec_()


		if res == 1:
			self.check_selected_target()
		else:
			self.check_selected_target(True)
			# self.set_node()

	def check_selected_target(self, ignore_messages=False):
		self.m_parent.point_settings.is_show_only = True

		warning = QtGui.QMessageBox.warning

		if ignore_messages:
			warning = lambda *args, **kwargs: None

		if self.last_is_storage and len(get_selected_nazodes(self.selected_nazods)) < 2:
			warning(
				self.m_parent,
				tr("Накопитель назодов"),
				tr("Выберите больше одного назода в накопителе"),
				QtGui.QMessageBox.Ok
			)
			return
		else:

			if self.current_node is None:
				warning(
					self.m_parent,
					tr("Выберите точку"),
					tr("Выберите точку!"),
					QtGui.QMessageBox.Ok
				)
				return

			if not self.current_node.usable_us_dia:
				warning(
					self.m_parent,
					tr("Выберите точку"),
					tr("Выберите точку на которой позволено проводить измирение!"),
					QtGui.QMessageBox.Ok
				)
				return

		self.set_node()

	def btn_select_point_clicked(self, event=None, initial=False):
		if not self.last_is_storage and not initial:
			return

		self.save_cur_node()
		self.last_is_storage = False
		self.ui.widget_points.setEnabled(True)
		self.ui.widget_storage.setVisible(False)
		self.ui.btn_select_point.setStyleSheet(_fromUtf8(active_btn))
		self.ui.btn_select_storage.setStyleSheet(_fromUtf8(normal_btn))
		if initial: return
		self.check_selected_target()

	def btn_select_storage_clicked(self, event=None, ignore_messages=False):
		if self.last_is_storage:
			return

		self.save_cur_node()
		self.last_is_storage = True
		self.ui.widget_points.setDisabled(True)
		self.ui.widget_storage.setVisible(True)
		self.check_selected_target(ignore_messages=ignore_messages)
		self.ui.btn_select_point.setStyleSheet(_fromUtf8(normal_btn))
		self.ui.btn_select_storage.setStyleSheet(_fromUtf8(active_btn))
