# -*- coding: utf-8 -*-
from applibs import _fromUtf8
from PyQt4 import QtGui
from applibs.main_ui.widgets.src.sessionListItem import Ui_Form

ITEM_ACTIVE_STYLES = """
#main_widget {
	background-color:rgba(0, 0, 0, 20);
	color:#333;
	border: 1px solid #dce1e4;
}

#main_widget:hover {
	background-color:rgba(0, 0, 0, 20);
}"""

ITEM_NORMAL_STYLES = """
#main_widget {
	background-color:#fff;
	color:#333;
	border: 1px solid #dce1e4;
}

#main_widget:hover {	background-color:rgba(0, 0, 0, 10);}"""


class SessionsListItemWidget(QtGui.QWidget):
	def __init__(self, parent=None, item=None):
		super(SessionsListItemWidget, self).__init__(parent)
		self.ui = Ui_Form()
		self.m_parent = parent

		self.ui.setupUi(self)
		self.item = item

		self.ui.label.setText(unicode(" ".join([item.patient.first_name, item.patient.last_name])))
		self.ui.label_2.setText(unicode(item.get_name()))

	def mousePressEvent(self, event):
		self.m_parent.event_selected_item(self)

	def set_selected(self, selected=True):
		style = ITEM_ACTIVE_STYLES if selected else ITEM_NORMAL_STYLES
		self.ui.main_widget.setStyleSheet(_fromUtf8(style))
