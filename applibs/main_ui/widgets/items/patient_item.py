# -*- coding: utf-8 -*-
from applibs import _fromUtf8
from PyQt4 import QtGui
import types
from applibs.main_ui.widgets.src.itemPatientList import Ui_Form

ITEM_ACTIVE_STYLES = """
#main_widget {
	background-color:rgba(0, 0, 0, 20);
	color:#333;
	border: 1px solid #dce1e4;
}

#main_widget:hover {
	background-color:rgba(0, 0, 0, 20);
}"""

ITEM_NORMAL_STYLES = """
#main_widget {
	background-color:#fff;
	color:#333;
	border: 1px solid #dce1e4;
}

#main_widget:hover {	background-color:rgba(0, 0, 0, 10);}"""


class PatientListItemWidget(QtGui.QWidget):
	def __init__(
			self,
			parent=None,
			on_click_item_method=None,
			on_dblclick_item_method=None,
			item=None
	):
		super(PatientListItemWidget, self).__init__(parent)
		self.ui = Ui_Form()
		self.ui.setupUi(self)
		self.item = item
		self.m_parent = parent

		self.on_click_item_method = on_click_item_method
		self.on_dblclick_item_method = on_dblclick_item_method

		self.ui.labelName.setText(unicode(
			u"%s %s %s" % (
				unicode(item.first_name),
				unicode(item.last_name),
				unicode(item.middle_name),
			)
		))

		self.ui.main_widget.mousePressEvent = types.MethodType(self.item_click, self.ui.main_widget)
		self.ui.main_widget.mouseDoubleClickEvent = types.MethodType(self.item_dbl_click, self.ui.main_widget)

	def item_dbl_click(self, e=None, ev=None):
		self.on_dblclick_item_method(self)

	def item_click(self, e=None, ev=None):
		self.on_click_item_method(self)
		for i in reversed(range(self.m_parent.ui.patients_list.count())):
			_t = self.m_parent.ui.patients_list.itemAt(i).widget()
			try:
				_t.set_selected(False)
			except:
				pass

		self.set_selected(True)

	def set_selected(self, selected=True):
		style = ITEM_ACTIVE_STYLES if selected else ITEM_NORMAL_STYLES
		self.ui.main_widget.setStyleSheet(_fromUtf8(style))
