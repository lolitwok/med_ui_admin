# -*- coding: utf-8 -*-
from applibs import _translate as tr
from PyQt4 import QtGui
from applibs.main_ui.widgets.src.userFileItem import Ui_Form


class UserFileItemWidget(QtGui.QWidget):
	def __init__(
			self,
			parent=None,
			on_btn1_click=None,
			on_btn2_click=None,
			item=None
	):
		super(UserFileItemWidget, self).__init__(parent)
		self.ui = Ui_Form()
		self.ui.setupUi(self)
		self.item = item

		self.on_btn1_click = on_btn1_click
		self.on_btn2_click = on_btn2_click

		self.ui.open_in_folder_btn.clicked.connect(self.on_btn1_clicked)
		self.ui.gen_open_btn.clicked.connect(self.on_btn2_clicked)
		self.refresh()

	def refresh(self):
		self.ui.filename_label.setText(unicode(self.item.file_name))
		self.ui.session_label.setText(unicode(self.item.session.get_name()))
		self.ui.date_label.setText(unicode(self.item.date.strftime('%d-%m-%Y %H:%M')))

		if self.item.is_exists():
			self.ui.open_in_folder_btn.setVisible(True)
			self.ui.label_4.setVisible(False)
			self.ui.gen_open_btn.setText(tr(u'Открыть'))
		else:
			self.ui.open_in_folder_btn.setVisible(False)
			self.ui.label_4.setVisible(True)
			self.ui.gen_open_btn.setText(tr(u'Сгенерировать'))

	def on_btn1_clicked(self):
		if self.on_btn1_click:
			self.on_btn1_click(self)

	def on_btn2_clicked(self):
		if self.on_btn2_click:
			self.on_btn2_click(self)
