# -*- coding: utf-8 -*-
import cStringIO
from applibs.dao import cur_main_session
from applibs.models.main_models import dnode, dimage
from PyQt4 import QtGui
from PIL import Image

from applibs.main_ui.widgets.src.nazod_list_item import Ui_FormEl


class NQLabel(QtGui.QLabel):
	def __init__(self, firstLabel, f=None):
		QtGui.QLabel.__init__(self, firstLabel)
		self.f = f

	def mousePressEvent(self, event):
		self.f()


class NazodListItemWidget(QtGui.QWidget):
	def __init__(
			self,
			parent=None,
			on_click_item_method=None,
			on_click_next_method=None,
			item=None
	):
		super(NazodListItemWidget, self).__init__(parent)
		self.on_click_item_method = on_click_item_method
		self.on_click_next_method = on_click_next_method

		self.ui = Ui_FormEl()
		self.ui.setupUi(self)
		self.ui.labelForNazod.setText(item.get_name())
		self.ui.btnForNazodInfo.clicked.connect(self.btnForNazodNext_click)
		self.node_id = item.id
		self.item = item

		self.ui.pictForNazod.clear()
		data = cur_main_session.query(dimage.DImage).filter(dimage.DImage.d_node_id == self.item.id).first()
		if data:
			as_file = cStringIO.StringIO(data.image)

			im = Image.open(as_file)

			im.thumbnail((30, 30))
			if im.mode == "RGB":
				pass
			elif im.mode == "L":
				im = im.convert("RGBA")
			data = im.convert("RGBA").tobytes("raw", "RGBA")

			qim = QtGui.QImage(data, im.size[0], im.size[1], QtGui.QImage.Format_ARGB32)

			pixmap = QtGui.QPixmap.fromImage(qim)
			pixmap.loadFromData(as_file.read())
			self.ui.pictForNazod.setPixmap(pixmap)

		items_count = cur_main_session.query(dnode.DNode) \
			.filter(dnode.DNode.parrent_id == item.id) \
			.count()

		if items_count == 0:
			self.ui.btnForNazodInfo.setParent(None)

	def btnForNazodNext_click(self):
		self.on_click_next_method(self)

	def showEvent(self, *args, **kwargs):
		pass

	def closeEvent(self, *args, **kwargs):
		# sys.exit(0)
		pass

	def mousePressEvent(self, event):
		self.on_click_item_method(self)

	def renew(self):
		pass
