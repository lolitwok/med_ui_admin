# -*- coding: utf-8 -*-
from applibs import _fromUtf8
from PyQt4 import QtGui
import types
from applibs import _translate as tr
from applibs.main_ui.widgets.src.item_current_delete import Ui_Form

ITEM_ACTIVE_STYLES = """
#main_widget {
background-color: #fff;
padding: 0 5px;
	border-top: 1px solid #ccd0d1;
	border-bottom: 1px solid #ccd0d1;
}

#main_widget:hover {
	background-color:rgba(0, 0, 0, 20);
}"""

ITEM_NORMAL_STYLES = """
#main_widget {
background-color: #e7ebec;
padding: 0 5px;
	border-top: 1px solid #ccd0d1;
	border-bottom: 1px solid #ccd0d1;
}

#main_widget:hover {	background-color:rgba(0, 0, 0, 10);}"""


class SurveyListItemWidget(QtGui.QWidget):
	def __init__(
			self,
			parent=None,
			on_click_item_method=None,
			on_click_delete_method=None,
			item=None
	):
		super(SurveyListItemWidget, self).__init__(parent)
		self.ui = Ui_Form()
		self.ui.setupUi(self)
		self.item = item

		self.on_click_item_method = on_click_item_method
		self.on_click_delete_method = on_click_delete_method

		if isinstance(item[1], int):
			self.ui.labelItemName.setText(u'%s%s (%s)' % (tr('Измерение №'), item[0], unicode(item[1])))
		else:
			self.ui.labelItemName.setText(item[1].name)

		self.ui.main_widget.mousePressEvent = types.MethodType(self.item_click, self.ui.main_widget)
		self.ui.btnDeleteCurrentItem.clicked.connect(self.btn_delete_clicked)

	def btn_delete_clicked(self):
		self.on_click_delete_method(self)

	def item_click(self, e=None, ev=None):
		if self.on_click_item_method:
			self.on_click_item_method(self)

	def set_selected(self, selected=True):
		style = ITEM_ACTIVE_STYLES if selected else ITEM_NORMAL_STYLES
		self.ui.main_widget.setStyleSheet(_fromUtf8(style))
