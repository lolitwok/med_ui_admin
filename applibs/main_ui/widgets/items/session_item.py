# -*- coding: utf-8 -*-
from PyQt4 import QtGui
from applibs.main_ui.widgets.src.sessionItem import Ui_Form


class PatientSessionListItemWidget(QtGui.QWidget):
	def __init__(
			self,
			parent=None,
			on_btn1_click=None,
			on_btn2_click=None,
			item=None
	):
		super(PatientSessionListItemWidget, self).__init__(parent)
		self.ui = Ui_Form()
		self.ui.setupUi(self)
		self.item = item

		self.on_btn1_click = on_btn1_click
		self.on_btn2_click = on_btn2_click

		self.ui.labelDataStart.setText(unicode(item.date_start.strftime('%d-%m-%Y %H:%M')))
		self.ui.labelDataEnd.setText(unicode(item.date_end.strftime('%d-%m-%Y %H:%M')))

		self.ui.label.setText(self.item.get_name())

		self.ui.dias_label_text.setText(self.get_short_all_dias())
		if not item.complaints:
			self.ui.open_complaints_btn.setDisabled(True)
		else:
			self.ui.open_complaints_btn.setEnabled(True)

		self.ui.open_session_btn.clicked.connect(self.on_btn1_clicked)
		self.ui.open_complaints_btn.clicked.connect(self.on_btn2_clicked)

	def on_btn1_clicked(self):
		if self.on_btn1_click:
			self.on_btn1_click(self)

	def on_btn2_clicked(self):
		if self.on_btn2_click:
			self.on_btn2_click(self)

	def get_short_all_dias(self):
		s = ', '.join([i.get_name() for i in self.item.diagnostics])
		return s if len(s) < 100 else s[:100] + '...'
