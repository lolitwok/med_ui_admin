# -*- coding: utf-8 -*-
from applibs.dao import  cur_user_session
from applibs.models.user_stuff.session_models import DReport, DSession
import os
from datetime import date

from PIL.ImageQt import ImageQt
from applibs.main_ui.dialog.complaints_info_dialog import ComplaintsDialog
from applibs.main_ui.widgets.items.session_item import PatientSessionListItemWidget
from applibs.main_ui.widgets.items.user_file_item import UserFileItemWidget
from  applibs import _translate as tr

from PyQt4 import QtCore, QtGui
from applibs.simple_logger import SLogger

base_path = os.path.dirname(os.path.abspath(__file__))
logger = SLogger.get_logger()

from applibs.main_ui.widgets.src.patientInfo import Ui_Form

try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s


class PatientInfoWidget(QtGui.QWidget):
	def __init__(self, parent=None, load_method=None, prev_ref='patients'):
		super(PatientInfoWidget, self).__init__(parent)
		self.load_method = load_method
		self.ui = Ui_Form()
		self.ui.setupUi(self)
		self.m_parent = parent
		self.prev_ref = prev_ref

		self.comp_dia = None

		self.ui.verticalLayout_3.setAlignment(QtCore.Qt.AlignTop)
		self.ui.scrollArea.setWidgetResizable(True)

		self.ui.ufs_layout.setAlignment(QtCore.Qt.AlignTop)
		self.ui.scrollArea_2.setWidgetResizable(True)

		self.ui.pushButton_8.clicked.connect(self.add_anamnes_row)
		self.ui.pushButton_9.clicked.connect(self.del_anamnes_row)
		self.ui.back_btn.clicked.connect(self.go_back)
		self.current_patient = None

	def renew(self, patient=None, prev_ref='patients'):
		self.current_patient = patient
		self.prev_ref = prev_ref

	def showEvent(self, *args, **kwargs):
		self.ui.labelPicture.setText(u"")

		img = self.current_patient.get_pic()
		img.thumbnail((200, 200))
		self.ui.labelPicture.setPixmap(QtGui.QPixmap.fromImage(ImageQt(img)))

		if self.current_patient.birthday_date:
			now = date.today()
			age = int((now - self.current_patient.birthday_date).days / 365.25)
			if age < 0:
				age = 0

			self.ui.labelYears.setText(u'Возраст %s лет' % unicode(age))
		else:
			self.ui.labelYears.setText(u'Нет информациио возрасте')

		self.ui.labelName.setText(unicode(
			u"%s %s %s" % (
				unicode(self.current_patient.first_name),
				unicode(self.current_patient.last_name),
				unicode(self.current_patient.middle_name),
			)
		))
		self.refresh_sessions_list()
		self.refresh_user_files_list()
		self.load_anamnes()

	def closeEvent(self, e):
		self.save_anamnes(self.current_patient)

	def refresh_sessions_list(self):

		for i in reversed(range(self.ui.verticalLayout_3.count())):
			self.ui.verticalLayout_3.itemAt(i).widget().deleteLater()
		sessions = self.current_patient.sessions.filter(DSession.date_end != None)
		for i in sessions:
			sliw = PatientSessionListItemWidget(
				parent=self,
				item=i,
				on_btn1_click=self.open_session_view,
				on_btn2_click=self.open_complaints_dialog,

			)
			self.ui.verticalLayout_3.addWidget(sliw)

	def refresh_user_files_list(self):
		for i in reversed(range(self.ui.ufs_layout.count())):
			self.ui.ufs_layout.itemAt(i).widget().deleteLater()

		ufiles = cur_user_session.query(DReport) \
			.join((DSession, DSession.id == DReport.session_id)) \
			.filter(DSession.patient_id == self.current_patient.id) \
			.order_by(DSession.id.asc(), DReport.date.desc()) \
			.all()

		for i in ufiles:
			ufiw = UserFileItemWidget(
				parent=self,
				item=i,
				on_btn1_click=self.try_show_in_folder,
				on_btn2_click=self.try_open_file,

			)

			self.ui.ufs_layout.addWidget(ufiw)

	def load_anamnes(self):
		tw = self.ui.tableWidget
		tw.setRowCount(0)
		patient = self.current_patient

		for ind, item in enumerate(patient.anamnes_list):
			tw.insertRow(ind)
			for j in xrange(3):
				tw.setItem(ind, j, QtGui.QTableWidgetItem(unicode(item[j])))

	def save_anamnes(self, patient):
		tw = self.ui.tableWidget
		l = []
		for i in xrange(tw.rowCount()):
			if not (tw.item(i, 0).text() or tw.item(i, 1).text() or tw.item(i, 2).text()):
				continue

			it1 = tw.item(i, 0)
			it2 = tw.item(i, 1)
			it3 = tw.item(i, 2)

			l.append(
				(
					unicode(it1.text()),
					unicode(it2.text()),
					unicode(it3.text()),
				)
			)

		patient.set_anamnes(l)
		cur_user_session.add(patient)
		cur_user_session.commit()

	def add_anamnes_row(self):
		tw = self.ui.tableWidget
		pos = tw.rowCount()
		tw.insertRow(pos)
		tw.setItem(pos, 0, QtGui.QTableWidgetItem(""))
		tw.setItem(pos, 1, QtGui.QTableWidgetItem(""))
		tw.setItem(pos, 2, QtGui.QTableWidgetItem(""))

	def del_anamnes_row(self):
		tw = self.ui.tableWidget
		sel = tw.currentRow()

		tw.removeRow(sel)

	def go_back(self):
		from applibs.main_ui.main_window import WIDGETS
		self.m_parent.load_widget(WIDGETS[self.prev_ref])

	def try_open_file(self, ufiw):
		if ufiw.item.is_exists():
			if not ufiw.item.try_open():
				ufiw.refresh()
			return

		if ufiw.ui.label_4.isVisible():
			ufiw.item.create_new(self.m_parent)
			QtGui.QMessageBox.information(
				self.m_parent,
				tr("Отчет"),
				tr("Отчет Сгенерированю"),
				QtGui.QMessageBox.Ok
			)
		ufiw.refresh()

	def try_show_in_folder(self, ufiw):
		if not ufiw.item.try_show_in_folder():
			ufiw.refresh()

	def open_session_view(self, ps):
		if ps.item:
			from applibs.main_ui.main_window import WIDGETS
			self.load_method(WIDGETS['session_view'], item=ps.item)

	def open_complaints_dialog(self, item):
		self.comp_dia = ComplaintsDialog(parent=self, session=item.item)
		self.comp_dia.exec_()
