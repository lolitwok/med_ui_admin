# -*- coding: utf-8 -*-
from PyQt4 import QtGui
from applibs.main_ui.widgets.src.diagnostics_form import Ui_FormDia


class DiagSelectWidget(QtGui.QWidget):
	def __init__(self, parent=None, load_method=None):
		super(DiagSelectWidget, self).__init__(parent)
		self.load_method = load_method
		self.ui = Ui_FormDia()
		self.ui.setupUi(self)
		self.m_parent = parent
		self.ui.btnFuncMap.clicked.connect(self.func_map_click)
		self.ui.btnNazodDia.clicked.connect(self.nazod_dia_click)
		self.ui.btnPointMapDia.clicked.connect(self.point_map_dia_click)

	def closeEvent(self, *args, **kwargs):
		# sys.exit(0)
		pass

	def start_dia(self, dia):
		from applibs.main_ui.main_window import DIAGNOSTICS

		self.m_parent.selected_diagnostic = DIAGNOSTICS[dia]

		if self.m_parent.cur_session is not None:
			self.m_parent.load_widget(self.m_parent.selected_diagnostic[1])

		else:
			self.m_parent.start_session()

	def func_map_click(self):
		self.start_dia(1)

	def nazod_dia_click(self):
		self.start_dia(2)

	def point_map_dia_click(self):
		self.start_dia(3)

	def renew(self):
		pass
