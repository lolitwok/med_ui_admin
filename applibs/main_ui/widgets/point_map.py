# -*- coding: utf-8 -*-
from applibs.dao import cur_main_session
import os
import types

from PyQt4.QtGui import QMessageBox
from PyQt4 import QtCore, QtGui, Qt

from sqlalchemy.orm.util import aliased
from sqlalchemy.sql.functions import count

from applibs import _translate as tr
from applibs.main_ui.widgets.items.survey_item import SurveyListItemWidget
from applibs.models.main_models import dnode, dpoint
from applibs.main_ui.utils import local_html, DiagnosticInfo, NodeInfo, middle_value
from applibs.main_ui.widgets.src.pointMapWidget import Ui_Form

base_path = os.path.dirname(os.path.abspath(__file__))

DISABLED_NODE_ITEM = """
	QListWidgetItem{
	background-color: #ccc;
}
"""

NORMAL_MAP_POINT_ITEM = """
	QWidget{
	border: 1px solid #fff;
}
"""

SELECTED_MAP_POINT_ITEM = """
	QWidget{
	border: 1px solid #f00;
}
"""


class PointMapWidget(QtGui.QWidget):
	def __init__(self, parent=None, load_method=None):
		super(PointMapWidget, self).__init__(parent)
		self.load_method = load_method
		self.ui = Ui_Form()
		self.mini_version = True
		self.showed = False

		self.ui.setupUi(self)
		self.m_parent = parent
		self.ui.btnGoToTop.clicked.connect(self.btnGoToTop_click)
		self.ui.btnShowPrevLebel.clicked.connect(self.btnShowPrevLebel_click)
		self.ui.btnSearch.clicked.connect(self.btnSearch_click)

		self.ui.btn_open_hand.clicked.connect(self.open_hand)

		self.ui.btn_show_tab1.clicked.connect(self.show_tab1)
		self.ui.btn_show_tab2.clicked.connect(self.show_tab2)
		self.ui.btn_show_tab3.clicked.connect(self.show_tab3)

		self.ui.btn_node_left.clicked.connect(self.node_left)
		self.ui.btn_node_right.clicked.connect(self.node_right)
		self.ui.btn_show_tab2.clicked.connect(self.show_tab2)

		self.ui.lineEdit.keyPressEvent = types.MethodType(self.btnSearch_kp, self.ui.lineEdit)

		self.ui.list.itemClicked.connect(self.node_item_click)
		self.ui.list.doubleClicked.connect(self.node_item_next_click)

		self.ui.btnClearData.clicked.connect(self.clear_list)

		self.ui.btn_end_dia.clicked.connect(self.m_parent.save_diagnostic_click(self))

		self.ui.btn_del_category.clicked.connect(self.del_category_click)

		self.ui.map_btn_del_point.clicked.connect(self.map_clear_list)

		self.m_parent.update_value.connect(self.on_update_value)
		self.m_parent.update_dia_list.connect(self.on_update_dia_list)

		self.ui.list_categories.itemClicked.connect(self.categories_item_click)
		self.ui.list_categories.doubleClicked.connect(self.categories_item_next_click)
		self.ui.btn_top_of_category.clicked.connect(self.categories_go_top_click)
		self.ui.btn_prev_category.clicked.connect(self.categories_go_back_click)
		self.ui.btn_category_search.clicked.connect(self.categories_search_click)

		self.ui.pointList.setAlignment(QtCore.Qt.AlignTop)
		self.ui.scrollArea_3.setWidgetResizable(True)

		self.ui.pointList_2.setAlignment(QtCore.Qt.AlignTop)
		self.ui.scrollArea_4.setWidgetResizable(True)

		self.current_node = None
		self.current_category = None
		self.current_category = None
		self.m_parent.device_btns.connect(self.slot_btn)

		self.cur_point_pos = None

		self.list_is_enabled = True

		self.ui.widget_27.mousePressEvent = types.MethodType(self.map_top_btn, self.ui.widget_27)
		self.ui.widget_29.mousePressEvent = types.MethodType(self.map_right_btn, self.ui.widget_29)
		self.ui.widget_31.mousePressEvent = types.MethodType(self.map_bottom_btn, self.ui.widget_31)
		self.ui.widget_28.mousePressEvent = types.MethodType(self.map_left_btn, self.ui.widget_28)

		from applibs.main_ui.utils import signals
		signals.kipspedo_changed.connect(self.kipspedo_changed_event)

	def slot_btn(self, _int):
		if self.ui.tab2.isVisible():
			if _int == 1:
				self.node_left()
			elif _int == 2:
				self.node_right()

		if self.ui.tab3.isVisible():
			if _int == 1:
				self.map_get_value()
			elif _int == 2:
				self.map_next_point()

	def refresh_nazod_list(self, n_item_guid=None, search=None):
		if search is not None:
			search = unicode(search).lower()

		self.ui.list.clear()
		main_table = aliased(dnode.DNode)
		second_table = aliased(dnode.DNode)

		nodes_query = cur_main_session.query(main_table, count(second_table.id)) \
			.outerjoin(second_table, main_table.guid == second_table.parrent_guid) \
			.filter(main_table.d_item_guid == '06dba33b-94ad-466a-8cea-3e5578ca865d') \
			.group_by(main_table.id)

		if search is None:
			nodes_query = nodes_query.filter(main_table.parrent_guid == n_item_guid)

		if search:
			nodes_query = nodes_query.filter(main_table.name_lower.like(u"%{0}%".format(search)))

		nodes = nodes_query.all()

		for i, _count in nodes:
			t = QtGui.QListWidgetItem()
			t.setText(i.get_name())
			t.setData(32, i)
			if _count:
				t.setBackground(QtGui.QColor(246, 230, 230))
			self.ui.list.addItem(t)

			if i.is_main:
				self.ui.list.setCurrentItem(t)
				self.node_item_click(None)

	def showEvent(self, *args, **kwargs):
		# super(FunctionalMapWidget, self).__dict__['showEvent'](*args, **kwargs)
		if not self.showed:
			self.list_is_enabled = True

			# self.ui.label_3.setText(unicode(self.ui.nazode_list_vl.itemAt(0).widget().item.name))
			self.refresh_dia_list()
			self.refresh_nazod_list()
			self.show_tab2()
			self.disable_node_list()

			# self.gl_widget.reinit(dev=True, changeable=True)
			# self.gl_widget.reinit(model_file="hand.obj", point=None)

			# 	si = self.gl_widget.size()
			# 	self.gl_widget.setMinimumWidth(si.height())
			#
			# def resizeEvent(self, *args, **kwargs):
			# 	si = self.gl_widget.size()
			# 	self.gl_widget.setMinimumWidth(si.height())
			self.showed = True

	def renew(self):

		self.m_parent.start_new_diagnostic()
		self.m_parent.cur_session.cur_diagnostic = DiagnosticInfo(dia_type=5)
		self.current_node = None

		self.ui.webGraphLiveValueGraph.load(Qt.QUrl("about:blank"))
		self.ui.webGraphLiveValueArrow.load(Qt.QUrl("about:blank"))

		self.ui.webGraphLiveValueGraph_2.load(Qt.QUrl("about:blank"))
		self.ui.webGraphLiveValueArrow_2.load(Qt.QUrl("about:blank"))

		self.ui.web_tab2_2.load(Qt.QUrl("about:blank"))
		self.ui.webViewRadar.load(Qt.QUrl("about:blank"))
		self.ui.web_kipsped.load(Qt.QUrl("about:blank"))

		self.ui.webViewGem.load(Qt.QUrl("about:blank"))

		self.show_tab2()
		self.showed = False

	def closeEvent(self, e):

		self.ui.webGraphLiveValueGraph.load(Qt.QUrl("about:blank"))
		self.ui.webViewRadar.load(Qt.QUrl("about:blank"))
		# self.ui.webViewBarMain.load(Qt.QUrl("about:blank"))
		self.ui.web_tab2_2.load(Qt.QUrl("about:blank"))
		self.ui.webGraphLiveValueArrow.load(Qt.QUrl("about:blank"))
		self.ui.web_kipsped.load(Qt.QUrl(local_html('about:blank')))
		self.ui.webViewGem.load(Qt.QUrl("about:blank"))

	def on_update_value(self, value):
		pass

	def on_update_dia_list(self, item):
		self.add_to_dia_list(item)

	def node_item_click(self, item):
		if len(self.ui.list.selectedItems()) == 0:
			return
		if not self.ui.list.selectedItems()[0].flags() & QtCore.Qt.ItemIsSelectable:
			return
		n_item = self.ui.list.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()
		if n_item is None or not n_item.usable_us_dia:
			return

		if self.m_parent.cur_session.cur_diagnostic is not None:
			self.m_parent.cur_session.cur_diagnostic.append_node()

		self.current_node = n_item

		point = cur_main_session.query(dpoint.DPoint) \
			.filter(dpoint.DPoint.d_node_guid == self.current_node.guid).first()

		if point:
			self.m_parent.hand_dialog.set_points([point, ])
			self.m_parent.hand_dialog.set_point_name(self.current_node.get_name())
		else:
			self.m_parent.hand_dialog.set_points()

		self.set_node()

	def node_item_next_click(self, item):
		if len(self.ui.list.selectedItems()) == 0:
			return

		n_item = self.ui.list.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()

		items_count = cur_main_session.query(dnode.DNode) \
			.filter(dnode.DNode.parrent_guid == n_item.guid) \
			.count()

		if items_count == 0:
			return

		self.refresh_nazod_list(n_item.id)

	def btnGoToTop_click(self):
		self.refresh_nazod_list()

	def btnShowPrevLebel_click(self):
		if self.ui.list.count() > 0:
			prev_item_id = self.ui.list.item(0).data(QtCore.Qt.UserRole).toPyObject().parrent_guid
			if prev_item_id is None:
				return
			prev_item = cur_main_session.query(dnode.DNode).filter(dnode.DNode.guid == prev_item_id).first()

			self.refresh_nazod_list(n_item_guid=prev_item.parrent_guid)

	def btnSearch_click(self):
		search = None
		if len(self.ui.lineEdit.text()) > 0:
			search = self.ui.lineEdit.text()

		self.refresh_nazod_list(search=search)

	def btnSearch_kp(self, e, event):
		if event.key() == QtCore.Qt.Key_Return:
			self.btnSearch_click()
			return

		Qt.QLineEdit.keyPressEvent(e, event)

	# point list

	def refresh_dia_list(self):
		ind = 0
		self.m_parent.selected_measurement_array = None
		for i in reversed(range(self.ui.pointList.count())):
			self.ui.pointList.itemAt(i).widget().deleteLater()
		sliw = None

		if self.m_parent.cur_session.cur_diagnostic.cur_node is None:
			return

		for i in self.m_parent.cur_session.cur_diagnostic.cur_node.normal_points:
			sliw = SurveyListItemWidget(
				parent=self,
				item=(ind, i),
				on_click_item_method=self.point_item_click,
				on_click_delete_method=self.point_btn_delete_click,
			)
			sliw.set_selected(False)

			self.ui.pointList.addWidget(sliw)
			ind += 1

		if sliw:
			sliw.item_click()

	def add_to_dia_list(self, item):
		self.m_parent.selected_measurement_array = None
		last = None
		if self.ui.pointList.count() > 0:
			last = self.ui.pointList.itemAt(self.ui.pointList.count() - 1).widget().item
		sliw = SurveyListItemWidget(
			parent=self,
			item=(last[0] + 1 if last else 0, item),
			on_click_item_method=self.point_item_click,
			on_click_delete_method=self.point_btn_delete_click,
		)
		sliw.set_selected(False)
		self.ui.pointList.addWidget(sliw)
		sliw.item_click()

	def point_item_click(self, item):
		for i in reversed(range(self.ui.pointList.count())):
			_t = self.ui.pointList.itemAt(i).widget()
			_t.set_selected(False)

		item.set_selected(True)
		self.m_parent.selected_measurement_array = item.item[1]

	def point_btn_delete_click(self, item):
		d_id = item.item[0]
		del self.m_parent.cur_session.cur_diagnostic.cur_node.normal_points[d_id]
		self.refresh_dia_list()

	def clear_list(self):
		if self.m_parent.cur_session.cur_diagnostic.cur_node:
			self.m_parent.cur_session.cur_diagnostic.cur_node.clear_cur_points()
			self.refresh_dia_list()

	def open_hand(self):
		if not self.m_parent.hand_dialog.isVisible():
			self.m_parent.hand_dialog.show()

	def show_tab1(self):

		self.m_parent.cur_session.cur_diagnostic.append_node()

		self.m_parent.point_settings.is_allow_measurement = False
		self.ui.webViewRadar.load(Qt.QUrl(local_html('radar.html')))
		# self.ui.webViewBarMain.load(Qt.QUrl(local_html('barMain.html')))

		self.ui.webGraphLiveValueGraph.load(Qt.QUrl("about:blank"))
		self.ui.webGraphLiveValueArrow.load(Qt.QUrl("about:blank"))

		self.ui.web_tab2_2.load(Qt.QUrl("about:blank"))
		self.ui.web_kipsped.load(Qt.QUrl("about:blank"))

		self.ui.webGraphLiveValueGraph_2.load(Qt.QUrl("about:blank"))
		self.ui.webGraphLiveValueArrow_2.load(Qt.QUrl("about:blank"))
		self.ui.webViewGem.load(Qt.QUrl("about:blank"))

		self.refresh_categories_list()

		self.ui.tab2.setVisible(False)
		self.ui.tab1.setVisible(True)
		self.ui.tab3.setVisible(False)

		self.ui.top_btns.setVisible(False)
		self.ui.btn_show_tab1.setDisabled(True)
		self.ui.btn_show_tab2.setEnabled(True)
		self.ui.btn_show_tab3.setEnabled(True)

	def show_tab2(self):

		self.m_parent.cur_session.cur_diagnostic.append_node()
		self.ui.webGraphLiveValueGraph.load(Qt.QUrl(local_html('live-chart.html')))
		self.ui.webGraphLiveValueArrow.load(Qt.QUrl(local_html('ArrowGraph.html')))

		self.ui.webGraphLiveValueGraph_2.load(Qt.QUrl("about:blank"))
		self.ui.webGraphLiveValueArrow_2.load(Qt.QUrl("about:blank"))

		self.ui.web_tab2_2.load(Qt.QUrl(local_html('falling.html')))
		self.ui.web_kipsped.load(Qt.QUrl(local_html('kipsped.html')))

		self.ui.webViewRadar.load(Qt.QUrl("about:blank"))
		# self.ui.webViewBarMain.load(Qt.QUrl("about:blank"))

		self.ui.webViewGem.load(Qt.QUrl("about:blank"))

		self.ui.tab2.setVisible(True)
		self.ui.tab1.setVisible(False)
		self.ui.tab3.setVisible(False)
		self.ui.top_btns.setVisible(True)
		self.ui.btn_show_tab1.setEnabled(True)
		self.ui.btn_show_tab2.setDisabled(True)
		self.ui.btn_show_tab3.setEnabled(True)

		if self.set_node() is False:
			return

		self.m_parent.point_settings.is_allow_measurement = True
		self.m_parent.point_settings.is_show_only = False

	def show_tab3(self):
		self.m_parent.cur_session.cur_diagnostic.append_node()
		old_node = self.m_parent.cur_session.cur_diagnostic.get_node_by_id(self.current_node.get_uuid())

		if old_node is not False:
			self.m_parent.cur_session.cur_diagnostic.cur_node = old_node

		if not len(self.m_parent.cur_session.cur_diagnostic.cur_node.normal_points) or \
				not self.m_parent.cur_session.cur_diagnostic.cur_node:
			QMessageBox.warning(
				self.m_parent,
				tr("Предупреждение"),
				tr("Сделайте измерение на точке"),
				QtGui.QMessageBox.Ok
			)
			return

		if len(self.m_parent.cur_session.cur_diagnostic.cur_node.normal_points) < 3:
			QMessageBox.warning(
				self.m_parent,
				tr("Предупреждение"),
				tr("Измерений на точке болжно быть больше 2х"),
				QtGui.QMessageBox.Ok
			)
			return

		self.ui.webGraphLiveValueGraph.load(Qt.QUrl("about:blank"))
		self.ui.webGraphLiveValueArrow.load(Qt.QUrl("about:blank"))

		self.ui.webGraphLiveValueGraph_2.load(Qt.QUrl(local_html('live-chart.html')))
		self.ui.webGraphLiveValueArrow_2.load(Qt.QUrl(local_html('ArrowGraph.html')))

		self.ui.web_tab2_2.load(Qt.QUrl("about:blank"))
		self.ui.web_kipsped.load(Qt.QUrl("about:blank"))

		self.ui.webViewRadar.load(Qt.QUrl("about:blank"))

		self.ui.webViewGem.load(Qt.QUrl(local_html('Diamond.html')))

		self.ui.tab3.setVisible(True)
		self.ui.tab1.setVisible(False)
		self.ui.tab2.setVisible(False)
		self.ui.top_btns.setVisible(False)
		self.ui.btn_show_tab1.setEnabled(True)
		self.ui.btn_show_tab2.setEnabled(True)
		self.ui.btn_show_tab3.setDisabled(True)

		self.ui.lcdNumber_4.display('%0.2f' % self.m_parent.cur_session.cur_diagnostic.cur_node.normal_points_middle)
		self.cur_point_pos = None
		self.map_update_point_values()
		self.map_apply_point_pos()
		self.m_parent.point_settings.is_allow_measurement = True
		self.m_parent.point_settings.is_show_only = True

	def set_node(self):
		if self.current_node is None:
			return False

		if self.m_parent.cur_session is None or self.m_parent.cur_session.cur_diagnostic is None:
			return False

		self.ui.label_2.setText(self.current_node.get_name())

		old_node = self.m_parent.cur_session.cur_diagnostic.get_node_by_id(self.current_node.get_uuid())

		if old_node is False:
			self.m_parent.cur_session.cur_diagnostic.cur_node = NodeInfo(node=self.current_node)
		else:
			self.m_parent.cur_session.cur_diagnostic.cur_node = old_node

		self.update_node_label()
		self.refresh_dia_list()
		return True

	def update_node_label(self):
		ind = self.ui.list.currentRow()
		before = False
		after = False

		for i in xrange(ind - 1, -1, -1):
			item = self.ui.list.item(i)

			if item.flags() & QtCore.Qt.ItemIsSelectable and item.data(QtCore.Qt.UserRole).toPyObject().usable_us_dia:
				before = i
				break

		for i in xrange(ind + 1, self.ui.list.count(), 1):
			item = self.ui.list.item(i)

			if item.flags() & QtCore.Qt.ItemIsSelectable and item.data(QtCore.Qt.UserRole).toPyObject().usable_us_dia:
				after = i
				break

		if before is False:
			self.ui.btn_node_left.setDisabled(True)
		else:
			self.ui.btn_node_left.setEnabled(True)

		if after is False:
			self.ui.btn_node_right.setDisabled(True)
		else:
			self.ui.btn_node_right.setEnabled(True)

	def node_left(self):
		ind = self.ui.list.currentRow()
		before = False

		for i in xrange(ind - 1, -1, -1):
			b = self.ui.list.item(i).data(QtCore.Qt.UserRole).toPyObject()
			if b.usable_us_dia:
				before = i
				break

		if before is False:
			return

		self.m_parent.cur_session.cur_diagnostic.append_node()
		self.ui.list.setCurrentRow(before)
		self.node_item_click(None)

		self.set_node()

	def node_right(self):
		ind = self.ui.list.currentRow()
		after = False

		for i in xrange(ind + 1, self.ui.list.count(), 1):
			b = self.ui.list.item(i).data(QtCore.Qt.UserRole).toPyObject()
			if b.usable_us_dia:
				after = i
				break

		if after is False:
			return

		self.m_parent.cur_session.cur_diagnostic.append_node()
		self.ui.list.setCurrentRow(after)
		self.node_item_click(None)
		self.set_node()

	def resizeEvent(self, QResizeEvent):
		pass

	def refresh_categories_list(self, search=None):
		self.ui.list_categories.clear()
		if self.current_category is None or search is not None:
			_list = self.m_parent.cur_session.cur_diagnostic.categories

		else:
			_list = self.current_category.childs

		if search is None:
			items = _list
		else:
			items = self.m_parent.cur_session.cur_diagnostic.search_category(search)

		for i in items:
			t = QtGui.QListWidgetItem()
			t.setText(i.node_name)
			t.setData(32, i)
			if not i.is_last_child:
				t.setBackground(QtGui.QColor(246, 230, 230))
			self.ui.list_categories.addItem(t)

		if self.ui.list_categories.count() > 0:
			self.ui.list_categories.setCurrentRow(0)

	def categories_item_next_click(self, item):
		if len(self.ui.list_categories.selectedItems()) == 0:
			return

		_item = self.ui.list_categories.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()

		if _item.is_last_child:
			return

		self.current_category = _item
		self.refresh_categories_list()

	def del_category_click(self):
		if len(self.ui.list_categories.selectedItems()) == 0:
			return
		_item = self.ui.list_categories.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()
		self.current_category = _item.parent
		self.m_parent.cur_session.cur_diagnostic.del_category(_item)
		self.refresh_categories_list()

	def categories_item_click(self, item):
		pass

	def categories_go_back_click(self):
		if self.current_category is None:
			return

		self.current_category = self.current_category.parent
		self.refresh_categories_list()

	def categories_go_top_click(self):
		# if self.current_category is None:
		# 	return

		self.current_category = None
		self.refresh_categories_list()

	def categories_search_click(self):
		search = None
		if len(self.ui.category_search.text()) > 0:
			search = self.ui.category_search.text()

		self.refresh_categories_list(search=search)

	def categories_search_kp(self, e, event):
		if event.key() == QtCore.Qt.Key_Return:
			self.categories_search_click()
			return

		Qt.QLineEdit.keyPressEvent(e, event)

	def kipspedo_changed_event(self, e):
		if e and not self.list_is_enabled:
			self.enable_node_list()
		elif not e and self.list_is_enabled:
			self.disable_node_list()

	def disable_node_list(self):
		main = None
		for i in xrange(self.ui.list.count()):
			t = self.ui.list.item(i)
			if t.data(QtCore.Qt.UserRole).toPyObject().is_main:
				main = i
				continue

			t.setFlags(t.flags() & ~QtCore.Qt.ItemIsSelectable)
			t.setBackground(QtGui.QColor(200, 200, 200))

		if main is not None:
			self.ui.list.setCurrentRow(main)
			self.node_item_click(None)
			self.set_node()

		self.list_is_enabled = False

	def enable_node_list(self):
		for i in xrange(self.ui.list.count()):
			t = self.ui.list.item(i)
			if not (t.flags() & QtCore.Qt.ItemIsSelectable):
				t.setBackground(QtGui.QColor(255, 255, 255))
				t.setFlags(t.flags() | QtCore.Qt.ItemIsSelectable)
		self.list_is_enabled = True
		self.update_node_label()

	# tab3
	def map_get_value(self):
		if self.cur_point_pos is None:
			return

		value = self.m_parent.server.get_last_value()[0]

		if self.cur_point_pos == 0:
			self.m_parent.cur_session.cur_diagnostic.cur_node.map_data['t'].append(value)
		elif self.cur_point_pos == 1:
			self.m_parent.cur_session.cur_diagnostic.cur_node.map_data['r'].append(value)
		elif self.cur_point_pos == 2:
			self.m_parent.cur_session.cur_diagnostic.cur_node.map_data['b'].append(value)
		elif self.cur_point_pos == 3:
			self.m_parent.cur_session.cur_diagnostic.cur_node.map_data['l'].append(value)

		self.map_update_point_values()

		ind = self.ui.pointList_2.itemAt(self.ui.pointList_2.count() - 1).widget().item[0] + 1 \
			if self.ui.pointList_2.count() > 0 else 0
		sliw = SurveyListItemWidget(
			parent=self,
			item=(ind, value),
			on_click_item_method=None,
			on_click_delete_method=self.map_point_btn_delete_click,
		)
		sliw.set_selected(False)

		self.ui.pointList_2.addWidget(sliw)

	def clear_map_items_selected(self):

		self.ui.widget_27.setStyleSheet(NORMAL_MAP_POINT_ITEM)
		self.ui.widget_28.setStyleSheet(NORMAL_MAP_POINT_ITEM)
		self.ui.widget_29.setStyleSheet(NORMAL_MAP_POINT_ITEM)
		self.ui.widget_31.setStyleSheet(NORMAL_MAP_POINT_ITEM)

	def map_apply_point_pos(self):

		self.clear_map_items_selected()

		if self.cur_point_pos == 0:
			self.ui.widget_27.setStyleSheet(SELECTED_MAP_POINT_ITEM)
		elif self.cur_point_pos == 1:
			self.ui.widget_29.setStyleSheet(SELECTED_MAP_POINT_ITEM)
		elif self.cur_point_pos == 2:
			self.ui.widget_31.setStyleSheet(SELECTED_MAP_POINT_ITEM)
		elif self.cur_point_pos == 3:
			self.ui.widget_28.setStyleSheet(SELECTED_MAP_POINT_ITEM)

		for i in reversed(range(self.ui.pointList_2.count())):
			self.ui.pointList_2.itemAt(i).widget().deleteLater()

		if self.cur_point_pos is None or self.cur_point_pos > 3:
			return

		ind = 1
		for i in self.m_parent.cur_session.cur_diagnostic.cur_node.map_data[self.map_get_key()]:
			sliw = SurveyListItemWidget(
				parent=self,
				item=(ind, i),
				on_click_item_method=None,
				on_click_delete_method=self.map_point_btn_delete_click,
			)
			sliw.set_selected(False)

			self.ui.pointList_2.addWidget(sliw)
			ind += 1

	def map_update_point_values(self):

		points = self.m_parent.cur_session.cur_diagnostic.cur_node.map_data

		for i in [
			(self.ui.lcdNumber, 't'),
			(self.ui.lcdNumber_3, 'r'),
			(self.ui.lcdNumber_5, 'b'),
			(self.ui.lcdNumber_2, 'l'), ]:
			i[0].display('%0.2f' % middle_value(points[i[1]]))

	def map_next_point(self):
		if self.cur_point_pos is None:
			self.cur_point_pos = 0
		else:
			self.cur_point_pos += 1

		if self.cur_point_pos > 3:
			self.cur_point_pos = None

		self.map_apply_point_pos()

	def map_top_btn(self, this, event):
		self.cur_point_pos = 0
		self.map_apply_point_pos()

	def map_right_btn(self, this, event):
		self.cur_point_pos = 1
		self.map_apply_point_pos()

	def map_bottom_btn(self, this, event):
		self.cur_point_pos = 2
		self.map_apply_point_pos()

	def map_left_btn(self, this, event):
		self.cur_point_pos = 3
		self.map_apply_point_pos()

	def map_point_btn_delete_click(self, item):
		val = item.item[1]
		self.m_parent.cur_session.cur_diagnostic.cur_node.map_data[self.map_get_key()].remove(val)
		for i in reversed(range(self.ui.pointList_2.count())):
			if self.ui.pointList_2.itemAt(i).widget().item[0] == item.item[0]:
				self.ui.pointList_2.itemAt(i).widget().deleteLater()

		self.map_update_point_values()

	def map_get_key(self):
		s = ''
		if self.cur_point_pos == 0:
			s = 't'
		elif self.cur_point_pos == 1:
			s = 'r'
		elif self.cur_point_pos == 2:
			s = 'b'
		elif self.cur_point_pos == 3:
			s = 'l'

		return s

	def map_clear_list(self):
		if not self.map_get_key():
			return

		del self.m_parent.cur_session.cur_diagnostic.cur_node.map_data[self.map_get_key()][:]
		for i in reversed(range(self.ui.pointList_2.count())):
			self.ui.pointList_2.itemAt(i).widget().deleteLater()

		self.map_update_point_values()

	def before_end_diagnostic(self):
		if self.m_parent.cur_session:
			if self.m_parent.cur_session.cur_diagnostic:
				self.m_parent.cur_session.cur_diagnostic.append_node()
