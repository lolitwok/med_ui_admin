# -*- coding: utf-8 -*-
from applibs import _translate as tr
from applibs.dao import cur_user_session
from applibs.models.user_stuff.customer_staff import CustomerNozod
import os
from PyQt4 import QtCore, QtGui
from applibs.simple_logger import SLogger
from applibs.main_ui.widgets.src.customer_nozodes_edit import Ui_Form

logger = SLogger.get_logger()
base_path = os.path.dirname(os.path.abspath(__file__))


class CustomerNozodsWidget(QtGui.QWidget):
	def __init__(self, parent=None, load_method=None):
		super(CustomerNozodsWidget, self).__init__(parent)
		self.load_method = load_method
		self.ui = Ui_Form()
		self.ui.setupUi(self)
		self.m_parent = parent
		self.cur_nozod = None

		self.ui.add_btn.clicked.connect(self.add_btn_clicked)
		self.ui.save_btn.clicked.connect(self.save_btn_clicked)
		self.ui.delete_btn.clicked.connect(self.delete_btn_clicked)

		self.ui.list.itemClicked.connect(self.list_itemClicked)
		self.showed = False

	def renew(self):
		self.showed = False

	def showEvent(self, *args, **kwargs):
		if not self.showed:
			self.showed = True
			self.refresh_nozods_list()

	def closeEvent(self, e):
		pass

	def refresh_info(self):
		if self.cur_nozod:
			self.ui.name.setText(unicode(self.cur_nozod.name))
			self.ui.description.setText(unicode(self.cur_nozod.description))
			return

		self.ui.name.setText('')
		self.ui.description.setText('')

	def add_btn_clicked(self):
		self.cur_nozod = None
		self.refresh_info()
		self.ui.name.setFocus(QtCore.Qt.OtherFocusReason)

	def delete_btn_clicked(self):
		if not self.cur_nozod:
			return

		mb = QtGui.QMessageBox.warning(
			self,
			tr(u"Удаление"),
			unicode(tr(u"Вы действительно хотите удалить нозод `%s`?")) % self.cur_nozod.get_name(),
			QtGui.QMessageBox.Yes | QtGui.QMessageBox.No
		)

		if mb != QtGui.QMessageBox.Yes:
			return

		cur_user_session.delete(self.cur_nozod)
		cur_user_session.commit()
		self.refresh_nozods_list()

	def save_btn_clicked(self):
		name = unicode(self.ui.name.text())
		if not name:
			QtGui.QMessageBox.warning(
				self,
				tr(u"Ошибка"),
				tr(u"Введите имя нозода"),
				QtGui.QMessageBox.Ok
			)
			return

		if len(name) < 3:
			QtGui.QMessageBox.warning(
				self,
				tr(u"Ошибка"),
				tr(u"Длинна имени нозода должна быть больше 2х символов"),
				QtGui.QMessageBox.Ok
			)
			return

		if cur_user_session.query(CustomerNozod).filter(CustomerNozod.name == name).count() > 0:
			QtGui.QMessageBox.warning(
				self,
				tr(u"Ошибка"),
				tr(u"Нозод с таким названием уже существует"),
				QtGui.QMessageBox.Ok
			)
			return

		if self.cur_nozod is None:
			self.cur_nozod = CustomerNozod()
			self.cur_nozod.user = self.m_parent.user

		self.cur_nozod.name = name
		self.cur_nozod.name_lower = name.lower()
		self.cur_nozod.description = unicode(self.ui.description.toPlainText())

		cur_user_session.add(self.cur_nozod)
		cur_user_session.commit()
		self.refresh_nozods_list(selected_id=self.cur_nozod.id)

	def list_itemClicked(self, item):
		if not self.ui.list.selectedItems():
			return

		selected_nozod = self.ui.list.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()
		self.cur_nozod = selected_nozod
		self.refresh_info()

	def refresh_nozods_list(self, selected_id=None):
		self.ui.list.clear()
		nozods = cur_user_session.query(CustomerNozod).filter(CustomerNozod.user == self.m_parent.user).all()
		for i in nozods:
			t = QtGui.QListWidgetItem()
			t.setText(i.get_name())
			t.setData(32, i)
			self.ui.list.addItem(t)
			if selected_id and i.id == selected_id:
				self.ui.list.setItemSelected(t, True)
				self.list_itemClicked(None)

		if nozods and not selected_id:
			self.ui.list.setCurrentRow(0)
			self.list_itemClicked(None)
