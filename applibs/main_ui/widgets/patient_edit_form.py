# -*- coding: utf-8 -*-
from applibs.dao import cur_user_session
import os
import base64
from datetime import date

from PyQt4 import QtGui, uic, Qt
from PIL.ImageQt import ImageQt
from applibs import _translate as tr
from applibs.models.user_stuff.patient import Patient, TYPE_ANIMAL, TYPE_MALE, TYPE_FEMALE, TYPE_MAN
from applibs.simple_logger import SLogger
from applibs.main_ui.widgets.src.create_user import Ui_Form

logger = SLogger.get_logger()

(ui_widget, MainQWidget) = uic.loadUiType('applibs/main_ui/widgets/src/create_user.ui')
base_path = os.path.dirname(os.path.abspath(__file__))

MONTHS = [
	(1, tr(u'Январь')),
	(2, tr(u'Февраль')),
	(3, tr(u'Март')),
	(4, tr(u'Апрель')),
	(5, tr(u'Май')),
	(6, tr(u'Июнь')),
	(7, tr(u'Июль')),
	(8, tr(u'Август')),
	(9, tr(u'Сентябрь')),
	(10, tr(u'Октябрь')),
	(11, tr(u'Ноябрь')),
	(12, tr(u'Декабрь')),
]


class PatientEditForm(QtGui.QWidget):
	def __init__(self, parent=None, load_method=None):
		super(PatientEditForm, self).__init__(parent)
		self.load_method = load_method
		self.ui = Ui_Form()
		self.ui.setupUi(self)
		self.m_parent = parent
		self.edit_mode = None
		self.current_patient = None
		self.item = None
		self.img_data = None
		self.p_type = None

		self.ui.btnAccept.clicked.connect(self.btn_accept_clicked)
		self.ui.btnLoadPicture.clicked.connect(self.btn_load_picture_clicked)
		self.ui.btnDecline.clicked.connect(self.btn_decline_clicked)
		self.ui.back_btn.clicked.connect(self.go_back)

		self.ui.comboBoxMonths.currentIndexChanged.connect(self.calc_age)
		self.ui.spinBoxDay.valueChanged.connect(self.calc_age)
		self.ui.spinBoxYear.valueChanged.connect(self.calc_age)

		self.ui.btnMan.clicked.connect(self.btn_man_click)
		self.ui.btnAnimal.clicked.connect(self.btn_animal_click)
		self.ui.btnWoman.clicked.connect(self.btn_woman_click)
		self.ui.btnPeople.clicked.connect(self.btn_people_click)

		self.ui.comboBoxMonths.clear()
		for i in MONTHS:
			self.ui.comboBoxMonths.addItem(i[1], i[0])

	def renew(self, edit_mode=False, item=None):
		self.edit_mode = edit_mode
		self.item = item
		self.img_data = None
		if item:
			self.p_type = item.patient_type

		if self.edit_mode:
			self.ui.btnAccept.setText(u"Сохранить")
			self.ui.lineEditFirstName.setText(self.item.first_name)
			self.ui.lineEditLastName.setText(self.item.last_name)
			self.ui.lineEditMiddleName.setText(self.item.middle_name)
			self.ui.lineEditProfession.setText(self.item.profession)
			self.ui.plainTextEdit.setPlainText(self.item.short_description)

			self.ui.spinBoxDay.setValue(self.item.birthday_date.day)
			self.ui.spinBoxYear.setValue(self.item.birthday_date.year)
			self.ui.comboBoxMonths.setCurrentIndex(self.item.birthday_date.month - 1)

			img = self.item.get_pic()
			img.thumbnail((200, 200))
			self.ui.labelPicture.setPixmap(QtGui.QPixmap.fromImage(ImageQt(img)))

			if self.item.get_type == TYPE_MAN:
				self.btn_people_click()
				if self.item.get_sex == TYPE_FEMALE:
					self.btn_woman_click()
				elif self.item.get_sex == TYPE_MALE:
					self.btn_man_click()

			elif self.item.get_type == TYPE_ANIMAL:
				self.btn_animal_click()


		else:
			self.ui.btnAccept.setText(u"Создать")
			self.ui.lineEditFirstName.setText("")
			self.ui.lineEditLastName.setText("")
			self.ui.lineEditMiddleName.setText("")
			self.ui.lineEditProfession.setText("")
			self.ui.plainTextEdit.setPlainText("")

			self.img_data = None
			self.ui.labelPicture.setText(u"Нет фотографии")
			self.ui.labelPicture.setPixmap(QtGui.QPixmap())

			self.ui.spinBoxDay.setValue(1)
			self.ui.spinBoxYear.setValue(2000)
			self.ui.comboBoxMonths.setCurrentIndex(0)
			self.clear_btns()
			self.change_pic()

	def showEvent(self, *args, **kwargs):
		self.calc_age()
		pass

	def closeEvent(self, e):
		if self.m_parent.start_dia_after_create:
			return
		from applibs.main_ui.main_window import WIDGETS
		self.load_method(WIDGETS['patients'])

	def clear_btns(self):
		self.ui.btnPeople.setEnabled(True)
		self.ui.btnAnimal.setEnabled(True)
		self.ui.btnMan.setEnabled(True)
		self.ui.btnWoman.setEnabled(True)
		self.p_type = None

	def btn_people_click(self):
		self.clear_btns()
		self.ui.btnPeople.setDisabled(True)
		self.btn_man_click()

	def btn_animal_click(self):
		self.clear_btns()
		self.ui.btnAnimal.setDisabled(True)
		self.ui.btnMan.setDisabled(True)
		self.ui.btnWoman.setDisabled(True)

		self.p_type = TYPE_ANIMAL
		self.change_pic()

	def btn_man_click(self):
		self.ui.btnWoman.setEnabled(True)
		self.ui.btnMan.setDisabled(True)
		self.p_type = TYPE_MALE
		self.change_pic()

	def btn_woman_click(self):
		self.ui.btnMan.setEnabled(True)
		self.ui.btnWoman.setDisabled(True)
		self.p_type = TYPE_FEMALE
		self.change_pic()

	def btn_load_picture_clicked(self):
		f = QtGui.QFileDialog.getOpenFileName(
			self,
			'Выберите картинку',
			os.path.expanduser("~"),
			selectedFilter='PNG(*.png);;JPG(*.jpg);;BMP(*.bmp)'
		)

		if f:
			blob_value = open(unicode(f), 'rb').read()
			self.img_data = Patient.to200x200(blob_value)
			pixmap = Qt.QPixmap()
			pixmap.loadFromData(self.img_data)
			self.ui.labelPicture.setPixmap(pixmap)

	def btn_decline_clicked(self):
		self.close()

	def btn_accept_clicked(self):
		if self.edit_mode:
			self.item.first_name = unicode(self.ui.lineEditFirstName.text())
			self.item.last_name = unicode(self.ui.lineEditLastName.text())
			self.item.middle_name = unicode(self.ui.lineEditMiddleName.text())
			self.item.profession = unicode(self.ui.lineEditProfession.text())
			self.item.short_description = unicode(self.ui.plainTextEdit.toPlainText())

			self.item.birthday_date = self.get_birthday
			self.item.patient_type = self.p_type

			if self.img_data:
				self.item.photo = base64.b64encode(self.img_data)

			cur_user_session.add(self.item)

		else:
			new_patient = Patient()
			new_patient.first_name = unicode(self.ui.lineEditFirstName.text())
			new_patient.last_name = unicode(self.ui.lineEditLastName.text())
			new_patient.middle_name = unicode(self.ui.lineEditMiddleName.text())
			new_patient.profession = unicode(self.ui.lineEditProfession.text())
			new_patient.short_description = unicode(self.ui.plainTextEdit.toPlainText())

			new_patient.birthday_date = self.get_birthday
			new_patient.patient_type = self.p_type
			new_patient.doctor = self.m_parent.user

			if self.img_data:
				new_patient.photo = base64.b64encode(self.img_data)

			cur_user_session.add(new_patient)

		cur_user_session.commit()

		if not self.edit_mode and self.m_parent.start_dia_after_create:
			from applibs.main_ui.main_window import WIDGETS

			self.m_parent.start_dia_after_create = False
			self.m_parent.set_patient(new_patient)
			self.load_method(
				self.m_parent.selected_diagnostic[1] if self.m_parent.selected_diagnostic else WIDGETS[
					'all_diagnostics']
			)

		else:
			self.close()

	@property
	def get_birthday(self):
		month = int(self.ui.comboBoxMonths.itemData(self.ui.comboBoxMonths.currentIndex()).toInt()[0])
		if month < 1:
			month = 1
		day = int(self.ui.spinBoxDay.value())
		year = int(self.ui.spinBoxYear.value())
		return date(year, month, day)

	def calc_age(self, *args):
		now = date.today()
		# print dir(self.ui.comboBoxMonths.itemData(self.ui.comboBoxMonths.currentIndex()))
		age = int((now - self.get_birthday).days / 365.25)
		if age < 0:
			age = 0

		self.ui.labelFullYears.setText(str(age))

	def change_pic(self):
		if (self.item and self.item.photo) or self.img_data:
			return

		img = Patient.get_pic_by_type(self.p_type)
		img.thumbnail((200, 200))
		self.ui.labelPicture.setPixmap(QtGui.QPixmap.fromImage(ImageQt(img)))

	def go_back(self):
		from applibs.main_ui.main_window import WIDGETS
		self.m_parent.load_widget(WIDGETS['patients'])
