# -*- coding: utf-8 -*-
from applibs.dao import cur_user_session
import os
import types
from PIL import Image
from PIL.ImageQt import ImageQt
from PyQt4 import QtCore, QtGui, Qt

from sqlalchemy import or_
from sqlalchemy import func

from applibs import _translate as tr, BASE_DIR
from applibs.main_ui.widgets.items.patient_item import PatientListItemWidget
from applibs.models.user_stuff.patient import Patient, IMAGE_BY_PATIENT_TYPE, TYPE_ANONYMOUS
from applibs.simple_logger import SLogger
from applibs.main_ui.widgets.src.patientsList import Ui_Form

logger = SLogger.get_logger()
base_path = os.path.dirname(os.path.abspath(__file__))

try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s


class PatientListWidget(QtGui.QWidget):
	def __init__(self, parent=None, load_method=None):
		super(PatientListWidget, self).__init__(parent)
		self.load_method = load_method
		self.ui = Ui_Form()
		self.ui.setupUi(self)
		self.m_parent = parent
		self.ui.patients_list.setAlignment(QtCore.Qt.AlignTop)
		self.ui.scrollArea.setWidgetResizable(True)

		self.ui.btnSearch.clicked.connect(self.btn_search_click)
		self.ui.lineEditSearch.keyPressEvent = types.MethodType(self.btnSearch_kp, self.ui.lineEditSearch)

		self.ui.btnAddPatient.clicked.connect(self.btn_add_patient_clicked)
		self.ui.btnStartSession.clicked.connect(self.btn_start_session_clicked)

		self.ui.infoPatient.clicked.connect(self.patient_item_btn_info_click)
		self.ui.editPatient.clicked.connect(self.patient_item_btn_edit_click)
		self.ui.deletePatient.clicked.connect(self.patient_item_btn_delete_click)

		self.current_patient = None

	def renew(self):
		self.current_patient = None

	def showEvent(self, *args, **kwargs):
		self.refresh_patients_list()
		self.show_cur_info()

	def closeEvent(self, e):
		pass

	def btnSearch_kp(self, e, event):
		if event.key() == QtCore.Qt.Key_Return:
			self.btn_search_click()
			return

		Qt.QLineEdit.keyPressEvent(e, event)

	def btn_add_patient_clicked(self):
		from applibs.main_ui.main_window import WIDGETS
		self.load_method(WIDGETS['add_edit_patient'], edit_mode=False)

	def btn_start_session_clicked(self):
		if not self.m_parent.try_end_session():
			return

		self.m_parent.cur_patient = self.current_patient
		self.m_parent.selected_diagnostic = None
		self.m_parent.start_session()

	def patient_item_click(self, patient_item):
		for i in reversed(range(self.ui.patients_list.count())):
			_t = self.ui.patients_list.itemAt(i).widget()
			try:
				_t.set_selected(False)
			except:
				pass

		patient_item.set_selected(True)
		self.current_patient = patient_item.item
		self.show_cur_info()

	def patient_item_btn_info_click(self, it=None):
		if self.current_patient is None:
			return

		from applibs.main_ui.main_window import WIDGETS
		self.load_method(WIDGETS['patient_info'], patient=self.current_patient)

	def patient_item_btn_edit_click(self):
		if self.current_patient is None:
			return

		from applibs.main_ui.main_window import WIDGETS
		self.load_method(WIDGETS['add_edit_patient'], edit_mode=True, item=self.current_patient)

	def patient_item_btn_delete_click(self):
		if self.current_patient is None:
			return

		mes = QtGui.QMessageBox.warning(
			self,
			tr(u"Удаление"),
			unicode(tr(u"Вы действительно хотите удалить пациента {0}?")).format(self.current_patient.full_name),
			QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
		if mes != QtGui.QMessageBox.Yes:
			return

		cur_user_session.delete(cur_user_session.query(Patient).filter(Patient.id == self.current_patient.id).first())
		cur_user_session.commit()
		self.current_patient = None
		self.refresh_patients_list()
		self.show_cur_info()

	def btn_search_click(self):
		search = None
		if len(self.ui.lineEditSearch.text()) > 0:
			search = unicode(self.ui.lineEditSearch.text())

		self.refresh_patients_list(search=search)

	def refresh_patients_list(self, search=None):

		for i in reversed(range(self.ui.patients_list.count())):
			item = self.ui.patients_list.itemAt(i).widget()
			item.deleteLater()
			self.ui.patients_list.removeWidget(item)
			del item

		patients_query = cur_user_session.query(Patient).filter(Patient.doctor_id == self.m_parent.user.id)

		if search is not None:
			search_words = search.strip().split(' ')
			if len(search_words) > 0:
				for i in search_words:
					query_list = []
					if len(i) > 1:
						query_list += [
							func.lower(Patient.first_name).like(func.lower(u"%{0}%".format(unicode(i)))),
							func.lower(Patient.last_name).like(func.lower(u"%{0}%".format(unicode(i)))),
							func.lower(Patient.middle_name).like(func.lower(u"%{0}%".format(unicode(i)))),
						]

					patients_query = patients_query.filter(or_(*query_list))

		patients = patients_query.all()

		first = True
		for i in patients:
			pliw = PatientListItemWidget(
				parent=self,
				item=i,
				on_click_item_method=self.patient_item_click,
				on_dblclick_item_method=self.patient_item_btn_info_click,
			)

			self.ui.patients_list.addWidget(pliw)

			if first:
				pliw.item_click()
				first = False
			else:
				pliw.set_selected(False)

	def clear_cur_info(self):
		self.ui.labelName.setText('-')
		self.ui.labelSurname.setText('-')
		self.ui.labelSecondName.setText('-')
		self.ui.labelLastSession.setText('-')

		self.ui.btnStartSession.setDisabled(True)
		self.ui.infoPatient.setDisabled(True)
		self.ui.editPatient.setDisabled(True)
		self.ui.deletePatient.setDisabled(True)

		img = Image.open(
			os.path.join(BASE_DIR, 'resources', 'images', IMAGE_BY_PATIENT_TYPE[TYPE_ANONYMOUS])
		)
		img.thumbnail((150, 150))
		self.ui.labelPatientImg.setPixmap(QtGui.QPixmap.fromImage(ImageQt(img)))

	def show_cur_info(self):
		if self.current_patient is None:
			self.clear_cur_info()
			return

		self.ui.btnStartSession.setEnabled(True)
		self.ui.infoPatient.setEnabled(True)
		self.ui.editPatient.setEnabled(True)
		self.ui.deletePatient.setEnabled(True)

		self.ui.labelName.setText(unicode(self.current_patient.first_name))
		self.ui.labelSurname.setText(unicode(self.current_patient.last_name))
		self.ui.labelSecondName.setText(unicode(self.current_patient.middle_name))

		self.ui.labelLastSession.setText(self.current_patient.last_session_str())

		img = self.current_patient.get_pic()
		img.thumbnail((150, 150))
		self.ui.labelPatientImg.setPixmap(QtGui.QPixmap.fromImage(ImageQt(img)))
