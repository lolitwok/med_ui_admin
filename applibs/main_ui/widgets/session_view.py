# -*- coding: utf-8 -*-
from applibs.dao import cur_user_session
import json
import os
import datetime

from PyQt4 import QtCore, QtGui, Qt

from applibs import _translate as tr
from applibs.main_ui.utils import TREE_ITEMS_TYPE
from applibs.main_ui.widgets.functional_map import local_html
from applibs.models.user_stuff.session_models import DReport, Diagnostic, DiaPoint
from applibs.simple_logger import SLogger
from applibs.main_ui.widgets.src.viewAllDiagnostics import Ui_Form

logger = SLogger.get_logger()
base_path = os.path.dirname(os.path.abspath(__file__))


class SessionsViewWidget(QtGui.QWidget):
	def __init__(self, parent=None, load_method=None):
		super(SessionsViewWidget, self).__init__(parent)
		self.m_parent = parent
		self.load_method = load_method
		self.ui = Ui_Form()
		# self.ui = main_ui_widget()
		self.ui.setupUi(self)

		self.ui.pushButton.clicked.connect(self.show_session)
		self.ui.pushButton_2.clicked.connect(self.print_session)

		self.ui.save_comp_btn.clicked.connect(self.btn_conclusion_save_click)
		self.ui.reset_comp_btn.clicked.connect(self.reset_comp_btn_clicked)

		self.ui.treeWidget.itemChanged.connect(self.on_item_changed)

		self.ui.checkBox.stateChanged.connect(self.on_change_falling)

		self.session = None

		self.ui.webView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

		self.falling_graph_for_points = []

		self.m_parent.report = None

	# self.from_cur_session = False

	def renew(self, item):
		self.session = item
		self.m_parent.report = None
		self.ui.save_comp_btn.show()

	def showEvent(self, *args, **kwargs):
		self.refresh_list()
		self.ui.webView.load(Qt.QUrl(local_html('report.html')))
		self.ui.textEdit.show()

		if not self.session.conclusion:
			self.ui.textEdit.setPlainText(unicode(u"Заключение програмы..."))

		else:
			self.ui.textEdit.setPlainText(unicode(self.session.conclusion))

	def closeEvent(self, e):
		pass

	def refresh_list(self):
		def add_item(p, title, udata=None, tristate=True):
			_item = QtGui.QTreeWidgetItem(p)
			_item.setText(0, title)
			_item.setToolTip(0, title)
			_item.setCheckState(0, QtCore.Qt.Checked)
			_item.setFlags(
				_item.flags() |
				QtCore.Qt.ItemIsUserCheckable |
				QtCore.Qt.ItemIsEnabled
			)
			if tristate:
				_item.setFlags(
					_item.flags() |
					QtCore.Qt.ItemIsTristate
				)

			_item.setData(0, QtCore.Qt.UserRole, udata)
			return _item

		def make_dia_item(parent, dia):
			# points -|:
			# 		  |-> falling
			for i in dia.points:
				nz = i.get_nazods()
				_parent = add_item(parent, i.get_node_name(), i, tristate=False)
				_fbox = add_item(_parent, TREE_ITEMS_TYPE[6][1], 6)
				if nz and isinstance(nz, dict):
					add_item(_parent, TREE_ITEMS_TYPE[7][1], 7)

				self.falling_graph_for_points.append(_fbox)

			# graph1:
			add_item(parent, TREE_ITEMS_TYPE[4][1], 4)
			# graph2:
			add_item(parent, TREE_ITEMS_TYPE[5][1], 5)

		self.falling_graph_for_points = list()
		self.ui.treeWidget.clear()

		if len(self.session.complaints) > 0:
			add_item(self.ui.treeWidget, TREE_ITEMS_TYPE[1][1], 1)

		for n in self.session.diagnostics:
			_p = add_item(self.ui.treeWidget, n.get_name(), n)
			make_dia_item(_p, n)

		add_item(self.ui.treeWidget, TREE_ITEMS_TYPE[2][1], 2)

		if len(self.session.conclusion) > 0:
			add_item(self.ui.treeWidget, TREE_ITEMS_TYPE[3][1], 3)

	def get_kipspedo(self, _parent_dia):
		for i in _parent_dia.points:
			dnode = i.get_dnode()
			if dnode is not None and dnode.is_main:
				return i.middle
		return 0.0

	def show_session(self):
		def parse_point(point_child, _parent_dia, dia_type=None):
			_item = point_child.data(0, QtCore.Qt.UserRole).toPyObject()
			_dnode = _item.get_dnode()
			res = {
				"name": _item.get_node_name(),
				"value": _item.middle,
				"delta": _item.middle - self.get_kipspedo(_parent_dia),
				"description": _dnode.description if _dnode else '',
			}

			if dia_type == 4:
				res['z_value'] = _item.with_nazod_middle

			if dia_type == 5 and point_child.childCount() > 1 and point_child.child(1).data(0, QtCore.Qt.UserRole) == 7:
				if point_child.child(1).checkState(0) == QtCore.Qt.Checked:
					res['map_points'] = _item.get_nazods()

			if point_child.child(0).checkState(0) == QtCore.Qt.Checked:
				res['falling_graph'] = json.loads(_item.best_ex)
				if dia_type == 4:
					res['z_falling_graph'] = json.loads(_item.with_nazod_best_ex)

			main_parent = _dnode.get_main_parent.get_name() if _dnode else False
			return res, main_parent

		def parse_dia(dia_child):
			_dia = dia_child.data(0, QtCore.Qt.UserRole).toPyObject()

			res = {
				"categories": {},
				"name": _dia.get_name(),
				'dia_type': _dia.dia_type,
			}

			for _dc in xrange(dia_child.childCount()):
				if dia_child.child(_dc).checkState(0) == QtCore.Qt.Unchecked:
					continue

				_dia_child_item = dia_child.child(_dc).data(0, QtCore.Qt.UserRole).toPyObject()
				if isinstance(_dia_child_item, DiaPoint):
					_cur_item, _main_name = parse_point(dia_child.child(_dc), _dia, dia_type=_dia.dia_type)
					if _main_name not in res['categories']:
						res["categories"][_main_name] = []

					res["categories"][_main_name].append(_cur_item)
					continue

				if isinstance(_dia_child_item, int):
					if _dia_child_item == 4:
						res['graph1'] = True

					if _dia_child_item == 5:
						res['graph2'] = True

					continue

			return res

		_out = dict()

		_out['dias'] = []

		_out['patient_info'] = {
			'all': unicode(u'%s %s %s' % (
				self.session.patient.first_name if self.session.patient.first_name else "",
				self.session.patient.last_name if self.session.patient.last_name else "",
				self.session.patient.middle_name if self.session.patient.middle_name else "",
			))
		}

		for _ti in xrange(self.ui.treeWidget.topLevelItemCount()):
			if self.ui.treeWidget.topLevelItem(_ti).checkState(0) == QtCore.Qt.Unchecked:
				continue

			_child_item = self.ui.treeWidget.topLevelItem(_ti).data(0, QtCore.Qt.UserRole).toPyObject()
			if isinstance(_child_item, Diagnostic):
				_out["dias"].append(parse_dia(self.ui.treeWidget.topLevelItem(_ti)))
				continue

			if isinstance(_child_item, int):
				if _child_item == 1:
					_out['complaints'] = self.session.complaints[0].text

				if _child_item == 2:
					_out['program_recommendations'] = self.get_program_recommendations().plain_text()

				if _child_item == 3:
					_out['doctor_recommendations'] = self.session.conclusion

				continue

		self.m_parent.report = _out
		self.ui.webView.reload()

	def print_session(self):
		new_report = DReport()
		new_report.file_name = 'report %s.pdf' % datetime.datetime.now().strftime('%d-%m-%Y %H-%M')
		new_report.session = self.session

		new_report.set_data(unicode(json.dumps(self.m_parent.report)))
		cur_user_session.add(new_report)
		cur_user_session.commit()

		new_report.create_new(self.m_parent)
		QtGui.QMessageBox.information(
			self.m_parent,
			tr(u"Отчет"),
			tr(u"Отчет Сохранен."),
			QtGui.QMessageBox.Ok
		)

	def on_item_changed(self, item):
		data = item.data(0, QtCore.Qt.UserRole).toPyObject()
		if isinstance(data, DiaPoint):
			if item.checkState(0) == QtCore.Qt.Unchecked:
				item.child(0).setCheckState(0, QtCore.Qt.Unchecked)

		if isinstance(data, int) and data == 6:
			if item.checkState(0) == QtCore.Qt.Checked:
				item.parent().setCheckState(0, QtCore.Qt.Checked)

			self.check_boxes()

	def set_state(self, state):
		self.ui.checkBox.blockSignals(True)
		self.ui.checkBox.setCheckState(state)
		self.ui.checkBox.blockSignals(False)

	def check_boxes(self):
		any_one_checked = False
		any_one_unchecked = False

		for i in self.falling_graph_for_points:
			if i.parent().checkState(0) == QtCore.Qt.Checked:
				if i.checkState(0) == QtCore.Qt.Checked:
					any_one_checked = True
				else:
					any_one_unchecked = True

		if any_one_checked and any_one_unchecked:
			self.set_state(QtCore.Qt.PartiallyChecked)
		elif any_one_checked:
			self.set_state(QtCore.Qt.Checked)
		else:
			self.set_state(QtCore.Qt.Unchecked)

	def on_change_falling(self, state):
		if state == QtCore.Qt.PartiallyChecked:
			self.ui.checkBox.setCheckState(QtCore.Qt.Checked)
			return

		if state == QtCore.Qt.Checked:
			for i in self.falling_graph_for_points:
				if i.parent().checkState(0) == QtCore.Qt.Checked:
					i.setCheckState(0, QtCore.Qt.Checked)
			return

		for i in self.falling_graph_for_points:
			if i.parent().checkState(0) == QtCore.Qt.Checked:
				i.setCheckState(0, QtCore.Qt.Unchecked)

	def get_program_recommendations(self):
		def add_to_list_by_key(_dict, _key, _value):
			if _key not in _dict:
				_dict[_key] = []

			_dict[_key].append(_value)

		class Recommendations(object):
			def __init__(self, source):
				self.source = source

			def plain_text(self):
				lines = []

				for i in self.source:
					lines.append(u"Диагностика \"%s\":" % i[0])
					for k, v in i[1].iteritems():
						lines.append(u'\t%s:' % DIFFERENCE_TYPES[k][1])
						for j in v:
							lines.append(u"\t\t%s" % j[0])

				return '\n'.join(lines)

			def html(self):
				lines = []
				for i in self.source:
					lines.append(u"Диагностика %s" % i[0])
					for k, v in i[1].iteritems():
						lines.append(u'&emsp;%s:' % DIFFERENCE_TYPES[k][1])
						for j in v:
							lines.append(u"&emsp;&emsp;%s" % j[0])

				return '<br>'.join(lines)

		DIFFERENCE_TYPES = {
			'p15': ((1, 15), unicode(tr(u'15+'))),
			'm15': ((-1, 15), unicode(tr(u'15-'))),
			'p10': ((1, 10), unicode(tr(u'10+'))),
			'm10': ((-1, 10), unicode(tr(u'10-'))),
			'p5': ((1, 5), unicode(tr(u'5+'))),
			'm5': ((-1, 5), unicode(tr(u'5-'))),
		}
		points_list = []

		for ses in self.session.diagnostics:
			if ses.dia_type == 4:
				continue

			f = {}
			count = 0
			for p in ses.points:
				_delta = p.middle - self.get_kipspedo(ses)
				node_name = p.get_node_name()
				for dk, dt in sorted(DIFFERENCE_TYPES.iteritems(), key=lambda x: x[1][0][1], reverse=True):
					if abs(_delta) >= 5:
						count += 1

					if _delta * dt[0][0] >= dt[0][1]:
						add_to_list_by_key(f, dk, (node_name,))
						break

			if count:
				points_list.append((ses.get_name(), f,))

		return Recommendations(source=points_list)

	def btn_conclusion_save_click(self):
		self.session.conclusion = unicode(self.ui.textEdit.toPlainText())
		cur_user_session.add(self.session)
		cur_user_session.commit()

		self.refresh_list()

	def reset_comp_btn_clicked(self):
		self.ui.textEdit.setText(self.get_program_recommendations().plain_text())
