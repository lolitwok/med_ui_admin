# -*- coding: utf-8 -*-
import sys
import copy
import os

from PyQt4 import QtCore
import datetime
from applibs import BASE_DIR, _translate as tr
from applibs.dao import cur_main_session, cur_user_session
from applibs.models.main_models import DNode
from applibs.models.user_stuff.customer_staff import CustomerNozod
from applibs.models.user_stuff.session_models import DiaPoint, Diagnostic, DSession, Complaint, DIAS_NAMES_WITH_ATTEMPT

DIAS_NAMES = {
	3: unicode(tr(u"Функциональная карта")),
	4: unicode(tr(u"Диагностика с нозодами")),
	5: unicode(tr(u"Карта точки")),
}

WITH_NAZODS = unicode(tr(u"c назодами: %s"))

TREE_ITEMS_TYPE = {
	1: (1, unicode(tr(u'Жалобы'))),
	2: (2, unicode(tr(u'Рекомендации программы'))),
	3: (3, unicode(tr(u'Рекомендации врача'))),
	4: (4, unicode(tr(u'График1'))),
	5: (5, unicode(tr(u'График2'))),
	6: (6, unicode(tr(u'Падение на точке'))),
	7: (7, unicode(tr(u'Карта точки'))),
}

DAI_NUMB = {
	'FUNC_MAP': 3,
	'NAZOD': 4,
	'POINT_MAP': 5,
}


class PSignals(QtCore.QObject):
	kipspedo_changed = QtCore.pyqtSignal(bool)

	def __init__(self):
		QtCore.QObject.__init__(self)


signals = PSignals()


def high_priority():
	try:
		import win32api, win32process, win32con

		pid = win32api.GetCurrentProcessId()
		handle = win32api.OpenProcess(win32con.PROCESS_ALL_ACCESS, True, pid)
		win32process.SetPriorityClass(handle, win32process.HIGH_PRIORITY_CLASS)

	except ImportError:
		os.nice(20)


def html_decode(s):
	HTML_CODES = (
		("'", '&#39;'),
		('"', '&quot;'),
		('>', '&gt;'),
		('<', '&lt;'),
		('&', '&amp;'),
		('\n', '<br/>'),
		('\t', '&emsp;'),

	)
	for code in HTML_CODES:
		s = s.replace(code[1], code[0])
	return s


class FakeParent(object):
	id = 0
	name = unicode(tr(u'Препарат'))

	def get_name(self):
		return self.name


def local_html(_file):
	return "file:///%s" % (os.path.join(BASE_DIR, 'templates', _file)).replace('\\', '/')


def middle_value(l):
	if not len(l):
		return 0

	t = 0
	for i in l:
		t += i
	return t / len(l)


def get_selected_nazodes(l):
	return [i for i in l if i[1]]


class PointMeasurementSettings(object):
	def __init__(self):
		self.is_start_measurement = False
		self.is_allow_next_measurement = False
		self.is_allow_measurement = False
		self.is_show_only = False
		self.last_dia_time = 0


class MeasurementArr(object):
	def __init__(self):
		self.points = []
		self.start_time = None
		self.end_time = None
		self._middle_value = None
		self.id = 0

	def __str__(self):
		return tr("Измерение №") + str(self.id)

	def __unicode__(self):
		return unicode(self.__str__())

	@property
	def middle_value(self):
		s = 0
		for i in self.points:
			s += i

		self._middle_value = s / len(self.points)

		return self._middle_value

	@property
	def max_value(self):
		return max(*self.points)

	@property
	def name(self):
		return tr("Измерение №") + str(self.id)


class NodeInfo(object):
	def __init__(self, node, nazods=False):
		self.for_node = node
		self.mea_numb = 0
		self.normal_points = []
		self.points_with_nazod = []
		self.nazod_trigger = False

		self.map_data = {
			't': [],
			'r': [],
			'b': [],
			'l': [],
		}

		self.dia_with_nazods = nazods if nazods is not False else None

		self.cur_measurement = MeasurementArr()

	def append_measurement(self):
		res = False
		if len(self.cur_measurement.points) > 0:
			self.mea_numb += 1
			self.cur_measurement.id = self.mea_numb

			if self.nazod_trigger:
				self.points_with_nazod.append(copy.deepcopy(self.cur_measurement))
			else:
				self.normal_points.append(copy.deepcopy(self.cur_measurement))

			if self.for_node and self.for_node.is_main:
				try:
					signals.kipspedo_changed.emit(True)
				except Exception as e:
					pass
				# print e.message

			res = True

		self.cur_measurement = MeasurementArr()
		return res

	def clear_cur_points(self):
		if self.nazod_trigger:
			self.points_with_nazod = []
		else:
			self.normal_points = []
		if not (self.points_with_nazod or self.normal_points) and self.for_node and self.for_node.is_main:
			signals.kipspedo_changed.emit(False)

	@property
	def normal_points_middle(self):
		_mid = 0.0
		for i in self.normal_points:
			_mid += i.middle_value

		if len(self.normal_points) > 0:
			_mid /= len(self.normal_points)

		return _mid

	@property
	def points_with_nazod_middle(self):
		_mid = 0.0
		for i in self.points_with_nazod:
			_mid += i.middle_value

		if len(self.points_with_nazod) > 0:
			_mid /= len(self.points_with_nazod)

		return _mid

	def get_name(self):
		if self.for_node:
			return self.for_node.get_name()

		titles = [i.get_name() for i in self.dia_with_nazods]
		return unicode(tr(u'Накопитель назодов:') + " " + ", ".join(titles))

	def get_model(self):
		_dp = DiaPoint()
		# dp.diagnostic_id = dia.id
		if self.for_node is None:
			_dp.is_storage = True
		elif isinstance(self.for_node, CustomerNozod):
			_dp.customer_node = self.for_node.name
		else:
			_dp.dnode_guid = self.for_node.guid

		_dp.middle = self.normal_points_middle
		if len(self.normal_points) > 0:
			_dp.best_ex = str(self.get_best_ex())

		if self.dia_with_nazods:
			_dp.with_nazod_middle = self.points_with_nazod_middle
			if len(self.points_with_nazod) > 0:
				_dp.with_nazod_best_ex = str(self.get_with_nazod_best_ex())

			_dp.set_nazods(self.dia_with_nazods)

		if self.is_point_map():
			_dp.set_mapdata(self.get_map_data())

		return _dp

	def get_cur_points(self):
		if self.nazod_trigger:
			return self.points_with_nazod
		return self.normal_points

	def get_cur_middle(self):
		if self.nazod_trigger:
			return self.points_with_nazod_middle
		return self.normal_points_middle

	def get_parents_list(self):
		res = []
		parent_node = cur_main_session.query(DNode).filter(DNode.id == self.for_node.id).first()
		while True:
			res.append((parent_node.id, parent_node.get_name()))
			if parent_node.parrent_guid is None:
				break
			parent_node = cur_main_session.query(DNode).filter(DNode.guid == parent_node.parrent_guid).first()

		return res

	def get_map_data(self):
		d = {
			't': 0,
			'r': 0,
			'b': 0,
			'l': 0,
		}
		for k in self.map_data:
			d[k] = middle_value(self.map_data[k])
		return d

	def is_point_map(self):
		for k in self.map_data:
			if len(self.map_data[k]):
				return True

		return False

	def get_best_ex(self):
		if not self.normal_points:
			return []

		middle = self.normal_points_middle
		_best_points = self.normal_points[0]
		_delta = abs(middle - _best_points.middle_value)
		for _lp in self.normal_points:
			if abs(middle - _lp.middle_value) < _delta:
				_best_points = _lp
				_delta = abs(middle - _best_points.middle_value)

		return _best_points.points

	def get_with_nazod_best_ex(self):
		if not self.points_with_nazod:
			return []

		with_nazod_middle = self.points_with_nazod_middle
		_best_points = self.points_with_nazod[0]
		_delta = abs(with_nazod_middle - _best_points.middle_value)
		for _lp in self.points_with_nazod:
			if abs(with_nazod_middle - _lp.middle_value) < _delta:
				_best_points = _lp
				_delta = abs(with_nazod_middle - _best_points.middle_value)

		return _best_points.points


class DiagnosticInfo(object):
	class CategoryInfo(object):
		node_id = None
		node_name = None
		parent = None
		childs = []

		@property
		def is_last_child(self):
			if len(self.childs) > 0:
				return False

			return True

	def __init__(self, dia_type=None):
		self.nodes = []
		self.dia_type = dia_type
		self.cur_node = None
		self.numb = 1
		self.kipspedo = None
		signals.kipspedo_changed.emit(False)
		self.categories = []

	def __nonzero__(self):
		if self.nodes:
			return True

		return False

	def append_node(self):
		if self.cur_node is None:
			return

		if self.dia_type != 4:
			for i in self.nodes:
				if i.for_node.get_uuid() == self.cur_node.for_node.get_uuid():
					self.nodes.pop(self.nodes.index(i))

		elif self.dia_type == 4:
			_id = self.cur_node.for_node.get_uuid() if self.cur_node.for_node else None
			_in = self.get_node_by_id(_id, nazods=self.cur_node.dia_with_nazods, _copy=False)
			if _in:
				self.nodes.pop(self.nodes.index(_in))

		# if self.cur_node.for_node.is_main:
		# 	self.kipspedo = None

		if not self.cur_node.normal_points and not self.cur_node.points_with_nazod:
			if self.cur_node.for_node and self.cur_node.for_node.is_main:
				self.apply_kipspedo(None)
			return

		self.nodes.append(copy.deepcopy(self.cur_node))
		if self.cur_node.for_node and self.cur_node.for_node.is_main:
			self.apply_kipspedo(self.cur_node.normal_points_middle)

		if self.dia_type != 4:
			p_list = self.cur_node.get_parents_list()

			parent = None

			for i in reversed(p_list):
				childs = self.categories if parent is None else parent.childs

				if i[0] not in [j.node_id for j in childs]:
					n = self.CategoryInfo()
					n.node_id = i[0]
					n.node_name = i[1]
					n.parent = parent

					if parent is not None:
						parent.childs.append(n)
					else:
						self.categories.append(n)
					n.childs = list()
					parent = n
				else:
					for j in childs:
						if j.node_id == i[0]:
							parent = j

		self.cur_node = None

	def get_model(self):
		_dia = Diagnostic()
		_dia.dia_type = self.dia_type
		_dia.numb = self.numb
		cur_user_session.add(_dia)
		cur_user_session.commit()

		for i in self.nodes:
			if (len(i.normal_points) or len(i.points_with_nazod)) == 0:
				continue

			_dp = i.get_model()
			_dp.diagnostic_id = _dia.id
			cur_user_session.add(_dp)

		cur_user_session.commit()
		return _dia

	def get_node_by_id(self, _id, nazods=[], _copy=True):
		def list_equals():
			pass

		if nazods:
			_id = None

		if _id is not None:
			for i in self.nodes:
				if i.for_node and i.for_node.get_uuid() == _id:
					if _copy:
						return copy.deepcopy(i)

					return i
			return False

		nazods_ids = set([i.get_uuid() for i in nazods])
		if len(nazods_ids) > 0:
			for i in self.nodes:
				if isinstance(i.dia_with_nazods, list) and set([j.get_uuid() for j in i.dia_with_nazods]) == nazods_ids:
					if _copy:
						return copy.deepcopy(i)

					return i

		return False

	def del_node_by_id(self, _id):
		for i in self.nodes:
			if i.for_node.get_uuid() != _id:
				continue
			if i.for_node and i.for_node.is_main:
				self.apply_kipspedo(None)

			self.nodes.remove(i)

	def del_category_by_id(self, _id):
		def _del(_list):
			for i in _list:
				if i.node_id == _id:
					# print('removed ', _id)
					i.parent = None
					for j in i.childs:
						j.parent = None

					i.childs = []
					_list.remove(i)
					return

			for i in _list:
				_del(i.childs)

		# print('try removed ', _id)

		_del(self.categories)

	def del_category(self, category):
		def del_req(c):
			for i in c.childs:
				del_req(i)

			self.del_node_by_id(c.node_id)

		del_req(category)
		self.del_category_by_id(category.node_id)

	def search_category(self, search_words):
		def search_f(c):

			_i_words = u' '.join(set([unicode(j).lower() for j in c.node_name.split(' ') if j != '']))
			if any([True for j in words if j in _i_words]):
				res.append(c)

			for _i in copy.deepcopy(c.childs):
				search_f(_i)

		res = []
		words = set([unicode(i).lower() for i in search_words.split(' ') if i != ''])

		for gg in self.categories:
			search_f(gg)

		return res

	def apply_kipspedo(self, value=None):
		old = copy.copy(self.kipspedo)
		self.kipspedo = value

		if type(self.kipspedo) == type(old):
			return

		if self.kipspedo is not None:
			signals.kipspedo_changed.emit(True)
			return
		signals.kipspedo_changed.emit(False)

	def get_name(self):
		return DIAS_NAMES_WITH_ATTEMPT[self.dia_type] % self.numb


class SessionInfo(object):
	def __init__(self):
		self.dias_array = []
		self.cur_patient = None

		self.cur_diagnostic = None

		self.complaints = None
		self.start_date = datetime.datetime.now()

	def append_current(self):
		if self.cur_diagnostic is None:
			return

		for d in self.dias_array:
			if self.cur_diagnostic.dia_type == d.dia_type:
				if self.cur_diagnostic.numb <= d.numb:
					self.cur_diagnostic.numb = d.numb + 1

		self.dias_array.append(self.cur_diagnostic)

		self.cur_diagnostic = None

	def save(self):
		if self.cur_patient is None:
			return
		if not any(self.dias_array):
			return

		sess = DSession()
		sess.date_start = self.start_date
		sess.date_end = datetime.datetime.now()
		sess.patient = self.cur_patient
		sess.session_num = self.cur_patient.last_session_num + 1 if self.cur_patient.last_session_num else 1
		self.cur_patient.last_session_num = sess.session_num

		cur_user_session.add(self.cur_patient)
		cur_user_session.add(sess)
		cur_user_session.commit()

		if self.complaints:
			_complaint = Complaint()
			_complaint.patient = self.cur_patient
			_complaint.text = self.complaints
			_complaint.session = sess
			cur_user_session.add(_complaint)

		for i in self.dias_array:

			if len(i.nodes) == 0:
				continue

			_dia = i.get_model()
			_dia.session = sess
			cur_user_session.add(_dia)

		cur_user_session.commit()
