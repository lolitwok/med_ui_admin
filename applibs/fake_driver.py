import time
import threading


class DriverThread(threading.Thread):
	class BATv2Value(object):
		def __init__(self):
			self.value = self.button1 = self.button2 = self.device_id = None
			self.time = time.time()

		def get_value(self):
			return self.value

		def __str__(self):
			return "value: %s, but1: %s, but2: %s at time:%s" % (self.value, self.button1, self.button2, self.time)

		def __repr__(self):
			return "<BATv2Value %s>" % self.value

	def __init__(self, l, main_window):
		self.values = []
		self.device_id = None
		self.terminated = False
		self.main_window = main_window

		super(DriverThread, self).__init__()


	def auto_init(self, g):
		pass

	def run(self):
		self.main_window.device_status.emit(0)
		import random
		last = 0
		rlist = [0, 0, 0, -4, 10, 12, -20]

		down = 0

		self.main_window.device_status.emit(1)
		self.main_window.device_btns.emit(0)

		while True:
			if self.terminated:
				self.main_window.device_status.emit(0)
				break

			value = self.BATv2Value()
			if down > 50:
				value.value = 0
				down = 0
			else:
				value.value = last + random.choice(rlist)
				down += 1

			if value.value > 100:
				value.value = 100

			if value.value < 0:
				value.value = 0

			last = value.value

			self.values.append(value)
			if len(self.values) > 50:
				del self.values[0]

			self.device_id = 202
			time.sleep(0.05)

	def terminate(self):
		self.terminated = True
