import os


class SLogger(object):
	def __init__(self):
		from applibs import BASE_DIR
		self.file_path = os.path.join(BASE_DIR, "last_log.txt")
		# print "log %s" % self.file_path
		if os.path.exists(self.file_path):
			os.remove(self.file_path)

	def write(self, s):
		# print s
		f = None
		try:
			f = open(self.file_path, 'a')
			f.write(str(s) + "\n")

		except Exception as e:
			# print e
			raise

		finally:
			if f is not None:
				f.close()

	@classmethod
	def get_logger(cls):
		if 'cur' not in cls.__dict__:
			cls.cur = cls()

		return cls.__dict__['cur']
