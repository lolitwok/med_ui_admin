import datetime

from sqlalchemy import *
from applibs.hash import to_md5

from applibs.dao import user_db_engine
from applibs.models.user_stuff.session_models import DiaPoint
from applibs.temp_migrations import add_column


def update():
	col = Column('customer_node', String(255), nullable=True, default=None)
	add_column(user_db_engine, DiaPoint.__tablename__, col)
