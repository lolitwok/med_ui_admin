def add_column(engine, table_name, column):
	column_name = column.compile(dialect=engine.dialect)
	column_type = column.type.compile(engine.dialect)
	engine.execute('ALTER TABLE %s ADD COLUMN %s %s' % (table_name, column_name, column_type))


def apply_migrations():
	from .m0000_diapoint_customer_dnode_add import update as m0000

	# m0000()
	# return True

	return False

