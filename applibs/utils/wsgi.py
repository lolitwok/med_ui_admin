import eventlet
import eventlet.wsgi

data_to_send = []
stop_thread = False


def terminate_server():
	global stop_thread
	stop_thread = True


def reset_terminate_server():
	global stop_thread
	stop_thread = False


def server(
		sock, site,
		log=None,
		environ=None,
		max_size=None,
		max_http_version=eventlet.wsgi.DEFAULT_MAX_HTTP_VERSION,
		protocol=eventlet.wsgi.HttpProtocol,
		server_event=None,
		minimum_chunk_size=None,
		log_x_forwarded_for=True,
		custom_pool=None,
		keepalive=True,
		log_output=True,
		log_format=eventlet.wsgi.DEFAULT_LOG_FORMAT,
		url_length_limit=eventlet.wsgi.MAX_REQUEST_LINE,
		debug=True,
		socket_timeout=None,
		capitalize_response_headers=True):
	serv = eventlet.wsgi.Server(
		sock, sock.getsockname(),
		site, log,
		environ=environ,
		max_http_version=max_http_version,
		protocol=protocol,
		minimum_chunk_size=minimum_chunk_size,
		log_x_forwarded_for=log_x_forwarded_for,
		keepalive=keepalive,
		log_output=log_output,
		log_format=log_format,
		url_length_limit=url_length_limit,
		debug=debug,
		socket_timeout=socket_timeout,
		capitalize_response_headers=capitalize_response_headers,
	)

	global stop_thread

	if server_event is not None:
		server_event.send(serv)
	if max_size is None:
		max_size = eventlet.wsgi.DEFAULT_MAX_SIMULTANEOUS_REQUESTS
	if custom_pool is not None:
		pool = custom_pool
	else:
		pool = eventlet.wsgi.greenpool.GreenPool(max_size)
	try:
		serv.log.info("(%s) wsgi starting up on %s" % (
			serv.pid, eventlet.wsgi.socket_repr(sock)))
		while eventlet.wsgi.is_accepting:
			if stop_thread:
				break

			try:
				client_socket = sock.accept()
				client_socket[0].settimeout(0.5)
				serv.log.debug("(%s) accepted %r" % (
					serv.pid, client_socket[1]))
				try:
					pool.spawn_n(serv.process_request, client_socket)
				except AttributeError:
					eventlet.wsgi.warnings.warn(
						"wsgi's pool should be an instance of "
						"eventlet.greenpool.GreenPool, is %s. Please convert your"
						" call site to use GreenPool instead" % type(pool),
						DeprecationWarning, stacklevel=2
					)
					pool.execute_async(serv.process_request, client_socket)

			except eventlet.wsgi.ACCEPT_EXCEPTIONS as e:
				if eventlet.wsgi.support.get_errno(e) not in eventlet.wsgi.ACCEPT_ERRNO:
					raise

			except (KeyboardInterrupt, SystemExit):
				serv.log.info("wsgi exiting")
				break

	finally:
		pool.waitall()
		serv.log.info("(%s) wsgi exited, is_accepting=%s" % (
			serv.pid, eventlet.wsgi.is_accepting))
		try:
			sock.close()

		except eventlet.wsgi.socket.error as e:
			if eventlet.wsgi.support.get_errno(e) not in eventlet.wsgi.BROKEN_SOCK:
				eventlet.wsgi.traceback.print_exc()
