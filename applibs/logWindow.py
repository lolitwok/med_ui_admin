from PyQt4 import QtCore, QtGui
import os
import logging


# Uncomment below for terminal log messages
# logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(name)s - %(levelname)s - %(message)s')

class QPlainTextEditLogger(logging.Handler):
	def __init__(self, parent):
		super(QPlainTextEditLogger, self).__init__()
		self.widget = QtGui.QPlainTextEdit(parent)
		self.widget.setReadOnly(True)

	def emit(self, record):
		msg = self.format(record)
		from applibs import BASE_DIR

		if record.levelno > logging.INFO:
			self.widget.appendPlainText(msg)

		if record.levelno != logging.INFO:
			with open(os.path.join(BASE_DIR, 'debug.log'), 'a') as f:
				f.write(msg)
				f.write('\n')


class LogDialog(QtGui.QDialog, QPlainTextEditLogger):
	def __init__(self, parent=None):
		super(LogDialog, self).__init__(parent)

		loggers = [
			logging.getLogger(),
			logging.getLogger('flask'),
			logging.getLogger('eventlet'),
			logging.getLogger('sqlalchemy'),
			logging.getLogger('OpenGL'),
			logging.getLogger('PyQt4'),
		]

		self.logTextBox = QPlainTextEditLogger(self)
		self.logTextBox.setFormatter(logging.Formatter('%(asctime)s [%(threadName)s] - %(levelname)s - %(message)s'))
		self.logTextBox.setLevel(logging.DEBUG)
		for l in loggers:
			l.addHandler(self.logTextBox)
			l.setLevel(logging.DEBUG)

		self._button = QtGui.QPushButton(self)
		self._button.setText('Clear')

		layout = QtGui.QVBoxLayout()
		layout.addWidget(self.logTextBox.widget)
		layout.addWidget(self._button)
		self.setLayout(layout)

		self._button.clicked.connect(self.clear)

	def clear(self):
		self.logTextBox.widget.clear()
