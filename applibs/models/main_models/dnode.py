import json
from lxml import etree
from sqlalchemy import *
from sqlalchemy.orm import relationship
from applibs.dao import cur_main_session
from applibs.models.main_models.dimage import DImage
from . import Base

association_table = Table(
	'nazodes_points', Base.metadata,
	Column('d_node_guid', String(36), ForeignKey('d_node.guid')),
	Column('d_point_guid', String(36), ForeignKey('d_point.guid'))
)


class DNode(Base):
	__tablename__ = 'd_node'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	guid = Column(String(36), index=True, unique=True)

	description = Column(Text, default="")
	usable_us_dia = Column(Boolean(), default=True)
	name = Column(String(50))
	name_lower = Column(String(50))
	parrent_guid = Column(String(36), ForeignKey("d_node.guid"))
	parrent = relationship("DNode", foreign_keys=[parrent_guid], lazy='dynamic')

	nazode_points = relationship("DPoint", secondary=association_table)

	d_item_guid = Column(String(36), ForeignKey("d_item.guid"), nullable=True, default=None)
	d_item = relationship("DItem", foreign_keys=[d_item_guid])

	d_template_guid = Column(String(36), ForeignKey("d_template.guid"), nullable=True, default=None)
	d_template = relationship("DTemplate", foreign_keys=[d_template_guid])

	is_main = Column(Boolean(), default=False)

	main_parent = False

	def has_child(self):
		if cur_main_session.query(DNode).filter(DNode.parrent_guid == self.guid).count():
			return True
		return False

	def is_parrent(self, n):
		def check_parrent_guid(self_p_id, p_guid):
			if self_p_id is None:
				return False

			_t = cur_main_session.query(DNode).filter(DNode.guid == self_p_id).first()
			if _t is None:
				return False

			if _t.guid == p_guid:
				return True

			return check_parrent_guid(_t.parrent_guid, p_guid)

		return check_parrent_guid(self.parrent_guid, n.guid)

	def delete_self(self, commit=False):
		items = cur_main_session.query(DNode).filter(DNode.parrent_guid == self.guid).all()

		if items is not None:
			for i in items:
				i.delete_self()

		cur_main_session.query(DImage).filter(DImage.d_node_guid == self.guid).delete()
		cur_main_session.query(DNode).filter(DNode.guid == self.guid).delete()

		if commit:
			cur_main_session.commit()

	@property
	def get_main_parent(self):
		def get_parent(node_guid):
			t_cur_node = cur_main_session.query(DNode).filter(DNode.guid == node_guid).first()
			if t_cur_node.parrent_guid is None:
				return t_cur_node

			return get_parent(t_cur_node.parrent_guid)

		if self.main_parent is False:
			self.main_parent = get_parent(self.guid)

		return self.main_parent

	def get_name(self):
		return unicode(self.name)

	def get_uuid(self):
		return self.id

	def __repr__(self):
		return "<DNode('%s','%s')>" % (self.guid, self.name)

	def dump_xls(self):
		_el = etree.Element('dNode', id="dn%s" % self.id)
		etree.SubElement(_el, 'guid').text = self.guid
		etree.SubElement(_el, 'name').text = self.name
		etree.SubElement(_el, 'nameLower').text = self.name_lower
		etree.SubElement(_el, 'description').text = self.description
		etree.SubElement(_el, 'usableUsDia').text = str(self.usable_us_dia)
		etree.SubElement(_el, 'parrentGuid').text = self.parrent_guid
		etree.SubElement(_el, 'dItemGuid').text = self.d_item_guid
		etree.SubElement(_el, 'dTemplateGuid').text = self.d_template_guid
		etree.SubElement(_el, 'isMain').text = str(self.is_main)

		return _el

	@classmethod
	def load_xls(cls, xml_element):
		e = cls()
		e.guid = xml_element.find('guid').text
		e.name = xml_element.find('name').text
		e.name_lower = xml_element.find('nameLower').text
		e.description = xml_element.find('description').text
		e.usable_us_dia = True if xml_element.find('usableUsDia').text == 'True' else False
		e.parrent_guid = xml_element.find('parrentGuid').text
		e.d_item_guid = xml_element.find('dItemGuid').text
		e.d_template_guid = xml_element.find('dTemplateGuid').text
		e.is_main = True if xml_element.find('isMain').text == 'True' else False

		return e
