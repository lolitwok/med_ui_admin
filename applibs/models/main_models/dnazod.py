# -*- coding: utf-8 -*-

from sqlalchemy import *
from sqlalchemy.orm import relationship
from . import Base
from applibs.dao import MainSession

session = MainSession()

association_table = Table(
	'nazods_templates', Base.metadata,
	Column('d_template_guid', Integer, ForeignKey('d_template.guid')),
	Column('d_nazod_guid', Integer, ForeignKey('d_nazod.guid'))
)


class DNazod(Base):
	__tablename__ = 'd_nazod'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	guid = Column(String(36), index=True, unique=True)

	name = Column(String(50))

	d_template_guid = Column(Integer, ForeignKey("d_template.guid"))
	d_template = relationship("DTemplate", foreign_keys=[d_template_guid])

	def __init__(self):
		pass

	def __repr__(self):
		return "<DNazod(name: %s)>" % self.name

	def __str__(self):
		return self.name

	def __unicode__(self):
		return unicode(self.name)


class DTemplate(Base):
	__tablename__ = 'd_template'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	guid = Column(String(36), index=True, unique=True)
	name = Column(String(50))
	is_default = Column(Boolean, default=False)

	nazode_points = relationship("DNazod", secondary=association_table)

	def __init__(self):
		pass

	def __repr__(self):
		return "<DTemplate(name: %s)>" % self.name

	def __str__(self):
		return self.name

	def __unicode__(self):
		return unicode(self.name)
