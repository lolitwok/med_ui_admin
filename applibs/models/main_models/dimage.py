import base64
from sqlalchemy import *
from sqlalchemy.orm import relationship
from lxml import etree

from . import Base
from applibs.dao import MainSession

session = MainSession()


class DImage(Base):
	__tablename__ = 'd_image'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	guid = Column(String(36), index=True, unique=True)
	description = Column(Text, default="")
	d_node_guid = Column(String(36), ForeignKey("d_node.guid"))
	d_node = relationship("DNode", foreign_keys=[d_node_guid])
	image = Column(BLOB)

	def image_to_base32(self):
		return base64.b32encode(self.image)

	def dump_xls(self):
		_el = etree.Element('dImage', id="dm%s" % self.id)
		etree.SubElement(_el, 'guid').text = self.guid
		etree.SubElement(_el, 'description').text = self.description
		etree.SubElement(_el, 'dNodeGuid').text = self.d_node_guid if self.d_node_guid else ''
		etree.SubElement(_el, 'imageSrc').text = self.image_to_base32()
		return _el

	@classmethod
	def load_xls(cls, xml_element):
		e = cls()
		e.guid = xml_element.find('guid').text
		e.description = xml_element.find('description').text
		e.d_node_guid = xml_element.find('dNodeGuid').text
		e.image = base64.b32decode(xml_element.find('imageSrc').text)
		return e

	def __init__(self):
		pass

	def __repr__(self):
		return "<Node('%s','%s')>" % (self.id, self.name)
