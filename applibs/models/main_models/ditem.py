# -*- coding: utf-8 -*-

from sqlalchemy import *
from sqlalchemy.orm import relationship
from lxml import etree

from . import Base
from applibs.dao import MainSession
import uuid

session = MainSession()


class DItem(Base):
	__tablename__ = 'd_item'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	guid = Column(String(36), index=True, unique=True)
	type = Column(String(20))
	name = Column(String(50))

	def __init__(self):
		pass

	def __repr__(self):
		return "<DItem('%s','%s')>" % (self.id, self.name)

	def __unicode__(self):
		return u"%s,%s" % (self.id, self.name)

	@classmethod
	def add_new(cls, name, _type):
		_n = cls()
		_n.name = name
		_n.guid = str(uuid.uuid4())
		_n.type = _type
		session.add(_n)
		session.commit()
		return _n.id

	@classmethod
	def rename(cls, _id, name):
		_n = session.query(cls).filter(cls.id == _id).first()
		if _n is None:
			return False

		_n.name = name
		session.add(_n)
		session.commit()
		return True

	@classmethod
	def delete(cls, _id):
		session.query(cls).filter(cls.id == _id).delete()

	def dump_xls(self):
		_el = etree.SubElement('dItem', id="di%s" % self.id)
		etree.SubElement(_el, 'guid').text = self.guid
		etree.SubElement(_el, 'type').text = self.type
		etree.SubElement(_el, 'name').text = self.name
		return _el

	@classmethod
	def load_xls(cls, xml_element):
		e = cls()
		e.guid = xml_element.find('guid').text
		e.type = xml_element.find('type').text
		e.name = xml_element.find('name').text

		return e
