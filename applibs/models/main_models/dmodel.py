from lxml import etree
from sqlalchemy import *
from sqlalchemy.orm import relationship
from . import Base
from applibs.dao import MainSession

session = MainSession()


class DModel(Base):
	__tablename__ = 'd_model'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	guid = Column(String(36), index=True, unique=True)
	name = Column(String(50))
	file_name = Column(String(40))

	def __init__(self):
		pass

	def __str__(self):
		return self.name

	def __repr__(self):
		return "<DModel('%s','%s')>" % (self.guid, self.name)

	def dump_xls(self):
		_el = etree.Element('dItem', id="di%s" % self.id)
		etree.SubElement(_el, 'guid').text = self.guid
		etree.SubElement(_el, 'name').text = self.name
		etree.SubElement(_el, 'fileName').text = self.file_name
		return _el

	@classmethod
	def load_xls(cls, xml_element):
		e = cls()
		e.guid = xml_element.find('guid').text
		e.name = xml_element.find('name').text
		e.file_name = xml_element.find('fileName').text
		return e
