# -*- coding: utf-8 -*-

from sqlalchemy import *
from sqlalchemy.orm import relationship
from . import Base
from applibs.dao import MainSession
from lxml import etree

session = MainSession()


class DPoint(Base):
	__tablename__ = 'd_point'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	guid = Column(String(36), index=True, unique=True)

	x = Column(Float(12))
	y = Column(Float(12))
	z = Column(Float(12))
	r = Column(Float(12), nullable=True)

	distance_x = Column(Float(12), nullable=True)
	distance_y = Column(Float(12), nullable=True)
	distance_z = Column(Float(12), nullable=True)
	distance_a = Column(Float(12), nullable=True)

	distance_x_offset = Column(Float(12), nullable=True)
	distance_y_offset = Column(Float(12), nullable=True)
	distance_z_offset = Column(Float(12), nullable=True)

	d_node_guid = Column(String(36), ForeignKey("d_node.guid"))
	d_node = relationship("DNode", foreign_keys=[d_node_guid])

	model_guid = Column(String(36), ForeignKey("d_model.guid"), nullable=True, default=None)
	model = relationship("DModel", foreign_keys=[model_guid])

	def __init__(self):
		pass

	def __repr__(self):
		return "<DPoint(x:%s, y:%s, z:%s)>" % (self.x, self.y, self.z)

	def __str__(self):
		return "Point (x:%s, y:%s, z:%s)" % (self.x, self.y, self.z)

	def __unicode__(self):
		return u"Точка (x:%s, y:%s, z:%s)" % (self.x, self.y, self.z)

	def dump_xls(self):
		_el = etree.Element('dPoint', id="dp%s" % self.id)
		etree.SubElement(_el, 'guid').text = self.guid

		etree.SubElement(
			_el,
			'point',
			x=str(self.x),
			y=str(self.y),
			z=str(self.z),
			r=str(self.r)
		)

		etree.SubElement(
			_el,
			'distance',
			x=str(self.distance_x),
			y=str(self.distance_y),
			z=str(self.distance_z),
			a=str(self.distance_a)
		)

		etree.SubElement(
			_el, 'offset',
			x=str(self.distance_x_offset),
			y=str(self.distance_y_offset),
			z=str(self.distance_z_offset)
		)

		etree.SubElement(_el, 'dNodeGuid').text = self.d_node_guid if self.d_node_guid else ''
		etree.SubElement(_el, 'dModelGuid').text = self.model_guid if self.model_guid else ''
		return _el

	@classmethod
	def load_xls(cls, xml_element):
		e = cls()
		e.guid = xml_element.find('guid').text

		point = xml_element.find('point')
		e.x = float(point.get('x'))
		e.y = float(point.get('y'))
		e.z = float(point.get('z'))
		e.r = float(point.get('r'))

		distance = xml_element.find('distance')
		e.distance_x = float(distance.get('x'))
		e.distance_y = float(distance.get('y'))
		e.distance_z = float(distance.get('z'))
		e.distance_a = float(distance.get('a'))

		offset = xml_element.find('offset')
		e.distance_x_offset = float(offset.get('x'))
		e.distance_y_offset = float(offset.get('y'))
		e.distance_z_offset = float(offset.get('z'))

		e.d_node_guid = xml_element.find('dNodeGuid').text
		e.model_guid = xml_element.find('dModelGuid').text

		return e

	def to_gl_point_dict(self):
		return {
			'x': self.x,
			'y': self.y,
			'z': self.z,
			'radius': self.r,

			'distance_x': self.distance_x,
			'distance_y': self.distance_y,
			'distance_z': self.distance_z,
			'distance_length': self.distance_a,

			'distance_x_offset': self.distance_x_offset,
			'distance_y_offset': self.distance_y_offset,
			'distance_z_offset': self.distance_z_offset,

		}
