# -*- coding: utf-8 -*-

from sqlalchemy import *
from sqlalchemy.orm import relationship
from . import Base
from applibs.dao import UserSession

session = UserSession()


class CustomerNozod(Base):
	__tablename__ = 'customer_nozods'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	name = Column(String(255))
	name_lower = Column(String(255))
	description = Column(TEXT(), nullable=True)
	user_id = Column(Integer, ForeignKey("user.id"))
	user = relationship("User", foreign_keys=[user_id])

	@property
	def is_main(self):
		return False

	@property
	def usable_us_dia(self):
		return True

	@property
	def get_main_parent(self):
		from applibs.main_ui.utils import FakeParent
		return FakeParent()

	@property
	def parrent_guid(self):
		return False

	def has_child(self):
		return False

	def get_uuid(self):
		return self.id + 100000

	def __init__(self):
		pass

	def __repr__(self):
		return "<CustomerNozod(id:%s, name:%s, user_id:%s)>" % (self.id, self.name, self.user_id)

	def __str__(self):
		return self.name

	def __unicode__(self):
		return u"Препарат \"%s\"" % self.name

	def get_name(self):
		return unicode(self)
