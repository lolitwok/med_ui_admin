# -*- coding: utf-8 -*-
import cStringIO
from applibs import BASE_DIR, _translate as tr
from applibs.models.user_stuff.session_models import DSession
import base64
import json
from sqlalchemy import *
import sqlalchemy
from sqlalchemy.orm import relationship
from . import Base
from applibs.dao import UserSession, cur_user_session
from PIL import Image
import os

session = UserSession()

PATIENT_TYPE = {
	-1: unicode(tr("Анонимный пациент")),
	0: unicode(tr("Мужчина")),
	1: unicode(tr("Женщина")),
	2: unicode(tr("Животное")),
	10: unicode(tr("Человек")),
}

TYPE_ANONYMOUS = -1
TYPE_MALE = 0
TYPE_FEMALE = 1
TYPE_ANIMAL = 2
TYPE_MAN = 10

IMAGE_BY_PATIENT_TYPE = {
	TYPE_ANONYMOUS: 'anonymous-user.png',
	TYPE_ANIMAL: 'animal.png',
	TYPE_MALE: 'male-user.png',
	TYPE_FEMALE: 'female-user.png',
}


class Patient(Base):
	__tablename__ = 'd_patient'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer(), primary_key=True, autoincrement=True)
	first_name = Column(String(50), default='')
	last_name = Column(String(50), default='')
	middle_name = Column(String(50), nullable=True, default='')
	photo = Column(BLOB, nullable=True)
	patient_type = Column(Integer())
	birthday_date = Column(Date(), nullable=True)

	profession = Column(String(128), nullable=True, default='')
	short_description = Column(Text(), default='')
	anamnesis = Column(Text(), default='')

	sessions = relationship("DSession", back_populates="patient", lazy='dynamic', )

	doctor_id = Column(Integer, ForeignKey('user.id'))
	doctor = relationship("User", foreign_keys=[doctor_id])

	last_session_num = Column(Integer(), default=0)

	@property
	def get_type(self):
		if self.patient_type < 2:
			return TYPE_MAN

		return TYPE_ANIMAL

	@property
	def get_sex(self):
		if self.patient_type == TYPE_MALE:
			return TYPE_MALE

		if self.patient_type == TYPE_FEMALE:
			return TYPE_FEMALE

		return None

	@staticmethod
	def to200x200(data):
		input_file = cStringIO.StringIO(data)
		img = Image.open(input_file)
		img.thumbnail((256, 256))
		output_file = cStringIO.StringIO()
		img.save(output_file, format='PNG')
		return output_file.getvalue()

	def set_photo(self, img_data):
		self.photo = self.to200x200(img_data)
		cur_user_session.add(self)
		cur_user_session.commit()

	@property
	def anamnes_list(self):
		if not self.anamnesis:
			return []

		return json.loads(self.anamnesis)

	def set_anamnes(self, value=None):
		if not value:
			value = []
		self.anamnesis = json.dumps(value)

	def get_pic(self):
		if self.photo:
			# input_file = cStringIO.StringIO(self.photo)
			return Image.open(cStringIO.StringIO(base64.b64decode(self.photo)))

		return self.get_pic_by_type(self.patient_type)

	@property
	def full_name(self):
		return unicode(
			u"%s %s %s" % (
				unicode(self.first_name),
				unicode(self.last_name),
				unicode(self.middle_name),
			)
		)

	def __init__(self):
		pass

	def __repr__(self):
		return "r<Patient %s>" % self.id

	def __str__(self):
		return "<Patient %s>" % self.id

	def __unicode__(self):
		return u"u<Patient %s>" % self.id

	@staticmethod
	def get_pic_by_type(t):
		if t in [
			TYPE_ANIMAL,
			TYPE_FEMALE,
			TYPE_MALE,
		]:
			return Image.open(
				os.path.join(BASE_DIR, 'resources', 'images', IMAGE_BY_PATIENT_TYPE[t])
			)

		return Image.open(
			os.path.join(BASE_DIR, 'resources', 'images', IMAGE_BY_PATIENT_TYPE[TYPE_ANONYMOUS])
		)

	def last_session_str(self):
		last_session = cur_user_session.query(DSession) \
			.filter(DSession.patient_id == self.id) \
			.order_by(sqlalchemy.desc(DSession.session_num)) \
			.first()

		if last_session is not None:
			return unicode(last_session.date_end.strftime('%d-%m-%Y %H:%M'))

		return "-"


class PatientFile(Base):
	__tablename__ = 'd_patient_file'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer(), primary_key=True, autoincrement=True)
	file_name = Column(String(255))
	file_store = Column(String(255))
	patient_id = Column(Integer, ForeignKey("d_patient.id"))
	patient = relationship("Patient", foreign_keys=[patient_id])

	session_id = Column(Integer, ForeignKey('d_session.id'))
	session = relationship("DSession", foreign_keys=[session_id])

	def __init__(self):
		pass

	def __str__(self):
		return "<PatientFile %s>" % self.id
