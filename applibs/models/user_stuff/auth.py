import datetime

from sqlalchemy import *

from applibs.hash import to_md5

from . import Base


class User(Base):
	__tablename__ = 'user'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	login = Column(String(100))
	name = Column(String(100), nullable=True, default='')
	lastname = Column(String(100), nullable=True, default='')
	patronymic = Column(String(100), nullable=True, default='')
	password = Column(String(32))
	birthday = Column(Date, default=datetime.datetime.utcnow)
	admin = Column(String(64), nullable=True, default='')
	sex = Column(String(20), nullable=True, default='')

	def __init__(self, login, password):
		self.login = login
		self.set_password(password)

	def __repr__(self):
		return "<User('%s','%s', '%s')>" % (self.login, self.name or "", self.password)

	def age(self):
		return (datetime.datetime.now().date() - self.birthday).year

	def check_password(self, passwd):
		return to_md5(passwd) == self.password

	def set_password(self, passwd):
		self.password = to_md5(passwd)

	@property
	def is_admin(self):
		if not self.admin:
			return False

		if to_md5("ad%sadm" % self.login) == self.admin:
			return True
		return False

	def set_adm(self):
		self.admin = to_md5("ad%sadm" % self.login)
