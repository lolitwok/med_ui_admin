# -*- coding: utf-8 -*-
import datetime
from applibs import BASE_DIR, _translate as tr
from applibs.models.main_models.dnode import DNode
import base64
import json
import pdfkit
from sqlalchemy import *
from sqlalchemy.orm import relationship
from . import Base
from applibs.dao import UserSession, cur_main_session
import subprocess
import os
import sys

session = UserSession()

DIAS_NAMES_WITH_ATTEMPT = {
	3: unicode(tr(u"Функциональная карта (попытка №%s)")),
	4: unicode(tr(u"Диагностика с назодами (попытка №%s)")),
	5: unicode(tr(u"Карта точки (попытка №%s)")),
}


class Diagnostic(Base):
	__tablename__ = 'd_diagnostic'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer(), primary_key=True, autoincrement=True)
	numb = Column(Integer(), default=1)
	dia_type = Column(Integer())
	points = relationship("DiaPoint", back_populates="diagnostic")
	session_id = Column(Integer, ForeignKey('d_session.id'), nullable=True)
	session = relationship("DSession", foreign_keys=[session_id])

	def __init__(self):
		pass

	def __str__(self):
		return "<Diagnostic %s>" % self.id

	def get_name(self):
		return DIAS_NAMES_WITH_ATTEMPT[self.dia_type] % self.numb


class DiaPoint(Base):
	__tablename__ = 'd_diapoint'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer(), primary_key=True, autoincrement=True)
	diagnostic_id = Column(Integer, ForeignKey('d_diagnostic.id'), nullable=True)
	diagnostic = relationship("Diagnostic", foreign_keys=[diagnostic_id])

	is_storage = Column(Boolean, default=False)

	dnode_guid = Column(String(40), nullable=True)

	customer_node = Column(String(255), nullable=True, default=None)

	middle = Column(Float(), default=0.0)
	with_nazod_middle = Column(Float(), default=0.0)
	best_ex = Column(Text(), default="[]")
	with_nazod_best_ex = Column(Text(), default="[]")

	with_nazods = Column(Text(), default="[]")

	from_category = False

	def set_nazods(self, _list=None):
		if not _list:
			return

		tmp = [(i.id, i.get_name()) for i in _list]

		self.with_nazods = json.dumps(tmp)

	def get_dnode(self):
		return cur_main_session.query(DNode).filter(DNode.guid == self.dnode_guid).first()

	def get_nazods(self):
		return json.loads(self.with_nazods)

	def __init__(self):
		pass

	def __str__(self):
		return "<DiaPoint %s>" % self.id

	def set_mapdata(self, d):
		self.with_nazods = json.dumps(d)

	def get_node_name(self):
		dnode = self.get_dnode()
		if dnode:
			return unicode(self.get_dnode().name)

		if self.customer_node:
			return unicode(tr(u'Препарат "%s"')) % self.customer_node

		titles = [nz_item[1] for nz_item in self.get_nazods()]
		return unicode(tr(u'Накопитель назодов:') + " " + ", ".join(titles))


class Complaint(Base):
	__tablename__ = 'd_complaint'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer(), primary_key=True, autoincrement=True)

	patient_id = Column(Integer, ForeignKey("d_patient.id"))
	patient = relationship("Patient", foreign_keys=[patient_id])

	session_id = Column(Integer, ForeignKey('d_session.id'), nullable=True, default=None)
	session = relationship("DSession", foreign_keys=[session_id])

	text = Column(Text, default='')

	def __init__(self):
		pass

	def __str__(self):
		return "<Diagnostic %s>" % self.id


class DSession(Base):
	__tablename__ = 'd_session'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer(), primary_key=True, autoincrement=True)
	patient_id = Column(Integer, ForeignKey("d_patient.id"), nullable=True, default=None)
	patient = relationship("Patient", foreign_keys=[patient_id])

	files = relationship("PatientFile", back_populates="session")

	diagnostics = relationship("Diagnostic", back_populates="session")

	complaints = relationship("Complaint", back_populates="session")

	date_start = Column(DateTime(), default=datetime.datetime.now)
	date_end = Column(DateTime(), default=None, nullable=True)

	program_conclusion = Column(Text, default='')
	conclusion = Column(Text, default='')

	session_num = Column(Integer(), default=0)

	def __init__(self):
		pass

	def __repr__(self):
		return "r<Session %s>" % self.id

	def __str__(self):
		return "<Session %s>" % self.id

	def __unicode__(self):
		return u"u<Session %s>" % self.id

	def get_name(self):
		return unicode(tr(u"Сеанс №%s")) % self.session_num


class DReport(Base):
	__tablename__ = 'd_report'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer(), primary_key=True, autoincrement=True)

	session_id = Column(Integer, ForeignKey('d_session.id'), nullable=True)
	session = relationship("DSession", foreign_keys=[session_id])

	file_name = Column(Text(), default="")

	data = Column(BLOB(), default="")

	date = Column(DateTime(), default=datetime.datetime.now)

	def __init__(self):
		pass

	def __repr__(self):
		return "r<DReport %s>" % self.id

	def __str__(self):
		return "<DReport %s>" % self.id

	def __unicode__(self):
		return u"u<DReport %s>" % self.id

	def set_data(self, value):
		self.data = base64.b64encode(value.encode('utf-8'))

	def get_data(self):
		if self.data:
			return unicode(base64.b64decode(self.data).decode('utf-8'))

		return ''

	def is_exists(self):
		return os.path.exists(self.get_full_path())

	def try_show_in_folder(self):
		if self.is_exists():
			import subprocess
			subprocess.Popen(r'explorer /select,"%s"' % self.get_full_path())
			return True

		return False

	def try_open(self):
		if not self.is_exists():
			return False

		file_path = self.get_full_path()

		try:
			if sys.platform.startswith('darwin'):
				subprocess.call(('open', file_path))
			elif os.name == 'nt':
				os.startfile(file_path)
			elif os.name == 'posix':
				subprocess.call(('xdg-open', file_path))
		except Exception:
			return False

		return True

	def get_full_path(self):
		return os.path.join(BASE_DIR, 'reports', self.file_name)

	def create_new(self, mw):
		if self.is_exists():
			try:
				os.remove(self.get_full_path())
			except Exception:
				return False

		config = pdfkit.configuration(wkhtmltopdf=os.path.join(BASE_DIR, 'other', 'wkhtmltopdf.exe'))
		mw.report = json.loads(self.get_data())
		pdfkit.from_file(
			os.path.join(BASE_DIR, 'templates', 'report.html'),
			self.get_full_path(),
			configuration=config,
			options={
				'javascript-delay': 10,
				# 'window-status': 'title',
			})
		return True
