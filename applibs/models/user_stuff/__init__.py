from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

from .auth import User
from .patient import Patient, PatientFile

from .customer_staff import CustomerNozod

from .session_models import (
	DSession,
	DiaPoint,
	Diagnostic,
	DReport,
	Complaint,
)
