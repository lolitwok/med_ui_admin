# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import os
import sys
import logging

_base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = _base_dir.decode('cp1251') if sys.platform.startswith('win') else _base_dir

try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s

try:
	_encoding = QtGui.QApplication.UnicodeUTF8


	def _translate(text, context='@default', disambig=None):
		return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
	def _translate(text, context='@default', disambig=None):
		return QtGui.QApplication.translate(context, text, disambig)
