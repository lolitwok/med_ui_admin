import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from applibs import BASE_DIR

DB_FILE1 = "static.sqlite"
DB_FILE2 = "user.sqlite"

connection_string1 = r'sqlite:///%s?check_same_thread=False' % os.path.join(BASE_DIR, DB_FILE1)
connection_string2 = r'sqlite:///%s?check_same_thread=False' % os.path.join(BASE_DIR, DB_FILE2)

main_db_engine = create_engine(connection_string1, echo=False)
user_db_engine = create_engine(connection_string2, echo=False)

MainSession = sessionmaker(bind=main_db_engine)
UserSession = sessionmaker(bind=user_db_engine)

cur_main_session = scoped_session(MainSession)
cur_user_session = scoped_session(UserSession)
