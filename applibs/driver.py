import time
import sys
import serial
import serial.tools.list_ports
import threading

coms = []


def logger(e):
	pass


# print "[x] Error %s" % e


class DriverThread(threading.Thread):
	""" class DriverThread"""

	class BATValue(object):
		def __init__(self, res):
			self.version = 1
			self.wait_time = 0.15
			self.value = self.button1 = self.button2 = self.device_id = None
			self.time = time.time()
			if len(res) == 11 and res[0:3] == "BAT":
				self.value = ord(res[3])
				self.time = time.time()
				self.button1 = res[4] == 'Y'
				self.button2 = res[5] == 'Y'
				self.device_id = (ord(res[9]) << 8) | ord(res[10])

		def get_value(self):
			return self.value

		def __str__(self):
			return "value: %s, but1: %s, but2: %s at time:%s" % (self.value, self.button1, self.button2, self.time)

		def __repr__(self):
			return "<BATValue %s>" % self.value

	class BATv2Value(object):
		def __init__(self, res):
			self.version = 2
			self.wait_time = 0.04
			self.value = self.button1 = self.button2 = self.device_id = None
			self.time = time.time()
			self.max_value = 100000
			self.min_value = 0

			# print ord(res[0]), ord(res[1]),(ord(res[2]) << 8) | ord(res[3]) , (ord(res[4]) << 8) | ord(res[5])

			if len(res) == 6 and ord(res[0]) == 67:
				self.value = (ord(res[2]) << 8) | ord(res[3])
				self.time = time.time()
				self.button1 = bool(ord(res[1]) & 0b00000001)
				self.button2 = bool(ord(res[1]) & 0b00000010)
				self.device_id = (ord(res[4]) << 8) | ord(res[5])

		def get_value(self):
			val = int(float(self.value - self.min_value) / float(self.max_value - self.min_value) * 100.0)
			return val if val <= 100 else 100

		def __str__(self):
			return "value: %s, but1: %s, but2: %s at time:%s" % (self.value, self.button1, self.button2, self.time)

		def __repr__(self):
			return "<BATv2Value %s>" % self.value

	def __init__(self, l, port=None, main_window=None):
		self.logger = l
		self.auto = False
		self.opened_port = None
		self.values = []
		self.device_id = None
		self.terminated = False
		self.wait_time = 1
		self.use_version = 1
		self.min = 0
		self.max = 100000
		self.main_window = main_window

		super(DriverThread, self).__init__()
		if port is not None:
			try:
				self.port = serial.Serial(
					port=port,
					baudrate=19200,
					parity=serial.PARITY_NONE,
					stopbits=serial.STOPBITS_ONE,
					bytesize=serial.EIGHTBITS
				)
			except serial.SerialException as e:
				self.logger.write(e)
				raise e
		else:
			self.port = None

	def auto_init(self, b):
		self.auto = b

	def isAlive(self):
		return self.port.is_open if self.port is not None else False

	def run(self):
		btn1 = False
		btn2 = False

		self.main_window.device_status.emit(0)
		self.main_window.device_btns.emit(0)

		while True:
			if self.terminated:
				if self.port is not None:
					self.port.close()
				break
			self.wait_time = 0.5

			if (self.port is None or not self.port.isOpen()) and self.auto:
				self.opened_port = None
				time.sleep(0.3)
				port, v, t = self.detect(self.logger)
				if port is not None:
					try:
						self.port = serial.Serial(
							port=port,
							baudrate=19200,
							parity=serial.PARITY_NONE,
							stopbits=serial.STOPBITS_ONE,
							bytesize=serial.EIGHTBITS
						)

						self.wait_time = t
						self.use_version = v

					except serial.SerialException as e:
						self.logger.write(e)

				else:
					time.sleep(1)
					continue

			if self.test_device() is None:
				continue

			self.main_window.device_status.emit(1)

			while True:
				if self.terminated:
					if self.port is not None:
						self.port.close()
					break

				if self.port is not None and self.port.isOpen():
					try:
						self.opened_port = self.port.port

					except serial.SerialException as e:
						self.port.close()
						self.logger.write(e)
						self.opened_port = None
				else:
					self.port = None
					break

				res = self.get_normal_resp()

				if res is None:
					break

				if res:
					value = self.get_value(res)
					if self.use_version == 2:
						value.min_value = self.min
						value.max_value = self.max

					if value.get_value() is not None:

						if (btn1 and not value.button1) or (btn2 and not value.button2):
							self.main_window.device_btns.emit(0)
						if value.button1 or value.button2:
							print value.button1, value.button2

						if not btn1 and value.button1:
							self.main_window.device_btns.emit(1)
						btn1 = value.button1

						if not btn2 and value.button2:
							self.main_window.device_btns.emit(2)
						btn2 = value.button2

						self.values.append(value)
						if len(self.values) > 20:
							del self.values[0]

						if self.device_id is None:
							self.device_id = value.device_id

			self.main_window.device_status.emit(0)

			if not self.auto:
				break

	def get_device_id(self):
		return self.device_id

	def terminate(self):
		self.terminated = True

	@classmethod
	def get_value(cls, res):
		if ord(res[0]) == 67:
			return cls.BATv2Value(res)

		return cls.BATValue(res)

	def get_normal_resp(self):
		res = ""
		try:
			self.port.write('\xa0' if self.use_version == 2 else 'WQ')
			time.sleep(self.wait_time)
		except serial.SerialException as e:
			self.port.close()
			self.logger.write(e)
			return None

		try:
			while self.port.inWaiting() > 0:
				res += self.port.read(1)

		except serial.SerialException as e:
			self.port.close()
			self.logger.write(e)
			return None

		return res

	def get_max_val_resp(self):
		res = ""
		try:
			self.port.write('\xa2')
			time.sleep(self.wait_time)
		except serial.SerialException as e:
			self.port.close()
			self.logger.write(e)
			return None

		try:
			while self.port.inWaiting() > 0:
				res += self.port.read(1)

		except serial.SerialException as e:
			self.port.close()
			self.logger.write(e)
			return None

		return res

	def test_device(self):
		res = self.get_normal_resp()
		if res is None:
			return None

		value = self.get_value(res)

		if value.version == 2:
			_min = 100000
			for i in xrange(20):
				res = self.get_normal_resp()
				value = self.get_value(res)

				if _min > value.value:
					_min = value.value

			_max = 0
			for i in xrange(20):
				res = self.get_max_val_resp()
				value = self.get_value(res)

				if _max < value.value:
					_max = value.value

			self.min = _min
			self.max = _max

		return True

	@classmethod
	def detect(cls, l=None):
		def dev_name(_com):
			if sys.platform.startswith('win'):
				return _com.device
			return _com[0]

		for com in serial.tools.list_ports.comports():
			port = None

			try:
				port = serial.Serial(
					port=dev_name(com),
					baudrate=19200,
					parity=serial.PARITY_NONE,
					stopbits=serial.STOPBITS_ONE,
					bytesize=serial.EIGHTBITS
				)
				port.close()

				req = ['WQ', '\xa0', ]
				for r in req:

					port = serial.Serial(
						port=dev_name(com),
						baudrate=19200,
						parity=serial.PARITY_NONE,
						stopbits=serial.STOPBITS_ONE,
						bytesize=serial.EIGHTBITS
					)
					for i in range(3):
						res = ""
						port.write(r)
						time.sleep(0.2)
						while port.inWaiting() > 0:
							res += port.read()

						if res and ord(res[0]) == 67:
							value = cls.BATv2Value(res)
						else:
							value = cls.BATValue(res)

						if value.device_id is not None:
							detected_com = dev_name(com)
							port.close()

							return detected_com, value.version, value.wait_time

					port.close()

			except serial.SerialException as e:
				if l:
					l.write(e)

			finally:
				if port is not None and port.isOpen():
					port.close()

		return None, None, None


if __name__ == "__main__":

	dev = DriverThread.detect()
	d_thread = DriverThread(dev)
	d_thread.start()
	time.sleep(0.1)

	ff = open('d:\\last_test.txt', "w")
	ff.write("DEV ID = %s" % d_thread.device_id + "\n\r")

	t = 0

	while 1:
		try:
			if len(d_thread.values) > 0:
				v = d_thread.values.pop()
				ff.write(str(v) + "\n\r")

			time.sleep(0.04)
			t += 1
			if t > 3000:
				d_thread.terminate()

			if not d_thread.isAlive():
				ff.close()
				break

		except KeyboardInterrupt:
			d_thread.terminate()
			ff.close()
			raise
		except Exception:
			d_thread.terminate()
			ff.close()
			raise
