#!/usr/bin/env python

import pygame
from OpenGL.GL import *


def load_texture(filename):
	""" This fuctions will return the id for the texture"""
	textureSurface = pygame.image.load(filename)
	textureData = pygame.image.tostring(textureSurface, "RGBA", 1)
	width = textureSurface.get_width()
	height = textureSurface.get_height()
	ID = glGenTextures(1)
	glBindTexture(GL_TEXTURE_2D, ID)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData)
	return ID


class ObjLoader(object):
	def __init__(self, filename):
		def tonumb(x):
			return round(float(x), 6)

		self.vertices = []
		self.triangle_faces = []
		self.quad_faces = []
		self.polygon_faces = []
		self.normals = []
		self.scene_list = None
		self.last_texture = None
		self.texture_source = None
		# -----------------------
		try:
			f = open(filename)
			n = 1
			for line in f:
				if line[:2] == "v ":
					vert = line[2:].strip().split(' ')
					vertex = (tonumb(vert[0]), tonumb(vert[1]), tonumb(vert[2]))
					self.vertices.append(vertex)

				elif line[:2] == "vn":
					norm = line[2:].strip().split(' ')
					normal = (tonumb(norm[0]), tonumb(norm[1]), tonumb(norm[2]))
					self.normals.append(normal)

				elif line[0] == "f":
					string = line.replace("//", "/")
					# ---------------------------------------------------
					i = string.find(" ") + 1
					face = []
					for item in xrange(string.count(" ")):
						if string.find(" ", i) == -1:
							face.append(string[i:-1])
							break
						face.append(string[i:string.find(" ", i)])
						i = string.find(" ", i) + 1
					# ---------------------------------------------------
					if string.count("/") == 3:
						self.triangle_faces.append(tuple(face))
					elif string.count("/") == 4:
						self.quad_faces.append(tuple(face))
					else:
						self.polygon_faces.append(tuple(face))
			f.close()
		except IOError:
			pass
			# print "Could not open the .obj file..."

	def create_scene_list(self, scale):
		
		def scalex(x, s):
			return tuple((i * s for i in x))

		self.scene_list = glGenLists(1)
		glNewList(self.scene_list, GL_COMPILE)
		if len(self.triangle_faces) > 0:
			# -------------------------------
			glBegin(GL_TRIANGLES)
			for face in (self.triangle_faces):
				n = face[0]
				normal = self.normals[int(n[n.find("/") + 1:]) - 1]
				glNormal3fv(scalex(normal, scale))
				for f in (face):
					glVertex3fv(scalex(self.vertices[int(f[:f.find("/")]) - 1], scale))
			glEnd()
			# ---------------------------------

		if len(self.quad_faces) > 0:
			# ----------------------------------
			glBegin(GL_QUADS)
			for face in (self.quad_faces):
				n = face[0]
				normal = self.normals[int(n.split("/")[1]) - 1 ]
				glNormal3fv(scalex(normal, scale))
				for f in (face):
					glVertex3fv(scalex(self.vertices[int(f[:f.find("/")]) - 1], scale))
			glEnd()
			# -----------------------------------

		if len(self.polygon_faces) > 0:
			# ----------------------------------
			for face in (self.polygon_faces):
				# ---------------------
				glBegin(GL_POLYGON)
				n = face[0]
				normal = self.normals[int(n.split("/")[0]) - 1]
				glNormal3fv(scalex(normal, scale))
				for f in (face):
					glVertex3fv(scalex(self.vertices[int(f[:f.find("/")]) - 1], scale))
				glEnd()
				# ----------------------
				# -----------------------------------  
		glEndList()

	def render_scene(self, scale=1):
		if not self.scene_list:
			self.create_scene_list(scale)

		glCallList(self.scene_list)

	def render_texture(self, textureID, texcoord):
		self.last_texture = glGenLists(1)
		glNewList(self.last_texture, GL_COMPILE)
		glEnable(GL_TEXTURE_2D)
		glBindTexture(GL_TEXTURE_2D, textureID)

		glBegin(GL_QUADS)
		for face in self.quad_faces:
			n = face[0]
			normal = self.normals[int(n[n.find("/") + 1:]) - 1]
			glNormal3fv(normal)
			for i, f in enumerate(face):
				glTexCoord2fv(texcoord[i])
				glVertex3fv(self.vertices[int(f[:f.find("/")]) - 1])
		glEnd()

		glDisable(GL_TEXTURE_2D)
		glEndList()

	def render_texture_last(self):
		glCallList(self.last_texture)
