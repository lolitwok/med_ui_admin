import string
import random
import hashlib
import time

UPDI = (string.ascii_uppercase + string.digits)[:-4]


def to_md5(s):
	m = hashlib.md5()
	m.update(s)
	return m.hexdigest()


def id_generator(size=2, chars=UPDI):
	return ''.join(random.choice(chars) for _ in range(size))

options = [
	True,
	False,
	True,
	False,
	True,
	False,
	True,
	False,
	True,
	True,
	False,
	True,
	False,
	True,
	True,
	False,
	True,
	False,
	True,
	True,
	False,
	True,
	False,
	True,
	False,
	False,
	True,
	False,
	True,
	False,
	True,
	False,
	True,
	True,
	False,
	True,
	False,
	True,
	True,
	True,
	True,
	True,
	True,
	False,
	False,
	False,
	False,
	False,

]


def UPDI_to_bin(c):
	return bin(UPDI.index(c)).lstrip('0b').zfill(5)


def bin_to_chars(b):
	chrs = ''
	start = 0
	while start + 8 <= len(a):
		chrs += chr(int(a[start: start + 8], base=2))

		start += 8

	if start < len(a) - 1:
		chrs += chr(int(a[start:], base=2))

	return chrs


def key_gen(args, dev_id):
	dev_id_bin = bin(dev_id).lstrip('0b').zfill(16)[:15]
	a = ''
	for i in args:
		if i:
			a += '1'
		else:
			a += '0'
	options_chars = bin_to_chars(a)

	rnd = id_generator()

	t_options = a[::2]


if __name__ == "__main__":
	a = ''
	for i in options:
		if i:
			a += '1'
		else:
			a += '0'

	# print "options code =" + str(int(a, base=2))

	key_gen(options, 1232)
