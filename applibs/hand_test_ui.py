import os, math

from OpenGL.GL import *
from OpenGL.GLU import *
from PyQt4.QtOpenGL import *
from PyQt4 import QtGui, QtCore

from applibs.objparser_1 import load_texture

# from run import base_path

resources_path = 'resources'



class MainWindow(QtGui.QWidget):
	def __init__(self):
		super(MainWindow, self).__init__()

		self.widget = glWidget(self)
		main_layout = QtGui.QHBoxLayout()
		main_layout.addWidget(self.widget)
		self.setLayout(main_layout)


class glWidget(QGLWidget):
	def __init__(self, parent):
		QGLWidget.__init__(self, parent)
		self.setMinimumSize(640, 480)
		self.x = 0
		self.y = 0
		self.z = 0
		self.m = QtCore.QPoint()
		self.length = -10

	# self.test_model = ObjLoader(os.path.join(base_path, resources_path, '1.obj'))

	def glInit(self):
		super(glWidget, self).glInit()
		k = load_texture(l.list_textures[0])
		l.list[0].texture_source = k
		l.list[0].render_scene(1)

	def paintGL(self):

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
		# glClearColor(0.7,0.9,1,1)
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()

		# Add ambient light:
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, [0.2, 0.2, 0.2, 1.0])

		# Add positioned light:
		glLightfv(GL_LIGHT0, GL_DIFFUSE, [2, 2, 2, 1])
		glLightfv(GL_LIGHT0, GL_POSITION, [4, 8, 1, 1])

		# glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
		# glLoadIdentity()

		glTranslatef(0.0, 0.0, self.length)
		glColor3f(0.6, 0.2, 0.1)
		glPolygonMode(GL_FRONT, GL_FILL)

		# glRotated(self.x * 0.8, 1.0, 0, 0)
		# glRotated(self.y * 0.8, 0, 1.0, 0)
		# glRotated(self.z * 0.8, 0, 0, 1.0)

		# self.z = 60 math.cos(math.radians(self.z)) *

		x =  math.sin(math.radians(self.x))
		y =  math.cos(math.radians(self.x))
		z = math.cos(math.radians(self.z))

		# if z > 0:
		# 	if math.sin(math.radians(self.x)) > 0:
		# 		y *= -1
		# 	if math.cos(math.radians(self.x)) > 0:
		# 		x *= -1

		gluLookAt(0, 0, 0, x, z, y * -1, 0, 2, 0)

		self.draw()

		glFlush()

	def draw(self):
		glPushMatrix()
		glTranslatef(0.5, 0.2, -1.0)
		if l.list[0].last_texture is None:
			l.list[0].render_texture(l.list[0].texture_source, ((0, 0), (1, 0), (1, 1), (0, 1)))
		else:
			l.list[0].render_texture_last()
		# from applibs.objloaderthread import
		if l.loaded:
			l.list[0].render_scene(1)
		glPopMatrix()

	def initializeGL(self):
		glClearDepth(1.0)
		glDepthFunc(GL_LESS)
		glEnable(GL_DEPTH_TEST)
		glShadeModel(GL_SMOOTH)

		glViewport(0, 0, 640, 480)

		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(45.0, 1.33, 0.1, 100.0)
		glMatrixMode(GL_MODELVIEW)

		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(45, 640.0 / 480.0, 0.1, 200.0)
		glEnable(GL_DEPTH_TEST)
		glEnable(GL_LIGHTING)
		glEnable(GL_LIGHT0)
		glEnable(GL_NORMALIZE)
		glEnable(GL_COLOR_MATERIAL)
		glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE)

	def resizeEvent(self, e):
		pass

	def mousePressEvent(self, e):
		self.m = e.pos()

	def wheelEvent(self, e):
		self.length += e.delta() / 100
		if self.length < -20:
			self.length = -20
		if self.length > -5:
			self.length = -5
		self.updateGL()

	def mouseMoveEvent(self, e):
		delX = self.m.x() - e.x()
		delY = self.m.y() - e.y()
		if e.buttons() & QtCore.Qt.LeftButton:
			self.x -= 0.5 * delX
			self.z -= 0.5 * delY
			if self.z > 180:
				self.z = 180

			if self.z < 0:
				self.z = 0

			if self.x > 360:
				self.x -= 360

			if self.x < 0:
				self.x += 360

		if e.buttons():
			self.updateGL()

		self.m = e.pos()

from applibs.objloaderthread import l
