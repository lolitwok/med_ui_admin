import time
from applibs.main_ui.utils import middle_value
from flask import Flask, render_template, request, jsonify
import json
import logging
import os
import requests
import random
from threading import Thread
from applibs import BASE_DIR
import eventlet
import eventlet.wsgi
from applibs.utils.wsgi import server as custome_server, terminate_server, reset_terminate_server
from flask.helpers import send_from_directory
from datetime import timedelta
from flask import make_response, request, current_app
from functools import update_wrapper

data_to_send = []


def crossdomain(origin=None, methods=None, headers=None,
				max_age=21600, attach_to_all=True,
				automatic_options=True):
	if methods is not None:
		methods = ', '.join(sorted(x.upper() for x in methods))
	if headers is not None and not isinstance(headers, basestring):
		headers = ', '.join(x.upper() for x in headers)
	if not isinstance(origin, basestring):
		origin = ', '.join(origin)
	if isinstance(max_age, timedelta):
		max_age = max_age.total_seconds()

	def get_methods():
		if methods is not None:
			return methods

		options_resp = current_app.make_default_options_response()
		return options_resp.headers['allow']

	def decorator(f):
		def wrapped_function(*args, **kwargs):
			if automatic_options and request.method == 'OPTIONS':
				resp = current_app.make_default_options_response()
			else:
				resp = make_response(f(*args, **kwargs))
			if not attach_to_all and request.method != 'OPTIONS':
				return resp

			h = resp.headers
			h['Access-Control-Allow-Origin'] = origin
			h['Access-Control-Allow-Methods'] = get_methods()
			h['Access-Control-Max-Age'] = str(max_age)
			h['Access-Control-Allow-Credentials'] = 'true'
			h['Access-Control-Allow-Headers'] = \
				"Origin, X-Requested-With, Content-Type, Accept, Authorization"
			if headers is not None:
				h['Access-Control-Allow-Headers'] = headers
			return resp

		f.provide_automatic_options = False
		return update_wrapper(wrapped_function, f)

	return decorator


class IOSocketThread(Thread):
	@staticmethod
	def is_port_open(port):
		import socket
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		result = s.connect_ex(('127.0.0.1', port))
		s.close()
		return result == 0

	@staticmethod
	def random_port():
		port = random.randint(45000, 56000)
		while IOSocketThread.is_port_open(port):
			port = random.randint(45000, 56000)

		return port

	def __init__(self, port=None, parent=None, *args, **kwargs):
		super(IOSocketThread, self).__init__(*args, **kwargs)

		self.port = port
		self.parent = parent
		self.daemon = True
		self.server_app = Flask(__name__, template_folder=os.path.join(BASE_DIR, 'templates'))
		# self.server_app.logger.disabled = True

		self.server_app.use_reloader = False

		self.server_app.config['SECRET_KEY'] = '1234'
		self.server_app.config['LOGGER_NAME'] = 'flask'
		# self.server_app.config['STATIC_FOLDER'] = os.path.join(BASE_DIR, 'templates', 'static')
		import logging
		# log = logging.getLogger('werkzeug')
		# log.setLevel(1000)
		# self.server_app.logger.disabled = True

		global data_to_send
		data_to_send = []

		# self.sender_thread = self.SenderThread(self)

		@self.server_app.route('/')
		def index():
			return render_template('socket.graph.html')

		@self.server_app.route('/static/<string:path>')
		def send_static(path):
			# print path

			return send_from_directory(os.path.join(BASE_DIR, 'templates', 'static'), path,
									   mimetype='application/octet-stream')

		@self.server_app.route("/data")
		@crossdomain(origin='*')
		def data():
			if self.parent.selected_diagnostic is None:
				return json.dumps(dict(data=[])), 200, {'ContentType': 'application/json'}

			_data = data_to_send if len(data_to_send) > 0 else [-10, ]

			return json.dumps(dict(data=_data)), 200, {'ContentType': 'application/json'}

		@self.server_app.route("/last_dias")
		@crossdomain(origin='*')
		def last_dia_data():
			if self.parent.selected_diagnostic is None:
				return json.dumps(dict(data=[])), 200, {'ContentType': 'application/json'}

			_t = []
			for i in self.parent.cur_session.cur_diagnostic.cur_node.normal_points:
				_t.append(
					{
						'name': i.name,
						'points': i.points,
					}
				)
			_data = _t

			return json.dumps(dict(data=_data)), 200, {'ContentType': 'application/json'}

		@self.server_app.route("/dias")
		@crossdomain(origin='*')
		def dias_data():
			try:
				if self.parent.selected_diagnostic is None:
					return json.dumps(dict(data=[])), 200, {'ContentType': 'application/json'}

				t = {
					'dias': [],
					'middle': 0,
				}

				if self.parent.cur_session.cur_diagnostic.cur_node:
					trigered_points = self.parent.cur_session.cur_diagnostic.cur_node.points_with_nazod if \
						self.parent.cur_session.cur_diagnostic.cur_node.nazod_trigger else \
						self.parent.cur_session.cur_diagnostic.cur_node.normal_points

					for i in trigered_points:
						t['dias'].append(
							{
								'name': unicode(i),
								'val': i.middle_value,
							}
						)

					t['middle'] = self.parent.cur_session.cur_diagnostic.cur_node.points_with_nazod_middle if \
						self.parent.cur_session.cur_diagnostic.cur_node.nazod_trigger else \
						self.parent.cur_session.cur_diagnostic.cur_node.normal_points_middle

				return json.dumps(dict(data=t)), 200, {'ContentType': 'application/json'}
			except Exception as e:
				# print e.message
				t = {
					'dias': [],
					'middle': 0,
				}
				return json.dumps(dict(data=t)), 200, {'ContentType': 'application/json'}

		@self.server_app.route("/all_point_data")
		@crossdomain(origin='*')
		def all_point_data():
			try:
				if self.parent.selected_diagnostic is None:
					return json.dumps(dict(data=[])), 200, {'ContentType': 'application/json'}

				t = []

				if self.parent.cur_session.cur_diagnostic.cur_node:
					for i in self.parent.cur_session.cur_diagnostic.cur_node.normal_points:
						t.append(
							{
								't': i.end_time,
								'v': i.middle_value,
								'type': 0,
							}
						)
					if self.parent.cur_session.cur_diagnostic.cur_node.points_with_nazod:
						for i in self.parent.cur_session.cur_diagnostic.cur_node.points_with_nazod:
							t.append(
								{
									't': i.end_time,
									'v': i.middle_value,
									'type': 1,
								}
							)
					t = sorted(t, key=lambda k: k['t'])

				return json.dumps(dict(data=t)), 200, {'ContentType': 'application/json'}

			except Exception as e:
				return json.dumps(dict(data=[], error=e.message)), 200, {'ContentType': 'application/json'}

		@self.server_app.route("/cur_dia")
		@crossdomain(origin='*')
		def cur_dias_data():
			if self.parent.selected_diagnostic is None:
				return json.dumps(dict(data=[])), 200, {'ContentType': 'application/json'}

			_data = {
				'name': "",
				'points': [],
			}

			if self.parent.selected_measurement_array:
				_data = {
					'points': self.parent.selected_measurement_array.points,
					'name': "",
				}

			return json.dumps(dict(data=_data)), 200, {'ContentType': 'application/json'}

		@self.server_app.route("/dias_for_points")
		@crossdomain(origin='*')
		def dias_for_points():
			if self.parent.selected_diagnostic is None or \
							self.parent.cur_session is None or \
							self.parent.cur_session.cur_diagnostic is None:
				return json.dumps(dict(data=[])), 200, {'ContentType': 'application/json'}

			if self.parent.cur_session.cur_diagnostic.dia_type == 4:
				_data = {
					'n': [],
					'z': [],
				}

				for i in self.parent.cur_session.cur_diagnostic.nodes:

					tmp1 = {
						'name': i.get_name(),
						'mid': i.normal_points_middle,
					}

					tmp2 = {
						'name': i.get_name(),
						'mid': i.points_with_nazod_middle,
					}

					if i.for_node and i.for_node.is_main:
						tmp1['is_main'] = True
						tmp2['is_main'] = True

					if i.for_node is None:
						tmp1['is_storage'] = True
						tmp2['is_storage'] = True

					_data['n'].append(tmp1)
					_data['z'].append(tmp2)

			else:
				_data = []
				for i in self.parent.cur_session.cur_diagnostic.nodes:
					tmp1 = {
						'name': i.get_name(),
						'mid': i.normal_points_middle,
					}
					if i.for_node and i.for_node.is_main:
						tmp1['is_main'] = True
					_data.append(tmp1)

			return json.dumps(dict(data=_data)), 200, {'ContentType': 'application/json'}

		@self.server_app.route("/report")
		@crossdomain(origin='*')
		def report():
			if not self.parent.report:
				return json.dumps(dict(data=[])), 200, {'ContentType': 'application/json'}

			return json.dumps(dict(data=self.parent.report)), 200, {'ContentType': 'application/json'}

		@self.server_app.route("/comparison")
		@crossdomain(origin='*')
		def comparison():
			if self.parent.selected_diagnostic is None or \
							self.parent.cur_session is None or \
							self.parent.cur_session.cur_diagnostic is None:
				return json.dumps(dict(data=[0.0, 0.0], fail=True)), 200, {'ContentType': 'application/json'}

			_f = 0.0
			_s = 0.0

			if self.parent.cur_session is not None and \
							self.parent.cur_session.cur_diagnostic is not None and \
							self.parent.cur_session.cur_diagnostic.cur_node is not None:
				_f = self.parent.cur_session.cur_diagnostic.cur_node.normal_points_middle
				_s = self.parent.cur_session.cur_diagnostic.cur_node.points_with_nazod_middle

			return json.dumps(dict(data=[_f, _s])), 200, {'ContentType': 'application/json'}

		@self.server_app.route("/kip")
		@crossdomain(origin='*')
		def kip():
			if self.parent.selected_diagnostic is None or \
							self.parent.cur_session is None or \
							self.parent.cur_session.cur_diagnostic is None:
				return json.dumps(dict(k=0, v=0)), 200, {'ContentType': 'application/json'}

			context = dict(
				k=self.parent.cur_session.cur_diagnostic.kipspedo
				if self.parent.cur_session.cur_diagnostic.kipspedo
				else 0,
				v=self.parent.cur_session.cur_diagnostic.cur_node.get_cur_middle()
				if self.parent.cur_session.cur_diagnostic.cur_node
				else 0,
			)
			if self.parent.cur_session.cur_diagnostic.cur_node and \
					self.parent.cur_session.cur_diagnostic.cur_node.for_node.is_main:
				context['is_kipspedo'] = ''
			return json.dumps(context), 200, {'ContentType': 'application/json'}

		@self.server_app.route("/pmap")
		@crossdomain(origin='*')
		def pmap():
			if self.parent.selected_diagnostic is None or \
							self.parent.cur_session is None or \
							self.parent.cur_session.cur_diagnostic.cur_node is None:
				return json.dumps(dict(data=dict(n=0, t=0, r=0, b=0, l=0))), 200, {'ContentType': 'application/json'}

			cur_node = self.parent.cur_session.cur_diagnostic.cur_node
			context = dict(
				n=cur_node.normal_points_middle,
				t=middle_value(cur_node.map_data['t']),
				r=middle_value(cur_node.map_data['r']),
				b=middle_value(cur_node.map_data['b']),
				l=middle_value(cur_node.map_data['l'])
			)
			return json.dumps(dict(data=context)), 200, {'ContentType': 'application/json'}

		@self.server_app.route("/k")
		@crossdomain(origin='*')
		def rr():

			return json.dumps(dict(
				data=str(self.parent.cur_session.cur_diagnostic.dia_type))
			), 200, {'ContentType': 'application/json'}

	def terminate(self):
		terminate_server()

		for i in xrange(10):
			try:
				requests.get('http://localhost:5001/', timeout=0.01)
			except:
				pass

	def send_msg(self, value):
		pass

	def run(self):
		reset_terminate_server()
		custome_server(eventlet.listen(('', self.port)), self.server_app, log_output=True,
					   log=logging.getLogger('eventlet'))

	# print "[X] server_app_socket_io Close"

	def push_data_value(self, data):
		global data_to_send
		data_to_send.append(data)
		if len(data_to_send) > 6:
			data_to_send = data_to_send[len(data_to_send) - 6:]

	@staticmethod
	def get_last_value():
		global data_to_send
		return data_to_send[len(data_to_send) - 1] if len(data_to_send) else 0
