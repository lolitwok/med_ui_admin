#!/usr/bin/env python


import time
import os
import sys

if __name__ == '__main__':
	sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from applibs import BASE_DIR
from applibs.dao import cur_main_session
from applibs.models.main_models import DModel

from threading import Thread

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from PyQt4.QtOpenGL import *

from applibs.objloader_default import OBJ

import sys
import time
from PyQt4.QtGui import QPaintDevice
import threading
from PyQt4 import QtCore, QtGui, QtOpenGL
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from PyQt4.QtOpenGL import *

import OpenGL

window_count = 0


# -------------------------------------------------------------------
#						   GL THREAD
# -------------------------------------------------------------------
# class GLThread(QtCore.QThread):  # Uncomment to use QThreads instead of python threads


class LGLWidget(QtOpenGL.QGLWidget):
	def __init__(self, parent=None):
		QtOpenGL.QGLWidget.__init__(self, parent)
		self.setAutoBufferSwap(True)
		self.resize(0, 0)
		self.doneCurrent()

	def resizeEvent(self, event):
		pass

	def paintEvent(self, event):
		# super(TestGLWidget, self).paintEvent(event)
		pass

	def closeEvent(self, event):
		pass


class GLLoader(threading.Thread, QtCore.QObject):
	loaded_signal = QtCore.pyqtSignal()

	def __init__(self, parent=None):
		threading.Thread.__init__(self)
		QtCore.QObject.__init__(self)
		self.glw = LGLWidget(parent)
		self.p_gll = None
		self.last_render_time = time.time()
		self.models_list = {}
		self.loaded = False
		self.daemon = True

		# print 'Loading Thread created.'

	def get_model(self, model_file):
		_id = 1

		if model_file not in self.models_list:
			for i in self.models_list.values():
				if i[0] > _id:
					_id = i[0]
			if len(self.models_list):
				_id += 1

			self.models_list[model_file] = (
				_id, OBJ(model_file, swapyz=True, _id=_id)
			)

		return self.models_list[model_file]

	def run(self):
		# print "Loading Thread started";
		self.glw.show()
		self.glw.makeCurrent()

		ind = 0
		for i in cur_main_session.query(DModel).all():
			if i.file_name in self.models_list:
				continue

			self.get_model(i.file_name)
			ind += 1
			# print u'%s/3' % ind

		# print "rendering time = %s" % (time.time() - self.last_render_time)
		self.loaded = True
		self.loaded_signal.emit()

	# def terminate(self):
	# 	self.stop = True


if __name__ == '__main__':
	l = GLLoader()
	l.start()

	# print l.models_list

	l.terminate()
	l.join()
