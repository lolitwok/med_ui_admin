__author__ = 'mall'


class Model(object):
	vertsOut = list()
	normsOut = list()

	def __init__(self, filename):
		self.filename = filename

	def obj_load(self):
		# num_verts = 0
		verts = []
		norms = []
		for line in open(self.filename, "r"):
			vals = line.split()
			if vals[0] == "v":
				v = map(float, vals[1:4])
				verts.append(v)
			if vals[0] == "vn":
				n = map(float, vals[1:4])
				norms.append(n)
			if vals[0] == "f":
				for f in vals[1:]:
					w = f.split("/")
					# OBJ Files are 1-indexed so we must subtract 1 below
					self.vertsOut.append(list(verts[int(w[0])-1]))
					self.normsOut.append(list(norms[int(w[2])-1]))
					# num_verts += 1

	def __str__(self):
		s = list()
		s.append("verts:")
		for i in self.vertsOut:
			s.append("{ vert# %s, %s}" % (self.vertsOut.index(i), i))

		return " ".join(s)
