from applibs.models.main_models import DPoint
import copy
from PyQt4 import QtGui, uic, QtCore, Qt

import pygame
from pygame.locals import *
from pygame.constants import *
from OpenGL.GL import *
from OpenGL.GLU import *

# IMPORT OBJECT LOADER
from applibs.objloader_default import *

from applibs.dao import cur_main_session
from applibs.ui.glwidget import glWidget

# from applibs.models import
from applibs.models.main_models import DModel

from applibs.ui.src.point_edit import Ui_MainWindow


class PointGLWindow(QtGui.QMainWindow):
	def __init__(self, parent=None):
		super(PointGLWindow, self).__init__(parent)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.node = None
		self.item = None
		self.w_mode = 0

		self.defaults = {
			'dockWidgetContents': self.ui.config.geometry()
		}

		self.glw = None

		self.ui.x_sp.valueChanged.connect(self.x_sp_onChanged)
		self.ui.y_sp.valueChanged.connect(self.y_sp_onChanged)
		self.ui.z_sp.valueChanged.connect(self.z_sp_onChanged)
		self.ui.radius_sp.valueChanged.connect(self.radius_sp_onChanged)
		self.ui.offset_x_sp.valueChanged.connect(self.offset_x_sp_onChanged)
		self.ui.offset_y_sp.valueChanged.connect(self.offset_y_sp_onChanged)
		self.ui.offset_z_sp.valueChanged.connect(self.offset_z_sp_onChanged)

		self.ui.rotate_y_sp.valueChanged.connect(self.rotate_y_sp_onChanged)

		self.ui.x_sl.valueChanged.connect(self.x_sl_onChanged)
		self.ui.y_sl.valueChanged.connect(self.y_sl_onChanged)
		self.ui.z_sl.valueChanged.connect(self.z_sl_onChanged)
		self.ui.radius_sl.valueChanged.connect(self.radius_sl_onChanged)

		self.ui.btn_save.clicked.connect(self.btn_save_clicked)
		self.ui.btn_save_2.clicked.connect(self.btn_save_2_clicked)

		self.ui.models_combobox.currentIndexChanged.connect(self.models_combobox_currentIndexChanged)

	def showEvent(self, *args, **kwargs):
		from applibs.ui.dia_main import Window

		while not Window.gll.loaded:
			msg = QtGui.QMessageBox()
			msg.setIcon(QtGui.QMessageBox.Information)
			msg.setText(u"Models not loaded yet")
			msg.setStandardButtons(QtGui.QMessageBox.Ok)
			msg.exec_()

		if self.glw is None:
			self.glw = glWidget(self, preloader=Window.gll)
			self.ui.hl_main.addWidget(self.glw)

		point = self.item.to_gl_point_dict() if self.item else copy.copy(self.glw.default['point'])
		model_file = None if self.item is None else self.item.model.file_name
		self.glw.reinit(model_file=model_file, point=point)

		self.parent().hide()

		if self.w_mode == 0:
			self.ui.config.setVisible(True)
			self.ui.config.setEnabled(True)

			self.ui.all_points.setVisible(True)
			self.ui.all_points.setEnabled(True)

			self.ui.all_points_list.itemDoubleClicked.connect(self.all_points_itemDoubleClicked)
			self.ui.add_empty_btn.clicked.connect(self.add_empty_btn_clicked)
			self.ui.del_from_list_btn.clicked.connect(self.del_from_list_btn_clicked)

			self.ui.lists_p.setVisible(False)
			self.ui.lists_p.setEnabled(False)

			self.glw.reinit(dev=True, changeable=True)

			target = self.item if self.item else copy.copy(self.glw.point)

			self.set_point(target)

			self.refresh_models()
			self.refresh_all_points()

		if self.w_mode == 1:
			self.glw.reinit(dev=False, changeable=False)

			self.ui.config.setEnabled(False)
			self.ui.config.setVisible(False)

			self.ui.all_points.setEnabled(False)
			self.ui.all_points.setVisible(False)

			self.ui.lists_p.setVisible(False)
			self.ui.lists_p.setEnabled(False)

		if self.w_mode == 2:
			self.glw.reinit(dev=False, changeable=False)

			self.ui.config.setEnabled(False)
			self.ui.config.setVisible(False)

			self.ui.all_points.setEnabled(False)
			self.ui.all_points.setVisible(False)

			self.ui.lists_p.setVisible(True)
			self.ui.lists_p.setEnabled(True)

			self.ui.up_button.clicked.connect(self.up_button_Clicked)
			self.ui.down_button.clicked.connect(self.down_button_Clicked)

			self.ui.list_all.itemDoubleClicked.connect(self.list_all_itemDoubleClicked)
			self.ui.list_points.itemDoubleClicked.connect(self.list_points_itemDoubleClicked)

			all_points = cur_main_session.query(DPoint).all()
			node_points = self.node.nazode_points

			self.ui.list_all.clear()
			self.ui.list_points.clear()

			for i in node_points:
				t = QtGui.QListWidgetItem()
				t.setText(unicode(i))
				t.setData(32, i)
				self.ui.list_points.addItem(t)

			for i in all_points:
				if i not in node_points:
					t = QtGui.QListWidgetItem()
					t.setText(unicode(i))
					t.setData(32, i)
					self.ui.list_all.addItem(t)



				# self.ui.dockWidget.setGeometry(0, 0, 0, 0)

	def set_point(self, target):

		if isinstance(target, DPoint):
			self.ui.x_sp.setValue(target.x)
			self.ui.x_sl.setValue(int(target.x * 100) + 500)

			self.ui.y_sp.setValue(target.y)
			self.ui.y_sl.setValue(int(target.y * 100) + 500)

			self.ui.z_sp.setValue(target.z)
			self.ui.z_sl.setValue(int(target.z * 100) + 500)

			self.ui.radius_sp.setValue(target.r)
			self.ui.radius_sl.setValue(int(target.r * 10000))

			self.ui.offset_x_sp.setValue(target.distance_x_offset)
			self.ui.offset_y_sp.setValue(target.distance_y_offset)
			self.ui.offset_z_sp.setValue(target.distance_z_offset)

			self.ui.rotate_y_sp.setValue(target.distance_y)

		else:
			self.ui.x_sp.setValue(target['x'])
			self.ui.x_sl.setValue(int(target['x'] * 100) + 500)

			self.ui.y_sp.setValue(target['y'])
			self.ui.y_sl.setValue(int(target['y'] * 100) + 500)

			self.ui.z_sp.setValue(target['z'])
			self.ui.z_sl.setValue(int(target['z'] * 100) + 500)

			self.ui.radius_sp.setValue(target['radius'])
			self.ui.radius_sl.setValue(int(target['radius'] * 10000))

			self.ui.offset_x_sp.setValue(target['distance_x_offset'])
			self.ui.offset_y_sp.setValue(target['distance_y_offset'])
			self.ui.offset_z_sp.setValue(target['distance_z_offset'])

			self.ui.rotate_y_sp.setValue(target['distance_y'])

	def reinit(self, node, item=None, mode=None):
		if mode is not None:
			self.w_mode = mode

		self.node = node
		self.item = item

	def closeEvent(self, *args, **kwargs):
		cur_main_session.rollback()
		self.parent().show()

	#
	# SP
	#

	def x_sp_onChanged(self, val):
		p = copy.copy(self.glw.point)

		p['x'] = val
		self.ui.x_sl.setValue(int(p['x'] * 100) + 500)
		self.glw.set_point_params(p)
		self.glw.updateGL()

	def y_sp_onChanged(self, val):
		p = copy.copy(self.glw.point)

		p['y'] = val
		self.ui.y_sl.setValue(int(p['y'] * 100) + 500)
		self.glw.set_point_params(p)
		self.glw.updateGL()

	def z_sp_onChanged(self, val):
		p = copy.copy(self.glw.point)

		p['z'] = val
		self.ui.z_sl.setValue(int(p['z'] * 100) + 500)
		self.glw.set_point_params(p)
		self.glw.updateGL()

	def radius_sp_onChanged(self, val):
		p = copy.copy(self.glw.point)

		p['radius'] = float(val)
		self.ui.radius_sl.setValue(int(p['radius'] * 10000))
		self.glw.set_point_params(p)
		self.glw.updateGL()

	def offset_x_sp_onChanged(self, val):
		p = copy.copy(self.glw.point)
		p['distance_x_offset'] = float(val)
		self.glw.set_point_params(p)
		self.glw.updateGL()

	def offset_y_sp_onChanged(self, val):
		p = copy.copy(self.glw.point)
		p['distance_y_offset'] = float(val)
		self.glw.set_point_params(p)
		self.glw.updateGL()

	def offset_z_sp_onChanged(self, val):
		p = copy.copy(self.glw.point)
		p['distance_z_offset'] = float(val)
		self.glw.set_point_params(p)
		self.glw.updateGL()

	def rotate_y_sp_onChanged(self, val):
		p = copy.copy(self.glw.point)
		p['distance_y'] = float(val)
		self.glw.set_point_params(p)
		p = copy.copy(self.glw.point)
		self.ui.rotate_y_sp.setValue(p['distance_y'])
		self.glw.updateGL()

	#
	# SL
	#

	def x_sl_onChanged(self, val):
		p = copy.copy(self.glw.point)

		p['x'] = float((val - 500) * 0.01)
		self.ui.x_sp.setValue(p['x'])
		self.glw.set_point_params(p)
		self.glw.updateGL()

	def y_sl_onChanged(self, val):
		p = copy.copy(self.glw.point)

		p['y'] = float((val - 500) * 0.01)
		self.ui.y_sp.setValue(p['y'])
		self.glw.set_point_params(p)
		self.glw.updateGL()

	def z_sl_onChanged(self, val):
		p = copy.copy(self.glw.point)

		p['z'] = float((val - 500) * 0.01)
		self.ui.z_sp.setValue(p['z'])
		self.glw.set_point_params(p)
		self.glw.updateGL()

	def radius_sl_onChanged(self, val):
		p = copy.copy(self.glw.point)
		p['radius'] = float(val * 0.0001)
		self.ui.radius_sp.setValue(p['radius'])
		self.glw.set_point_params(p)
		self.glw.updateGL()

	def btn_save_clicked(self):
		p = self.glw.point
		if self.item is not None:
			n = self.item
		else:
			n = DPoint()

		n.x = p['x']
		n.y = p['y']
		n.z = p['z']
		n.r = p['radius']

		n.distance_a = self.glw.point['distance_length']
		n.distance_x = self.glw.point['distance_x']
		n.distance_y = self.glw.point['distance_y']
		n.distance_z = self.glw.point['distance_z']

		n.distance_x_offset = self.glw.point['distance_x_offset']
		n.distance_y_offset = self.glw.point['distance_y_offset']
		n.distance_z_offset = self.glw.point['distance_z_offset']

		model_guid = self.ui.models_combobox.itemData(self.ui.models_combobox.currentIndex()).toString()
		n.model_guid = None if (model_guid is None) or model_guid == '' else model_guid

		n.d_node_guid = self.node.guid

		cur_main_session.add(n)
		cur_main_session.commit()
		self.refresh_all_points()

	# self.close()

	def list_all_itemDoubleClicked(self):
		if len(self.ui.list_all.selectedItems()) > 0:
			item = self.ui.list_all.selectedItems()[0]
			self.show_point(item)

	def list_points_itemDoubleClicked(self):
		if len(self.ui.list_points.selectedItems()) > 0:
			item = self.ui.list_points.selectedItems()[0]
			self.show_point(item)

	def show_point(self, item):
		item = item.data(32).toPyObject()
		point = {
			'x': item.x,
			'y': item.y,
			'z': item.z,
			'radius': item.r,

			'distance_x': item.distance_x,
			'distance_y': item.distance_y,
			'distance_z': item.distance_z,
			'distance_length': item.distance_a,

			'distance_x_offset': item.distance_x_offset,
			'distance_y_offset': item.distance_y_offset,
			'distance_z_offset': item.distance_z_offset,
		}
		self.glw.reinit(point=point)
		self.glw.updateGL()

	def up_button_Clicked(self):
		if len(self.ui.list_all.selectedItems()) > 0:
			point = self.ui.list_all.selectedItems()[0].data(32).toPyObject()
			self.node.nazode_points.append(self.ui.list_all.selectedItems()[0].data(32).toPyObject())
			# cur_session.commit()
			t = QtGui.QListWidgetItem()
			t.setText(unicode(point))
			t.setData(32, point)
			self.ui.list_points.addItem(t)
			self.ui.list_all.takeItem(self.ui.list_all.row(self.ui.list_all.selectedItems()[0]))
			self.ui.list_all.clearSelection()
			self.ui.list_all.clearFocus()

	def down_button_Clicked(self):
		if len(self.ui.list_points.selectedItems()) > 0:
			point = self.ui.list_points.selectedItems()[0].data(32).toPyObject()
			self.node.nazode_points.remove(point)
			# cur_session.commit()
			t = QtGui.QListWidgetItem()
			t.setText(unicode(point))
			t.setData(32, point)
			self.ui.list_all.addItem(t)
			self.ui.list_points.takeItem(self.ui.list_points.row(self.ui.list_points.selectedItems()[0]))
			self.ui.list_points.clearSelection()
			self.ui.list_points.clearFocus()

	def btn_save_2_clicked(self):
		cur_main_session.commit()
		self.close()

	def refresh_models(self):
		self.ui.models_combobox.clear()
		self.ui.models_combobox.addItem('No model', None)
		models = cur_main_session.query(DModel).all()
		for i in models:
			self.ui.models_combobox.addItem(i.name, i.guid)
			if self.item is not None and self.item.model_guid == i.guid:
				index = self.ui.models_combobox.findText(i.name)
				self.ui.models_combobox.setCurrentIndex(index)

	def models_combobox_currentIndexChanged(self, item):
		model_guid = self.ui.models_combobox.itemData(self.ui.models_combobox.currentIndex()).toString()
		if model_guid is None or model_guid == "":
			model_guid = None

		print model_guid
		model = cur_main_session.query(DModel).filter(DModel.guid == str(model_guid)).first()

		file_name = None
		if model:
			file_name = model.file_name

		self.glw.reinit(model_file=file_name)
		self.glw.updateGL()

	def all_points_itemDoubleClicked(self):
		if len(self.ui.all_points_list.selectedItems()) > 0:
			item = self.ui.all_points_list.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()
			self.item = item

			self.set_point(item)
			self.refresh_models()
			self.glw.reinit(point=item.to_gl_point_dict(), model_file=item.model.file_name)
			self.glw.updateGL()

	def refresh_all_points(self):
		self.ui.all_points_list.clear()
		all_points = cur_main_session.query(DPoint).all()
		cur_id = self.item.id if self.item else None

		if len(self.ui.all_points_list.selectedItems()):
			item = self.ui.all_points_list.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()
			cur_id = item.id

		for i in all_points:
			t = QtGui.QListWidgetItem()
			t.setText(u"%s (%s)" % (unicode(i), i.d_node.name if i.d_node else "None"))
			t.setData(32, i)
			self.ui.all_points_list.addItem(t)
			if i.id == cur_id:
				self.ui.all_points_list.setItemSelected(t, True)

	def del_from_list_btn_clicked(self):
		if not len(self.ui.all_points_list.selectedItems()):
			return
		item = self.ui.all_points_list.selectedItems()[0].data(QtCore.Qt.UserRole).toPyObject()
		msg = QtGui.QMessageBox()
		msg.setIcon(QtGui.QMessageBox.Warning)
		msg.setText(u"Delete %s(%s)?" % (unicode(item), item.d_node.name if item.d_node else 'None'))
		msg.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.Cancel)
		result = msg.exec_()
		if result != QtGui.QMessageBox.Yes:
			return

		cur_main_session.query(DPoint).filter(DPoint.id == item.id).delete()
		cur_main_session.commit()
		self.refresh_all_points()

	def add_empty_btn_clicked(self):
		point = copy.deepcopy(self.glw.default['point'])
		default_model = None
		try:
			default_model_guid = self.ui.models_combobox.itemData(1).toPyObject()
			default_model = cur_main_session.query(DModel).filter(DModel.guid == default_model_guid).first()
			self.ui.models_combobox.setCurrentIndex(1)
		except Exception as e:
			pass
			# print e.message

		self.item = None
		self.set_point(point)
		self.glw.reinit(point=point, model_file=default_model.file_name)
		self.glw.updateGL()
