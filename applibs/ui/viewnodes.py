# -*- coding: utf-8 -*-
from time import time

import os
from sqlalchemy.orm.util import aliased
from sqlalchemy.sql.functions import count
import types
from PyQt4 import QtGui, uic, QtCore, Qt

from applibs.dao import cur_main_session
from applibs.models.main_models import DNode, DImage, DPoint
from .addnode import NodeAddWindow
from .add_point import PointGLWindow
from .pictureview import PicWindow

(Ui_MainWindow, QMainWindow) = uic.loadUiType('applibs/ui/src/nodes_edit.ui')


class Window(QtGui.QMainWindow):
	def __init__(self, parent=None):
		super(Window, self).__init__(parent)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.add_node_window = None
		self.current_node = None
		self.d_item_guid = None
		self.p_point_w = None
		self.w_pic_w = None
		self.showed = False

		self.reinit_tree(self.ui.nodesView)

		self.ui.buttonAddNode.clicked.connect(self.open_add_node)
		self.ui.buttonDeleteNode.clicked.connect(self.buttonDeleteNode_click)

		self.ui.buttonReset.clicked.connect(self.rtnReset_click)
		self.ui.buttonSave.clicked.connect(self.save_current_node)

		self.ui.buttonAddPoint.clicked.connect(self.point_add_click)
		self.ui.buttonDeletePoint.clicked.connect(self.point_delete_click)

		self.ui.btnImg_add.clicked.connect(self.btnImg_add)
		self.ui.btnImg_del.clicked.connect(self.btnImg_del)

		self.ui.naz_buttonEditPoints.clicked.connect(self.naz_buttonEditPoints_clicked)

		self.ui.nodesView.itemDoubleClicked.connect(self.nodes_view_item_selection_changed)

		self.ui.listImages.itemDoubleClicked.connect(self.listImages_itemDoubleClicked)

		self.ui.listPoints.itemDoubleClicked.connect(self.listPoints_itemDoubleClicked)

		self.ui.nodesView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

		self.ui.nodesView.customContextMenuRequested.connect(self.nodesView_openMenu)
		self.ui.nodesView.itemExpanded.connect(self.item_expanded)

	def showEvent(self, *args, **kwargs):
		if not self.showed:
			self.parent().hide()
			self.current_node = None
			self.refresh_current_node()
			self.refresh_nodes()

			# if cur_main_session.query(ditem.DItem).filter(ditem.DItem.id == self.d_item_id).first().model_id is not None \
			# 		or self.d_item_id == 2:
			if True:
				self.ui.points_widget.setEnabled(True)
			else:
				self.ui.points_widget.setEnabled(False)

			if self.d_item_guid != 'b2b4dcb6-f5ea-4101-a7d8-7ae122c64171':
				self.ui.non_nazode_point_button.setVisible(True)
				self.ui.nazode_point_button.setVisible(False)

			else:
				self.ui.non_nazode_point_button.setVisible(False)
				self.ui.nazode_point_button.setVisible(True)

			self.showed = False

	def reinit(self, d_item_guid=None):
		self.d_item_guid = d_item_guid

	def reinit_tree(self, tree):
		def startDrag(t_self, event):
			# create mime data object
			mime = QtCore.QMimeData()
			mime.setData('application/x-item', "112")
			# start drag
			drag = QtGui.QDrag(t_self)
			drag.setMimeData(mime)
			drag.start(QtCore.Qt.MoveAction | QtCore.Qt.MoveAction)

		def dragMoveEvent(t_self, event):
			if event.mimeData().hasFormat("application/x-item"):
				event.setDropAction(QtCore.Qt.CopyAction)
				event.accept()
			else:
				event.ignore()

		def dragEnterEvent(t_self, event):
			if (event.mimeData().hasFormat('application/x-item')):
				event.accept()
			else:
				event.ignore()

		def dropEvent(t_self, event):
			sourceQCustomTreeWidget = event.source()
			if isinstance(sourceQCustomTreeWidget, QtGui.QTreeWidget):
				sourceQTreeWidgetItem = sourceQCustomTreeWidget.currentItem()
				if sourceQTreeWidgetItem is None:
					return

				s_data = sourceQTreeWidgetItem.data(0, 32).toPyObject().item
				destinationQTreeWidgetItem = t_self.itemAt(event.pos())
				if destinationQTreeWidgetItem is None:
					return

				d_data = destinationQTreeWidgetItem.data(0, 32).toPyObject().item
				if s_data.id == d_data.id:
					return
				if d_data.is_parrent(s_data):
					return

				s_data.parrent_guid = d_data.guid
				cur_main_session.add(s_data)
				cur_main_session.commit()
				self.refresh_nodes()

		tree.setDragEnabled(True)
		tree.setAcceptDrops(True)
		tree.setHeaderLabels([u'Название', 'ID'])
		tree.startDrag = types.MethodType(startDrag, tree)
		tree.dragMoveEvent = types.MethodType(dragMoveEvent, tree)
		tree.dragEnterEvent = types.MethodType(dragEnterEvent, tree)
		tree.dropEvent = types.MethodType(dropEvent, tree)

	def nodes_view_item_selection_changed(self):
		selected = self.ui.nodesView.selectedItems()
		if selected:
			item = selected[0].data(0, 32).toPyObject()
			if item is None or item is False:
				return
			self.current_node = (selected[0], item.item)
			self.refresh_current_node()

	def rtnReset_click(self):
		self.refresh_current_node()

	def open_add_node(self):
		if self.add_node_window is None:
			self.add_node_window = NodeAddWindow(self)
		if self.add_node_window.isMinimized():
			self.add_node_window.showNormal()
		self.add_node_window.reinit(self.current_node[1] if self.current_node is not None else None)
		self.add_node_window.show()
		self.add_node_window.raise_()

	def closeEvent(self, *args, **kwargs):
		self.parent().show()

	def refresh_current_node(self):
		self.refresh_images()
		self.refresh_points()

		if self.current_node is None:
			self.ui.nameEdit.setText("")
			self.ui.textDescription.setText("")
			return

		c_data = self.current_node[1]
		self.ui.nameEdit.setText(c_data.name)
		self.ui.textDescription.setText(c_data.description)
		self.ui.is_dia.setChecked(c_data.usable_us_dia)

	# self.ui.textDescription.setText(c_data.description)

	def load_tree_childs(self, target=None, p_guid=None):
		class ItemData(object):
			def __init__(self, item, is_loaded, has_child):
				self.item = item
				self.is_loaded = is_loaded
				self.has_child = has_child

		if target is None:
			target = self.ui.nodesView

		main_table = aliased(DNode)
		second_table = aliased(DNode)

		nodes = cur_main_session.query(main_table, count(second_table.id)) \
			.outerjoin(second_table, main_table.guid == second_table.parrent_guid) \
			.filter(main_table.parrent_guid == p_guid) \
			.filter(main_table.d_item_guid == self.d_item_guid) \
			.group_by(main_table.id) \
			.all()

		for n, c in nodes:
			t = QtGui.QTreeWidgetItem(target)
			t.setText(0, n.name)
			t.setText(1, str(n.id))
			t.setData(0, 32, ItemData(n, False, True if c > 0 else False))

			if c > 0:
				_t = QtGui.QTreeWidgetItem(t)
				_t.setText(0, '')
				_t.setText(1, '')
				_t.setData(0, QtCore.Qt.UserRole, False)

	def refresh_nodes(self):
		self.ui.nodesView.clear()
		self.load_tree_childs()

	def save_current_node(self):
		if self.current_node is None:
			return
		node_name = unicode(self.ui.nameEdit.text())
		if node_name == "":
			msg = QtGui.QMessageBox()
			msg.setIcon(QtGui.QMessageBox.Warning)
			msg.setText("Node name is empty")
			msg.setStandardButtons(QtGui.QMessageBox.Ok)
			msg.exec_()
			return

		db_item = self.current_node[1]
		db_item.name = node_name
		db_item.name_lower = node_name.lower()
		db_item.description = unicode(self.ui.textDescription.toPlainText())
		db_item.usable_us_dia = self.ui.is_dia.isChecked()

		cur_main_session.add(db_item)
		cur_main_session.commit()
		self.refresh_nodes()

	def point_add_click(self):
		if self.current_node is not None:
			if self.p_point_w is None:
				self.p_point_w = PointGLWindow(self)
			if self.p_point_w.isMinimized():
				self.p_point_w.showNormal()

			self.p_point_w.reinit(self.current_node[1], item=None, mode=0)
			self.p_point_w.show()
			self.p_point_w.raise_()

	def btnImg_add(self):
		if not self.current_node:
			return
		f = QtGui.QFileDialog.getOpenFileName(
			self.ui.centralwidget,
			'Выберите картинку',
			os.path.expanduser("~"),
			selectedFilter='PNG(*.png);;JPG(*.jpg);;BMP(*.bmp)'
		)

		if f:
			blob_value = open(unicode(f), 'rb').read()
			dm = DImage()
			dm.image = blob_value

			dm.d_node_guid = self.current_node[1].guid
			dm.description = os.path.basename(unicode(f))

			cur_main_session.add(dm)
			cur_main_session.commit()
			self.refresh_images()

	def btnImg_del(self):
		if len(self.ui.listImages.selectedItems()) > 0:
			item = self.ui.listImages.selectedItems()[0].data(32).toPyObject()
			cur_main_session.query(DImage).filter(DImage.id == item).delete()
			cur_main_session.commit()
			self.refresh_images()

	def refresh_images(self):
		self.ui.listImages.clear()
		if self.current_node is None:
			return

		images = cur_main_session.query(DImage) \
			.filter(DImage.d_node_guid == self.current_node[1].guid).all()

		for i in images:
			t = QtGui.QListWidgetItem()
			t.setText(i.description)
			t.setData(32, i.id)
			self.ui.listImages.addItem(t)

	def refresh_points(self):
		self.ui.listPoints.clear()
		if self.current_node is None:
			return

		if self.d_item_guid != 'b2b4dcb6-f5ea-4101-a7d8-7ae122c64171':
			points = cur_main_session.query(DPoint) \
				.filter(DPoint.d_node_guid == self.current_node[1].guid).all()
		else:
			points = self.current_node[1].nazode_points

		for i in points:
			t = QtGui.QListWidgetItem()
			t.setText(unicode(i))
			t.setData(32, i)
			self.ui.listPoints.addItem(t)

	def buttonDeleteNode_click(self):
		if len(self.ui.nodesView.selectedItems()) > 0:
			node = self.ui.nodesView.selectedItems()[0].data(0, 32).toPyObject().item
			node.delete_self(commit=True)
			self.refresh_nodes()
			if self.current_node and self.current_node[1].id == node.id:
				self.current_node = None
				self.refresh_current_node()

	def point_delete_click(self):
		if len(self.ui.listPoints.selectedItems()) > 0:
			item = self.ui.listPoints.selectedItems()[0].data(32).toPyObject()
			cur_main_session.query(DPoint).filter(DPoint.id == item.id).delete()
			cur_main_session.commit()
			self.refresh_points()

	def listImages_itemDoubleClicked(self):
		if len(self.ui.listImages.selectedItems()) > 0:
			item = self.ui.listImages.selectedItems()[0].data(32).toPyObject()
			if self.w_pic_w is None:
				self.w_pic_w = PicWindow(self)
			if self.w_pic_w.isMinimized():
				self.w_pic_w.showNormal()

			self.w_pic_w.reinit(item)
			self.w_pic_w.show()
			self.w_pic_w.raise_()

	def listPoints_itemDoubleClicked(self):
		if self.current_node is not None:
			if len(self.ui.listPoints.selectedItems()) > 0:
				item = self.ui.listPoints.selectedItems()[0].data(32).toPyObject()
				if self.p_point_w is None:
					self.p_point_w = PointGLWindow(self)
				if self.p_point_w.isMinimized():
					self.p_point_w.showNormal()

				if self.d_item_guid == 'b2b4dcb6-f5ea-4101-a7d8-7ae122c64171':
					self.p_point_w.reinit(self.current_node[1], item, mode=1)
				else:
					self.p_point_w.reinit(self.current_node[1], item, mode=0)

				self.p_point_w.show()
				self.p_point_w.raise_()

	def naz_buttonEditPoints_clicked(self):
		if self.current_node is not None:
			if self.p_point_w is None:
				self.p_point_w = PointGLWindow(self)
			if self.p_point_w.isMinimized():
				self.p_point_w.showNormal()

			self.p_point_w.reinit(self.current_node[1], mode=2)
			self.p_point_w.show()
			self.p_point_w.raise_()

	def nodesView_openMenu(self, pos):
		menu = QtGui.QMenu()
		to_top = menu.addAction(u"На верхний уровень")

		if to_top == menu.exec_(self.ui.nodesView.viewport().mapToGlobal(pos)):
			if len(self.ui.nodesView.selectedItems()) == 0:
				return

			node = self.ui.nodesView.selectedItems()[0].data(0, 32).toPyObject().item
			node.parrent_guid = self.d_node_parent.guid
			cur_main_session.commit()
			self.refresh_nodes()

	def item_expanded(self, item):
		item_data = item.data(0, QtCore.Qt.UserRole).toPyObject()
		if item is not None and (item_data.is_loaded or not item_data.has_child):
			return

		for i in xrange(item.childCount() - 1, -1, -1):
			child = item.child(i)
			item.removeChild(child)
			del child

		self.load_tree_childs(item, item_data.item.guid)

		item_data.is_loaded = True
		item.setData(0, QtCore.Qt.UserRole, item_data)
