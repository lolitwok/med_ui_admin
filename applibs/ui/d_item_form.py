# -*- coding: utf-8 -*-
import sys
from PyQt4 import QtGui, uic, QtCore, Qt

from applibs.dao import cur_main_session
from applibs.models.main_models import DNode
import uuid

(Ui_MainWindow, QMainWindow) = uic.loadUiType('applibs/ui/src/change_name.ui')


class DItemFormWindow(QtGui.QMainWindow):
	def __init__(self, parent=None):
		super(DItemFormWindow, self).__init__(parent)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.item = None
		self.d_item_guid = None

		self.ui.btn_change.clicked.connect(self.btn_change_click)
		self.ui.btn_exit.clicked.connect(self.close)

	def reinit(self, item=None, d_item_guid=None):
		self.item = item
		self.d_item_guid = d_item_guid

		if item is None:
			self.ui.btn_change.setText(u'Сохранить')
		else:
			self.ui.btn_change.setText(u'Изменить название')

	def showEvent(self, *args, **kwargs):
		self.parent().hide()
		self.ui.name_edit.clear()

		if self.item is not None:
			self.ui.name_edit.setText(self.item.name)

	def closeEvent(self, *args, **kwargs):
		self.parent().show()

	def btn_change_click(self):

		if unicode(self.ui.name_edit.text()).strip() == "":

			QtGui.QMessageBox.warning(self.ui.centralwidget, u"Сообщение", u"Имя не заполнено")
			# msg = QtGui.QMessageBox()
			# # msg.setIcon(QtGui.QMessageBox.Warning)
			# msg.setWindowTitle("dsdsad1")
			# msg.setText("dsdsad")
			# # msg.setText(u"Имя не заполнено")
			# msg.setStandardButtons(QtGui.QMessageBox.Ok)
			# msg.buttonClicked.connect(msgbtn)
			return True

		else:
			if self.item is not None:
				item = self.item
			else:
				item = DNode()
				item.d_item_guid = self.d_item_guid
				item.d_item_guid = self.d_item_guid
				item.usable_us_dia = False
				item.guid = uuid.uuid4()

			item.name = unicode(self.ui.name_edit.text()).strip()
			item.name_lower = unicode(item.name).lower()
			cur_main_session.add(item)
			cur_main_session.commit()
			self.parent().refresh_items()
			self.close()
