# -*- coding: utf-8 -*-
import sys
from applibs.objloaderthread import GLLoader
from applibs.ui.add_point import PointGLWindow
from applibs.ui.glwidget import glWidget
import types
from PyQt4 import QtGui, uic, QtCore, Qt

from applibs.dao import MainSession
from .list_sub_d import DListWindow
from .viewnodes import Window as NWindow

(Ui_MainWindow, QMainWindow) = uic.loadUiType('applibs/ui/src/main_diagnostics.ui')


class Window(QtGui.QMainWindow):
	def __init__(self, parent=None):
		super(Window, self).__init__(parent)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.dl_window = None
		self.n_window = None
		self.ui.btn1.clicked.connect(self.btn1_click)
		self.ui.btn2.clicked.connect(self.btn2_click)
		# self.ui.btn3.clicked.connect(self.btn3_click)
		# self.ui.btn4.clicked.connect(self.btn4_click)
		# self.ui.btn5.clicked.connect(self.btn5_click)

	# self.gl = gl
	# widget_layout = QtGui.QHBoxLayout()
	# self.ui.widget.setLayout(widget_layout)
	# widget_layout.addWidget(self.gl)


	def showEvent(self, *args, **kwargs):
		Window.gll = GLLoader(self)
		Window.gll.start()
		pass

	def open_by_type(self, t):
		# if self.dl_window is None:
		# 	self.dl_window = DListWindow(self)
		# if self.dl_window.isMinimized():
		# 	self.dl_window.showNormal()
		#
		# self.dl_window.reinit(_ditem_guid=t)
		# self.dl_window.show()
		# self.dl_window.raise_()


		if self.n_window is None:
			self.n_window = NWindow(self)
		if self.n_window.isMinimized():
			self.n_window.showNormal()

		self.n_window.reinit(t)
		self.n_window.show()
		self.n_window.raise_()


	def btn1_click(self):
		self.open_by_type("06dba33b-94ad-466a-8cea-3e5578ca865d")

	def btn2_click(self):
		self.open_by_type("b2b4dcb6-f5ea-4101-a7d8-7ae122c64171")

	# def btn3_click(self):
	# 	self.open_by_type("2")
	#
	# def btn4_click(self):
	# 	self.open_by_type("3")
	#
	# def btn5_click(self):
	# 	self.open_by_type("4")

	def closeEvent(self, *args, **kwargs):
		sys.exit(0)
