# -*- coding: utf-8 -*-

from PyQt4 import QtGui, uic, QtCore, Qt

from applibs.dao import cur_main_session
from applibs.models.main_models import DImage
import cStringIO

(Ui_MainWindow, QMainWindow) = uic.loadUiType('applibs/ui/src/pict.ui')


class PicWindow(QtGui.QMainWindow):
	def __init__(self, parent=None):
		super(PicWindow, self).__init__(parent)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self._id = None

	def showEvent(self, *args, **kwargs):
		self.show_pict()

	def reinit(self, _id):
		self._id = _id

	def show_pict(self):
		self.ui.label.clear()
		if self._id:
			data = cur_main_session.query(DImage).filter(DImage.id == self._id).first()
			if data:
				as_file = cStringIO.StringIO(data.image)
				pixmap = Qt.QPixmap()
				pixmap.loadFromData(as_file.read())
				self.ui.label.setPixmap(pixmap)
