# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'applibs\ui\src\point_edit.ui'
#
# Created: Tue Jun 20 20:35:39 2017
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(935, 572)
        MainWindow.setMinimumSize(QtCore.QSize(628, 572))
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setContentsMargins(9, -1, -1, -1)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.all_points = QtGui.QWidget(self.centralwidget)
        self.all_points.setMinimumSize(QtCore.QSize(272, 0))
        self.all_points.setMaximumSize(QtCore.QSize(272, 16777215))
        self.all_points.setObjectName(_fromUtf8("all_points"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.all_points)
        self.verticalLayout_4.setMargin(0)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.label_5 = QtGui.QLabel(self.all_points)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.verticalLayout_4.addWidget(self.label_5)
        self.all_points_list = QtGui.QListWidget(self.all_points)
        self.all_points_list.setObjectName(_fromUtf8("all_points_list"))
        self.verticalLayout_4.addWidget(self.all_points_list)
        self.add_empty_btn = QtGui.QPushButton(self.all_points)
        self.add_empty_btn.setObjectName(_fromUtf8("add_empty_btn"))
        self.verticalLayout_4.addWidget(self.add_empty_btn)
        self.del_from_list_btn = QtGui.QPushButton(self.all_points)
        self.del_from_list_btn.setObjectName(_fromUtf8("del_from_list_btn"))
        self.verticalLayout_4.addWidget(self.del_from_list_btn)
        self.horizontalLayout_2.addWidget(self.all_points)
        self.config = QtGui.QWidget(self.centralwidget)
        self.config.setMinimumSize(QtCore.QSize(272, 550))
        self.config.setMaximumSize(QtCore.QSize(272, 16777215))
        self.config.setObjectName(_fromUtf8("config"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.config)
        self.verticalLayout_3.setMargin(0)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.label_4 = QtGui.QLabel(self.config)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sans Uralic"))
        font.setPointSize(12)
        self.label_4.setFont(font)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayout_3.addWidget(self.label_4)
        self.models_combobox = QtGui.QComboBox(self.config)
        self.models_combobox.setMaximumSize(QtCore.QSize(16777215, 37))
        self.models_combobox.setStyleSheet(_fromUtf8("#models_combobox\n"
"{    border-radius: 10px;\n"
"    background-color: white;\n"
"    padding: 5px 10px;\n"
"    max-height: 27px;\n"
"}"))
        self.models_combobox.setObjectName(_fromUtf8("models_combobox"))
        self.verticalLayout_3.addWidget(self.models_combobox)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setSizeConstraint(QtGui.QLayout.SetMinimumSize)
        self.horizontalLayout_3.setMargin(5)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label = QtGui.QLabel(self.config)
        self.label.setMaximumSize(QtCore.QSize(30, 16777215))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sans Uralic"))
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_3.addWidget(self.label)
        self.x_sp = QtGui.QDoubleSpinBox(self.config)
        self.x_sp.setDecimals(4)
        self.x_sp.setMinimum(-5.0)
        self.x_sp.setMaximum(5.0)
        self.x_sp.setSingleStep(0.001)
        self.x_sp.setObjectName(_fromUtf8("x_sp"))
        self.horizontalLayout_3.addWidget(self.x_sp)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.x_sl = QtGui.QSlider(self.config)
        self.x_sl.setMinimumSize(QtCore.QSize(0, 30))
        self.x_sl.setStyleSheet(_fromUtf8("QSlider::groove:horizontal {\n"
"    border: 1px solid #999999;\n"
"    height: 6px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);\n"
"    margin: 5px 0 15px 0;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 7px;\n"
"    margin: -7px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 3px;\n"
"}"))
        self.x_sl.setMaximum(1000)
        self.x_sl.setProperty("value", 500)
        self.x_sl.setOrientation(QtCore.Qt.Horizontal)
        self.x_sl.setObjectName(_fromUtf8("x_sl"))
        self.verticalLayout_3.addWidget(self.x_sl)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setSizeConstraint(QtGui.QLayout.SetMinimumSize)
        self.horizontalLayout_4.setContentsMargins(5, 0, 5, 0)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_2 = QtGui.QLabel(self.config)
        self.label_2.setMaximumSize(QtCore.QSize(30, 16777215))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sans Uralic"))
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_4.addWidget(self.label_2)
        self.y_sp = QtGui.QDoubleSpinBox(self.config)
        self.y_sp.setDecimals(4)
        self.y_sp.setMinimum(-5.0)
        self.y_sp.setMaximum(5.0)
        self.y_sp.setSingleStep(0.001)
        self.y_sp.setObjectName(_fromUtf8("y_sp"))
        self.horizontalLayout_4.addWidget(self.y_sp)
        self.verticalLayout_3.addLayout(self.horizontalLayout_4)
        self.y_sl = QtGui.QSlider(self.config)
        self.y_sl.setMinimumSize(QtCore.QSize(0, 30))
        self.y_sl.setStyleSheet(_fromUtf8("QSlider::groove:horizontal {\n"
"    border: 1px solid #999999;\n"
"    height: 6px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);\n"
"    margin: 5px 0 15px 0;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 7px;\n"
"    margin: -7px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 3px;\n"
"}"))
        self.y_sl.setMaximum(1000)
        self.y_sl.setProperty("value", 500)
        self.y_sl.setOrientation(QtCore.Qt.Horizontal)
        self.y_sl.setObjectName(_fromUtf8("y_sl"))
        self.verticalLayout_3.addWidget(self.y_sl)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setSizeConstraint(QtGui.QLayout.SetMinimumSize)
        self.horizontalLayout_5.setContentsMargins(5, 0, 5, 0)
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.label_3 = QtGui.QLabel(self.config)
        self.label_3.setMinimumSize(QtCore.QSize(0, 0))
        self.label_3.setMaximumSize(QtCore.QSize(30, 16777215))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sans Uralic"))
        font.setPointSize(12)
        self.label_3.setFont(font)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_5.addWidget(self.label_3)
        self.z_sp = QtGui.QDoubleSpinBox(self.config)
        self.z_sp.setDecimals(4)
        self.z_sp.setMinimum(-5.0)
        self.z_sp.setMaximum(5.0)
        self.z_sp.setSingleStep(0.001)
        self.z_sp.setObjectName(_fromUtf8("z_sp"))
        self.horizontalLayout_5.addWidget(self.z_sp)
        self.verticalLayout_3.addLayout(self.horizontalLayout_5)
        self.z_sl = QtGui.QSlider(self.config)
        self.z_sl.setMinimumSize(QtCore.QSize(0, 30))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.z_sl.setFont(font)
        self.z_sl.setStyleSheet(_fromUtf8("QSlider::groove:horizontal {\n"
"    border: 1px solid #999999;\n"
"    height: 6px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);\n"
"    margin: 5px 0 15px 0;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 7px;\n"
"    margin: -7px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 3px;\n"
"}"))
        self.z_sl.setMinimum(0)
        self.z_sl.setMaximum(1000)
        self.z_sl.setProperty("value", 500)
        self.z_sl.setSliderPosition(500)
        self.z_sl.setOrientation(QtCore.Qt.Horizontal)
        self.z_sl.setObjectName(_fromUtf8("z_sl"))
        self.verticalLayout_3.addWidget(self.z_sl)
        self.horizontalLayout_8 = QtGui.QHBoxLayout()
        self.horizontalLayout_8.setSizeConstraint(QtGui.QLayout.SetMinimumSize)
        self.horizontalLayout_8.setMargin(5)
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        self.label_6 = QtGui.QLabel(self.config)
        self.label_6.setMaximumSize(QtCore.QSize(30, 16777215))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sans Uralic"))
        font.setPointSize(12)
        self.label_6.setFont(font)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.horizontalLayout_8.addWidget(self.label_6)
        self.radius_sp = QtGui.QDoubleSpinBox(self.config)
        self.radius_sp.setDecimals(5)
        self.radius_sp.setMinimum(0.0001)
        self.radius_sp.setMaximum(0.5)
        self.radius_sp.setSingleStep(0.001)
        self.radius_sp.setProperty("value", 0.2)
        self.radius_sp.setObjectName(_fromUtf8("radius_sp"))
        self.horizontalLayout_8.addWidget(self.radius_sp)
        self.verticalLayout_3.addLayout(self.horizontalLayout_8)
        self.radius_sl = QtGui.QSlider(self.config)
        self.radius_sl.setMinimumSize(QtCore.QSize(0, 30))
        self.radius_sl.setStyleSheet(_fromUtf8("QSlider::groove:horizontal {\n"
"    border: 1px solid #999999;\n"
"    height: 6px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */\n"
"    background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);\n"
"    margin: 5px 0 15px 0;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 7px;\n"
"    margin: -7px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 3px;\n"
"}"))
        self.radius_sl.setMinimum(1)
        self.radius_sl.setMaximum(5000)
        self.radius_sl.setProperty("value", 2000)
        self.radius_sl.setOrientation(QtCore.Qt.Horizontal)
        self.radius_sl.setObjectName(_fromUtf8("radius_sl"))
        self.verticalLayout_3.addWidget(self.radius_sl)
        self.label_10 = QtGui.QLabel(self.config)
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.verticalLayout_3.addWidget(self.label_10)
        self.horizontalLayout_9 = QtGui.QHBoxLayout()
        self.horizontalLayout_9.setSizeConstraint(QtGui.QLayout.SetMinimumSize)
        self.horizontalLayout_9.setMargin(5)
        self.horizontalLayout_9.setObjectName(_fromUtf8("horizontalLayout_9"))
        self.label_7 = QtGui.QLabel(self.config)
        self.label_7.setMaximumSize(QtCore.QSize(30, 16777215))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sans Uralic"))
        font.setPointSize(12)
        self.label_7.setFont(font)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.horizontalLayout_9.addWidget(self.label_7)
        self.offset_x_sp = QtGui.QDoubleSpinBox(self.config)
        self.offset_x_sp.setDecimals(5)
        self.offset_x_sp.setMinimum(-10.0)
        self.offset_x_sp.setMaximum(10.0)
        self.offset_x_sp.setSingleStep(0.01)
        self.offset_x_sp.setProperty("value", 0.0)
        self.offset_x_sp.setObjectName(_fromUtf8("offset_x_sp"))
        self.horizontalLayout_9.addWidget(self.offset_x_sp)
        self.verticalLayout_3.addLayout(self.horizontalLayout_9)
        self.horizontalLayout_10 = QtGui.QHBoxLayout()
        self.horizontalLayout_10.setSizeConstraint(QtGui.QLayout.SetMinimumSize)
        self.horizontalLayout_10.setMargin(5)
        self.horizontalLayout_10.setObjectName(_fromUtf8("horizontalLayout_10"))
        self.label_8 = QtGui.QLabel(self.config)
        self.label_8.setMaximumSize(QtCore.QSize(30, 16777215))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sans Uralic"))
        font.setPointSize(12)
        self.label_8.setFont(font)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.horizontalLayout_10.addWidget(self.label_8)
        self.offset_y_sp = QtGui.QDoubleSpinBox(self.config)
        self.offset_y_sp.setDecimals(5)
        self.offset_y_sp.setMinimum(-10.0)
        self.offset_y_sp.setMaximum(10.0)
        self.offset_y_sp.setSingleStep(0.01)
        self.offset_y_sp.setProperty("value", 0.0)
        self.offset_y_sp.setObjectName(_fromUtf8("offset_y_sp"))
        self.horizontalLayout_10.addWidget(self.offset_y_sp)
        self.verticalLayout_3.addLayout(self.horizontalLayout_10)
        self.horizontalLayout_11 = QtGui.QHBoxLayout()
        self.horizontalLayout_11.setSizeConstraint(QtGui.QLayout.SetMinimumSize)
        self.horizontalLayout_11.setMargin(5)
        self.horizontalLayout_11.setObjectName(_fromUtf8("horizontalLayout_11"))
        self.label_9 = QtGui.QLabel(self.config)
        self.label_9.setMaximumSize(QtCore.QSize(30, 16777215))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sans Uralic"))
        font.setPointSize(12)
        self.label_9.setFont(font)
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.horizontalLayout_11.addWidget(self.label_9)
        self.offset_z_sp = QtGui.QDoubleSpinBox(self.config)
        self.offset_z_sp.setDecimals(5)
        self.offset_z_sp.setMinimum(-10.0)
        self.offset_z_sp.setMaximum(10.0)
        self.offset_z_sp.setSingleStep(0.01)
        self.offset_z_sp.setProperty("value", 0.0)
        self.offset_z_sp.setObjectName(_fromUtf8("offset_z_sp"))
        self.horizontalLayout_11.addWidget(self.offset_z_sp)
        self.verticalLayout_3.addLayout(self.horizontalLayout_11)
        self.label_11 = QtGui.QLabel(self.config)
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.verticalLayout_3.addWidget(self.label_11)
        self.horizontalLayout_12 = QtGui.QHBoxLayout()
        self.horizontalLayout_12.setSizeConstraint(QtGui.QLayout.SetMinimumSize)
        self.horizontalLayout_12.setMargin(5)
        self.horizontalLayout_12.setObjectName(_fromUtf8("horizontalLayout_12"))
        self.label_12 = QtGui.QLabel(self.config)
        self.label_12.setMaximumSize(QtCore.QSize(30, 16777215))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sans Uralic"))
        font.setPointSize(12)
        self.label_12.setFont(font)
        self.label_12.setObjectName(_fromUtf8("label_12"))
        self.horizontalLayout_12.addWidget(self.label_12)
        self.rotate_y_sp = QtGui.QDoubleSpinBox(self.config)
        self.rotate_y_sp.setDecimals(3)
        self.rotate_y_sp.setMinimum(-360.0)
        self.rotate_y_sp.setMaximum(360.0)
        self.rotate_y_sp.setSingleStep(0.5)
        self.rotate_y_sp.setProperty("value", 0.0)
        self.rotate_y_sp.setObjectName(_fromUtf8("rotate_y_sp"))
        self.horizontalLayout_12.addWidget(self.rotate_y_sp)
        self.verticalLayout_3.addLayout(self.horizontalLayout_12)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem)
        self.btn_save = QtGui.QPushButton(self.config)
        self.btn_save.setObjectName(_fromUtf8("btn_save"))
        self.verticalLayout_3.addWidget(self.btn_save)
        self.horizontalLayout_2.addWidget(self.config)
        self.lists_p = QtGui.QWidget(self.centralwidget)
        self.lists_p.setMinimumSize(QtCore.QSize(274, 0))
        self.lists_p.setMaximumSize(QtCore.QSize(274, 16777215))
        self.lists_p.setObjectName(_fromUtf8("lists_p"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.lists_p)
        self.verticalLayout_2.setMargin(0)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.list_points = QtGui.QListWidget(self.lists_p)
        self.list_points.setObjectName(_fromUtf8("list_points"))
        self.verticalLayout_2.addWidget(self.list_points)
        self.horizontalLayout_13 = QtGui.QHBoxLayout()
        self.horizontalLayout_13.setObjectName(_fromUtf8("horizontalLayout_13"))
        self.up_button = QtGui.QPushButton(self.lists_p)
        self.up_button.setObjectName(_fromUtf8("up_button"))
        self.horizontalLayout_13.addWidget(self.up_button)
        self.down_button = QtGui.QPushButton(self.lists_p)
        self.down_button.setObjectName(_fromUtf8("down_button"))
        self.horizontalLayout_13.addWidget(self.down_button)
        self.verticalLayout_2.addLayout(self.horizontalLayout_13)
        self.list_all = QtGui.QListWidget(self.lists_p)
        self.list_all.setObjectName(_fromUtf8("list_all"))
        self.verticalLayout_2.addWidget(self.list_all)
        self.btn_save_2 = QtGui.QPushButton(self.lists_p)
        self.btn_save_2.setEnabled(True)
        self.btn_save_2.setObjectName(_fromUtf8("btn_save_2"))
        self.verticalLayout_2.addWidget(self.btn_save_2)
        self.horizontalLayout_2.addWidget(self.lists_p)
        self.hl_main = QtGui.QHBoxLayout()
        self.hl_main.setObjectName(_fromUtf8("hl_main"))
        self.horizontalLayout_2.addLayout(self.hl_main)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.label_5.setText(_translate("MainWindow", "Все точки", None))
        self.add_empty_btn.setText(_translate("MainWindow", "Добавить", None))
        self.del_from_list_btn.setText(_translate("MainWindow", "Удалить", None))
        self.label_4.setText(_translate("MainWindow", "Модель:", None))
        self.label.setText(_translate("MainWindow", "X:", None))
        self.label_2.setText(_translate("MainWindow", "Y:", None))
        self.label_3.setText(_translate("MainWindow", "Z:", None))
        self.label_6.setText(_translate("MainWindow", "R:", None))
        self.label_10.setText(_translate("MainWindow", "Сдвиг", None))
        self.label_7.setText(_translate("MainWindow", "X:", None))
        self.label_8.setText(_translate("MainWindow", "Y:", None))
        self.label_9.setText(_translate("MainWindow", "Z:", None))
        self.label_11.setText(_translate("MainWindow", "Поворот", None))
        self.label_12.setText(_translate("MainWindow", "A:", None))
        self.btn_save.setText(_translate("MainWindow", "Сохранить", None))
        self.up_button.setText(_translate("MainWindow", "UP", None))
        self.down_button.setText(_translate("MainWindow", "DOWN", None))
        self.btn_save_2.setText(_translate("MainWindow", "Сохранить", None))

