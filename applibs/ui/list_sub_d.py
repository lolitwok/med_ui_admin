# -*- coding: utf-8 -*-
import sys
import types
from PyQt4 import QtGui, uic, QtCore, Qt

from applibs.dao import cur_main_session
from applibs.models.main_models import DItem, DNode
from .d_item_form import DItemFormWindow
from .viewnodes import Window as NWindow

LIST_DIA = {
	'0': {
		'name': u'Функциональная карта'
	},

	'1': {
		'name': u'Диагностика с назодами'
	},

	'2': {
		'name': u'Тест ауры'
	},

	'3': {
		'name': u'Вегето-резонансный тест'
	},

	'4': {
		'name': u'Психологический тест'
	},
}

(Ui_MainWindow, QMainWindow) = uic.loadUiType('applibs/ui/src/sub_diagnostics.ui')


class DListWindow(QtGui.QMainWindow):
	def __init__(self, parent=None):
		super(DListWindow, self).__init__(parent)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.add_item_window = None
		self.n_window = None
		self._ditem_guid = None

		self.ui.btn_add.clicked.connect(self.btn_add_click)
		self.ui.btn_edit.clicked.connect(self.btn_edit_click)
		self.ui.btn_del.clicked.connect(self.btn_del_click)
		self.ui.main_list.itemDoubleClicked.connect(self.main_list_item_double_clicked)

	def reinit(self, _ditem_guid):
		self._ditem_guid = _ditem_guid

	def showEvent(self, *args, **kwargs):
		self.parent().hide()
		caption = cur_main_session.query(DItem).filter(DItem.guid == self._ditem_guid).first().name
		self.ui.label.setText(caption)
		self.refresh_items()

	def closeEvent(self, *args, **kwargs):
		self.parent().show()

	def refresh_items(self):
		self.ui.main_list.clear()
		items = cur_main_session.query(DNode)\
			.filter(DNode.parrent_guid == None)\
			.filter(DNode.d_item_guid == self._ditem_guid)\
			.all()

		for n in items:
			t = QtGui.QListWidgetItem(n.name)
			t.setData(32, n)
			self.ui.main_list.addItem(t)

	def btn_edit_click(self):
		if len(self.ui.main_list.selectedItems()) > 0:
			item = self.ui.main_list.selectedItems()[0].data(32).toPyObject()
			if self.add_item_window is None:
				self.add_item_window = DItemFormWindow(self)
			if self.add_item_window.isMinimized():
				self.add_item_window.showNormal()

			self.add_item_window.reinit(item=item)
			self.add_item_window.show()
			self.add_item_window.raise_()

	def btn_add_click(self):

		if self.add_item_window is None:
			self.add_item_window = DItemFormWindow(self)
		if self.add_item_window.isMinimized():
			self.add_item_window.showNormal()

		self.add_item_window.reinit(d_item_guid=self._ditem_guid)
		self.add_item_window.show()
		self.add_item_window.raise_()

	def btn_del_click(self):
		if len(self.ui.main_list.selectedItems()) > 0:
			item = self.ui.main_list.selectedItems()[0].data(32).toPyObject()
			cur_main_session.query(DItem).filter(DItem.id == item.id).delete()
			item.delete_self(commit=True)
			self.refresh_items()

	def main_list_item_double_clicked(self, item):
		item = item.data(32).toPyObject()
		if self.n_window is None:
			self.n_window = NWindow(self)
		if self.n_window.isMinimized():
			self.n_window.showNormal()

		self.n_window.reinit(item)
		self.n_window.show()
		self.n_window.raise_()
