import sys
from PyQt4 import QtGui, uic, QtCore, Qt

from applibs.dao import cur_main_session
from applibs.models.main_models import DNode
import uuid

(Ui_MainWindow, QMainWindow) = uic.loadUiType('applibs/ui/src/nodes_edit.ui')
(Ui_NodeAddWindow, QNodeAddWindow) = uic.loadUiType('applibs/ui/src/node_add.ui')


class NodeAddWindow(QtGui.QMainWindow):
	def __init__(self, parent=None):
		super(NodeAddWindow, self).__init__(parent)
		self.ui = Ui_NodeAddWindow()
		# self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
		self.ui.setupUi(self)
		self.ui.buttonAdd.clicked.connect(self.add_node)
		self.dnode = None

	def reinit(self, dnode):
		self.dnode = dnode

	def showEvent(self, *args, **kwargs):
		nodes = cur_main_session.query(DNode) \
			.filter(DNode.d_item_guid == self.parent().d_item_guid)

		if self.dnode:
			nodes = nodes.filter(DNode.parrent_guid == self.dnode.parrent_guid)
		nodes = nodes.all()

		self.ui.nodesList.clear()
		item = QtGui.QListWidgetItem("(None)")
		item.setData(32, None)
		self.ui.nodesList.addItem(item)
		selected_item = self.parent().current_node
		if selected_item is None:
			self.ui.nodesList.setItemSelected(item, True)

		for i in nodes:
			item = QtGui.QListWidgetItem("Item %s, name %s" % (i.id, i.name))
			item.setData(32, i)
			self.ui.nodesList.addItem(item)
			if selected_item is not None and selected_item[1].id == i.id:
				self.ui.nodesList.setItemSelected(item, True)

	def add_node(self):
		if len(self.ui.nodesList.selectedIndexes()) > 0:
			if unicode(self.ui.nodeName.text()).strip() == "":
				msg = QtGui.QMessageBox()
				msg.setIcon(QtGui.QMessageBox.Warning)
				msg.setText("Node name is empty")
				msg.setStandardButtons(QtGui.QMessageBox.Ok)
				msg.exec_()
				return

			dnode = self.ui.nodesList.selectedIndexes()[0].data(32).toPyObject()
			n = DNode()
			n.guid = str(uuid.uuid4())
			n.parrent_guid = dnode.guid if dnode is not None else None
			n.d_item_guid = dnode.d_item_guid if dnode is not None else self.parent().d_item_guid
			n.name = unicode(self.ui.nodeName.text())
			n.name_lower = unicode(self.ui.nodeName.text()).lower()
			cur_main_session.add(n)
			cur_main_session.commit()
			self.parent().refresh_nodes()
			self.close()

		else:
			msg = QtGui.QMessageBox()
			msg.setIcon(QtGui.QMessageBox.Warning)
			msg.setText("Please select Node")
			msg.setStandardButtons(QtGui.QMessageBox.Ok)
			msg.exec_()
			return
