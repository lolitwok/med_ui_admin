# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s

try:
	_encoding = QtGui.QApplication.UnicodeUTF8


	def _translate(context, text, disambig):
		return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
	def _translate(context, text, disambig):
		return QtGui.QApplication.translate(context, text, disambig)


class UiWindowMain(object):
	def setup_Ui(self, window_main):
		window_main.setObjectName(_fromUtf8("window_main"))
		window_main.setWindowModality(QtCore.Qt.NonModal)
		window_main.setEnabled(True)
		window_main.resize(257, 282)
		window_main.setMinimumSize(QtCore.QSize(257, 282))
		window_main.setMaximumSize(QtCore.QSize(257, 282))
		window_main.setSizeIncrement(QtCore.QSize(0, 0))
		window_main.setWhatsThis(_fromUtf8(""))
		window_main.setSizeGripEnabled(False)
		window_main.setModal(False)
		self.lcdNumber = QtGui.QLCDNumber(window_main)
		self.lcdNumber.setGeometry(QtCore.QRect(8, 8, 241, 111))
		font = QtGui.QFont()
		font.setPointSize(10)
		self.lcdNumber.setFont(font)
		self.lcdNumber.setObjectName(_fromUtf8("lcdNumber"))
		self.label_btn1 = QtGui.QLabel(window_main)
		self.label_btn1.setGeometry(QtCore.QRect(20, 130, 91, 31))
		font = QtGui.QFont()
		font.setPointSize(10)
		font.setBold(True)
		font.setWeight(75)
		self.label_btn1.setFont(font)
		self.label_btn1.setAlignment(QtCore.Qt.AlignCenter)
		self.label_btn1.setObjectName(_fromUtf8("label_btn1"))
		self.label_btn2 = QtGui.QLabel(window_main)
		self.label_btn2.setGeometry(QtCore.QRect(140, 130, 101, 31))
		font = QtGui.QFont()
		font.setPointSize(10)
		font.setBold(True)
		font.setWeight(75)
		self.label_btn2.setFont(font)
		self.label_btn2.setLayoutDirection(QtCore.Qt.LeftToRight)
		self.label_btn2.setTextFormat(QtCore.Qt.PlainText)
		self.label_btn2.setAlignment(QtCore.Qt.AlignCenter)
		self.label_btn2.setObjectName(_fromUtf8("label_btn2"))
		self.button_open_hand = QtGui.QPushButton(window_main)
		self.button_open_hand.setGeometry(QtCore.QRect(9, 180, 241, 41))
		font = QtGui.QFont()
		font.setPointSize(10)
		self.button_open_hand.setFont(font)
		self.button_open_hand.setObjectName(_fromUtf8("button_open_hand"))
		self.label_status = QtGui.QLabel(window_main)
		self.label_status.setGeometry(QtCore.QRect(20, 240, 61, 31))
		font = QtGui.QFont()
		font.setPointSize(10)
		self.label_status.setFont(font)
		self.label_status.setObjectName(_fromUtf8("label_status"))
		self.label_status_text = QtGui.QLabel(window_main)
		self.label_status_text.setGeometry(QtCore.QRect(80, 240, 161, 31))
		font = QtGui.QFont()
		font.setPointSize(10)
		self.label_status_text.setFont(font)
		self.label_status_text.setText(_fromUtf8(""))
		self.label_status_text.setObjectName(_fromUtf8("label_status_text"))

		self.retranslateUi(window_main)
		QtCore.QMetaObject.connectSlotsByName(window_main)

	def retranslateUi(self, window_main):
		window_main.setWindowTitle(_translate("window_main", "Dialog", None))
		self.label_btn1.setText(_translate("window_main", "Button1", None))
		self.label_btn2.setText(_translate("window_main", "Button2", None))
		self.button_open_hand.setText(_translate("window_main", "Open hand", None))
		self.label_status.setText(_translate("window_main", "Status", None))
