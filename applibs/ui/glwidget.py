import math
import copy
import os

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from PyQt4.QtOpenGL import *
from PyQt4 import QtGui, QtCore

from applibs.objloader_default import OBJ
from applibs.dao import BASE_DIR
from threading import Thread


class glWidget(QGLWidget):
	def set_x(self, value):
		self.point['distance_x'] += value

		if self.point['distance_x'] > 359.99:
			self.point['distance_x'] -= 359.99

		if self.point['distance_x'] < 0:
			self.point['distance_x'] += 359.99

	def set_z(self, value):
		self.point['distance_z'] += value

		if self.point['distance_z'] > 179.99:
			self.point['distance_z'] = 179.99

		if self.point['distance_z'] < -179.99:
			self.point['distance_z'] = -179.99

	def __init__(self, parent, preloader=None):
		# QGLWidget.__init__(self, None)
		super(glWidget, self).__init__(parent, preloader.glw)
		# QGLWidget.__init__(self, parent)
		self.parent1 = parent
		self.setMinimumSize(640, 480)

		self.m = QtCore.QPoint()
		self.test_model = None
		self.initia = False
		self.changeable = True
		self.model_file = None
		self.point_list = None
		self.dev_mode = False
		self.preloader = preloader
		self.models_list = {}

		self.points = []

		self.default = {
			'point': {
				'x': 0.0,
				'y': 0.0,
				'z': 0.0,
				'radius': 0.1,

				'distance_x': 1.0,
				'distance_y': 0.0,
				'distance_z': 1.0,
				'distance_length': -10.0,

				'distance_x_offset': 0.0,
				'distance_y_offset': 0.0,
				'distance_z_offset': 0.0,
			}
		}

		self.point = copy.copy(self.default['point'])

	def reinit(self, model_file=None, point=None, changeable=None, dev=None):
		if dev is not None:
			self.dev_mode = dev

		if changeable is not None:
			self.changeable = changeable

		if point is not None:
			self.points = [point, ]
			self.point = point

		if model_file is not None:
			if self.model_file != model_file:
				self.model_file = model_file
				if self.initia:
					self.test_model = self.preloader.get_model(self.model_file)[1]
				# self.test_model.render()
				# self.test_model = OBJ(model_file, swapyz=True, res_dir=os.path.join(BASE_DIR, 'resources'))

		if self.point_list is not None:
			if self.initia:
				self.render_points()

	def glInit(self):
		super(glWidget, self).glInit()
		self.initia = True
		self.load()
		self.render_points()

	def render_points(self):
		import math
		def draw1(radius, lats, longs):
			for i in range(0, lats + 1):
				lat0 = 2 * math.pi * float(i) / lats
				z0 = math.sin(lat0) * radius
				zr0 = math.cos(lat0) * radius

				lat1 = 2 * math.pi * float(i + 1) / lats
				z1 = math.sin(lat1) * radius
				zr1 = math.cos(lat1) * radius

				glColor3f(1.0, 0.0, 0.0)
				glBegin(GL_QUAD_STRIP)
				for j in range(0, longs + 1):
					lng = 2 * math.pi * float(j) / longs
					x = math.cos(lng)
					y = math.sin(lng)

					glNormal3f(x * zr0, y * zr0, z0)
					glVertex3f(x * zr0, y * zr0, z0)
					glNormal3f(x * zr1, y * zr1, z1)
					glVertex3f(x * zr1, y * zr1, z1)
				glEnd()

		self.point_list = glGenLists(1)
		glNewList(self.point_list, GL_COMPILE)
		for point in self.points:
			glPushMatrix()
			glTranslatef(point['x'], point['y'], point['z'])

			if self.dev_mode:
				glBegin(GL_LINES)
				glColor3f(1.0, 0.0, 0.0)
				glVertex3f(0.0, 0.0, 0.0)
				glVertex3f(3.0, 0.0, 0.0)
				glEnd()

				glBegin(GL_LINES)
				glColor3f(0.0, 1.0, 0.0)
				glVertex3f(0.0, 0.0, 0.0)
				glVertex3f(0.0, 3.0, 0.0)
				glEnd()

				glBegin(GL_LINES)
				glColor3f(0.0, 0.0, 1.0)
				glVertex3f(0.0, 0.0, 0.0)
				glVertex3f(0.0, 0.0, 3.0)
				glEnd()

			draw1(point['radius'], 25, 25)
			glPopMatrix()
		glEndList()

	# def start_load(self):
	# 	print 'start1'
	# 	ml.start()
	# 	print 'start2'

	def draw_point(self):
		glPushMatrix()
		# glTranslatef(self.point['x'], self.point['y'], self.point['z'])
		#
		# if self.dev_mode:
		# 	glBegin(GL_LINES)
		# 	glColor3f(1.0, 0.0, 0.0)
		# 	glVertex3f(0.0, 0.0, 0.0)
		# 	glVertex3f(3.0, 0.0, 0.0)
		# 	glEnd()
		#
		# 	glBegin(GL_LINES)
		# 	glColor3f(0.0, 1.0, 0.0)
		# 	glVertex3f(0.0, 0.0, 0.0)
		# 	glVertex3f(0.0, 3.0, 0.0)
		# 	glEnd()
		#
		# 	glBegin(GL_LINES)
		# 	glColor3f(0.0, 0.0, 1.0)
		# 	glVertex3f(0.0, 0.0, 0.0)
		# 	glVertex3f(0.0, 0.0, 3.0)
		# 	glEnd()

		if self.point_list is not None:
			glCallList(self.point_list)
		glPopMatrix()

	def set_point_params(self, point):
		# last_radius = copy.copy(self.point['radius'])

		self.points = [point, ]
		self.point = point

		if self.initia:
			# if self.initia and self.point_list is not None:
			# if last_radius != self.point['radius']:
			self.render_points()

			self.draw_point()

	def load(self):
		if self.model_file is not None:
			# self.test_model = OBJ(self.model_file, swapyz=True, res_dir=os.path.join(BASE_DIR, 'resources'))
			self.test_model = self.preloader.get_model(self.model_file)[1]
		# self.test_model.render()

	def paintGL(self):

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
		# glClearColor(0.7,0.9,1,1)
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()

		# Add ambient light:
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, [0.2, 0.2, 0.2, 1.0])

		# Add positioned light:
		# glLightfv(GL_LIGHT0, GL_DIFFUSE, [2, 2, 2, 1])
		# glLightfv(GL_LIGHT0, GL_POSITION, [4, 8, 1, 1])

		# glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
		# glLoadIdentity()

		glTranslatef(0.0, 0.0, self.point['distance_length'])
		glColor3f(1.0, 1.0, 1.0)
		glPolygonMode(GL_FRONT, GL_FILL)

		glRotated(self.point['distance_x'] * 1, 1.0, 0, 0)
		glRotated(self.point['distance_y'] * 1, 0, 1.0, 0)
		glRotated(self.point['distance_z'] * 1, 0, 0, 1.0)

		glTranslatef(self.point['distance_x_offset'], self.point['distance_y_offset'], self.point['distance_z_offset'])

		if self.model_file is not None:
			if self.test_model is None:
				self.load()

			self.draw()

		glFlush()

	def draw(self):
		self.makeCurrent()
		glPushMatrix()
		glCallList(self.test_model.gl_list)

		if self.dev_mode:
			glBegin(GL_LINES)
			glColor3f(1.0, 0.0, 0.0)
			glVertex3f(0.0, 0.0, 0.0)
			glVertex3f(5.0, 0.0, 0.0)
			glEnd()

			glBegin(GL_LINES)
			glColor3f(0.0, 1.0, 0.0)
			glVertex3f(0.0, 0.0, 0.0)
			glVertex3f(0.0, 5.0, 0.0)
			glEnd()

			glBegin(GL_LINES)
			glColor3f(0.0, 0.0, 1.0)
			glVertex3f(0.0, 0.0, 0.0)
			glVertex3f(0.0, 0.0, 5.0)
			glEnd()

		glPopMatrix()
		if self.point_list is not None:
			self.draw_point()

	def initializeGL(self):
		glClearDepth(1.0)
		glDepthFunc(GL_LESS)
		glEnable(GL_DEPTH_TEST)
		glShadeModel(GL_SMOOTH)

		glViewport(0, 0, self.geometry().width(), self.geometry().height())

		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(45.0, 1.33, 0.1, 100.0)
		glMatrixMode(GL_MODELVIEW)

		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(45, 640.0 / 480.0, 0.1, 200.0)
		glEnable(GL_DEPTH_TEST)
		# glEnable(GL_LIGHTING)
		glEnable(GL_LIGHT0)
		glEnable(GL_NORMALIZE)

		glLightfv(GL_LIGHT0, GL_POSITION, (-40, 200, 100, 0.0))
		glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
		glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
		glEnable(GL_LIGHT0)
		glEnable(GL_LIGHTING)
		glEnable(GL_COLOR_MATERIAL)
		glEnable(GL_DEPTH_TEST)
		glShadeModel(GL_SMOOTH)
		glClearColor(0.2, 0.2, 0.2, 1.0)

	def resizeEvent(self, e):

		#
		# si = self.size()
		# self.setMinimumWidth(si.height())
		width = self.width()
		height = self.height()
		if self.initia:
			glViewport(0, 0, width, height)
			glMatrixMode(GL_PROJECTION)
			glLoadIdentity()
			gluPerspective(45.0, float(width) / float(height), 0.1, 100.0)
			glMatrixMode(GL_MODELVIEW)
		# glViewport(0, 0, self.geometry().width(), self.geometry().height())
		# gluPerspective(45, self.geometry().width() / self.geometry().height(), 0.1, 200.0)

	def mousePressEvent(self, e):
		self.m = e.pos()

	def wheelEvent(self, e):
		if not self.changeable:
			return

		self.point['distance_length'] += e.delta() / 100
		self.updateGL()

	def mouseMoveEvent(self, e):
		if not self.changeable:
			return

		delX = self.m.x() - e.x()
		delY = self.m.y() - e.y()

		self.set_x(delY)
		self.set_z(delX)

		if e.buttons():
			self.updateGL()

		self.m = e.pos()
