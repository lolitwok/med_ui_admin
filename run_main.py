# -*- coding: utf-8 -*-

import sys, os

from PyQt4 import QtGui, QtCore

from applibs import BASE_DIR
from applibs.logWindow import LogDialog
from applibs.models.main_models import Base as MainBase
from applibs.models.user_stuff import Base as UserBase
from applibs.dao import main_db_engine, cur_main_session
from applibs.dao import user_db_engine, cur_user_session
from applibs.models.main_models import DNode


def import_xml():
	from lxml import etree
	from applibs.models.main_models import DItem, DModel, DNode, DPoint, DImage

	with open(os.path.join(BASE_DIR, 'db_tools', 'func_dia.xml'), 'r') as xml_file:
		# with open(os.path.join(BASE_DIR, 'db_tools', 'new_nozod_dia.xml'), 'r') as xml_file:
		# _str = xml_file.read()
		# print _str
		parser = etree.XMLParser(encoding="utf-8")
		_root = etree.parse(xml_file, parser=parser)
		print _root
		items = []
		for i in _root.find('dItemRoot').iter(tag='dItem'):
			el = DItem.load_xls(i)
			items.append(el)
			q = cur_main_session.query(DItem).filter(DItem.guid == el.guid)
			if q.count() > 0:
				q.delete()

		for i in _root.find('dModelRoot').iter(tag='dModel'):
			el = DModel.load_xls(i)
			items.append(el)
			q = cur_main_session.query(DModel).filter(DModel.guid == el.guid)
			if q.count() > 0:
				q.delete()

		for i in _root.find('dNodeRoot').iter(tag='dNode'):
			el = DNode.load_xls(i)
			items.append(el)
			q = cur_main_session.query(DNode).filter(DNode.guid == el.guid)
			if q.count() > 0:
				q.delete()

		for i in _root.find('dPointRoot').iter(tag='dPoint'):
			el = DPoint.load_xls(i)
			items.append(el)
			q = cur_main_session.query(DPoint).filter(DPoint.guid == el.guid)
			if q.count() > 0:
				q.delete()

		for i in _root.find('dImageRoot').iter(tag='dImage'):
			el = DImage.load_xls(i)
			items.append(el)
			q = cur_main_session.query(DImage).filter(DImage.guid == el.guid)
			if q.count() > 0:
				q.delete()

		cur_main_session.commit()

		for i in items:
			cur_main_session.add(i)
		cur_main_session.commit()


if __name__ == "__main__":
	# o = 0
	# for i in cur_session.query(dnode.DNode).all():
	# 	if i.usable_us_dia:
	# 		continue
	#
	# 	if not i.has_child():
	# 		o += 1
	# 		i.usable_us_dia = True
	# 		cur_session.add(i)
	#
	# cur_session.commit()
	# print 'changed', o
	#
	# sys.exit(0)
	import applibs.models.main_models
	import applibs.models.user_stuff
	from applibs.models.user_stuff import auth

	from applibs.temp_migrations import apply_migrations

	if apply_migrations():
		sys.exit(100)

	MainBase.metadata.create_all(main_db_engine)
	UserBase.metadata.create_all(user_db_engine)

	from applibs.main_ui.main_window import Window

	if not cur_user_session.query(auth.User).filter(auth.User.login == 'adm').first():
		u = auth.User('adm', '123')
		u.set_adm()
		cur_user_session.add(u)
		cur_user_session.commit()

	# import_xml()
	# sys.exit(101)

	from applibs.main_ui.utils import high_priority

	high_priority()

	app = QtGui.QApplication(sys.argv)
	app.setQuitOnLastWindowClosed(False)
	translator = QtCore.QTranslator()
	# print translator.load(os.path.join(BASE_DIR, "locale", "locale_en.qm"))
	app.installTranslator(translator)

	mui = Window()
	app.aboutToQuit.connect(mui.close)
	mui.show()

	log_dialog = LogDialog()
	log_dialog.show()
	sys.exit(app.exec_())
