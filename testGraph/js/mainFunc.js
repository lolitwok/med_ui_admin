var _data = new Array(150);


window.onload = function () {
	var tmp_rnd     = 25;

	var RG        = RGraph;
	var ma        = Math;
	var canvas    = document.getElementById("cvs");
	var canvas2   = document.getElementById("cvs2");
	var obj       = null;
	var data      = [];
	var l         = 0; // The letter 'L' - NOT a one
	var numvalues = 150;
	var updates   = 0;

	$("#cvs").css("width", $(window).width() - 100) ;
	$("#cvs").attr("width", $(window).width() - 100) ;
	$("#cvs_rgraph_domtext_wrapper").css("width", $(window).width() - 100);
	$("#cvs_rgraph_domtext_wrapper").attr("width", $(window).width() - 100);
	$("#cvs").css("height", 250) ;

	// Pre-pad the arrays with null values
	for (var i=0; i<numvalues; ++i) {
		data.push(null);
	}

	setInterval(function () {
		tmp_rnd = tmp_rnd + RG.random(-3,3);
		tmp_rnd = Math.max(0,tmp_rnd);
		tmp_rnd = Math.min(50,tmp_rnd);

		//obj.original_data[0].push(Math.min(tmp_rnd + 5, 50));
		updateChart(Math.min(tmp_rnd + 5, 50));

	}, 25);

	var updateChart = function (val) {
		_data.push(val);

		if (_data.length > 150) {
			_data.shift();
		}

		obj.original_data[0] = _data;
		//RGraph.redraw();
		drawGraph();
	};

	function drawGraph ()
	{
		RG.Clear(canvas);

		if (!obj) {
			obj = new RG.Line({
				id: 'cvs',
				data: [],
				options: {
					colors: ['white'],
					linewidth: 2,
					//yaxispos: 'right',
					shadow: false,
					noaxes: true,
					tickmarks: null,
					gutterTop: 10,
					gutterBottom: 15,
					gutterRight: 0,
					gutterLeft:40,
					backgroundGridVlines: false,
					numyticks: 5,
					numxticks: 0,
					//ylabelsCount: 5,
					textAccessible: true,
					scaleZerostart: true,
					noxaxis: true,
					//noaxes: true,
					spline: true,
					ymin: 0,
					ymax: 100,
					backgroundHbars: [
						[0,20,'rgba(66, 139, 202,1)'],
						[20,40,'rgba(91, 192, 222,1)'],
						[40,60,'rgba(92, 184, 92,1)'],
						[60,80,'rgba(240, 173, 78,1)'],
						[80,100,'rgba(217, 83, 79,1)']
					]
				}
			})
		}

		RG.Clear(canvas2);
		obj2 = new RG.VProgress({
			id: 'cvs2',
			min: 0,
			max: 100,
			value: [0],
			options: {
				gutterTop: 10,
				gutterBottom: 15,
				gutterRight: 40,
				gutterLeft:0,
				textAccessible: true,
				//margin: 7,
				bevel: false,
				colors: ['red']
			}
		}).draw();

		obj.original_data[0] = _data;
		obj2.value = _data[_data.length - 1];
		obj.draw();
		obj2.draw();

		updates++;
		if (updates % 100 === 0) {
			console.log(updates);
		}
	}

	drawGraph();
};


$(window).resize(
		function () {
			$("#cvs").css("width", $(window).width() - 100) ;
			$("#cvs").attr("width", $(window).width() - 100) ;
			$("#cvs_rgraph_domtext_wrapper").css("width", $(window).width() - 100);
			$("#cvs_rgraph_domtext_wrapper").attr("width", $(window).width() - 100);
			$("#cvs").css("height", 250) ;
			RGraph.Reset(canvas);
		}
);

// media query change


