var _data = [[],[]];
var index = 0;
var tmp = [[]];


window.onload = function () {
	resetData();
	var tmp_rnd      = 25;
	var tmp_rnd2     = 30;
	var RG           = RGraph;
	var ma           = Math;
	var canvas       = document.getElementById("cvs");
	var obj          = null;
	var updates      = 0;
	var numvalues    = 30;
	var data = [];
	var i = 0;


	$("#cvs").css("width", $(window).width()) ;
	$("#cvs").attr("width", $(window).width()) ;
	$("#cvs_rgraph_domtext_wrapper").css("width", $(window).width());
	$("#cvs_rgraph_domtext_wrapper").attr("width", $(window).width());

	// Pre-pad the arrays with null values
	//for (var i=0; i<numvalues; ++i) {
	//	data.push(null);
	//}

	function resetData () {
		tmp.push([]);

		for (var k=0; k<30; k++) {
			for (var h=0; h<tmp.length; h++) {
				tmp[h][k] = null;
			}
		}
	}

	//

	setInterval(function () {
		for (var i=0; i<tmp.length;i++) {
			tmp[i][index] = tmp[i][index] + RG.random(-3,3);
			tmp[i][index] = Math.max(0,tmp[i][index]);
			tmp[i][index] = Math.min(50,tmp[i][index]);

		}
		index++;

		//tmp_rnd = tmp_rnd + RG.random(-3,3);
		//tmp_rnd = Math.max(0,tmp_rnd);
		//tmp_rnd = Math.min(50,tmp_rnd);
		//dataset1[index] = tmp_rnd;
		//
		//
		//tmp_rnd2 = tmp_rnd2 + RG.random(-5,5);
		//tmp_rnd2 = Math.max(10,tmp_rnd2);
		//tmp_rnd2 = Math.min(70,tmp_rnd2);
		//dataset2[index] = tmp_rnd2;
		//
		//tmp = [dataset1,dataset2];
		updateChart(tmp);
	}, 100);


	var updateChart = function (tarray) {
		//index++;

		if (index > 29) {
			index = 0;
			resetData();
		}
		for (var i=0; i<tmp.length; i++) {
			obj.original_data[i] = tmp[i];
		}

		drawGraph();
	};

	function drawGraph ()
	{

		console.log(tmp);
		//console.log(dataset1);
		//console.log(dataset2);
		RG.Clear(canvas);

		if (!obj) {
			obj = new RG.Line({
				id: 'cvs',
				data: tmp,
				options: {
					colors: ['black', "white","red","yellow","green"],
					linewidth: 3,
					//shadow: false,
					noaxes: true,
					tickmarks: null,
					gutterTop: 10,
					gutterBottom: 15,
					gutterRight: 0,
					gutterLeft:40,
					backgroundGridVlines: false,
					numyticks: 5,
					numxticks: 0,
					//textAccessible: true,
					scaleZerostart: true,
					noxaxis: true,
					//spline: false,
					ymin: 0,
					ymax: 100,
					backgroundHbars: [
						[0,20,'rgba(66, 139, 202,0.15)'],
						[20,40,'rgba(91, 192, 222,0.15)'],
						[40,60,'rgba(92, 184, 92,0.15)'],
						[60,80,'rgba(240, 173, 78,0.15)'],
						[80,100,'rgba(217, 83, 79,0.15)']
					]
					//filled: true,
					//fillstyle: [
					//	'Gradient(rgba(255,0,0,0.3):rgba(255,0,0,0.5))',
					//	'Gradient(rgba(0,0,0,0.75):rgba(0,230,0,0.5))'
					//]
				}
			});
		}

		//obj.original_data[0] = _data[0];
		//obj.original_data[1] = _data[1];

		obj.draw();

		updates++;
		if (updates % 100 === 0) {
			console.log(updates);
		}
	}

	drawGraph();
};


$(window).resize(
		function () {
			$("#cvs").css("width", $(window).width()) ;
			$("#cvs").attr("width", $(window).width()) ;
			$("#cvs_rgraph_domtext_wrapper").css("width", $(window).width() );
			$("#cvs_rgraph_domtext_wrapper").attr("width", $(window).width());
			$("#cvs").css("height", 250) ;
			RGraph.Reset(canvas);
		}
);


