// #include <iostream>
// #include <bits/stdc++.h>

// using namespace std;

// int main(){
// 	// int a = system("start python2.7_win32\\pythonw.exe run_main.py");
// 	int a = system("cmd /c start python2.7_win32\\pythonw.exe run_main.py");
// 	// int a = system("powershell start-process 'python2.7_win32\\pythonw.exe' 'run_main.py'");
// 	return a;
// }

// #include <windows.h>

// int WinMain(HINSTANCE hInstance,
//          HINSTANCE hPrevInstance,
//          LPTSTR    lpCmdLine,
//          int       nCmdShow)
// {
//     // int a = system("start python2.7_win32\\pythonw.exe run_main.py");
// 	int a = system("powershell start-process 'python2.7_win32\\pythonw.exe' 'run_main.py'");

//     return 0;
// }   

// #include <windows.h>
// #include <stdio.h>
// #include <tchar.h>

// int _tmain( int argc, TCHAR *argv[] )
// {
//     STARTUPINFO si;
//     PROCESS_INFORMATION pi;

//     ZeroMemory( &si, sizeof(si) );
//     si.cb = sizeof(si);
//     ZeroMemory( &pi, sizeof(pi) );

//     // Start the child process. 
//     if( !CreateProcess( NULL,   // No module name (use command line)
//         "python2.7_win32\\pythonw.exe run_main.py",        // Command line
//         NULL,           // Process handle not inheritable
//         NULL,           // Thread handle not inheritable
//         FALSE,          // Set handle inheritance to FALSE
//         0,              // No creation flags
//         NULL,           // Use parent's environment block
//         NULL,           // Use parent's starting directory 
//         &si,            // Pointer to STARTUPINFO structure
//         NULL
//          )           // Pointer to PROCESS_INFORMATION structure
//     ) 
//     {
//         printf( "CreateProcess failed (%d).\n", GetLastError() );
//         return 0;
//     }

//     // Wait until child process exits.
//     // WaitForSingleObject( pi.hProcess, INFINITE );

//     // Close process and thread handles. 
//     // CloseHandle( pi.hProcess );
//     // CloseHandle( pi.hThread );
//     return 0;
// }

#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#include <iostream>
#include <string>

using namespace std;


std::string getexepath(char* a)
{
	char result[ MAX_PATH ];

 char *p = strrchr(a, '\\');
    if(p) p[0] = 0;


// 	std::string gg = std::string( result, GetModuleFileName( NULL, result, MAX_PATH ) );
// 	cout << gg << endl;
// 	cout << a << endl;
// 	std::string to_delete = "\\";
// 	to_delete += a;
// 	gg.erase(gg.find(to_delete), to_delete.length());
// cout << gg << endl;
	return a;
}


int _tmain( int argc, TCHAR *argv[] )
{
	STARTUPINFO cif;
	ZeroMemory(&cif,sizeof(STARTUPINFO));
	PROCESS_INFORMATION pi;

	std::string str=getexepath(argv[0]);
	// str += "\\python2.7_win32\\pythonw.exe run_main.py";
	str += "\\python2.7_win32\\pythonw.exe";
	// cout << str << endl;

	std::string parr = "import sys;";
	parr += "from PyQt4 import QtGui;";
	parr += "from applibs.models import Base;";
	parr += "from applibs.dao import engine, cur_session;";
	parr += "import applibs.models.mains;";
	parr += "import applibs.models.auth as auth;";
	parr += "Base.metadata.create_all(engine);";
	parr += "from applibs.main_ui.main_window import Window;";
	parr += "app = QtGui.QApplication(sys.argv);";
	parr += "app.setQuitOnLastWindowClosed(False);";
	parr += "mui = Window();app.aboutToQuit.connect(mui.close);mui.show();sys.exit(app.exec_());";


//	cout << parr << endl;
	std::string  cmdArgs = "pythonw.exe -c \"" + parr +"\"";
//	char  cmdArgs[] = "pythonw.exe -c \"\"";

	if (CreateProcess(str.c_str(),const_cast<char *>(cmdArgs.c_str()),
		NULL,NULL,FALSE,0,NULL,NULL,&cif,&pi)==TRUE)
	{
		// cout << "process" << endl;
		// cout << "handle " << pi.hProcess << endl;
		// Sleep(5000);				// подождать
		// TerminateProcess(pi.hProcess,NO_ERROR);	// убрать процесс
	}else{
		// cout << "fail" << endl;		
		// cout << "handle " << pi.hProcess << endl;
	}
	return 0;

}