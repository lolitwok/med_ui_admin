import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from applibs import BASE_DIR

DB_FILE = "test100.sqlite"
DB_FILE1 = "test1.sqlite"

connection_string = 'sqlite:///%s' % os.path.join(BASE_DIR, DB_FILE)
connection1_string = 'sqlite:///%s' % os.path.join(BASE_DIR, DB_FILE1)

engine = create_engine(connection_string, echo=True)
Session = sessionmaker(bind=engine)
cur_session = Session()

from applibs.models import Base



from applibs.models.dnode import DNode
from applibs.models.dpoint import DPoint

Base.metadata.create_all(engine)

engine1 = create_engine(connection1_string, echo=True)

res = engine1.execute('SELECT * FROM d_point;')

for i in res.fetchall():
	a = DPoint()
	a.id = i[0]
	a.x = i[1]
	a.y = i[2]
	a.z = i[3]
	a.r = i[4]
	a.distance_x = i[5]
	a.distance_y = i[6]
	a.distance_z = i[7]
	a.distance_a = i[8]

	a.distance_x_offset = i[9]
	a.distance_y_offset = i[10]
	a.distance_z_offset = i[11]

	a.d_node_id = i[12]
	a.model_id = 1
	print i
	cur_session.add(a)
	cur_session.commit()

res.close()
