CREATE TABLE MY_TABLE (
  id INTEGER,
  x FLOAT,
  y FLOAT,
  z FLOAT,
  r FLOAT,
  distance_x FLOAT,
  distance_y FLOAT,
  distance_z FLOAT,
  distance_a FLOAT,
  distance_x_offset FLOAT,
  distance_y_offset FLOAT,
  distance_z_offset FLOAT,
  d_node_id INTEGER,
  model_id INTEGER
);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (9, 0.954, -3.676999999999999, 0.183, 0.1, 291.99, 0, 5.009999999999991, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (17, 0.799, -2.789, 0.51, 0.1, 319.99, 0, 17, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (18, 1.1039999999999996, -2.59, 0.43, 0.1, 293.99, 0, -52, -7, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (19, 1.16, -2.71, 0.399, 0.1, 292.99, 0, -48, -7, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (20, 1.271, -3.6279999999999992, 0.189, 0.1, 289, 0, -31, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (21, 1.29, -3.45, 0.31, 0.0776, 287.99, 0, -37, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (22, 1.2, -3.08, 0.35000000000000003, 0.1, 299.99, 0, -46, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (23, 0.96, -1.86, 0.63, 0.1, 303.99, 0, -62, -6, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (24, 0.88, -1.57, 0.67, 0.1, 315.99, 0, -64, -6, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (25, 0.71, -0.22, 0.96, 0.1, 333.99, 0, -57, -5, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (26, 0.71, 0.35000000000000003, 0.96, 0.1, 322.99, 0, -58, -6, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (27, 0.67, 0.8, 0.96, 0.1, 330.99, 0, -64, -6, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (28, 1.33, -1.57, 0.55, 0.1, 310.99, 0, 4, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (29, 1.82, -2.71, 0.31, 0.1, 299.99, 0, -13, -7, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (30, 1.69, -2.27, 0.43, 0.1, 298.99, 0, -9, -7, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (31, 1.61, -2.1, 0.47000000000000003, 0.1, 306.99, 0, -7, -6, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (32, 1.86, -1.98, 0.43, 0.1, 302.99, 0, -53, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (33, 2.079, -2.63, 0.25, 0.1, 299.99, 0, -63, -7, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (34, 1.57, -1.16, 0.43, 0.1, 297.99, 0, -64, -7, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (35, 1.53, -1, 0.479, 0.1, 306.99, 0, -64, -5, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (36, 0.14, -3, 0.59, 0.1, 308, 0, -37, -9, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (37, 0.119, -4.1, 0.257, 0.1, 306.98, 0, -39, -9, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (38, 0.1, -3.98, 0.321, 0.1, 305.99, 0, -37, -9, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (39, 0.06, -3.49, 0.51, 0.1, 302.99, 0, -31, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (40, 0.02, -2.27, 0.84, 0.1, 310.99, 0, -32, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (41, 0.009000000000000001, -1.9629999999999996, 0.88, 0.1, 316.99, 0, -37, -6, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (42, -0.31, -3.12, 0.71, 0.1, 303.99, 0, 27, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (43, -0.27, -4.0920000000000005, 0.329, 0.1, 289.99, 0, 12, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (44, -0.35000000000000003, -2.06, 0.92, 0.1, 306.99, 0, 26, -6, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (45, -1.16, -3, 0.71, 0.1, 299.99, 0, -11, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (46, -1.37, -3.94, 0.365, 0.1, 293.99, 0, -6, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (47, -1.37, -3.73, 0.51, 0.1, 299.99, 0, -10, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (48, -1.2, -3.2, 0.55, 0.1, 301.99, 0, -15, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (49, -1.0690000000000002, -2.119, 0.88, 0.1, 313.99, 0, -13, -7, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (50, -1.08, -1.78, 1, 0.1, 337.99, 0, -18, -6, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (51, -0.679, -0.644, 1.2139999999999995, 0.1, 347.99, 0, -23, -5, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (52, -0.63, 0.1, 1.2099999999999989, 0.1, 347.99, 0, -15, -5, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (53, -1.6500000000000001, -2.8429999999999995, 0.67, 0.1, 298.99, 0, 43, -7, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (54, -1.73, -3.73, 0.43, 0.1, 285.99, 0, 36, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (55, -1.45, -1.94, 0.96, 0.1, 318.99, 0, 55, -8, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (56, -1.49, -1.61, 0.96, 0.1, 309.99, 0, 48, -6, 0, 0, 0, 1000, 7);
INSERT INTO MY_TABLE(id, x, y, z, r, distance_x, distance_y, distance_z, distance_a, distance_x_offset, distance_y_offset, distance_z_offset, d_node_id, model_id) VALUES (57, -1.2, -0.63, 1.16, 0.1, 317.99, 0, 55, -6, 0, 0, 0, 1000, 7);