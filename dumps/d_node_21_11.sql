CREATE TABLE MY_TABLE (
  id INTEGER,
  description TEXT,
  usable_us_dia BOOLEAN,
  name VARCHAR,
  parrent_id INTEGER,
  d_item_id INTEGER
);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (7, '', 1, 'КИП СПЭД', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (14, '', 1, 'т. СПЕД брюшной полости', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (15, '', 1, 'КИП эндокринной системы', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (16, '', 1, 'т. половых органов', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (17, '', 1, 'т. мочевого пузыря', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (18, '', 1, 'т. почек', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (19, '', 1, 'т. углеводного обмена', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (20, '', 1, 'т. грудных желез', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (21, '', 1, 'т. щитовидной железы (и паращитовидной)', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (22, '', 1, 'т. гипофиза', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (23, '', 1, 'т. гипоталамуса', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (24, '', 1, 'КИП сердца', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (25, '', 1, 'т. клапанов сердца', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (26, '', 1, 'т. проводящей системы', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (27, '', 1, 'т. миокарда', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (28, '', 1, 'КИП тонкого кишечника', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (29, '', 1, 'т. слизистой нонк. киш. и 12-перст. киш.', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (30, '', 1, 'т. брюшины тонкого кишечника', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (31, '', 1, 'т. желудка', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (32, '', 1, 'КИП имунной системы', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (33, '', 1, 'т. имунной реакции брюшной полости', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (34, '', 1, 'т. имунной реакции соединительной ткани', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (35, '', 1, 'т. имунной реакции нервной системы', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (36, '', 1, 'т. имунной реакции сосудов', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (37, '', 1, 'т. имунной реакции грудной клетки', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (38, '', 1, 'КИП сосудов', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (39, '', 1, 'т. переферического кровообращения', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (40, '', 1, 'т. грудного лимфатического протока', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (41, '', 1, 'Кип нервной системы', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (42, '', 1, 'т. пояснично-крестцового отдела', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (43, '', 1, 'т. соединительной ткани', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (44, '', 1, 'КИП вегетативной нервной системы', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (45, '', 1, 'т. оболочек мозга', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (46, '', 1, 'т. шейно-грудного отдела', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (47, '', 1, 'т. мозга и сосудов головного мозга', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (48, '', 1, 'т. черепно-мозговvх нервов', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (49, '', 1, 'КИП толстого кишечника', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (50, '', 1, 'т. слизистой толстого кишечника', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (51, '', 1, 'т. брюшины толстого кишечника', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (52, '', 1, 'т. печени', null, 3);
INSERT INTO MY_TABLE(id, description, usable_us_dia, name, parrent_id, d_item_id) VALUES (53, '', 1, 'т. селезенки', null, 3);