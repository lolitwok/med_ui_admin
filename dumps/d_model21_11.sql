CREATE TABLE MY_TABLE (
  id INTEGER,
  name VARCHAR,
  file_name VARCHAR
);
INSERT INTO MY_TABLE(id, name, file_name) VALUES (1, 'Hand Default', 'default_hand.obj');
INSERT INTO MY_TABLE(id, name, file_name) VALUES (6, 'Hand New Right', 'new_right.obj');
INSERT INTO MY_TABLE(id, name, file_name) VALUES (7, 'Hand New Left', 'new_left.obj');