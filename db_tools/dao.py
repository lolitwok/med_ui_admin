import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from init import BASE_DIR

DB_FILE = "tt2.sqlite"

connection_string = r'sqlite:///%s?check_same_thread=False' % os.path.join(BASE_DIR, DB_FILE)
print 'db file path: %s' % connection_string
engine = create_engine(connection_string, echo=False)
Session = sessionmaker(bind=engine)

cur_session = scoped_session(Session)
