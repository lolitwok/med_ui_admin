from sqlalchemy import *
from sqlalchemy.orm import relationship
from . import Base
from dao import Session

session = Session()


class DItem(Base):
	__tablename__ = 'd_item'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	type = Column(String(20))
	name = Column(String(50))

	def __init__(self):
		pass

	def __repr__(self):
		return "<DItem('%s','%s')>" % (self.id, self.name)

	@classmethod
	def add_new(cls, name, _type):
		_n = cls()
		_n.name = name
		_n.type = _type
		session.add(_n)
		session.commit()
		return _n.id

	@classmethod
	def rename(cls, _id, name):
		_n = session.query(cls).filter(cls.id == _id).first()
		if _n is None:
			return False

		_n.name = name
		session.add(_n)
		session.commit()
		return True

	@classmethod
	def delete(cls, _id):
		session.query(cls).filter(cls.id == _id).delete()
