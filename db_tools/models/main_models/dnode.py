from dao import cur_session
from models.main_models.dimage import DImage
from sqlalchemy import *
from sqlalchemy.orm import relationship
from . import Base
from dao import Session

association_table = Table(
	'nazodes_points', Base.metadata,
	Column('d_node_id', Integer, ForeignKey('d_node.id')),
	Column('d_point_id', Integer, ForeignKey('d_point.id'))
)


class DNode(Base):
	__tablename__ = 'd_node'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	description = Column(Text, default="")
	usable_us_dia = Column(Boolean(), default=True)
	name = Column(String(50))
	parrent_id = Column(Integer, ForeignKey("d_node.id"))
	parrent = relationship("DNode", foreign_keys=[parrent_id], lazy='dynamic')

	nazode_points = relationship("DPoint", secondary=association_table)

	d_item_id = Column(Integer, ForeignKey("d_item.id"), nullable=True, default=None)
	d_item = relationship("DItem", foreign_keys=[d_item_id])

	# d_template_id = Column(Integer, ForeignKey("d_template.id"), nullable=True, default=None)
	# d_template = relationship("DTemplate", foreign_keys=[d_template_id])

	# is_main = Column(Boolean(), default=False)

	main_parent = False

	def has_child(self):
		if cur_session.query(DNode).filter(DNode.parrent_id == self.id).count():
			return True
		return False

	def is_parrent(self, n):
		def check_parrent_id(self_p_id, p_id):
			if self_p_id is None:
				return False

			_t = cur_session.query(DNode).filter(DNode.id == self_p_id).first()
			if _t is None:
				return False

			if _t.id == p_id:
				return True

			return check_parrent_id(_t.parrent_id, p_id)

		return check_parrent_id(self.parrent_id, n.id)

	def delete_self(self, commit=False):
		items = cur_session.query(DNode).filter(DNode.parrent_id == self.id).all()

		if items is not None:
			for i in items:
				i.delete_self()

		cur_session.query(DImage).filter(DImage.d_node_id == self.id).delete()
		cur_session.query(DNode).filter(DNode.id == self.id).delete()

		if commit:
			cur_session.commit()

	@property
	def get_main_parent(self):
		def get_parent(cur_node):
			t_cur_node = cur_session.query(DNode).filter(DNode.id == cur_node).first()
			if t_cur_node.parrent_id is None:
				return t_cur_node

			return get_parent(t_cur_node.parrent_id)

		if self.main_parent is False:
			self.main_parent = get_parent(self.id)

		return self.main_parent

	def get_name(self):
		return unicode(self.name)

	def get_guid(self):
		return self.id

	def __repr__(self):
		return "<DNode('%s','%s')>" % (self.id, self.name)
