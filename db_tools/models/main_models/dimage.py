import base64
from sqlalchemy import *
from sqlalchemy.orm import relationship
from lxml import etree
from . import Base
from dao import Session

session = Session()


class DImage(Base):
	__tablename__ = 'd_image'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	description = Column(Text, default="")
	d_node_id = Column(Integer, ForeignKey("d_node.id"))
	d_node = relationship("DNode", foreign_keys=[d_node_id])
	image = Column(BLOB)

	def __init__(self):
		pass

	def __repr__(self):
		return "<Node('%s','%s')>" % (self.id, self.name)

	def image_to_base32(self):
		return base64.b32encode(self.image)

	def dump_xml(self):
		pass

	# _el = etree.Element('dImage', id="DImage#%s" % self.id)
	# etree.SubElement(_el, 'guid').text = rel["dm%s" % i.id]
	# etree.SubElement(_el,'description').text = self.description
	# etree.SubElement(_el,'dNodeGuid').text = rel["dn%s" % i.d_node_id] if i.d_node_id else ''
	# etree.SubElement(_el,'imageSrc').text =self.image_to_base64()



	def load_from_xml(self):
		pass
