from sqlalchemy import *
from sqlalchemy.orm import relationship
from . import Base
from dao import Session


session = Session()


class DModel(Base):
	__tablename__ = 'd_model'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	name = Column(String(50))
	file_name = Column(String(40))

	def __init__(self):
		pass

	def __str__(self):
		return self.name

	def __repr__(self):
		return "<DModel('%s','%s')>" % (self.id, self.name)
