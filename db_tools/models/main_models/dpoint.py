# -*- coding: utf-8 -*-

from sqlalchemy import *
from sqlalchemy.orm import relationship
from . import Base
from dao import Session


session = Session()


class DPoint(Base):
	__tablename__ = 'd_point'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	x = Column(Float(12))
	y = Column(Float(12))
	z = Column(Float(12))
	r = Column(Float(12), nullable=True)

	distance_x = Column(Float(12), nullable=True)
	distance_y = Column(Float(12), nullable=True)
	distance_z = Column(Float(12), nullable=True)
	distance_a = Column(Float(12), nullable=True)

	distance_x_offset = Column(Float(12), nullable=True)
	distance_y_offset = Column(Float(12), nullable=True)
	distance_z_offset = Column(Float(12), nullable=True)

	d_node_id = Column(Integer, ForeignKey("d_node.id"))
	d_node = relationship("DNode", foreign_keys=[d_node_id])

	model_id = Column(Integer, ForeignKey("d_model.id"), nullable=True, default=None)
	model = relationship("DModel", foreign_keys=[model_id])

	def __init__(self):
		pass

	def __repr__(self):
		return "<DPoint(x:%s, y:%s, z:%s)>" % (self.x, self.y, self.z)

	def __str__(self):
		return "Point (x:%s, y:%s, z:%s)" % (self.x, self.y, self.z)

	def __unicode__(self):
		return u"Точка (x:%s, y:%s, z:%s)" % (self.x, self.y, self.z)

	def to_gl_point_dict(self):
		return {
			'x': self.x,
			'y': self.y,
			'z': self.z,
			'radius': self.r,

			'distance_x': self.distance_x,
			'distance_y': self.distance_y,
			'distance_z': self.distance_z,
			'distance_length': self.distance_a,

			'distance_x_offset': self.distance_x_offset,
			'distance_y_offset': self.distance_y_offset,
			'distance_z_offset': self.distance_z_offset,

		}
