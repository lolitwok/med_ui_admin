# -*- coding: utf-8 -*-

from sqlalchemy import *
from sqlalchemy.orm import relationship
from . import Base
from dao import Session


session = Session()

association_table = Table(
	'nazods_templates', Base.metadata,
	Column('d_template_id', Integer, ForeignKey('d_template.id')),
	Column('d_nazod_id', Integer, ForeignKey('d_nazod.id'))
)


class DNazod(Base):
	__tablename__ = 'd_nazod'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	name = Column(String(50))

	d_template_id = Column(Integer, ForeignKey("d_template.id"))
	d_template = relationship("DTemplate", foreign_keys=[d_template_id])

	def __init__(self):
		pass

	def __repr__(self):
		return "<DNazod(name: %s)>" % self.name

	def __str__(self):
		return self.name

	def __unicode__(self):
		return unicode(self.name)


class DTemplate(Base):
	__tablename__ = 'd_template'
	__table_args__ = {'sqlite_autoincrement': True}

	id = Column(Integer, primary_key=True, autoincrement=True)
	name = Column(String(50))
	is_default = Column(Boolean, default=False)

	nazode_points = relationship("DNazod", secondary=association_table)

	def __init__(self):
		pass

	def __repr__(self):
		return "<DTemplate(name: %s)>" % self.name

	def __str__(self):
		return self.name

	def __unicode__(self):
		return unicode(self.name)
