# -*- coding: utf-8 -*-
from datetime import date
import datetime
import uuid
from lxml import etree

from models.main_models import (
	DImage,
	DNode,
	DModel,
	DItem,
	DPoint
)
from dao import cur_session


if __name__ == "__main__":

	rel = {}

	# rel['di10'] = str(uuid.uuid4())
	_nodes =  cur_session.query(DNode).filter(DNode.d_item_id != 4)

	model_guid = None
	for i in _nodes.all():
		_guid = str(uuid.uuid4())
		rel["dn%s" % i.id] = _guid

	for i in cur_session.query(DItem).all():
		_guid = str(uuid.uuid4())
		# rel["dii%s" % i.id] = _guid
		rel["di%s" % i.id] = _guid

	for i in cur_session.query(DImage).all():
		_guid = str(uuid.uuid4())
		rel["dm%s" % i.id] = _guid

	for i in cur_session.query(DModel).all():
		_guid = str(uuid.uuid4())
		rel["dd%s" % i.id] = _guid

		if model_guid is None:
			model_guid = _guid

	for i in cur_session.query(DPoint).all():
		_guid = str(uuid.uuid4())
		rel["dp%s" % i.id] = _guid

	root = etree.Element('root')
	d_item_root = etree.SubElement(root, 'dItemRoot')
	d_model_root = etree.SubElement(root, 'dModelRoot')
	d_node_root = etree.SubElement(root, 'dNodeRoot')
	d_point_root = etree.SubElement(root, 'dPointRoot')
	d_image_root = etree.SubElement(root, 'dImageRoot')

	# # DItem - > DNode ##########################################################################
	#
	# for i in cur_session.query(DItem).all():
	# 	_el = etree.SubElement(d_node_root, 'dNode', id="dii%s" % i.id)
	# 	etree.SubElement(_el, 'guid').text = rel["dii%s" % i.id]
	# 	etree.SubElement(_el, 'name').text = i.name
	# 	etree.SubElement(_el, 'nameLower').text = i.name.lower()
	# 	etree.SubElement(_el, 'description').text = ''
	# 	etree.SubElement(_el, 'usableUsDia').text = 'False'
	# 	etree.SubElement(_el, 'parrentGuid').text = ''
	# 	etree.SubElement(_el, 'dItemGuid').text = rel["di%s" % 10]
	# 	etree.SubElement(_el, 'dTemplateGuid').text = ''
	# 	etree.SubElement(_el, 'isMain').text = 'False'


	# DItem normal ##########################################################################
	#
	for i in cur_session.query(DItem).all():
		_el = etree.SubElement(d_item_root, 'dItem', id="di%s" % i.id)
		etree.SubElement(_el, 'guid').text = rel["di%s" % i.id]
		etree.SubElement(_el, 'type').text = '0'
		etree.SubElement(_el, 'name').text = i.name



	# # # Item static ####################################################################################
	#
	# _el = etree.SubElement(d_item_root, 'dItem', id="di%s" % 10)
	# etree.SubElement(_el, 'guid').text = rel["di%s" % 10]
	# etree.SubElement(_el, 'type').text = '0'
	# etree.SubElement(_el, 'name').text = u'Диагностика с нозодами'


	# DModel ####################################################################################

	for i in cur_session.query(DModel).all():
		_el = etree.SubElement(d_model_root, 'dModel', id="dd%s" % i.id)
		etree.SubElement(_el, 'guid').text = rel["dd%s" % i.id]
		etree.SubElement(_el, 'name').text = i.name
		etree.SubElement(_el, 'fileName').text = i.file_name


	# DImage ####################################################################################

	for i in cur_session.query(DImage).all():
		_el = etree.SubElement(d_image_root, 'dImage', id="dm%s" % i.id)
		etree.SubElement(_el, 'guid').text = rel["dm%s" % i.id]
		etree.SubElement(_el, 'description').text = i.description
		etree.SubElement(_el, 'dNodeGuid').text = rel["dn%s" % i.d_node_id] if i.d_node_id else ''
		etree.SubElement(_el, 'imageSrc').text = i.image_to_base32()


	# point ####################################################################################




	for i in cur_session.query(DPoint).all():
		if "dn%s" % i.d_node_id not in rel:
			continue
		_el = etree.SubElement(d_point_root, 'dPoint', id="dp%s" % i.id)
		etree.SubElement(_el, 'guid').text = rel["dp%s" % i.id]

		etree.SubElement(_el, 'point', x=str(i.x), y=str(i.y), z=str(i.z), r=str(i.r))
		etree.SubElement(
			_el,
			'distance',
			x=str(i.distance_x),
			y=str(i.distance_y),
			z=str(i.distance_z),
			a=str(i.distance_a)
		)

		etree.SubElement(
			_el, 'offset',
			x=str(i.distance_x_offset),
			y=str(i.distance_y_offset),
			z=str(i.distance_z_offset)
		)

		etree.SubElement(_el, 'dNodeGuid').text = rel["dn%s" % i.d_node_id] if i.d_node_id else ''
		etree.SubElement(_el, 'dModelGuid').text = rel["dd%s" % i.model_id] if i.model_id else ''



	# Node ####################################################################################



	for i in _nodes.all():
		_el = etree.SubElement(d_node_root, 'dNode', id="dn%s" % i.id)
		etree.SubElement(_el, 'guid').text = rel["dn%s" % i.id]
		etree.SubElement(_el, 'name').text = i.name
		etree.SubElement(_el, 'nameLower').text = i.name.lower()
		etree.SubElement(_el, 'description').text = i.description
		etree.SubElement(_el, 'usableUsDia').text = str(i.usable_us_dia)

		if i.parrent_id is None and ("dii%s" % i.d_item_id) in rel:
			etree.SubElement(_el, 'parrentGuid').text = rel["dii%s" % i.d_item_id]
		else:
			etree.SubElement(_el, 'parrentGuid').text = rel["dn%s" % i.parrent_id] if i.parrent_id else ''

		etree.SubElement(_el, 'dItemGuid').text = rel["di%s" % i.d_item_id]

		# etree.SubElement(_el, 'dItemGuid').text = rel["di%s" % 10]
		etree.SubElement(_el, 'dTemplateGuid').text = ''
		etree.SubElement(_el, 'isMain').text = 'False'




	############################################################################################



	print etree.tostring(root, pretty_print=True, encoding='UTF-8')

	with open('main_models_dump%s.xml' % datetime.datetime.now().isoformat('-').replace(':', '.'), 'w') as f:
		f.write(etree.tostring(root, pretty_print=True, encoding='UTF-8'))

if __name__ == "__main__1":

	# xml = '''<?xml version="1.0" encoding="UTF-8"?>
	# <soft>
	# 	<os>
	# 		<item name="linux" dist="ubuntu">
	# 			This text about linux
	# 		</item>
	# 		<item name="mac os">
	# 			Apple company
	# 		</item>
	# 		<item name="windows" dist="XP" />
	# 	</os>
	# </soft>'''
# <root>
#   <dItemRoot>
#     <dItem id="di3">

	from lxml import etree

	tree = etree.parse('main_models_dump2017-05-21-19.02.52.957000.xml')  # Парсинг строки
	# tree = etree.parse('1.xml') # Парсинг файла

	nodes = tree.xpath('/root/dItemRoot/dItem')  # Открываем раздел
	for node in nodes:  # Перебираем элементы
		print node.tag, node.keys(), node.values()
		print 'id =', node.get('id')  # Выводим параметр name
		print 'text =', [node.text]  # Выводим текст элемента
		print node.find('guid').text

	# # Доступ к тексту напрямую, с указанием фильтра
	# print 'text1', tree.xpath('/soft/os/item[@name="linux"]/text()')
	# print 'text2', tree.xpath('/soft/os/item[2]/text()')
	# # Доступ к параметру напрямую
	# print 'dist', tree.xpath('/soft/os/item[@name="linux"]')[0].get('dist')
	# # Выборка по ключу
	# print 'dist by key', tree.xpath('//*[@name="windows"]')[0].get('dist')
	#
	# print 'iterfind:'
	# for node in tree.iterfind('.//item'):  # поиск элементов
	# 	print node.get('name')
	#
	# # Рекурсивный перебор элементов
	# print 'recursiely:'
	#
	#
	# def getn(node):
	# 	print node.tag, node.keys()
	# 	for n in node:
	# 		getn(n)
	#
	#
	# getn(tree.getroottree().getroot())
