import glob, os
from applibs import BASE_DIR

directory = os.path.join(BASE_DIR, 'applibs')
# os.chdir()
# for f in glob.glob("*.py"):
# 	print(f)

pro = open(os.path.join(BASE_DIR, '.pro'), 'w')

pys = []
uis = []
LANGUAGES = [
	'ua',
	'en',
]
# pro.write('CODECFORTR = UTF-8\n')
# pro.write('CODEC = UTF-8\n')
# pro.write('CODECFORSRC = UTF-8\n')
pro.write('SOURCES = ')

for root, dirs, files in os.walk(directory):
	for file in files:
		print "src" not in (root.replace(BASE_DIR, '')).split(os.sep)[1:]
		if file.endswith('.py') and "src" not in (root.replace(BASE_DIR, '')).split(os.sep)[1:]:
			pys.append(os.path.join(root, file))
			print os.path.join(root, file)

		if file.endswith('.ui'):
			uis.append(os.path.join(root, file))
			print os.path.join(root, file)

pro.write(" \\ \n".join(pys))
pro.write('\n')
pro.write('\n')

pro.write("FORMS = ")
pro.write(" \\ \n".join(uis))
pro.write('\n')
pro.write('\n')

pro.write("TRANSLATIONS = ")

pro.write(' \\ \n'.join([os.path.join(BASE_DIR, r'locale\locale_%s.ts' % i) for i in LANGUAGES]))
pro.write('\n')

pro.write('CODECFORTR      = UTF-8 \n')
pro.write('CODECFORSRC      = UTF-8 \n')
pro.close()


# import glob, os
# from applibs import BASE_DIR
#
# directory = os.path.join(BASE_DIR, 'applibs')
# # os.chdir()
# # for f in glob.glob("*.py"):
# # 	print(f)
#
# script = open(os.path.join(BASE_DIR, 'make_translations.bat'), 'w')
#
# for root, dirs, files in os.walk(directory):
# 	for file in files:
# 		if file.endswith('.py') or file.endswith('.ui'):
#
# 			script.write(os.path.join(BASE_DIR, r'python2.7_win32\Lib\site-packages\PyQt4\pylupdate4.exe '))
# 			script.write(str(os.path.join(root, "\\".join(dirs), file)))
# 			script.write(' -ts ' + os.path.join(BASE_DIR, r'locate\en.ts'))
# 			script.write('\n')
#
# 			print os.path.join(root, file)
# script.close()

