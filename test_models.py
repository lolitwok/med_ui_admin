from applibs.models.auth import User, Base
from applibs.models.node import Node
from applibs.dao import Session, engine

Base.metadata.create_all(engine)

db_session = Session()

user = User(login="lox", password="123")
db_session.add(user)
db_session.commit()

print db_session.query(User).all()

n1 = Node()
n1.description = "DSadsdasd"
db_session.add(n1)
db_session.commit()

n2 = Node()
n2.parrent_id = n1.id
n2.description = "Asdasda1222"

db_session.add(n2)
db_session.commit()

print db_session.query(Node).filter(Node.parrent_id == None).all()
print db_session.query(Node).filter(Node.parrent_id == 1).all()