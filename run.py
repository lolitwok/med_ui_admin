# -*- coding: utf-8 -*-
from applibs.serverthread import IOSocketThread
import os, sys
import random

base_path = os.path.dirname(os.path.abspath(__file__))

from PyQt4 import QtCore, QtGui, uic

from applibs.driver import DriverThread
from applibs.simple_logger import SLogger
from applibs.ui.mainwindow import UiWindowMain
from applibs.hand_test_ui import MainWindow
# from applibs.test_drow import TestBrow

logger = SLogger(base_path)



class MWindow(UiWindowMain):
	def __init__(self):
		self.timer = QtCore.QTimer()
		self.timer.setInterval(50)
		self.timer.timeout.connect(self.timer_tick)
		self.driver = DriverThread(logger)
		self.driver.auto_init(True)
		self.gl_window = MainWindow()
		# (Ui_MainWindow, QMainWindow) = uic.loadUiType('applibs/ui/src/change_name.ui')
		# self.gl_window = TestBrow()
		self.server = IOSocketThread(port=5001)
		self.server.start()

	def setup_Ui(self, dialog):
		super(MWindow, self).setup_Ui(dialog)

		self.button_open_hand.clicked.connect(self.gl_window.show)

		self.timer.start(100)
		self.driver.start()
		dialog.closeEvent = self.closeEvent

	def closeEvent(self, e):
		self.close()

	def timer_tick(self):
		self.label_status_text.setText(
			self.driver.opened_port if self.driver.opened_port is not None else "Not connected"
		)

		# self.server.push_data_value(random.randint(5000, 6000))


		if len(self.driver.values) > 0:
			value = self.driver.values.pop()

			# self.server.send_msg(dict(value=value.get_value()))

			if value and value.get_value() is not None:
				self.lcdNumber.display('%s' % value.get_value())

				if value.button1:
					self.label_btn1.setStyleSheet("QLabel { background-color : rgb(86, 255, 97);}")
				else:
					self.label_btn1.setStyleSheet("QLabel { background-color : rgb(255, 96, 81);}")

				if value.button2:
					self.label_btn2.setStyleSheet("QLabel { background-color : rgb(86, 255, 97);}")
				else:
					self.label_btn2.setStyleSheet("QLabel { background-color : rgb(255, 96, 81);}")

	def close(self):
		print "[+] close starting"
		self.driver.terminate()
		print "[+] self.driver.terminate terminate send"
		self.server.terminate()
		print "[+] self.server.terminate terminate send"
		self.driver.join()
		print "[+] self.driver.join"
		self.server.join()
		print "[+] self.server.join"
		self.gl_window.close()
		print "[+] gl_window.close"

		exit(0)
		print "[+] exit"


if __name__ == "__main__":
	import sys
	app = QtGui.QApplication(sys.argv)
	import applibs.objloaderthread

	mwindow = QtGui.QDialog()
	mui = MWindow()
	mui.setup_Ui(mwindow)
	app.aboutToQuit.connect(mui.close)
	mwindow.show()
	sys.exit(app.exec_())
