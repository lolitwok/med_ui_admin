# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
from applibs.driver import DriverThread

try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s

try:
	_encoding = QtGui.QApplication.UnicodeUTF8


	def _translate(context, text, disambig):
		return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
	def _translate(context, text, disambig):
		return QtGui.QApplication.translate(context, text, disambig)


class Ui_Dialog(object):
	def __init__(self):
		self.timer = QtCore.QTimer()
		self.timer.setInterval(30)
		self.timer.timeout.connect(self.timer_tick)
		self.driver = DriverThread(DriverThread.detect())

	def setupUi(self, Dialog):
		Dialog.setObjectName(_fromUtf8("Dialog"))
		Dialog.resize(211, 133)
		self.pushButton = QtGui.QPushButton(Dialog)
		self.pushButton.setGeometry(QtCore.QRect(120, 100, 75, 23))
		self.pushButton.setObjectName(_fromUtf8("pushButton"))
		self.pushButton.clicked.connect(self.close)

		self.lcdNumber = QtGui.QLCDNumber(Dialog)
		self.lcdNumber.setGeometry(QtCore.QRect(10, 10, 181, 71))
		self.lcdNumber.setObjectName(_fromUtf8("lcdNumber"))

		self.retranslateUi(Dialog)
		QtCore.QMetaObject.connectSlotsByName(Dialog)
		self.timer.start(100)
		self.driver.start()

	def retranslateUi(self, Dialog):
		Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
		self.pushButton.setText(_translate("Dialog", "PushButton", None))

	def timer_tick(self):
		value = self.driver.values.pop()
		if value and value.value is not None:
			self.lcdNumber.display('%s' % value.value)

	def close(self):
		self.driver.terminate()
		self.driver.join()
		exit(0)


if __name__ == "__main__":
	import sys

	app = QtGui.QApplication(sys.argv)
	Dialog = QtGui.QDialog()
	ui = Ui_Dialog()
	ui.setupUi(Dialog)
	Dialog.show()
	sys.exit(app.exec_())
