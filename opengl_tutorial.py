#!/usr/bin/env python
import os

import pygame
from OpenGL.GL import *
from OpenGL.GLU import *
import math
from applibs import graphics


resources_path = 'resources'

class Cube(object):
	left_key = False
	right_key = False
	up_key = False
	down_key = False
	angle = 0
	z_angle = 0
	cube_angle = 0
	key_last = (0, 0)
	#-------------------------------------
	def __init__(self):
		self.vertices = []
		self.faces = []
		self.rubik_id = graphics.load_texture(os.path.join(resources_path, "rubik.png"))
		self.surface_id = graphics.load_texture(os.path.join(resources_path, "ConcreteTriangles.png"))
		#---Coordinates----[x,y,z]-----------------------------
		self.coordinates = [0,0,0]
		self.ground = graphics.ObjLoader(os.path.join(resources_path, "plane.txt"))
		self.pyramid = graphics.ObjLoader(os.path.join(resources_path, "scene.txt"))
		self.cube = graphics.ObjLoader(os.path.join(resources_path, "cube.txt"))
		self.monkey = graphics.ObjLoader(os.path.join(resources_path, "m.obj"))
		pygame.mouse.set_pos([512, 384])
		pygame.mouse.set_visible(False)

		self.key_last = pygame.mouse.get_pos()

	def render_scene(self):
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
		#glClearColor(0.7,0.9,1,1)
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()
		
		#Add ambient light:
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT,[0.2,0.2,0.2,1.0])
		
		#Add positioned light:
		glLightfv(GL_LIGHT0,GL_DIFFUSE,[2,2,2,1])
		glLightfv(GL_LIGHT0,GL_POSITION,[4,8,1,1])
		

		gluLookAt(0,0,0, math.sin(math.radians(self.angle)),  math.cos(math.radians(self.z_angle)) , math.cos(math.radians(self.angle)) *-1, 0,2,0)
		glTranslatef(self.coordinates[0],self.coordinates[1],self.coordinates[2])

		glTranslatef(0,-0.5,0)  
		glPushMatrix()
		 
		
		
		if self.ground.last_texture is None:
			self.ground.render_texture(self.surface_id,((0,0),(2,0),(2,2),(0,2)))
		else:
			self.ground.render_texture_last()
		
		self.pyramid.render_scene()
		glPopMatrix()
		
		glPushMatrix()
		glTranslatef(-7.5,2,0)
		glRotatef(self.cube_angle,0,1,0)
		glRotatef(45,1,0,1)
		if self.cube.last_texture is None:
			self.cube.render_texture(self.rubik_id,((0,0),(1,0),(1,1),(0,1)))
		else:
			self.cube.render_texture_last()
		glPopMatrix()

		glPushMatrix()
		# glTranslatef(7.5,2,0)
		if self.monkey.last_texture is None:
			self.monkey.render_texture(self.surface_id,((0,0),(1,0),(1,1),(0,1)))
		else:
			self.monkey.render_texture_last()
		self.monkey.render_scene()
		glPopMatrix()

	def move_forward(self):
		self.coordinates[2] += 0.1 * math.cos(math.radians(self.angle))
		self.coordinates[0] -= 0.1 * math.sin(math.radians(self.angle))
		
	def move_back(self):
		self.coordinates[2] -= 0.1 * math.cos(math.radians(self.angle))
		self.coordinates[0] += 0.1 * math.sin(math.radians(self.angle))
			
	def move_left(self):
		self.coordinates[0] += 0.1 * math.cos(math.radians(self.angle))
		self.coordinates[2] += 0.1 * math.sin(math.radians(self.angle))
		
	def move_right(self):
		self.coordinates[0] -= 0.1 * math.cos(math.radians(self.angle))
		self.coordinates[2] -= 0.1 * math.sin(math.radians(self.angle))
		
	def rotate(self,n):
		if self.angle >= 360 or self.angle <= -360:
			self.angle = 0
		self.angle += n

	def rotate_z(self,n):
		self.z_angle += n

		if self.z_angle > 180:
			self.z_angle = 180
		
		if self.z_angle < 0:
			self.z_angle = 0

	def update(self):
		if self.left_key:
			self.move_left()
		if self.right_key:
			self.move_right()
		if self.up_key:
			self.move_forward()
		if self.down_key:
			self.move_back()
			
		pos = pygame.mouse.get_pos()
		x = pos[0] - self.key_last[0]
		y = pos[1] - self.key_last[1]
		if x != 0:
			self.rotate(0.3*x)
		# elif pos[0] > 565:
			# self.rotate(1.2)
		if y != 0:
			self.rotate_z(0.3*y)
		# elif pos[1] > 700:
			# self.z_angle -= 1.2


		if self.cube_angle >= 360:
			self.cube_angle = 0
		else:
			self.cube_angle += 0.5

		pygame.mouse.set_pos(self.key_last)
		# self.key_last = pos
	
	def keyup(self):
		self.left_key = False
		self.right_key = False
		self.up_key = False
		self.down_key = False
	
	def delete_texture(self):
		glDeleteTextures(self.rubik_id)
		glDeleteTextures(self.surface_id)
	
def main():
	pygame.init()
	pygame.display.set_mode((1024,768),pygame.DOUBLEBUF|pygame.OPENGL)
	pygame.display.set_caption("PyOpenGL Tutorial")
	clock = pygame.time.Clock()
	done = False
	
	glMatrixMode(GL_PROJECTION)
	glLoadIdentity()
	gluPerspective(45,640.0/480.0,0.1,200.0)
	glEnable(GL_DEPTH_TEST)
	glEnable(GL_LIGHTING)
	glEnable(GL_LIGHT0)
	glEnable(GL_NORMALIZE)

	cube = Cube()
	#----------- Main Program Loop -------------------------------------
	while not done:
		# --- Main event loop
		for event in pygame.event.get(): # User did something
			if event.type == pygame.QUIT: # If user clicked close
				done = True # Flag that we are done so we exit this loop
			
			if event.type == pygame.KEYDOWN:

				if event.key == pygame.K_LEFT or event.key == pygame.K_a:
					cube.move_left()
					cube.left_key = True
				if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
					cube.move_right()
					cube.right_key = True
				if event.key == pygame.K_UP or event.key == pygame.K_w:
					cube.move_forward()
					cube.up_key = True
				if event.key == pygame.K_DOWN or event.key == pygame.K_s:
					cube.move_back()
					cube.down_key = True
				if event.key == pygame.K_ESCAPE:
					done =True
					break
					
			if event.type == pygame.KEYUP:

				if event.key == pygame.K_LEFT or event.key == pygame.K_a:
					cube.keyup()
				if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
					cube.keyup()
				if event.key == pygame.K_UP or event.key == pygame.K_w:
					cube.keyup()
				if event.key == pygame.K_DOWN or event.key == pygame.K_s:
					cube.keyup()
		
		cube.update()
		cube.render_scene()
		glFlush()

		pygame.display.flip()
		clock.tick(50)
	
	cube.delete_texture()
	pygame.quit()

if __name__ == '__main__': 
	main()
	a = raw_input("")
